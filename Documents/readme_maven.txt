README file version 1.0, last edited: 2016-04-22

included:
explanation how to install maven
how to open the project in eclipse
useful commands of maven
other useful info

main goal:
Use this README file to install, use maven, i.e: (i) installing maven (ii) getting all necessary dependencies (iii) opening the project in eclipse (iv) explaining the significant commands of maven (v) other useful information

(i, ii):
1. install maven, https://maven.apache.org/download.cgi#Installation -> something meaningful should appear when entering in cmd: mvn -version.

For proxy bypass of NTLM proxy at ENEXIS add wagon-http-lightweight-2.7.jar to maven in lib/ext dir. Via http://repo.maven.apache.org/maven2/org/apache/maven/wagon/wagon-http-lightweight/2.10/wagon-http-lightweight-2.10.jar
Edit settings.xml in conf dir maven to add proxy config:
<proxy>
	<id>ENEXIS</id>
	<active>true</active>
	<protocol>http</protocol>
	<host>swenr1293.enexis.local</host>
	<port>8080</port>
	<username>ENEXIS\username</username>
	<password>password</password>
</proxy>

Change location of defautl local repository (user home is on H: network drive):
<!-- localRepository
 | The path to the local repository maven will use to store artifacts.
 |
 | Default: ${user.home}/.m2/repository
-->
<localRepository>D:/Java/apache-maven-3.3.9/repo</localRepository>

For proxy issues you can also use CNTLM. Which can hide NTLM difficulties from programs which can not handle this.
CNTLM --> D:\Program Files (x86)\Cntlm>cntlm.exe -v -a NTLMv2 -c cntlm.ini
Edit cntlm.ini to the correct NTLM proxy values.
Edit settings.xml in conf dir maven to add proxy config:  
<proxy>
  <active>true</active>
  <protocol>http</protocol>
  <host>localhost</host>
  <port>3128</port>
  <nonProxyHosts>localhost|127.0.0.1</nonProxyHosts>
</proxy>
  
2. open cmd in the basedir directory of ELMO (where all the modules are located, including the license.txt and the pom.xml)

<!-- Also downloads all the necessary dependencies -->
3. execute in cmd: mvn compile

(iii)
<!-- Maven projects can also be imported in other IDE's. Eclipse is used as an example -->
4. run eclipse -> right-click in browse project -> import -> maven -> existing maven project -> browse to directory -> select all POMs (should be done automatically) -> finish. Note that in some versions of eclipse, eclipse asks to install "setup maven plugin connectors". Do this. Select plugin -> press finish -> follow the instructions

That's all for importing maven in eclipse.



(iv)
useful commands of maven:
mvn (clean) compile 
Compiles the source code. The compiled classes can be found in ${basedir}/target/classes

mvn (clean) package
Builds the project: Generates the ear, war and jar files and adds the needed dependencies, which in this case are to be found in {basedir}/SCiP/target/SCiP 1.0 snapshot

mvn (clean) test
Compiles the test sources and runs the tests. This is very useful because you can run all tests at once.

(v)
other useful info:
In case of class path errors in eclipse, it could be that maven and eclipse are not in sync. Then right click the project(s) and select Maven -> Update project.

http://search.maven.org/ or http://mvnrepository.com/ you can find libraries. You can add the info to the POM's displayed in the URL below. This is an example of commons-net library.
http://search.maven.org/#artifactdetails%7Ccommons-net%7Ccommons-net%7C3.4%7Cjar
more info on that: https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html

it is also advised to look at:
https://maven.apache.org/guides/introduction/introduction-to-the-pom.html
which also includes an explanation to the parent-child relationship in the POMs used in this project