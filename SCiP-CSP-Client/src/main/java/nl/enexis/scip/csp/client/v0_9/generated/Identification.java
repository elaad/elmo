
package nl.enexis.scip.csp.client.v0_9.generated;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Element used in message header to identify the DSO and CSP for the concerned message
 * 
 * <p>Java class for Identification complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Identification">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DSO" type="{http://OSCP/CSP/2013/06/}Ean13"/>
 *         &lt;element name="CSP" type="{http://OSCP/CSP/2013/06/}Ean13"/>
 *         &lt;element name="DateTime" type="{http://OSCP/CSP/2013/06/}DateTime"/>
 *         &lt;element name="EventId" type="{http://OSCP/CSP/2013/06/}EventId"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Identification", propOrder = {
    "dso",
    "csp",
    "dateTime",
    "eventId"
})
public class Identification {

    @XmlElement(name = "DSO", required = true)
    protected String dso;
    @XmlElement(name = "CSP", required = true)
    protected String csp;
    @XmlElement(name = "DateTime", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    protected Date dateTime;
    @XmlElement(name = "EventId", required = true)
    protected String eventId;

    /**
     * Gets the value of the dso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDSO() {
        return dso;
    }

    /**
     * Sets the value of the dso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDSO(String value) {
        this.dso = value;
    }

    /**
     * Gets the value of the csp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCSP() {
        return csp;
    }

    /**
     * Sets the value of the csp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCSP(String value) {
        this.csp = value;
    }

    /**
     * Gets the value of the dateTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * Sets the value of the dateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTime(Date value) {
        this.dateTime = value;
    }

    /**
     * Gets the value of the eventId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * Sets the value of the eventId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventId(String value) {
        this.eventId = value;
    }

}
