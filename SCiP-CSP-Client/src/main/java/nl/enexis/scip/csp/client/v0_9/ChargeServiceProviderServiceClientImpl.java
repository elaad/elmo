package nl.enexis.scip.csp.client.v0_9;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;

import nl.enexis.scip.action.ActionContext;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.csp.client.ChargeServiceProviderServiceClient;
import nl.enexis.scip.csp.client.v0_9.generated.Cable;
import nl.enexis.scip.csp.client.v0_9.generated.Cable.Connection;
import nl.enexis.scip.csp.client.v0_9.generated.CapacityBlock;
import nl.enexis.scip.csp.client.v0_9.generated.ChargeServiceProviderService;
import nl.enexis.scip.csp.client.v0_9.generated.ChargeServiceProviderService_Service;
import nl.enexis.scip.csp.client.v0_9.generated.DetailLevel;
import nl.enexis.scip.csp.client.v0_9.generated.Identification;
import nl.enexis.scip.csp.client.v0_9.generated.Location;
import nl.enexis.scip.csp.client.v0_9.generated.Result;
import nl.enexis.scip.csp.client.v0_9.generated.Result.Details;
import nl.enexis.scip.csp.client.v0_9.generated.Result.Details.Detail;
import nl.enexis.scip.csp.client.v0_9.generated.ResultType;
import nl.enexis.scip.csp.client.v0_9.generated.UnitOfMeasure;
import nl.enexis.scip.csp.client.v0_9.generated.UpdateCableCapacityForecastRequest;
import nl.enexis.scip.csp.client.v0_9.generated.UpdateCableCapacityForecastRequest.Forecast;
import nl.enexis.scip.csp.client.v0_9.generated.UpdateCableCapacityForecastResponse;
import nl.enexis.scip.csp.client.v0_9.generated.UpdateWeatherValuesRequest;
import nl.enexis.scip.csp.client.v0_9.generated.UpdateWeatherValuesRequest.WeatherForecast;
import nl.enexis.scip.csp.client.v0_9.generated.UpdateWeatherValuesResponse;
import nl.enexis.scip.csp.client.v0_9.generated.ValueType;
import nl.enexis.scip.csp.client.v0_9.generated.WeatherValueType;
import nl.enexis.scip.csp.client.v0_9.generated.WeatherBlock;
import nl.enexis.scip.csp.client.v0_9.generated.WeatherNameOfMeasure;
import nl.enexis.scip.csp.client.v0_9.generated.WeatherUnitOfMeasure;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspConnection;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.LocationMeasurement;
import nl.enexis.scip.model.LocationMeasurementValue;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.util.ForecastBlockUtil;

public class ChargeServiceProviderServiceClientImpl implements ChargeServiceProviderServiceClient {

	@Override
	public void updateCableCapacityForecast(CspForecastMessage cspForecastMessage, Csp csp) throws ActionException {
		String cspEan = "<<unknown>>";
		String dsoEan = "<<unknown>>";

		try {

			cspEan = cspForecastMessage.getCsp();
			dsoEan = cspForecastMessage.getDso();

			UpdateCableCapacityForecastRequest request = new UpdateCableCapacityForecastRequest();

			List<CspForecast> cspForecasts = cspForecastMessage.getCspForecasts();

			for (CspForecast cspForecast : cspForecasts) {

				Forecast forecast = new Forecast();

				Cable cable = new Cable();
				cable.setCableId(cspForecast.getCableCsp().getCable().getCableId());

				UnitOfMeasure uom;
				if (cspForecast.getCableCsp().getCable().getBehaviour().equals(BehaviourType.SC)) {
					uom = UnitOfMeasure.AMP;
				} else {
					uom = UnitOfMeasure.WH;
				}

				for (CspConnection cspConnection : cspForecast.getCableCsp().getCspConnections()) {
Connection connection = new Connection();
connection.setEan(cspConnection.getEan18());
cable.getConnection().add(connection);
}

				forecast.setCable(cable);

				List<CspForecastValue> values = cspForecast.getCspForecastValues();

				// for number of normal output blocks
				int numberOfBlocks = cspForecast.getNumberOfBlocks() != null ? cspForecast.getNumberOfBlocks()
						.intValue() : values.size();

				// Placeholder for possible extra 'big' capacity block
				CapacityBlock lastCapBlock = null;

				int blockNumber = 1;
				for (Iterator<CspForecastValue> iterator = values.iterator(); iterator.hasNext(); blockNumber++) {
					CspForecastValue cspForecastValue = iterator.next();

					Date startTime = ForecastBlockUtil.getStartDateOfBlock(cspForecastValue.getCableForecastOutput()
							.getDay(), cspForecastValue.getCableForecastOutput().getHour(), cspForecastValue
							.getCableForecastOutput().getBlock(), cspForecast.getCableForecast().getBlockMinutes());
					Date endTime = ForecastBlockUtil.getEndDateOfBlock(cspForecastValue.getCableForecastOutput()
							.getDay(), cspForecastValue.getCableForecastOutput().getHour(), cspForecastValue
							.getCableForecastOutput().getBlock(), cspForecast.getCableForecast().getBlockMinutes());

					if (endTime.before(new Date())) {
						blockNumber--;
						continue;
					}

					if (blockNumber <= numberOfBlocks) {
						// for normal blocks in message
						CapacityBlock capBlock = new CapacityBlock();

						capBlock.setStartTime(startTime);
						capBlock.setEndTime(endTime);

						ValueType capacity = new ValueType();
						capacity.setValue(cspForecastValue.getAssigned());
						capacity.setUnitOfMeasure(uom);
						capBlock.setCapacity(capacity);

						ValueType remaining = new ValueType();
						remaining.setValue(cspForecastValue.getRemaining());
						remaining.setUnitOfMeasure(uom);
						capBlock.setRemainingCapacity(remaining);

						forecast.getCapacityBlock().add(capBlock);
					} else {
						// for one big last extra block with the minimum of al
						// blocks
						// (safest value)
						// for remaining the remaing for the block with least
						// assigned
						// capacity is used but times 0.5
						// as decided by Paul Klapwijk and Ronald Steeghs
						if (lastCapBlock == null) {
							lastCapBlock = new CapacityBlock();
						}

						if (lastCapBlock.getStartTime() == null) {
							lastCapBlock.setStartTime(startTime);
						}
						lastCapBlock.setEndTime(endTime);

						if (lastCapBlock.getCapacity() == null) {
							ValueType capacity = new ValueType();
							capacity.setValue(cspForecastValue.getAssigned());
							capacity.setUnitOfMeasure(uom);
							lastCapBlock.setCapacity(capacity);

							ValueType remaining = new ValueType();
							remaining.setValue(cspForecastValue.getRemaining().multiply(new BigDecimal("0.50"))
									.setScale(2, RoundingMode.HALF_UP));
							remaining.setUnitOfMeasure(uom);
							lastCapBlock.setRemainingCapacity(remaining);
						} else {
							if (lastCapBlock.getCapacity().getValue().compareTo(cspForecastValue.getAssigned()) > 0) {
								lastCapBlock.getCapacity().setValue(cspForecastValue.getAssigned());
								lastCapBlock.getRemainingCapacity().setValue(
										cspForecastValue.getRemaining().multiply(new BigDecimal("0.50"))
												.setScale(2, RoundingMode.HALF_UP));
							}
						}
					}
				}

				if (lastCapBlock != null) {
					forecast.getCapacityBlock().add(lastCapBlock);
				}

				request.getForecast().add(forecast);
			}

			Identification identification = new Identification();
			identification.setCSP(cspEan);
			identification.setDSO(dsoEan);
			identification.setDateTime(cspForecastMessage.getDateTime());
			identification.setEventId(cspForecastMessage.getEventId());

			UpdateCableCapacityForecastResponse response = this.updateCableCapacityForecast(csp, request,
					identification, cspForecastMessage.getPriority());

			Result result = response.getResult();

			this.processResponseResult(result, csp);
		} catch (Exception e) {
			if (e instanceof ActionException) {
				throw (ActionException) e;
			}

			String message = "Unexpected exception pushing forecast(s) to Csp with EAN " + cspEan + "!";
			throw new ActionException(ActionExceptionType.UNEXPECTED, message, e);
		}
	}

	private UpdateCableCapacityForecastResponse updateCableCapacityForecast(Csp csp,
			UpdateCableCapacityForecastRequest request, Identification identification, int priority) throws Exception {

		String endpointURL = csp.getEndpoint();

		URL wsdlURL = ChargeServiceProviderServiceClient.class.getClassLoader().getResource(
				"META-INF/wsdl/0.9/ChargeServiceProviderService.wsdl");

		ChargeServiceProviderService_Service service1 = new ChargeServiceProviderService_Service(wsdlURL);

		ChargeServiceProviderService port1 = service1.getChargeServiceProviderServiceSoap12();

		BindingProvider bp = (BindingProvider) port1;

		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);

		Holder<Integer> priorityHolder = new Holder<Integer>(new Integer(priority));
		Holder<Identification> identificationHolder = new Holder<Identification>(identification);

		UpdateCableCapacityForecastResponse response = port1.updateCableCapacityForecast(request, identificationHolder,
				priorityHolder);
		try {
			response = port1.updateCableCapacityForecast(request, identificationHolder, priorityHolder);
		} catch (Exception e) {
			ActionException ae = new ActionException(ActionExceptionType.CONNECTION,
					"UpdateCableCapacityForecast failed for csp: " + csp.getEan13(), e);
			ae.getTargets().add(csp);
			throw ae;
		}

		return response;

	}

	@Override
	public void updateWeahterValues(int blockMinutes, LocationMeasurement locationMeasurement,
			List<LocationMeasurementValue> locationMeasurementValues, Csp csp, Dso dso, int priority)
			throws ActionException {
		String cspEan = "<<unknown>>";
		String dsoEan = "<<unknown>>";

		try {
			cspEan = csp.getEan13();
			dsoEan = dso.getEan13();

			UpdateWeatherValuesRequest request = new UpdateWeatherValuesRequest();

			List<WeatherForecast> weatherForecasts = request.getWeatherForecast();

			WeatherForecast weatherForecast = new WeatherForecast();
			weatherForecasts.add(weatherForecast);

			weatherForecast.setIsForecast(locationMeasurement.isForecast());
			Location location = new Location();
			location.setLocationId(locationMeasurement.getLocation().getLocationId());
			weatherForecast.setLocation(location);

			List<WeatherBlock> weatherBlocks = weatherForecast.getWeatherBlock();
			for (LocationMeasurementValue lmv : locationMeasurementValues) {
				WeatherBlock wb = new WeatherBlock();
				wb.setStartTime(lmv.getKey().getDatetime());
				wb.setEndTime(ForecastBlockUtil.getEndDateOfBlock(lmv.getKey().getDatetime(), blockMinutes));

				List<WeatherValueType> weatherValues = wb.getWeatherValue();
				WeatherValueType wv = new WeatherValueType();
				wv.setValue(lmv.getValue());
				wv.setNameOfMeasure(WeatherNameOfMeasure.IRRADIATION);
				wv.setUnitOfMeasure(WeatherUnitOfMeasure.WM_2);
				weatherValues.add(wv);
				weatherBlocks.add(wb);
			}

			Identification identification = new Identification();
			identification.setCSP(cspEan);
			identification.setDSO(dsoEan);
			identification.setDateTime(new Date());
			identification.setEventId(ActionContext.getId());

			UpdateWeatherValuesResponse response = this.updateWeatherValues(csp, request, identification, 2);

			Result result = response.getResult();

			this.processResponseResult(result, csp);
		} catch (Exception e) {
			if (e instanceof ActionException) {
				throw (ActionException) e;
			}

			String message = "Unexpected exception pushing weather update(s) to Csp with EAN " + cspEan + "!";
			throw new ActionException(ActionExceptionType.UNEXPECTED, message, e);
		}
	}

	private UpdateWeatherValuesResponse updateWeatherValues(Csp csp, UpdateWeatherValuesRequest request,
			Identification identification, int priority) throws Exception {

		String endpointURL = csp.getEndpoint();

		URL wsdlURL = ChargeServiceProviderServiceClient.class.getClassLoader().getResource(
				"META-INF/wsdl/0.9/ChargeServiceProviderService.wsdl");

		ChargeServiceProviderService_Service service1 = new ChargeServiceProviderService_Service(wsdlURL);

		ChargeServiceProviderService port1 = service1.getChargeServiceProviderServiceSoap12();

		BindingProvider bp = (BindingProvider) port1;

		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);

		Holder<Integer> priorityHolder = new Holder<Integer>(new Integer(priority));
		Holder<Identification> identificationHolder = new Holder<Identification>(identification);

		UpdateWeatherValuesResponse response;
		try {
			response = port1.updateWeatherValues(request, identificationHolder, priorityHolder);
		} catch (Exception e) {
			ActionException ae = new ActionException(ActionExceptionType.CONNECTION,
					"UpdateWeatherValues failed for csp: " + csp.getEan13(), e);
			ae.getTargets().add(csp);
			throw ae;
		}

		return response;

	}

	@Override
	public void heartbeat(Dso dso, Csp csp, int forecastInterval, int heartbeatInterval, int priority)
			throws ActionException {
		try {
			throw new ActionException(ActionExceptionType.NOT_SUPPORTED, "Heartbeat is not supported OSCP version 0.9!");
		} catch (ActionException e) {
			ActionLogger.warn(e.getMessage(), new Object[] {}, e, true);
		}

	}

	private void processResponseResult(Result result, Csp csp) throws ActionException {
		boolean success = result.getType().equals(ResultType.SUCCESS);

		Details detailsHolder = result.getDetails();

		if (detailsHolder != null) {
			List<Detail> details = detailsHolder.getDetail();
			for (Detail detail : details) {
				if (!detail.getLevel().equals(DetailLevel.INFO)) {
					String detailCode = detail.getCode().name();
					String detailMessage = detail.getMessage();
					String detailDesc = detail.getDescription();

					String message = "Response detail: {0} - {1} - {2}";
					Object[] messageParams = { detailCode, detailMessage, detailDesc };

					if (detail.getLevel().equals(DetailLevel.WARN)) {
						ActionLogger.warn(message, messageParams, null, true);
					} else if (detail.getLevel().equals(DetailLevel.ERROR)) {
						success = false;
						ActionLogger.error(message, messageParams, null, true);
					}
				}
			}
		}

		if (!success) {
			throw new ActionException(ActionExceptionType.MESSAGE, "Error(s) reported by csp " + csp.getEan13() + " ("
					+ csp.getName() + ")");
		}
	}
}
