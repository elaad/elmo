
package nl.enexis.scip.csp.client.v0_9.generated;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for WeahterValueType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WeahterValueType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://OSCP/CSP/2013/06/>Value">
 *       &lt;attribute name="NameOfMeasure" use="required" type="{http://OSCP/CSP/2013/06/}WeatherNameOfMeasure" />
 *       &lt;attribute name="UnitOfMeasure" use="required" type="{http://OSCP/CSP/2013/06/}WeatherUnitOfMeasure" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WeahterValueType", propOrder = {
    "value"
})
public class WeatherValueType {

    @XmlValue
    protected BigDecimal value;
    @XmlAttribute(name = "NameOfMeasure", required = true)
    protected WeatherNameOfMeasure nameOfMeasure;
    @XmlAttribute(name = "UnitOfMeasure", required = true)
    protected WeatherUnitOfMeasure unitOfMeasure;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * Gets the value of the nameOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link WeatherNameOfMeasure }
     *     
     */
    public WeatherNameOfMeasure getNameOfMeasure() {
        return nameOfMeasure;
    }

    /**
     * Sets the value of the nameOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeatherNameOfMeasure }
     *     
     */
    public void setNameOfMeasure(WeatherNameOfMeasure value) {
        this.nameOfMeasure = value;
    }

    /**
     * Gets the value of the unitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link WeatherUnitOfMeasure }
     *     
     */
    public WeatherUnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Sets the value of the unitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeatherUnitOfMeasure }
     *     
     */
    public void setUnitOfMeasure(WeatherUnitOfMeasure value) {
        this.unitOfMeasure = value;
    }

}
