
package nl.enexis.scip.csp.client.v1_0.generated;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.enexis.scip.csp.client.v1_0.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UpdateCableCapacityForecastRequest_QNAME = new QName("http://OSCP/CSP/2013/06/", "UpdateCableCapacityForecastRequest");
    private final static QName _Priority_QNAME = new QName("http://OSCP/CSP/2013/06/", "Priority");
    private final static QName _UpdateCableCapacityForecastResponse_QNAME = new QName("http://OSCP/CSP/2013/06/", "UpdateCableCapacityForecastResponse");
    private final static QName _Parties_QNAME = new QName("http://OSCP/CSP/2013/06/", "Parties");
    private final static QName _HeartbeatResponse_QNAME = new QName("http://OSCP/CSP/2013/06/", "HeartbeatResponse");
    private final static QName _DateTime_QNAME = new QName("http://OSCP/CSP/2013/06/", "DateTime");
    private final static QName _HeartbeatRequest_QNAME = new QName("http://OSCP/CSP/2013/06/", "HeartbeatRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.enexis.scip.csp.client.v1_0.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Cable }
     * 
     */
    public Cable createCable() {
        return new Cable();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link Result.Details }
     * 
     */
    public Result.Details createResultDetails() {
        return new Result.Details();
    }

    /**
     * Create an instance of {@link UpdateCableCapacityForecastRequest }
     * 
     */
    public UpdateCableCapacityForecastRequest createUpdateCableCapacityForecastRequest() {
        return new UpdateCableCapacityForecastRequest();
    }

    /**
     * Create an instance of {@link HeartbeatResponse }
     * 
     */
    public HeartbeatResponse createHeartbeatResponse() {
        return new HeartbeatResponse();
    }

    /**
     * Create an instance of {@link HeartbeatRequest }
     * 
     */
    public HeartbeatRequest createHeartbeatRequest() {
        return new HeartbeatRequest();
    }

    /**
     * Create an instance of {@link Cable.Connection }
     * 
     */
    public Cable.Connection createCableConnection() {
        return new Cable.Connection();
    }

    /**
     * Create an instance of {@link ForecastedBlock }
     * 
     */
    public ForecastedBlock createForecastedBlock() {
        return new ForecastedBlock();
    }

    /**
     * Create an instance of {@link ValueType }
     * 
     */
    public ValueType createValueType() {
        return new ValueType();
    }

    /**
     * Create an instance of {@link Parties }
     * 
     */
    public Parties createParties() {
        return new Parties();
    }

    /**
     * Create an instance of {@link UpdateCableCapacityForecastResponse }
     * 
     */
    public UpdateCableCapacityForecastResponse createUpdateCableCapacityForecastResponse() {
        return new UpdateCableCapacityForecastResponse();
    }

    /**
     * Create an instance of {@link Result.Details.Detail }
     * 
     */
    public Result.Details.Detail createResultDetailsDetail() {
        return new Result.Details.Detail();
    }

    /**
     * Create an instance of {@link UpdateCableCapacityForecastRequest.Forecast }
     * 
     */
    public UpdateCableCapacityForecastRequest.Forecast createUpdateCableCapacityForecastRequestForecast() {
        return new UpdateCableCapacityForecastRequest.Forecast();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCableCapacityForecastRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/CSP/2013/06/", name = "UpdateCableCapacityForecastRequest")
    public JAXBElement<UpdateCableCapacityForecastRequest> createUpdateCableCapacityForecastRequest(UpdateCableCapacityForecastRequest value) {
        return new JAXBElement<UpdateCableCapacityForecastRequest>(_UpdateCableCapacityForecastRequest_QNAME, UpdateCableCapacityForecastRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/CSP/2013/06/", name = "Priority")
    public JAXBElement<Integer> createPriority(Integer value) {
        return new JAXBElement<Integer>(_Priority_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCableCapacityForecastResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/CSP/2013/06/", name = "UpdateCableCapacityForecastResponse")
    public JAXBElement<UpdateCableCapacityForecastResponse> createUpdateCableCapacityForecastResponse(UpdateCableCapacityForecastResponse value) {
        return new JAXBElement<UpdateCableCapacityForecastResponse>(_UpdateCableCapacityForecastResponse_QNAME, UpdateCableCapacityForecastResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Parties }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/CSP/2013/06/", name = "Parties")
    public JAXBElement<Parties> createParties(Parties value) {
        return new JAXBElement<Parties>(_Parties_QNAME, Parties.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HeartbeatResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/CSP/2013/06/", name = "HeartbeatResponse")
    public JAXBElement<HeartbeatResponse> createHeartbeatResponse(HeartbeatResponse value) {
        return new JAXBElement<HeartbeatResponse>(_HeartbeatResponse_QNAME, HeartbeatResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Date }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/CSP/2013/06/", name = "DateTime")
    @XmlJavaTypeAdapter(Adapter1 .class)
    public JAXBElement<Date> createDateTime(Date value) {
        return new JAXBElement<Date>(_DateTime_QNAME, Date.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HeartbeatRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/CSP/2013/06/", name = "HeartbeatRequest")
    public JAXBElement<HeartbeatRequest> createHeartbeatRequest(HeartbeatRequest value) {
        return new JAXBElement<HeartbeatRequest>(_HeartbeatRequest_QNAME, HeartbeatRequest.class, null, value);
    }

}
