
package nl.enexis.scip.csp.client.v1_0.generated;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines the heartbeat request from DSO to CSP
 * 					
 * 
 * <p>Java class for HeartbeatRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HeartbeatRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;sequence>
 *           &lt;element name="HeartBeatTimeInterval" type="{http://OSCP/CSP/2013/06/}HeartBeatTimeInterval"/>
 *           &lt;element name="ForecastTimeInterval" type="{http://OSCP/CSP/2013/06/}ForecastTimeInterval"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeartbeatRequest", propOrder = {
    "heartBeatTimeInterval",
    "forecastTimeInterval"
})
public class HeartbeatRequest {

    @XmlElement(name = "HeartBeatTimeInterval")
    protected int heartBeatTimeInterval;
    @XmlElement(name = "ForecastTimeInterval")
    protected int forecastTimeInterval;

    /**
     * Gets the value of the heartBeatTimeInterval property.
     * 
     */
    public int getHeartBeatTimeInterval() {
        return heartBeatTimeInterval;
    }

    /**
     * Sets the value of the heartBeatTimeInterval property.
     * 
     */
    public void setHeartBeatTimeInterval(int value) {
        this.heartBeatTimeInterval = value;
    }

    /**
     * Gets the value of the forecastTimeInterval property.
     * 
     */
    public int getForecastTimeInterval() {
        return forecastTimeInterval;
    }

    /**
     * Sets the value of the forecastTimeInterval property.
     * 
     */
    public void setForecastTimeInterval(int value) {
        this.forecastTimeInterval = value;
    }

}
