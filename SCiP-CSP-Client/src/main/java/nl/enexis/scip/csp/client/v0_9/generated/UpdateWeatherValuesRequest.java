
package nl.enexis.scip.csp.client.v0_9.generated;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines the DSO request towards CSP with weather values
 * 
 * <p>Java class for UpdateWeatherValuesRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateWeatherValuesRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WeatherForecast" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://OSCP/CSP/2013/06/}IsForecast"/>
 *                   &lt;element ref="{http://OSCP/CSP/2013/06/}Location"/>
 *                   &lt;element ref="{http://OSCP/CSP/2013/06/}WeatherBlock" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateWeatherValuesRequest", propOrder = {
    "weatherForecast"
})
public class UpdateWeatherValuesRequest {

    @XmlElement(name = "WeatherForecast", required = true)
    protected List<UpdateWeatherValuesRequest.WeatherForecast> weatherForecast;

    /**
     * Gets the value of the weatherForecast property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the weatherForecast property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWeatherForecast().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateWeatherValuesRequest.WeatherForecast }
     * 
     * 
     */
    public List<UpdateWeatherValuesRequest.WeatherForecast> getWeatherForecast() {
        if (weatherForecast == null) {
            weatherForecast = new ArrayList<UpdateWeatherValuesRequest.WeatherForecast>();
        }
        return this.weatherForecast;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://OSCP/CSP/2013/06/}IsForecast"/>
     *         &lt;element ref="{http://OSCP/CSP/2013/06/}Location"/>
     *         &lt;element ref="{http://OSCP/CSP/2013/06/}WeatherBlock" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "isForecast",
        "location",
        "weatherBlock"
    })
    public static class WeatherForecast {

        @XmlElement(name = "IsForecast")
        protected boolean isForecast;
        @XmlElement(name = "Location", required = true)
        protected Location location;
        @XmlElement(name = "WeatherBlock", required = true)
        protected List<WeatherBlock> weatherBlock;

        /**
         * Gets the value of the isForecast property.
         * 
         */
        public boolean isIsForecast() {
            return isForecast;
        }

        /**
         * Sets the value of the isForecast property.
         * 
         */
        public void setIsForecast(boolean value) {
            this.isForecast = value;
        }

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link Location }
         *     
         */
        public Location getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link Location }
         *     
         */
        public void setLocation(Location value) {
            this.location = value;
        }

        /**
         * Gets the value of the weatherBlock property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the weatherBlock property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getWeatherBlock().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link WeatherBlock }
         * 
         * 
         */
        public List<WeatherBlock> getWeatherBlock() {
            if (weatherBlock == null) {
                weatherBlock = new ArrayList<WeatherBlock>();
            }
            return this.weatherBlock;
        }

    }

}
