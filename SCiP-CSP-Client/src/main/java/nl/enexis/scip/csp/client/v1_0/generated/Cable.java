
package nl.enexis.scip.csp.client.v1_0.generated;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CableId" type="{http://OSCP/CSP/2013/06/}CableId"/>
 *         &lt;element name="Connection" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ConnectionReferenceId" type="{http://OSCP/CSP/2013/06/}ConnectionReferenceId"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cableId",
    "connection"
})
@XmlRootElement(name = "Cable")
public class Cable {

    @XmlElement(name = "CableId", required = true)
    protected String cableId;
    @XmlElement(name = "Connection")
    protected List<Cable.Connection> connection;

    /**
     * Gets the value of the cableId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCableId() {
        return cableId;
    }

    /**
     * Sets the value of the cableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCableId(String value) {
        this.cableId = value;
    }

    /**
     * Gets the value of the connection property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the connection property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConnection().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cable.Connection }
     * 
     * 
     */
    public List<Cable.Connection> getConnection() {
        if (connection == null) {
            connection = new ArrayList<Cable.Connection>();
        }
        return this.connection;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ConnectionReferenceId" type="{http://OSCP/CSP/2013/06/}ConnectionReferenceId"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "connectionReferenceId"
    })
    public static class Connection {

        @XmlElement(name = "ConnectionReferenceId", required = true)
        protected String connectionReferenceId;

        /**
         * Gets the value of the connectionReferenceId property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getConnectionReferenceId() {
            return connectionReferenceId;
        }

        /**
         * Sets the value of the connectionReferenceId property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setConnectionReferenceId(String value) {
            this.connectionReferenceId = value;
        }

    }

}
