package nl.enexis.scip.csp.client;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.DsoCsp;
import nl.enexis.scip.model.LocationMeasurement;
import nl.enexis.scip.model.LocationMeasurementValue;
import nl.enexis.scip.service.CspMessagingServiceLocal;
import nl.enexis.scip.service.GeneralService;

@Named("cspMessagingService")
@Stateless
public class CspMessagingServiceImpl implements CspMessagingServiceLocal {

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Override
	public void pushCspForecast(CspForecastMessage cspForecastMessage, Csp csp) throws ActionException {
		ChargeServiceProviderServiceClient cspClient = ChargeServiceProviderServiceClientFactory.getCspClient(csp);
		cspClient.updateCableCapacityForecast(cspForecastMessage, csp);
	}

	@Override
	public void pushWeatherUpdate(int blockMinutes, LocationMeasurement locationMeasurement,
			List<LocationMeasurementValue> locationMeasurementValues, Csp csp, Dso dso) throws ActionException {
		ChargeServiceProviderServiceClient cspClient = ChargeServiceProviderServiceClientFactory.getCspClient(csp);
		cspClient.updateWeahterValues(blockMinutes, locationMeasurement, locationMeasurementValues, csp, dso, 2);
	}

	@Override
	@Asynchronous
	@ActionContext(event = ActionEvent.HEARTBEAT, doRethrow = false)
	public void heartbeat(DsoCsp dsoCsp) throws ActionException {

		int priority = generalService.getConfigurationParameterAsInteger("CspMessageDefaultPriority");
		String[] forecastIntervalSetting = generalService.getConfigurationParameterAsString("CspMessageTimerMinutes").split("/");
		String[] heartbeatIntervalSetting = generalService.getConfigurationParameterAsString("CspHeartbeatTimerMinutes").split("/");

		ChargeServiceProviderServiceClient cspClient = ChargeServiceProviderServiceClientFactory.getCspClient(dsoCsp.getCsp());
		cspClient.heartbeat(dsoCsp.getDso(), dsoCsp.getCsp(), new Integer(forecastIntervalSetting[1]).intValue() * 60, new Integer(
				heartbeatIntervalSetting[1]).intValue() * 60, priority);
	}

}
