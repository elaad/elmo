package nl.enexis.scip.csp.client.v1_0.generated.clientsample;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import nl.enexis.scip.csp.client.v1_0.generated.*;

public class ClientSample {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        ChargeServiceProviderService_Service service1 = new ChargeServiceProviderService_Service();
	        System.out.println("Create Web Service...");
	        ChargeServiceProviderService port1 = service1.getChargeServiceProviderServiceSoap12();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port1.heartbeat(null,null,null,null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.updateCableCapacityForecast(null,null,null,null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Create Web Service...");
	        ChargeServiceProviderService port2 = service1.getChargeServiceProviderServiceSoap12();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port2.heartbeat(null,null,null,null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.updateCableCapacityForecast(null,null,null,null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
