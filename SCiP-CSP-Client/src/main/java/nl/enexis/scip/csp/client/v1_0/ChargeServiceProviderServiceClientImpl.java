package nl.enexis.scip.csp.client.v1_0;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import javax.xml.ws.soap.AddressingFeature;

import nl.enexis.scip.action.ActionContext;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.csp.client.ChargeServiceProviderServiceClient;
import nl.enexis.scip.csp.client.v1_0.generated.Cable;
import nl.enexis.scip.csp.client.v1_0.generated.Cable.Connection;
import nl.enexis.scip.csp.client.v1_0.generated.ChargeServiceProviderService;
import nl.enexis.scip.csp.client.v1_0.generated.ChargeServiceProviderService_Service;
import nl.enexis.scip.csp.client.v1_0.generated.ForecastedBlock;
import nl.enexis.scip.csp.client.v1_0.generated.HeartbeatRequest;
import nl.enexis.scip.csp.client.v1_0.generated.HeartbeatResponse;
import nl.enexis.scip.csp.client.v1_0.generated.LogLevelType;
import nl.enexis.scip.csp.client.v1_0.generated.Parties;
import nl.enexis.scip.csp.client.v1_0.generated.Result;
import nl.enexis.scip.csp.client.v1_0.generated.Result.Details;
import nl.enexis.scip.csp.client.v1_0.generated.Result.Details.Detail;
import nl.enexis.scip.csp.client.v1_0.generated.ResultType;
import nl.enexis.scip.csp.client.v1_0.generated.UnitOfMeasure;
import nl.enexis.scip.csp.client.v1_0.generated.UpdateCableCapacityForecastRequest;
import nl.enexis.scip.csp.client.v1_0.generated.UpdateCableCapacityForecastRequest.Forecast;
import nl.enexis.scip.csp.client.v1_0.generated.UpdateCableCapacityForecastResponse;
import nl.enexis.scip.csp.client.v1_0.generated.ValueType;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspConnection;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.LocationMeasurement;
import nl.enexis.scip.model.LocationMeasurementValue;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.util.ForecastBlockUtil;

import org.apache.cxf.ws.addressing.AddressingProperties;
import org.apache.cxf.ws.addressing.AttributedURIType;
import org.apache.cxf.ws.addressing.JAXWSAConstants;

public class ChargeServiceProviderServiceClientImpl implements ChargeServiceProviderServiceClient {

	@Override
	public void updateCableCapacityForecast(CspForecastMessage cspForecastMessage, Csp csp) throws ActionException {
		String cspEan = "<<unknown>>";
		String dsoEan = "<<unknown>>";

		try {

			cspEan = cspForecastMessage.getCsp();
			dsoEan = cspForecastMessage.getDso();

			UpdateCableCapacityForecastRequest request = new UpdateCableCapacityForecastRequest();

			List<CspForecast> cspForecasts = cspForecastMessage.getCspForecasts();

			for (CspForecast cspForecast : cspForecasts) {

				Forecast forecast = new Forecast();

				Cable cable = new Cable();
				cable.setCableId(cspForecast.getCableCsp().getCable().getCableId());

				UnitOfMeasure uom;
				if (cspForecast.getCableCsp().getCable().getBehaviour().equals(BehaviourType.SC)) {
					uom = UnitOfMeasure.A;
				} else {
					uom = UnitOfMeasure.WH;
				}

				for (CspConnection cspConnection : cspForecast.getCableCsp().getCspConnections()) {
Connection connection = new Connection();
connection.setConnectionReferenceId(cspConnection.getEan18());
cable.getConnection().add(connection);
}

				forecast.setCable(cable);

				List<CspForecastValue> values = cspForecast.getCspForecastValues();

				// for number of normal output blocks
				int numberOfBlocks = cspForecast.getNumberOfBlocks() != null ? cspForecast.getNumberOfBlocks()
						.intValue() : values.size();

				// Placeholder for possible extra 'big' capacity block
				ForecastedBlock lastCapBlock = null;

				int blockNumber = 1;
				for (Iterator<CspForecastValue> iterator = values.iterator(); iterator.hasNext(); blockNumber++) {
					CspForecastValue cspForecastValue = iterator.next();

					Date startTime = ForecastBlockUtil.getStartDateOfBlock(cspForecastValue.getCableForecastOutput()
							.getDay(), cspForecastValue.getCableForecastOutput().getHour(), cspForecastValue
							.getCableForecastOutput().getBlock(), cspForecast.getCableForecast().getBlockMinutes());
					Date endTime = ForecastBlockUtil.getEndDateOfBlock(cspForecastValue.getCableForecastOutput()
							.getDay(), cspForecastValue.getCableForecastOutput().getHour(), cspForecastValue
							.getCableForecastOutput().getBlock(), cspForecast.getCableForecast().getBlockMinutes());

					if (endTime.before(new Date())) {
						blockNumber--;
						continue;
					}

					if (blockNumber <= numberOfBlocks) {
						// for normal blocks in message
						ForecastedBlock capBlock = new ForecastedBlock();

						capBlock.setStartTime(startTime);
						capBlock.setEndTime(endTime);

						ValueType capacity = new ValueType();
						capacity.setValue(cspForecastValue.getAssigned());
						capacity.setUnit(uom);
						capBlock.getCapacity().add(capacity);

						ValueType remaining = new ValueType();
						remaining.setValue(cspForecastValue.getRemaining());
						remaining.setUnit(uom);
						capBlock.getRemainingCapacity().add(remaining);

						forecast.getForecastedBlock().add(capBlock);
					} else {
						// for one big last extra block with the minimum of al
						// blocks
						// (safest value)
						// for remaining the remaing for the block with least
						// assigned
						// capacity is used but times 0.5
						// as decided by Paul Klapwijk and Ronald Steeghs
						if (lastCapBlock == null) {
							lastCapBlock = new ForecastedBlock();
						}

						if (lastCapBlock.getStartTime() == null) {
							lastCapBlock.setStartTime(startTime);
						}
						lastCapBlock.setEndTime(endTime);

						if (lastCapBlock.getCapacity().isEmpty()) {
							ValueType capacity = new ValueType();
							capacity.setValue(cspForecastValue.getAssigned());
							capacity.setUnit(uom);
							lastCapBlock.getCapacity().add(capacity);

							ValueType remaining = new ValueType();
							remaining.setValue(cspForecastValue.getRemaining().multiply(new BigDecimal("0.50"))
									.setScale(2, RoundingMode.HALF_UP));
							remaining.setUnit(uom);
							lastCapBlock.getRemainingCapacity().add(remaining);
						} else {
							if (lastCapBlock.getCapacity().get(0).getValue().compareTo(cspForecastValue.getAssigned()) > 0) {
								lastCapBlock.getCapacity().get(0).setValue(cspForecastValue.getAssigned());
								lastCapBlock
										.getRemainingCapacity()
										.get(0)
										.setValue(
												cspForecastValue.getRemaining().multiply(new BigDecimal("0.50"))
														.setScale(2, RoundingMode.HALF_UP));
							}
						}
					}
				}

				if (lastCapBlock != null) {
					forecast.getForecastedBlock().add(lastCapBlock);
				}

				request.getForecast().add(forecast);
			}

			Parties parties = new Parties();
			parties.setReceiverID(cspEan);
			parties.setSenderID(dsoEan);

			UpdateCableCapacityForecastResponse response = this.updateCableCapacityForecast(csp, request, parties,
					cspForecastMessage.getPriority(), new Date(), cspForecastMessage.getEventId());

			Result result = response.getResult();

			this.processResponseResult(result, csp);
		} catch (Exception e) {
			if (e instanceof ActionException) {
				throw (ActionException) e;
			}

			String message = "Unexpected exception pushing forecast(s) to Csp with EAN " + cspEan + "!";
			throw new ActionException(ActionExceptionType.UNEXPECTED, message, e);
		}
	}

	private UpdateCableCapacityForecastResponse updateCableCapacityForecast(Csp csp,
			UpdateCableCapacityForecastRequest request, Parties parties, int priority, Date datetime, String eventId)
			throws ActionException {

		String endpointURL = csp.getEndpoint();

		URL wsdlURL = ChargeServiceProviderServiceClient.class.getClassLoader().getResource(
				"META-INF/wsdl/1.0/ChargeServiceProviderService.wsdl");

		ChargeServiceProviderService_Service service1 = new ChargeServiceProviderService_Service(wsdlURL,
				new AddressingFeature());

		ChargeServiceProviderService port1 = service1.getChargeServiceProviderServiceSoap12();

		BindingProvider bp = (BindingProvider) port1;

		AddressingProperties addrProps = new AddressingProperties();
		AttributedURIType messageId = new AttributedURIType();
		messageId.setValue(eventId);
		addrProps.setMessageID(messageId);
		AttributedURIType endPoint = new AttributedURIType();
		endPoint.setValue(endpointURL);
		addrProps.setTo(endPoint);

		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
		bp.getRequestContext().put(JAXWSAConstants.CLIENT_ADDRESSING_PROPERTIES, addrProps);

		Holder<Integer> priorityHolder = new Holder<Integer>(new Integer(priority));
		Holder<Parties> partiesHolder = new Holder<Parties>(parties);
		Holder<Date> datetimeHolder = new Holder<Date>(datetime);

		UpdateCableCapacityForecastResponse response;
		try {
			response = port1.updateCableCapacityForecast(request, partiesHolder, priorityHolder, datetimeHolder);
		} catch (Exception e) {
			ActionException ae = new ActionException(ActionExceptionType.CONNECTION,
					"UpdateCableCapacityForecast failed for csp: " + csp.getEan13(), e);
			ae.getTargets().add(csp);
			throw ae;
		}

		return response;
	}

	@Override
	public void heartbeat(Dso dso, Csp csp, int forecastInterval, int heartbeatInterval, int priority)
			throws ActionException {
		String cspEan = "<<unknown>>";
		String dsoEan = "<<unknown>>";

		try {

			cspEan = csp.getEan13();
			dsoEan = dso.getEan13();

			HeartbeatRequest request = new HeartbeatRequest();
			request.setForecastTimeInterval(forecastInterval);
			request.setHeartBeatTimeInterval(heartbeatInterval);

			String eventId = ActionContext.getAction().getId();

			Parties parties = new Parties();
			parties.setReceiverID(cspEan);
			parties.setSenderID(dsoEan);

			HeartbeatResponse response = this.heartbeat(csp, request, parties, priority, new Date(), eventId);

			Result result = response.getResult();

			this.processResponseResult(result, csp);
		} catch (Exception e) {
			if (e instanceof ActionException) {
				throw (ActionException) e;
			}

			String message = "Unexpected exception pushing heartbeat to Csp with EAN " + cspEan + "!";
			throw new ActionException(ActionExceptionType.UNEXPECTED, message, e);
		}
	}

	private HeartbeatResponse heartbeat(Csp csp, HeartbeatRequest request, Parties parties, int priority,
			Date datetime, String eventId) throws Exception {

		String endpointURL = csp.getEndpoint();

		URL wsdlURL = ChargeServiceProviderServiceClient.class.getClassLoader().getResource(
				"META-INF/wsdl/1.0/ChargeServiceProviderService.wsdl");

		ChargeServiceProviderService_Service service1 = new ChargeServiceProviderService_Service(wsdlURL,
				new AddressingFeature());

		ChargeServiceProviderService port1 = service1.getChargeServiceProviderServiceSoap12();

		BindingProvider bp = (BindingProvider) port1;

		AddressingProperties addrProps = new AddressingProperties();
		AttributedURIType messageId = new AttributedURIType();
		messageId.setValue(eventId);
		addrProps.setMessageID(messageId);
		AttributedURIType endPoint = new AttributedURIType();
		endPoint.setValue(endpointURL);
		addrProps.setTo(endPoint);

		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
		bp.getRequestContext().put(JAXWSAConstants.CLIENT_ADDRESSING_PROPERTIES, addrProps);

		Holder<Integer> priorityHolder = new Holder<Integer>(new Integer(priority));
		Holder<Parties> partiesHolder = new Holder<Parties>(parties);
		Holder<Date> datetimeHolder = new Holder<Date>(datetime);

		HeartbeatResponse response;
		try {
			response = port1.heartbeat(request, partiesHolder, priorityHolder, datetimeHolder);
		} catch (Exception e) {
			ActionException ae = new ActionException(ActionExceptionType.CONNECTION, "Heartbeat failed for csp: "
					+ csp.getEan13(), e);
			ae.getTargets().add(csp);
			throw ae;
		}

		return response;
	}

	@Override
	public void updateWeahterValues(int blockMinutes, LocationMeasurement locationMeasurement,
			List<LocationMeasurementValue> locationMeasurementValues, Csp csp, Dso dso, int priority)
			throws ActionException {
		throw new ActionException(ActionExceptionType.NOT_SUPPORTED,
				"UpdateWeatherValues is not supported OSCP version 0.9!");
	}

	private void processResponseResult(Result result, Csp csp) throws ActionException {
		boolean success = result.getType().equals(ResultType.SUCCESS);

		Details detailsHolder = result.getDetails();

		if (detailsHolder != null) {
			List<Detail> details = detailsHolder.getDetail();
			for (Detail detail : details) {
				if (!(detail.getLevel().equals(LogLevelType.INFO) || detail.getLevel().equals(LogLevelType.DEBUG))) {
					String detailCode = detail.getCode();
					String detailMessage = detail.getMessage();
					String detailDesc = detail.getDescription();

					String message = "Response detail: {0} - {1} - {2}";
					Object[] messageParams = { detailCode, detailMessage, detailDesc };

					if (detail.getLevel().equals(LogLevelType.WARN)) {
						ActionLogger.warn(message, messageParams, null, true);
					} else if (detail.getLevel().equals(LogLevelType.ERROR)) {
						success = false;
						ActionLogger.error(message, messageParams, null, true);
					}
				}
			}
		}

		if (!success) {
			throw new ActionException(ActionExceptionType.MESSAGE, "Error(s) reported by csp " + csp.getEan13() + " ("
					+ csp.getName() + ")");
		}
	}

}
