package nl.enexis.scip.csp.client;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.model.Csp;

public class ChargeServiceProviderServiceClientFactory {

	public static ChargeServiceProviderServiceClient getCspClient(Csp csp) throws ActionException {
		ChargeServiceProviderServiceClient client = null;
		if ("0.9".equals(csp.getProtocolVersion())) {
			client = new nl.enexis.scip.csp.client.v0_9.ChargeServiceProviderServiceClientImpl();
		} else if ("1.0".equals(csp.getProtocolVersion())) {
			client = new nl.enexis.scip.csp.client.v1_0.ChargeServiceProviderServiceClientImpl();
		} else {
			throw new ActionException(ActionExceptionType.NOT_SUPPORTED, "OSCP protocol version "
					+ csp.getProtocolVersion() + " not known/supported for messages towards CSP!");
		}
		return client;

	}
}
