
package nl.enexis.scip.csp.client.v1_0.generated;

/*
 * #%L
 * SCiP-CSP-Client
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines the DSO request towards CSP with a
 * 						capacity forecast
 * 					
 * 
 * <p>Java class for UpdateCableCapacityForecastRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateCableCapacityForecastRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Forecast" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://OSCP/CSP/2013/06/}Cable"/>
 *                   &lt;element ref="{http://OSCP/CSP/2013/06/}ForecastedBlock" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateCableCapacityForecastRequest", propOrder = {
    "forecast"
})
public class UpdateCableCapacityForecastRequest {

    @XmlElement(name = "Forecast", required = true)
    protected List<UpdateCableCapacityForecastRequest.Forecast> forecast;

    /**
     * Gets the value of the forecast property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the forecast property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForecast().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateCableCapacityForecastRequest.Forecast }
     * 
     * 
     */
    public List<UpdateCableCapacityForecastRequest.Forecast> getForecast() {
        if (forecast == null) {
            forecast = new ArrayList<UpdateCableCapacityForecastRequest.Forecast>();
        }
        return this.forecast;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://OSCP/CSP/2013/06/}Cable"/>
     *         &lt;element ref="{http://OSCP/CSP/2013/06/}ForecastedBlock" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cable",
        "forecastedBlock"
    })
    public static class Forecast {

        @XmlElement(name = "Cable", required = true)
        protected Cable cable;
        @XmlElement(name = "ForecastedBlock", required = true)
        protected List<ForecastedBlock> forecastedBlock;

        /**
         * Gets the value of the cable property.
         * 
         * @return
         *     possible object is
         *     {@link Cable }
         *     
         */
        public Cable getCable() {
            return cable;
        }

        /**
         * Sets the value of the cable property.
         * 
         * @param value
         *     allowed object is
         *     {@link Cable }
         *     
         */
        public void setCable(Cable value) {
            this.cable = value;
        }

        /**
         * Gets the value of the forecastedBlock property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the forecastedBlock property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getForecastedBlock().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ForecastedBlock }
         * 
         * 
         */
        public List<ForecastedBlock> getForecastedBlock() {
            if (forecastedBlock == null) {
                forecastedBlock = new ArrayList<ForecastedBlock>();
            }
            return this.forecastedBlock;
        }

    }

}
