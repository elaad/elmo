package nl.enexis.scip.csp.client;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.DsoCsp;
import nl.enexis.scip.service.GeneralService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ChargeServiceProviderServiceClientFactory.class })
public class CspMessagingServiceImplTest {

	@InjectMocks
	private CspMessagingServiceImpl cspMessagingServiceImpl;

	@Mock
	private GeneralService generalService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ChargeServiceProviderServiceClientFactory.class);
	}

	// Assure that correct values are parsed to the cspClient heartbeat
	@Test
	public void testHeartbeat() throws Exception {
		int priority = 5;
		String forecastInterval = "2432/4532/39";
		String heartbeatIntervalSetting = "493/923/8943/543";
		when(generalService.getConfigurationParameterAsInteger(eq("CspMessageDefaultPriority"))).thenReturn(priority);
		when(generalService.getConfigurationParameterAsString(eq("CspMessageTimerMinutes"))).thenReturn(forecastInterval);
		when(generalService.getConfigurationParameterAsString(eq("CspHeartbeatTimerMinutes"))).thenReturn(heartbeatIntervalSetting);

		Csp csp = new Csp();
		csp.setEan13("CSPEAN13");
		Dso dso = new Dso();
		dso.setEan13("DSOEAN13");
		DsoCsp dsoCsp = new DsoCsp();
		dsoCsp.setDso(dso);
		dsoCsp.setCsp(csp);
		ChargeServiceProviderServiceClient cspClient = mock(ChargeServiceProviderServiceClient.class);
		when(ChargeServiceProviderServiceClientFactory.getCspClient(any(Csp.class))).thenReturn(cspClient);

		cspMessagingServiceImpl.heartbeat(dsoCsp);
		int expectedForecastIntervalResult = 4532 * 60;
		int expectedHearbeatIntervalSettingResult = 923 * 60;
		verify(cspClient, times(1)).heartbeat(eq(dso), eq(csp), eq(expectedForecastIntervalResult),
				eq(expectedHearbeatIntervalSettingResult), eq(5));
	}

}
