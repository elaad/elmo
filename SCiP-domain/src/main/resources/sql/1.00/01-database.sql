---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: scip
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `SC_ADMIN_USER`
--

DROP TABLE IF EXISTS `SC_ADMIN_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_ADMIN_USER` (
  `USER_ID` varchar(10) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `MAIL` varchar(100) NOT NULL,
  `FIRSTNAME` varchar(100) NOT NULL,
  `LASTNAME` varchar(100) NOT NULL,
  `PASSWORD` varchar(10) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CABLE`
--

DROP TABLE IF EXISTS `SC_CABLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CABLE` (
  `CABLE_ID` varchar(20) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ADDED_AT` datetime NOT NULL,
  `CHANGED_AT` datetime NOT NULL,
  `FIXED_FORECAST` tinyint(1) NOT NULL,
  `FIXED_FORECAST_DEVIATION` decimal(18,2) DEFAULT NULL,
  `FIXED_FORECAST_DEVIATION_BLOCKS` varchar(20) DEFAULT NULL,
  `FIXED_FORECAST_MAX` decimal(18,2) DEFAULT NULL,
  `FIXED_FORECAST_RANDOM` tinyint(1) DEFAULT NULL,
  `MAX_CAPACITY` decimal(18,2) NOT NULL,
  `MAX_CAR_CAPACITY` decimal(18,2) NOT NULL,
  `USAGE_MEASURED_BY` varchar(8) NOT NULL,
  `DSO` varchar(13) NOT NULL,
  `TRANSFORMER` varchar(20) NOT NULL,
  PRIMARY KEY (`CABLE_ID`),
  KEY `FK2C2E256E863F1ADC` (`DSO`),
  KEY `FK2C2E256EE6F78D8E` (`TRANSFORMER`),
  CONSTRAINT `FK2C2E256EE6F78D8E` FOREIGN KEY (`TRANSFORMER`) REFERENCES `SC_TRANSFORMER` (`TRAFO_ID`),
  CONSTRAINT `FK2C2E256E863F1ADC` FOREIGN KEY (`DSO`) REFERENCES `SC_DSO` (`EAN13`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CABLE_CSP`
--

DROP TABLE IF EXISTS `SC_CABLE_CSP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CABLE_CSP` (
  `ID` bigint(20) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ADDED_AT` datetime NOT NULL,
  `CHANGED_AT` datetime NOT NULL,
  `CABLE` varchar(20) NOT NULL,
  `CSP` varchar(13) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `key1` (`CABLE`,`CSP`),
  KEY `FK74CDF38FF2B700D6` (`CABLE`),
  KEY `FK74CDF38F863F135C` (`CSP`),
  CONSTRAINT `FK74CDF38F863F135C` FOREIGN KEY (`CSP`) REFERENCES `SC_CSP` (`EAN13`),
  CONSTRAINT `FK74CDF38FF2B700D6` FOREIGN KEY (`CABLE`) REFERENCES `SC_CABLE` (`CABLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CABLE_FORECAST`
--

DROP TABLE IF EXISTS `SC_CABLE_FORECAST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CABLE_FORECAST` (
  `ID` varchar(50) NOT NULL,
  `BLOCK_MINUTES` int(11) DEFAULT NULL,
  `DATETIME` datetime NOT NULL,
  `forecastSafetyFactor` decimal(19,2) DEFAULT NULL,
  `REMAINING_CAPACITY_FACTOR` decimal(22,2) DEFAULT NULL,
  `SUCCESS` tinyint(1) NOT NULL,
  `CABLE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK8C5701ACF2B700D6` (`CABLE`),
  CONSTRAINT `FK8C5701ACF2B700D6` FOREIGN KEY (`CABLE`) REFERENCES `SC_CABLE` (`CABLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CABLE_FORECAST_INPUT`
--

DROP TABLE IF EXISTS `SC_CABLE_FORECAST_INPUT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CABLE_FORECAST_INPUT` (
  `ID` bigint(20) NOT NULL,
  `BLOCK` int(11) NOT NULL,
  `CAR_USAGE1` decimal(22,2) DEFAULT NULL,
  `CAR_USAGE2` decimal(22,2) DEFAULT NULL,
  `CAR_USAGE3` decimal(22,2) DEFAULT NULL,
  `DAY` date NOT NULL,
  `HOUR` int(11) NOT NULL,
  `NOT_AVAILABLE` tinyint(1) NOT NULL,
  `TOTAL1` decimal(22,2) DEFAULT NULL,
  `TOTAL2` decimal(22,2) DEFAULT NULL,
  `TOTAL3` decimal(22,2) DEFAULT NULL,
  `VALUE_DAY` date NOT NULL,
  `CABLE_FORECAST` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKC647D357A52C9D71` (`CABLE_FORECAST`),
  CONSTRAINT `FKC647D357A52C9D71` FOREIGN KEY (`CABLE_FORECAST`) REFERENCES `SC_CABLE_FORECAST` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CABLE_FORECAST_OUTPUT`
--

DROP TABLE IF EXISTS `SC_CABLE_FORECAST_OUTPUT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CABLE_FORECAST_OUTPUT` (
  `ID` bigint(20) NOT NULL,
  `AVAILABLE_CAPACITY` decimal(22,2) NOT NULL,
  `BLOCK` int(11) NOT NULL,
  `DAY` date NOT NULL,
  `DAY_OF_WEEK` int(11) NOT NULL,
  `HOUR` int(11) NOT NULL,
  `CABLE_FORECAST` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKD540EF4A52C9D71` (`CABLE_FORECAST`),
  CONSTRAINT `FKD540EF4A52C9D71` FOREIGN KEY (`CABLE_FORECAST`) REFERENCES `SC_CABLE_FORECAST` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CONFIG_PARAMETER`
--

DROP TABLE IF EXISTS `SC_CONFIG_PARAMETER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CONFIG_PARAMETER` (
  `NAME` varchar(50) NOT NULL,
  `CHANGEABLE` tinyint(1) DEFAULT NULL,
  `VALUE` varchar(250) NOT NULL,
  PRIMARY KEY (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP`
--

DROP TABLE IF EXISTS `SC_CSP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP` (
  `EAN13` varchar(13) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ADDED_AT` datetime NOT NULL,
  `CHANGED_AT` datetime NOT NULL,
  `ENDPOINT` varchar(255) DEFAULT NULL,
  `NAME` varchar(100) NOT NULL,
  `CLIENT_CERT_PATH` longtext,
  `CLIENT_CERT_PWD` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`EAN13`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_ADJUSTMENT`
--

DROP TABLE IF EXISTS `SC_CSP_ADJUSTMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_ADJUSTMENT` (
  `ID` bigint(20) NOT NULL,
  `CABLE_CSP` bigint(20) NOT NULL,
  `CSP_ADJUSTMENT_MESSAGE` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK355736FBCA92B9A5` (`CABLE_CSP`),
  KEY `FK355736FB7B2D93B2` (`CSP_ADJUSTMENT_MESSAGE`),
  CONSTRAINT `FK355736FB7B2D93B2` FOREIGN KEY (`CSP_ADJUSTMENT_MESSAGE`) REFERENCES `SC_CSP_ADJUSTMENT_MESSAGE` (`ID`),
  CONSTRAINT `FK355736FBCA92B9A5` FOREIGN KEY (`CABLE_CSP`) REFERENCES `SC_CABLE_CSP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_ADJUSTMENT_MESSAGE`
--

DROP TABLE IF EXISTS `SC_CSP_ADJUSTMENT_MESSAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_ADJUSTMENT_MESSAGE` (
  `ID` bigint(20) NOT NULL,
  `CSP` varchar(13) NOT NULL,
  `DATE_TIME` datetime NOT NULL,
  `DSO` varchar(13) NOT NULL,
  `EVENT_ID` varchar(50) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `SUCCESS` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EVENT_ID_` (`EVENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_ADJUSTMENT_VALUE`
--

DROP TABLE IF EXISTS `SC_CSP_ADJUSTMENT_VALUE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_ADJUSTMENT_VALUE` (
  `ID` bigint(20) NOT NULL,
  `ASSIGNED` decimal(22,2) DEFAULT NULL,
  `BLOCK` int(11) NOT NULL,
  `DAY` date NOT NULL,
  `DAY_OF_WEEK` int(11) NOT NULL,
  `ENDED_AT` datetime NOT NULL,
  `EXTRA_CAPACITY` tinyint(1) NOT NULL,
  `HOUR` int(11) NOT NULL,
  `REQUESTED` decimal(22,2) NOT NULL,
  `STARTED_AT` datetime NOT NULL,
  `CABLE_CSP` bigint(20) NOT NULL,
  `CSP_ADJUSTMENT` bigint(20) NOT NULL,
  `CSP_FORECAST_VALUE` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKAD49CB0DCA92B9A5` (`CABLE_CSP`),
  KEY `FKAD49CB0D2825D8F5` (`CSP_ADJUSTMENT`),
  KEY `FKAD49CB0D5C9A5286` (`CSP_FORECAST_VALUE`),
  CONSTRAINT `FKAD49CB0D5C9A5286` FOREIGN KEY (`CSP_FORECAST_VALUE`) REFERENCES `SC_CSP_FORECAST_VALUE` (`ID`),
  CONSTRAINT `FKAD49CB0D2825D8F5` FOREIGN KEY (`CSP_ADJUSTMENT`) REFERENCES `SC_CSP_ADJUSTMENT` (`ID`),
  CONSTRAINT `FKAD49CB0DCA92B9A5` FOREIGN KEY (`CABLE_CSP`) REFERENCES `SC_CABLE_CSP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_CONNECTION`
--

DROP TABLE IF EXISTS `SC_CSP_CONNECTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_CONNECTION` (
  `EAN18` varchar(18) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ADDED_AT` datetime NOT NULL,
  `CAPACITY` decimal(18,2) NOT NULL,
  `CHANGED_AT` datetime NOT NULL,
  `CABLE_CSP` bigint(20) NOT NULL,
  PRIMARY KEY (`EAN18`),
  KEY `FK9143C86CCA92B9A5` (`CABLE_CSP`),
  CONSTRAINT `FK9143C86CCA92B9A5` FOREIGN KEY (`CABLE_CSP`) REFERENCES `SC_CABLE_CSP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_FORECAST`
--

DROP TABLE IF EXISTS `SC_CSP_FORECAST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_FORECAST` (
  `ID` bigint(20) NOT NULL,
  `CALCULATED_AT` datetime NOT NULL,
  `CABLE_CSP` bigint(20) NOT NULL,
  `CABLE_FORECAST` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKB2432309CA92B9A5` (`CABLE_CSP`),
  KEY `FKB2432309A52C9D71` (`CABLE_FORECAST`),
  CONSTRAINT `FKB2432309A52C9D71` FOREIGN KEY (`CABLE_FORECAST`) REFERENCES `SC_CABLE_FORECAST` (`ID`),
  CONSTRAINT `FKB2432309CA92B9A5` FOREIGN KEY (`CABLE_CSP`) REFERENCES `SC_CABLE_CSP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_FORECAST_MESSAGE`
--

DROP TABLE IF EXISTS `SC_CSP_FORECAST_MESSAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_FORECAST_MESSAGE` (
  `ID` bigint(20) NOT NULL,
  `CSP` varchar(13) NOT NULL,
  `DATE_TIME` datetime NOT NULL,
  `DSO` varchar(13) NOT NULL,
  `EVENT_ID` varchar(50) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `SUCCESS` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EVENT_ID_` (`EVENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_FORECAST_MESSAGE_LINK`
--

DROP TABLE IF EXISTS `SC_CSP_FORECAST_MESSAGE_LINK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_FORECAST_MESSAGE_LINK` (
  `CSP_MESSAGE` bigint(20) NOT NULL,
  `CSP_FORECAST` bigint(20) NOT NULL,
  KEY `FK57CF6AC82285CD1` (`CSP_FORECAST`),
  KEY `FK57CF6AC869DB9958` (`CSP_MESSAGE`),
  CONSTRAINT `FK57CF6AC869DB9958` FOREIGN KEY (`CSP_MESSAGE`) REFERENCES `SC_CSP_FORECAST_MESSAGE` (`ID`),
  CONSTRAINT `FK57CF6AC82285CD1` FOREIGN KEY (`CSP_FORECAST`) REFERENCES `SC_CSP_FORECAST` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_FORECAST_VALUE`
--

DROP TABLE IF EXISTS `SC_CSP_FORECAST_VALUE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_FORECAST_VALUE` (
  `ID` bigint(20) NOT NULL,
  `ASSIGNED` decimal(22,2) NOT NULL,
  `REMAINING` decimal(22,2) NOT NULL,
  `CABLE_FORECAST_OUTPUT` bigint(20) NOT NULL,
  `CSP_ADJUSTMENT_VALUE` bigint(20) DEFAULT NULL,
  `CSP_FORECAST` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKB12BF49B379BDB98` (`CABLE_FORECAST_OUTPUT`),
  KEY `FKB12BF49B82094F86` (`CSP_ADJUSTMENT_VALUE`),
  KEY `FKB12BF49B2285CD1` (`CSP_FORECAST`),
  CONSTRAINT `FKB12BF49B2285CD1` FOREIGN KEY (`CSP_FORECAST`) REFERENCES `SC_CSP_FORECAST` (`ID`),
  CONSTRAINT `FKB12BF49B379BDB98` FOREIGN KEY (`CABLE_FORECAST_OUTPUT`) REFERENCES `SC_CABLE_FORECAST_OUTPUT` (`ID`),
  CONSTRAINT `FKB12BF49B82094F86` FOREIGN KEY (`CSP_ADJUSTMENT_VALUE`) REFERENCES `SC_CSP_ADJUSTMENT_VALUE` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_GET_FORECAST_MESSAGE`
--

DROP TABLE IF EXISTS `SC_CSP_GET_FORECAST_MESSAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_GET_FORECAST_MESSAGE` (
  `ID` bigint(20) NOT NULL,
  `CSP` varchar(13) NOT NULL,
  `DATE_TIME` datetime NOT NULL,
  `DSO` varchar(13) NOT NULL,
  `EVENT_ID` varchar(50) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `SUCCESS` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EVENT_ID_` (`EVENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_USAGE`
--

DROP TABLE IF EXISTS `SC_CSP_USAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_USAGE` (
  `ID` bigint(20) NOT NULL,
  `CABLE_CSP` bigint(20) NOT NULL,
  `CSP_USAGE_MESSAGE` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKC8826733CA92B9A5` (`CABLE_CSP`),
  KEY `FKC88267335773360C` (`CSP_USAGE_MESSAGE`),
  CONSTRAINT `FKC88267335773360C` FOREIGN KEY (`CSP_USAGE_MESSAGE`) REFERENCES `SC_CSP_USAGE_MESSAGE` (`ID`),
  CONSTRAINT `FKC8826733CA92B9A5` FOREIGN KEY (`CABLE_CSP`) REFERENCES `SC_CABLE_CSP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_USAGE_MESSAGE`
--

DROP TABLE IF EXISTS `SC_CSP_USAGE_MESSAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_USAGE_MESSAGE` (
  `ID` bigint(20) NOT NULL,
  `CSP` varchar(13) NOT NULL,
  `DATE_TIME` datetime NOT NULL,
  `DSO` varchar(13) NOT NULL,
  `EVENT_ID` varchar(50) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `SUCCESS` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EVENT_ID_` (`EVENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_CSP_USAGE_VALUE`
--

DROP TABLE IF EXISTS `SC_CSP_USAGE_VALUE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_CSP_USAGE_VALUE` (
  `ID` bigint(20) NOT NULL,
  `AVERAGE_CURRENT` decimal(12,2) NOT NULL,
  `BLOCK` int(11) NOT NULL,
  `DAY` date NOT NULL,
  `DAY_OF_WEEK` int(11) NOT NULL,
  `ENDED_AT` datetime NOT NULL,
  `ENERGY_CONSUMPTION` decimal(12,2) NOT NULL,
  `HOUR` int(11) NOT NULL,
  `STARTED_AT` datetime NOT NULL,
  `CABLE_CSP` bigint(20) NOT NULL,
  `CSP_USAGE` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKB1ADF145CA92B9A5` (`CABLE_CSP`),
  KEY `FKB1ADF145B6915847` (`CSP_USAGE`),
  CONSTRAINT `FKB1ADF145B6915847` FOREIGN KEY (`CSP_USAGE`) REFERENCES `SC_CSP_USAGE` (`ID`),
  CONSTRAINT `FKB1ADF145CA92B9A5` FOREIGN KEY (`CABLE_CSP`) REFERENCES `SC_CABLE_CSP` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_DSO`
--

DROP TABLE IF EXISTS `SC_DSO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_DSO` (
  `EAN13` varchar(13) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ADDED_AT` datetime NOT NULL,
  `CHANGED_AT` datetime NOT NULL,
  `NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`EAN13`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_DSO_CSP`
--

DROP TABLE IF EXISTS `SC_DSO_CSP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_DSO_CSP` (
  `ID` bigint(20) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ADDED_AT` datetime NOT NULL,
  `CHANGED_AT` datetime NOT NULL,
  `CSP` varchar(13) NOT NULL,
  `DSO` varchar(13) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `key1` (`DSO`,`CSP`),
  KEY `FK2D97CFD2863F135C` (`CSP`),
  KEY `FK2D97CFD2863F1ADC` (`DSO`),
  CONSTRAINT `FK2D97CFD2863F1ADC` FOREIGN KEY (`DSO`) REFERENCES `SC_DSO` (`EAN13`),
  CONSTRAINT `FK2D97CFD2863F135C` FOREIGN KEY (`CSP`) REFERENCES `SC_CSP` (`EAN13`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_ERROR`
--

DROP TABLE IF EXISTS `SC_ERROR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_ERROR` (
  `ID` bigint(20) NOT NULL,
  `CAUSE` varchar(254) NOT NULL,
  `CAUSE_DETAIL` longtext NOT NULL,
  `CODE` varchar(50) NOT NULL,
  `DATE_TIME` datetime NOT NULL,
  `DETAIL` longtext NOT NULL,
  `EVENT` varchar(50) NOT NULL,
  `EVENT_ID` varchar(50) DEFAULT NULL,
  `MESSAGE` varchar(254) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_MEASUREMENT`
--

DROP TABLE IF EXISTS `SC_MEASUREMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_MEASUREMENT` (
  `ID` bigint(20) NOT NULL,
  `DATE` datetime NOT NULL,
  `CABLE` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `key1` (`CABLE`,`DATE`),
  KEY `FK9665E02DF2B700D6` (`CABLE`),
  CONSTRAINT `FK9665E02DF2B700D6` FOREIGN KEY (`CABLE`) REFERENCES `SC_CABLE` (`CABLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_MEASUREMENT_VALUE`
--

DROP TABLE IF EXISTS `SC_MEASUREMENT_VALUE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_MEASUREMENT_VALUE` (
  `ID` bigint(20) NOT NULL,
  `IGEM1` decimal(18,2) DEFAULT NULL,
  `IGEM2` decimal(18,2) DEFAULT NULL,
  `IGEM3` decimal(18,2) DEFAULT NULL,
  `IMAX1` decimal(18,2) DEFAULT NULL,
  `IMAX2` decimal(18,2) DEFAULT NULL,
  `IMAX3` decimal(18,2) DEFAULT NULL,
  `PMAX0` decimal(18,2) DEFAULT NULL,
  `PMAX1` decimal(18,2) DEFAULT NULL,
  `PMAX2` decimal(18,2) DEFAULT NULL,
  `PMAX3` decimal(18,2) DEFAULT NULL,
  `SMAX0` decimal(18,2) DEFAULT NULL,
  `SMAX1` decimal(18,2) DEFAULT NULL,
  `SMAX2` decimal(18,2) DEFAULT NULL,
  `SMAX3` decimal(18,2) DEFAULT NULL,
  `UGEM1` decimal(18,2) DEFAULT NULL,
  `UGEM2` decimal(18,2) DEFAULT NULL,
  `UGEM3` decimal(18,2) DEFAULT NULL,
  `COS1` decimal(18,2) DEFAULT NULL,
  `COS2` decimal(18,2) DEFAULT NULL,
  `COS3` decimal(18,2) DEFAULT NULL,
  `EP0` decimal(18,2) DEFAULT NULL,
  `EP1` decimal(18,2) DEFAULT NULL,
  `EP2` decimal(18,2) DEFAULT NULL,
  `EP3` decimal(18,2) DEFAULT NULL,
  `MEASUREMENT` bigint(20) NOT NULL,
  `METER` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKDDED46BFFD3AAAD4` (`MEASUREMENT`),
  KEY `FKDDED46BFF3D4FF6E` (`METER`),
  CONSTRAINT `FKDDED46BFF3D4FF6E` FOREIGN KEY (`METER`) REFERENCES `SC_METER` (`METER_ID`),
  CONSTRAINT `FKDDED46BFFD3AAAD4` FOREIGN KEY (`MEASUREMENT`) REFERENCES `SC_MEASUREMENT` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_METER`
--

DROP TABLE IF EXISTS `SC_METER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_METER` (
  `METER_ID` varchar(20) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ADDED_AT` datetime NOT NULL,
  `CHANGED_AT` datetime NOT NULL,
  `MEASURES` varchar(8) NOT NULL,
  `METER_NAME` varchar(100) DEFAULT NULL,
  `CABLE` varchar(20) NOT NULL,
  `TRANSFORMER` varchar(20) NOT NULL,
  PRIMARY KEY (`METER_ID`),
  KEY `FK2CBD24BAF2B700D6` (`CABLE`),
  KEY `FK2CBD24BAE6F78D8E` (`TRANSFORMER`),
  CONSTRAINT `FK2CBD24BAE6F78D8E` FOREIGN KEY (`TRANSFORMER`) REFERENCES `SC_TRANSFORMER` (`TRAFO_ID`),
  CONSTRAINT `FK2CBD24BAF2B700D6` FOREIGN KEY (`CABLE`) REFERENCES `SC_CABLE` (`CABLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SC_TRANSFORMER`
--

DROP TABLE IF EXISTS `SC_TRANSFORMER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SC_TRANSFORMER` (
  `TRAFO_ID` varchar(20) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ADDED_AT` datetime NOT NULL,
  `CHANGED_AT` datetime NOT NULL,
  `LAST_MEASUREMENT` datetime DEFAULT NULL,
  PRIMARY KEY (`TRAFO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-13  7:48:29

insert into hibernate_sequence (next_val) values (1);
