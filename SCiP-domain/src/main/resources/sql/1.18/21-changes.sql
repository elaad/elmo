---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.18.1 (WF9)' WHERE `NAME`='ApplicationVersion';

ALTER TABLE `scip`.`SC_CABLE_CSP` 
DROP COLUMN `CHANGED_AT`;

ALTER TABLE `scip`.`SC_CSP` 
DROP COLUMN `CHANGED_AT`;

ALTER TABLE `scip`.`SC_CSP_CONNECTION` 
DROP COLUMN `CHANGED_AT`;

ALTER TABLE `scip`.`SC_DSO` 
DROP COLUMN `CHANGED_AT`;

ALTER TABLE `scip`.`SC_DSO_CSP` 
DROP COLUMN `CHANGED_AT`;

ALTER TABLE `scip`.`SC_LOCATION` 
DROP COLUMN `CHANGED_AT`,
DROP COLUMN `ADDED_AT`;

ALTER TABLE `scip`.`SC_METER` 
DROP COLUMN `CHANGED_AT`;

ALTER TABLE `scip`.`SC_TRANSFORMER` 
DROP COLUMN `CHANGED_AT`;
