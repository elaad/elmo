---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.12' WHERE `NAME`='ApplicationVersion';

-- Adjustment for notifications 
ALTER TABLE `SC_ADMIN_USER_NOTIFICATION` 
ADD COLUMN `LEVEL` VARCHAR(5) NOT NULL AFTER `NOTIFY_INTERVAL`;

UPDATE `SC_ADMIN_USER_NOTIFICATION` SET `LEVEL`='ERROR';

ALTER TABLE `SC_ADMIN_USER_NOTIFICATION`
DROP PRIMARY KEY,
ADD PRIMARY KEY (`USER`, `EVENT`, `NOTIFY_INTERVAL`, `LEVEL`);

-- Timer settings daily/weekly notifications
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('NotificationTimerWeeklyDay', '1', '5', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('NotificationTimerHour', '1', '6', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('NotificationTimerMinute', '1', '0', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('MailDoAuthentication', '1', 'false', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('MailUseTLS', '1', 'false', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('MailDoDebug', '1', 'false', '1');
	
-- Drop old errors table
DROP TABLE `SC_ERROR`;

-- Param for check if for cast has dropped under certain number of blocks
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('ForecastMinimalOutputBlocks', '1', '48', '1');

-- Fields added to indicatie which direction the forecast is developing
ALTER TABLE `SC_CSP_FORECAST` 
ADD COLUMN `CHANGE_DIRECTION` VARCHAR(5) NULL AFTER `NUMBER_OF_BLOCKS`;
ALTER TABLE `SC_CSP_FORECAST`
ADD COLUMN `CHANGE_VALUE` DECIMAL(22,2) NULL AFTER `CHANGE_DIRECTION`;
ALTER TABLE `SC_CSP_FORECAST`
ADD COLUMN `CHANGE_BLOCK` BIGINT(20) NULL AFTER `CHANGE_VALUE`;
ALTER TABLE `SC_CSP_FORECAST_VALUE` 
ADD COLUMN `ORIGINAL_ASSIGNED` DECIMAL(22,2) NULL AFTER `ASSIGNED`;


-- Factor to indicate that CSP forecast is not changed
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('NoCspChangeFactor', '1', '0.01', '1');

