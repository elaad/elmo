These scripts implement a database cleanup plan. 

How to install:
First execute the _setup_cleanup script to add cascading to all relevant tables.
After that, run proc_cleanup to create the cleanup procedure.

How to schedule:
-- start event scheduler (SET @@global.event_scheduler = ON; will also work)
-- permanent activation: [mysqld] event_scheduler=on in my.cnf
-- to check if running: SELECT @@global.event_scheduler;
SET GLOBAL event_scheduler = ON;

-- schedule cleanup as a recurring event at 01:00 every day
CREATE EVENT cleanup62 ON SCHEDULE EVERY 1 DAY STARTS '2015-01-01 01:00:00' DO CALL cleanup(62);

-- one time only event to delete everything older than a year:
CREATE EVENT cleanup365 ON SCHEDULE AT '2015-04-02 03:00:00' DO CALL cleanup(365);



