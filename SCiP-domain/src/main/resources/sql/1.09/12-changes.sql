---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.9' WHERE `NAME`='ApplicationVersion';

-- Add parameter for incomplete measurement export
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('ExportIncompleteMeasurementsTemplate', '1', '/mnt/data/templates/exportIncompleteMeasurementsTemplate.xls', '1');

-- Change EXCEPTION_TYPE TO NOT NULL
ALTER TABLE `SC_ACTION_MESSAGE` 
CHANGE COLUMN `EXCEPTION_TYPE` `EXCEPTION_TYPE` VARCHAR(30) NOT NULL ;

-- Drop SSL settings from CSP. Didn't make sense
ALTER TABLE `SC_CSP` 
DROP COLUMN `CLIENT_CERT_PWD`,
DROP COLUMN `CLIENT_CERT_PATH`;

-- For SSmE needed to know the last received CspUsageValue for a certain CableCsp, day, hour and block. Start with true (1) for all
ALTER TABLE `SC_CSP_USAGE_VALUE` ADD COLUMN `LAST` TINYINT(1) NOT NULL DEFAULT 0 AFTER `CSP_USAGE`;
UPDATE `SC_CSP_USAGE_VALUE` SET `LAST` = 1;

-- For SSmE forecast calculation are possible the values for last one or two days not based on complete input at SHIFFT. So offset needed
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) VALUES ('SuForecastInputDaysOffset', '1', '2', '0');
