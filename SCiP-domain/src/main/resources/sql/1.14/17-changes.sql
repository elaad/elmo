---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.14' WHERE `NAME`='ApplicationVersion';

-- Update to add deviation CSP usage from assigned
ALTER TABLE `SC_CSP_USAGE_VALUE` 
ADD COLUMN `DEVIATION` DECIMAL(12,2) NOT NULL AFTER `LAST`;

-- Dashboard scheduler
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`) VALUES ('DashboardTimerMinutes', '1', '0/10');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`) VALUES ('DashboardTimerHours', '1', '*');

