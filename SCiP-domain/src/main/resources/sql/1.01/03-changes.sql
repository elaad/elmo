---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- For adjustable number of output blocks in CSP message
ALTER TABLE `SC_CABLE_CSP` ADD COLUMN `NUMBER_OF_BLOCKS` INT(20) NOT NULL  AFTER `CSP` ;
UPDATE `SC_CABLE_CSP` SET `NUMBER_OF_BLOCKS`=96;
ALTER TABLE `SC_CSP_FORECAST` ADD COLUMN `NUMBER_OF_BLOCKS` INT(20) NULL  AFTER `CABLE_FORECAST` ;

-- Version number of application
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`) VALUES ('ApplicationVersion', '1', '1.1');

-- For move from safety and remaining factor from global to cable
DELETE FROM `SC_CONFIG_PARAMETER` WHERE `NAME`='ForecastRemainingCapacityFactor';
DELETE FROM `SC_CONFIG_PARAMETER` WHERE `NAME`='ForecastSafetyFactor';
ALTER TABLE `SC_CABLE` ADD COLUMN `SAFETY_FACTOR` DECIMAL(18,2) NOT NULL  AFTER `TRANSFORMER`,
	ADD COLUMN `REMAINING_FACTOR` DECIMAL(18,2) NOT NULL  AFTER `SAFETY_FACTOR` ;
	
-- added placeholders for calling event in error table
ALTER TABLE `SC_ERROR` ADD COLUMN `CALLING_EVENT` VARCHAR(50) NULL  AFTER `MESSAGE`,
	ADD COLUMN `CALLING_EVENT_ID` VARCHAR(50) NULL  AFTER `CALLING_EVENT` ;
