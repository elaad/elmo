---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Added DELETED field to topology tables
ALTER TABLE `SC_CABLE` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;
ALTER TABLE `SC_CSP` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;
ALTER TABLE `SC_DSO` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;
ALTER TABLE `SC_MEASUREMENT_PROVIDER` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;
ALTER TABLE `SC_METER` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;
ALTER TABLE `SC_TRANSFORMER` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;

-- Make SC_DSO_CSP manageable
ALTER TABLE `SC_DSO_CSP` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT 0 AFTER `DSO`;
ALTER TABLE `SC_DSO_CSP` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;
ALTER TABLE `SC_DSO_CSP` 
DROP COLUMN `ADDED_AT`;

-- Make SC_CABLE_CSP manageable
ALTER TABLE `SC_CABLE_CSP` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT 0 AFTER `NUMBER_OF_BLOCKS`;
ALTER TABLE `SC_CABLE_CSP` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;
ALTER TABLE `SC_CABLE_CSP` 
DROP COLUMN `ADDED_AT`;

-- Make SC_CSP_CONNECTION manageable
ALTER TABLE `SC_CSP_CONNECTION` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT 0 AFTER `CABLE_CSP`;
ALTER TABLE `SC_CSP_CONNECTION` 
ADD COLUMN `DELETED` TINYINT(1) NULL DEFAULT 0 AFTER `OPTLOCK`;
ALTER TABLE `SC_CSP_CONNECTION` 
DROP COLUMN `ADDED_AT`;

-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.8' WHERE `NAME`='ApplicationVersion';

-- Delete cable from meter. Model more simple to use
ALTER TABLE `SC_METER` 
DROP FOREIGN KEY `FK2CBD24BAF2B700D6`;
ALTER TABLE `SC_METER` 
DROP COLUMN `CABLE`,
DROP INDEX `FK2CBD24BAF2B700D6`;

-- Update measurment provider 4Top endpoint URL
UPDATE `scip`.`SC_MEASUREMENT_PROVIDER` SET `ENDPOINT`='http://enexis.4top.nl/sc/-/sc/index' WHERE `MP_ID`='4top';

