---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- create new entity table for measurement provider
CREATE TABLE `SC_MEASUREMENT_PROVIDER` (
  `MP_ID` varchar(16) NOT NULL,
  `ACTIVE` tinyint(1) NOT NULL,
  `ENDPOINT` varchar(255) DEFAULT NULL,
  `NAME` varchar(100) NOT NULL,
  `OPTLOCK` bigint(19) DEFAULT '0',
  PRIMARY KEY (`MP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- current provider is 4top
INSERT INTO `SC_MEASUREMENT_PROVIDER` (`MP_ID`, `ACTIVE`, `ENDPOINT`, `NAME`, `OPTLOCK`)
VALUES ('4top', 1, (SELECT `VALUE` FROM `SC_CONFIG_PARAMETER` WHERE `NAME` = 'GetMeasurementsBaseURL'), 'forTop Automation & Energy Control', 0);

-- add foreign key in transformer
ALTER TABLE `SC_TRANSFORMER` 
ADD COLUMN `MEASUREMENT_PROVIDER` VARCHAR(16) NOT NULL DEFAULT '4top' AFTER `LAST_MEASUREMENT`,
ADD FOREIGN KEY (`MEASUREMENT_PROVIDER`) REFERENCES `SC_MEASUREMENT_PROVIDER` (`MP_ID`),
ADD INDEX (`MEASUREMENT_PROVIDER` ASC);

-- remove the now obsolete parameter from config
DELETE FROM `SC_CONFIG_PARAMETER` WHERE `NAME` = 'GetMeasurementsBaseURL';

-- for audit trail: add locking and removed added_at
ALTER TABLE `SC_TRANSFORMER` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT '0' AFTER `MEASUREMENT_PROVIDER`;

ALTER TABLE `SC_TRANSFORMER` 
DROP COLUMN `ADDED_AT`;