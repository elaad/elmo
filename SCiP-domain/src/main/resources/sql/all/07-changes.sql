---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- remove comments from tables, only stored in audit trail now
ALTER TABLE `SC_CABLE` 
DROP COLUMN `COMMENTS`;
ALTER TABLE `SC_CONFIG_PARAMETER` 
DROP COLUMN `COMMENTS`;

-- Add locking to DSO table.
ALTER TABLE `SC_DSO` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT '0' AFTER `NAME`;

ALTER TABLE `SC_DSO` 
DROP COLUMN `ADDED_AT`;

-- Add locking to CSP table.
ALTER TABLE `SC_CSP` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT '0' AFTER `NAME`;

ALTER TABLE `SC_CSP` 
DROP COLUMN `ADDED_AT`;


-- rsteeghs (2014-12-4): From now on last critical capacity change for cable only registered directly on Cable
ALTER TABLE `SC_CABLE` 
CHANGE COLUMN `CHANGED_AT` `CSP_CAPACITY_CHANGED_AT` DATETIME NOT NULL ;
