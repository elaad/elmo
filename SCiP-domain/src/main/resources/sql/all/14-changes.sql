---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.11' WHERE `NAME`='ApplicationVersion';

INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('DeviationDayDeterminationTimerHours', '1', '23', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('DeviationDayDeterminationTimerMinutes', '1', '0', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('NumberOfDeviationDaysForCalculations', '1', '30', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('MaximumDeviationDayFactor', '1', '3.5', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('SuForecastMinimalOutputBlocks', '1', '196', '1');

CREATE TABLE `SC_CABLE_FORECAST_DEVIATION_DAY` (
  `CABLE` varchar(20) NOT NULL,
  `DAY` date NOT NULL,
  `DEVIATION` decimal(18,2) NOT NULL,
  `AVERAGE_DEVIATION` decimal(18,2) NOT NULL,
  `DEVIATED` tinyint(1) NOT NULL,
  PRIMARY KEY (`CABLE`,`DAY`),
  KEY `FK_DEV_DAY_CABLE` (`CABLE`),
  CONSTRAINT `FK_DEV_DAY_CABLE` FOREIGN KEY (`CABLE`) REFERENCES `SC_CABLE` (`CABLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
	

-- Add comma seperated string with roles
ALTER TABLE `SC_ADMIN_USER` 
ADD COLUMN `ROLES` VARCHAR(100) DEFAULT NULL AFTER `RECEIVE_EMAIL`;

DROP TABLE `SC_EVENT_CONFIG`;