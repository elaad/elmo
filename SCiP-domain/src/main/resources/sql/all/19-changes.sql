---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.16' WHERE `NAME`='ApplicationVersion';

-- For SCiP minimal number of input blocks needed for good forecast
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) VALUES ('ForecastMinimalInputBlocks', '1', '1344', '0');

-- Change deviation days determination JIRA SCIP-53
ALTER TABLE `SC_CABLE_FORECAST_DEVIATION_DAY` 
ADD COLUMN `DEVIATION_TYPE` VARCHAR(16) NOT NULL;

UPDATE SC_CABLE_FORECAST_DEVIATION_DAY SET DEVIATION_TYPE = 'NO_DEVIATION' WHERE DEVIATED = 0;
UPDATE SC_CABLE_FORECAST_DEVIATION_DAY SET DEVIATION_TYPE = 'DEVIATION' WHERE DEVIATED = 1;

ALTER TABLE `SC_CABLE_FORECAST_DEVIATION_DAY` DROP COLUMN `DEVIATED`;