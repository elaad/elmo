---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Version number of application
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.6' WHERE `NAME`='ApplicationVersion';

-- Add column for optimistic lock
ALTER TABLE `SC_CONFIG_PARAMETER` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT 0 AFTER `VALUE`;

-- make ApplicationVersion readonly
UPDATE `SC_CONFIG_PARAMETER` SET `CHANGEABLE`=0 WHERE `NAME`='ApplicationVersion';

-- Add Audit Trail comment to config_parameter column
ALTER TABLE `SC_CONFIG_PARAMETER` 
ADD COLUMN `COMMENTS` VARCHAR(2048) NULL AFTER `VALUE`;

-- Add Audit Trail/Log table
CREATE TABLE `SC_AUDIT_LOG_ITEM` (
  `ID` BIGINT NOT NULL,
  `TABLE_NAME` VARCHAR(45) NOT NULL,
  `RECORD_KEY` VARCHAR(45) NULL,
  `DB_ACTION` CHAR(2) NULL,
  `USER_ID` VARCHAR(10) NULL,
  `NEW_VALUES` VARCHAR(4096) NULL,
  `OLD_VALUES` VARCHAR(4096) NULL,
  `COMMENTS` VARCHAR(2048) NULL,
  `ADDED_AT` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`));
  
-- add Audit Tail/Locking to Cable
ALTER TABLE `SC_CABLE` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT 0 AFTER `BEHAVIOUR`;
ALTER TABLE `SC_CABLE` 
ADD COLUMN `COMMENTS` VARCHAR(2048) NULL AFTER `BEHAVIOUR`;

-- remove unneeded cable added_at column 
ALTER TABLE `SC_CABLE` 
DROP COLUMN `ADDED_AT`;

-- Add CORRECTED column on SC_CABLE_FORECAST_INPUT
ALTER TABLE `SC_CABLE_FORECAST_INPUT` ADD COLUMN `CORRECTED` TINYINT(1) NOT NULL; 
UPDATE `SC_CABLE_FORECAST_INPUT` SET CORRECTED = 0;