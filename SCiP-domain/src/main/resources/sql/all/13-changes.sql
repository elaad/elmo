---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.10' WHERE `NAME`='ApplicationVersion';

-- Add table for user notifications on errors
CREATE TABLE `SC_ADMIN_USER_NOTIFICATION` (
  `USER` VARCHAR(10) NOT NULL,
  `EVENT` VARCHAR(30) NOT NULL,
  `NOTIFY_INTERVAL` VARCHAR(6) NOT NULL,
  `ENABLED` TINYINT(1) NOT NULL,
  PRIMARY KEY (`USER`, `EVENT`));

-- Add parameter for csp usage deviations export
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('ExportCspUsageDeviationsTemplate', '1', '/mnt/data/templates/exportCspUsageDeviationsTemplate.xls', '1');

-- Adding support for different versions of OSCP protocol per CSP and initialize it to version '0.9'
ALTER TABLE `SC_CSP` ADD COLUMN `PROTOCOL_VERSION` VARCHAR(5) NULL AFTER `DELETED`;
UPDATE `SC_CSP` SET `PROTOCOL_VERSION` = '0.9';

-- Add parameter for csp heartbeat message timer
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('CspHeartbeatTimerHours', '1', '*', '1');
INSERT INTO `SC_CONFIG_PARAMETER` (`NAME`, `CHANGEABLE`, `VALUE`, `OPTLOCK`) 
	VALUES ('CspHeartbeatTimerMinutes', '1', '0/5', '1');

-- Services OSCP use different UoM for Ampere. Now config param is pipe separated string
UPDATE `SC_CONFIG_PARAMETER` SET `NAME`='CapacityUnitOfMeasures', `VALUE`='Amp|A' WHERE `NAME`='CapacityUnitOfMeasure';

