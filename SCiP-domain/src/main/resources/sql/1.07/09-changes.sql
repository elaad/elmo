---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- extend password column to hold encrypted password
ALTER TABLE `SC_ADMIN_USER` 
CHANGE COLUMN `PASSWORD` `PASSWORD` VARCHAR(100) NOT NULL ;

-- for audit trail: add locking and removed added_at
ALTER TABLE `SC_METER` 
ADD COLUMN `OPTLOCK` BIGINT(19) NULL DEFAULT '0' AFTER `TRANSFORMER`;

ALTER TABLE `SC_METER` 
DROP COLUMN `ADDED_AT`;