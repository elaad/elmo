---
-- #%L
-- SCiP-domain
-- %%
-- Copyright (C) 2015 - 2016 ENEXIS
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
-- rsteeg20: Added tables for action context logging.
create table SC_ACTION (
    ID varchar(50) not null,
    ACTOR varchar(20) not null,
    END datetime,
    START datetime not null,
    SUCCESS bit not null,
    EVENT varchar(30) not null,
    PARENT varchar(50),
    primary key (ID)
);

create table SC_ACTION_MESSAGE (
    NUMBER integer not null,
    CAUSE longtext,
    CAUSE_MESSAGE longtext,
    DATETIME datetime not null,
    EXCEPTION_TYPE varchar(30),
    LEVEL varchar(5) not null,
    MESSAGE longtext not null,
    ROOT_CAUSE longtext,
    ROOT_CAUSE_MESSAGE longtext,
    ACTION varchar(50) not null,
    primary key (ACTION, NUMBER)
);

create table SC_ACTION_MESSAGE_TARGET (
    NUMBER integer not null,
    TYPE longtext not null,
    VALUE longtext not null,
    ACTION varchar(50) not null,
    MSG_NUMBER integer not null,
    primary key (ACTION, MSG_NUMBER, NUMBER)
);

create table SC_ACTION_TARGET (
    NUMBER integer not null,
    DIRECTION varchar(3) not null,
    TYPE longtext not null,
    VALUE longtext not null,
    ACTION varchar(50) not null,
    primary key (ACTION, NUMBER)
);

alter table SC_ACTION 
    add index FK56512B05A02449E (PARENT), 
    add constraint FK56512B05A02449E 
    foreign key (PARENT) 
    references SC_ACTION (ID);

alter table SC_ACTION_MESSAGE 
    add index FKC99288ADF086BA2A (ACTION), 
    add constraint FKC99288ADF086BA2A 
    foreign key (ACTION) 
    references SC_ACTION (ID);

alter table SC_ACTION_MESSAGE_TARGET 
    add index FKB5B12D6357772D70 (ACTION, MSG_NUMBER), 
    add constraint FKB5B12D6357772D70 
    foreign key (ACTION, MSG_NUMBER) 
    references SC_ACTION_MESSAGE (ACTION, NUMBER);

alter table SC_ACTION_TARGET 
    add index FK2AFFB80BF086BA2A (ACTION), 
    add constraint FK2AFFB80BF086BA2A 
    foreign key (ACTION) 
    references SC_ACTION (ID);

-- Update version
UPDATE `SC_CONFIG_PARAMETER` SET `VALUE`='1.7' WHERE `NAME`='ApplicationVersion';