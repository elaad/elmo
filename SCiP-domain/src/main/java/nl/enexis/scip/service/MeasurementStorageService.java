package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;
import java.util.Map;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Measurement;

public interface MeasurementStorageService {

	public void storeMeasurements(Map<Long, Measurement> measurements) throws ActionException;

	public void mergeMeasurements(Map<Long, Measurement> measurements) throws ActionException;

	public Measurement getMeasurementForCableAndDate(Date date, Cable cable) throws ActionException;

	public List<Measurement> getMeasurementForCableBetweenDates(Date startDate, Date endDate, Cable cable)
			throws ActionException;

	public List<Measurement> getMeasurementForProviderIdBetweenDates(Date startDate, Date endDate, String providerId)
			throws ActionException;

	public List<Measurement> getMeasurementBetweenDates(Date startDate, Date endDate) throws ActionException;

	public Measurement getClosestCompleteMeasurement(Measurement measurement, boolean doForward) throws ActionException;

}
