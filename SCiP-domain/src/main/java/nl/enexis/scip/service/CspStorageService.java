package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CspAdjustment;
import nl.enexis.scip.model.CspAdjustmentMessage;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspUsageMessage;
import nl.enexis.scip.model.CspUsageValue;

public interface CspStorageService {

	public List<CspUsageValue> getCspUsageValuesForCableForecastBlock(Cable cable, int dayOfWeek, int hour, int block,
			Date fromDate) throws ActionException;

	public List<CspUsageValue> getCspUsageValuesByDate(Date fromDate, Date toDate) throws ActionException;

	public void storeCspUsages(CspUsageMessage cspUsageMessage) throws ActionException;

	public void storeCspUsageMessage(CspUsageMessage cspUsageMessage) throws ActionException;

	public void setCspUsageMessageSuccessful(Long id) throws ActionException;

	public CspUsageMessage findCspUsageMessageByEventId(String eventId) throws ActionException;

	public CspAdjustmentMessage findCspAdjustmentMessageByEventId(String eventId) throws ActionException;

	public CspAdjustmentMessage storeCspAdjustmentMessage(CspAdjustmentMessage cspAdjustmentMessage)
			throws ActionException;

	public void setCspAdjustmentMessageSuccessful(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException;

	public void storeCspAdjustments(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException;

	public CspAdjustmentValue getLastCspAdjustmentsValueForBlock(CableCsp cableCsp, Date day, int hour, int block)
			throws ActionException;

	List<CspAdjustmentMessage> getCspAdjustmentMessageByCableIdBetweenDates(String cableId, Date fromDate, Date toDate)
			throws ActionException;

	List<CspAdjustment> getCspAdjustmentByCspAdjustmentMessageId(long msgId) throws ActionException;

	List<CspAdjustmentValue> getCspAdjustmentValueByCspAdjustmentId(long adId) throws ActionException;

	public void updateCspUsageValue(CspUsageValue cspUsageValue) throws ActionException;

}
