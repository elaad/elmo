package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Set;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.CspGetForecastMessage;

public interface CspService {

	public void sendForecastRequestedByCsp(CspGetForecastMessage cspGetForecastMessage, Set<String> forCables)
			throws ActionException;

	public void doCspForecastAsync(String cableForecastId);

	public void doCspForecast(String cableForecastId) throws ActionException;

	public void sendForecastForCspAsync(Long cspForecastId, int priority);

	public void sendForecastForCsp(Long cspForecastId, int priority) throws ActionException;

	public void sendForecastForCable(String cableId, int priority) throws ActionException;

}
