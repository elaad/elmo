package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspConnection;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.DsoCsp;
import nl.enexis.scip.model.MeasurementProvider;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.model.Transformer;

public interface TopologyStorageService {

	public Csp findCspByEan13(String cspEan) throws ActionException;

	public Dso findDsoByEan13(String dsoEan) throws ActionException;

	public Cable findCableByCableId(String cableId) throws ActionException;

	public List<Cable> getCablesByCspEan13(String ean13) throws ActionException;

	List<Csp> getCspsByCableId(String cableId) throws ActionException;

	public CableCsp findCableCspByCableAndCsp(Csp csp, Cable cable) throws ActionException;

	public List<Cable> getAllCables() throws ActionException; // does not show
																// deleted
																// cables

	public List<Cable> getAllCables(boolean showDeleted) throws ActionException;

	public List<Cable> getAllActiveCables(boolean isActive) throws ActionException;

	public List<Dso> getAllDsos() throws ActionException; // does not show
															// deleted
															// DSOs

	public List<Dso> getAllDsos(boolean showDeleted) throws ActionException;

	public List<Csp> getAllCsps() throws ActionException; // does not show
															// deleted
															// CSPs

	public List<Csp> getAllCsps(boolean showDeleted) throws ActionException;

	public List<DsoCsp> getAllDsoCsps() throws ActionException; // does not show
																// deleted
																// DSOCSPs

	public List<DsoCsp> getAllDsoCsps(boolean showDeleted) throws ActionException;

	public List<CableCsp> getAllCableCsps() throws ActionException; // does not
																	// show
																	// deleted
																	// cableCSPs

	public List<CableCsp> getAllCableCsps(boolean showDeleted) throws ActionException;

	public List<CspConnection> getAllCspConnections() throws ActionException; // does
																				// not
																				// show
																				// deleted
																				// CSPConnections

	public List<CspConnection> getAllCspConnections(boolean showDeleted) throws ActionException;

	public List<MeasurementProvider> getAllMPs() throws ActionException; // does
																			// not
																			// show
																			// deleted
																			// Measurement
																			// providers

	public List<MeasurementProvider> getAllMPs(boolean showDeleted) throws ActionException;

	public Cable getCompleteCableTopology(String cableId) throws ActionException;

	public Csp getCompleteCspTopology(String cspEan) throws ActionException;

	public List<Transformer> getAllTransformers() throws ActionException;

	public List<Transformer> getAllTransformers(boolean showDeleted) throws ActionException;

	public Meter findMeterByMeterId(String meterId) throws ActionException;

	public Dso createDso(Dso dso) throws ActionException;

	public Transformer findDsoByTrafoId(String trafoId) throws ActionException;

	public Transformer createTransformer(Transformer transformer) throws ActionException;

	public Cable createCable(Cable cable) throws ActionException;

	public Meter createMeter(Meter meter) throws ActionException;

	public Csp createCsp(Csp csp) throws ActionException;

	public CableCsp createCableCsp(CableCsp cableCsp) throws ActionException;

	public CspConnection findCspConnectionByEan18(String ean18) throws ActionException;

	public CspConnection createCspConnection(CspConnection cspConnection) throws ActionException;

	public Dso updateDso(Dso dso) throws ActionException;

	public MeasurementProvider updateMP(MeasurementProvider mp) throws ActionException;

	public Cable updateCable(Cable cable) throws ActionException;

	public Meter updateMeter(Meter meter) throws ActionException;

	public List<Meter> getAllMeters() throws ActionException; // does not show
																// deleted
																// meters

	public List<Meter> getAllMeters(boolean showDeleted) throws ActionException;

	public Csp updateCsp(Csp csp) throws ActionException;

	public CspConnection updateCspConnection(CspConnection cspConnection) throws ActionException;

	public CableCsp updateCableCsp(CableCsp cableCsp) throws ActionException;

	public DsoCsp findDsoCspByDsoAndCsp(Dso dso, Csp csp) throws ActionException;

	public DsoCsp updateDsoCsp(DsoCsp dsoCsp) throws ActionException;

	public DsoCsp createDsoCsp(DsoCsp dsoCsp) throws ActionException;

	public Transformer updateTransformer(Transformer transformer) throws ActionException;

	public Transformer findTransformerByTrafoId(String trafoId) throws ActionException;

	public MeasurementProvider findMeasurementProviderById(String mpId) throws ActionException;

	public void updateTransformerLastMeasurement(String trafoId, Date lastMeasurement) throws ActionException;

	public MeasurementProvider createMeasurementProvider(MeasurementProvider mp) throws ActionException;

	public CableCsp findCableCspById(long id) throws ActionException;

	public void deleteCable(Cable cable) throws ActionException;

	public void deleteCsp(Csp csp) throws ActionException;

	public void deleteDso(Dso dso) throws ActionException;

	public void deleteMeasurementProvider(MeasurementProvider measurementProvider) throws ActionException;

	public void deleteMeter(Meter meter) throws ActionException;

	public void deleteTransformer(Transformer transformer) throws ActionException;

	public void deleteDsoCsp(DsoCsp dsoCsp) throws ActionException;

	public void deleteCableCsp(CableCsp cableCsp) throws ActionException;

	public void deleteCspConnection(CspConnection cspConnection) throws ActionException;

	public List<CableCsp> getAllCompleteActiveCableCsps() throws ActionException;;
}
