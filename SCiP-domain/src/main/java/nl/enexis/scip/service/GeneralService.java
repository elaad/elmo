package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.ConfigurationParameter;

public interface GeneralService {

	public ConfigurationParameter getConfigurationParameter(String name) throws ActionException;

	public Integer getConfigurationParameterAsInteger(String name) throws ActionException;

	public String getConfigurationParameterAsString(String name) throws ActionException;

	public Boolean getConfigurationParameterAsBoolean(String name) throws ActionException;

	public Double getConfigurationParameterAsDouble(String string) throws ActionException;

}
