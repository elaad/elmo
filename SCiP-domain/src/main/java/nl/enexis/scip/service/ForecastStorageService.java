package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastDeviationDay;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspGetForecastMessage;
import nl.enexis.scip.model.CspUsageValue;

public interface ForecastStorageService {

	public CableForecastInput getLastCableForecastInput(Cable cable, Date fromDate) throws ActionException;

	public void storeCableForecast(CableForecast cableForecast) throws ActionException;

	public CableForecast findCableForecastById(String cableForecastId) throws ActionException;

	public void storeCspForecasts(List<CspForecast> cspForecasts) throws ActionException;

	public void storeCspForecastMessage(CspForecastMessage cspForecastMessage) throws ActionException;

	public List<CspForecast> getCspForcastForCableCsp(CableCsp cableCsp) throws ActionException;

	public CspForecast getLastCspForcastForCableCsp(CableCsp cableCsp) throws ActionException;

	public CspForecast getCspForcast(Long id) throws ActionException;

	public List<CspForecast> getCspForcastByCspForecastMessageId(Long cspForecastMessageId) throws ActionException;

	public List<CspForecast> getCspForcastByDate(Date dateFrom, Date dateTo, boolean getCableCsp)
			throws ActionException;

	public List<CspForecastValue> getCspForecastValuesByCspForecastId(Long cspForecastId, boolean fetchCableForecast, boolean fetchCspAdjustmentValue)
			throws ActionException;

	public List<CspForecastMessage> getMessagesForCspForecast(CspForecast cspForecast) throws ActionException;

	public List<CspForecastMessage> getCspForecastMessagesByDateCspAndCable(String csp, Date dateFrom, Date dateTo,
			Cable cable) throws ActionException;

	public void storeCspGetForecastMessage(CspGetForecastMessage cspGetForecastMessage) throws ActionException;

	public void updateCspForecastMessageWithCspForecasts(CspForecastMessage cspForecastMessage) throws ActionException;

	public void updateCableForecast(CableForecast cableForecast) throws ActionException;

	// public void addInputsForCableForecast(CableForecast cableForecast) throws
	// ActionException;

	public void addOutputsForCableForecast(CableForecast cableForecast) throws ActionException;

	public CspGetForecastMessage findCspGetForecastMessageByEventId(String eventId) throws ActionException;

	public CableForecastOutput getLastCableForecastOutput(CableForecast cableForecast, Date day, int hour, int block)
			throws ActionException;

	public CspForecastValue getLastCspForecastValueForCableForecastOutput(CableForecastOutput cableForecastOutput,
			Csp csp) throws ActionException;

	public CableForecast getLastCableForecast(Cable cable) throws ActionException;

	public void setCspGetForecastMessageSuccessful(Long id) throws ActionException;

	public void setCspForecastMessageSuccessful(Long id) throws ActionException;

	public List<CableForecast> searchCableForecasts(Cable cable, Date fromDate, Date toDate) throws ActionException;

	public CableForecastInput findCableForecastInputForBlock(Cable cable, Date blockDay, Integer blockHour,
			Integer blockNumber) throws ActionException;

	public void createCableForecastInput(CableForecastInput input) throws ActionException;

	public List<CableForecastInput> getForForecastInput(Cable cable, int totalNumber) throws ActionException;

	public CableForecast getFirstCableForecastOfDay(Cable cable, Date date) throws ActionException;

	public List<CableForecastInput> getCableForecastInputs(CableForecast cf) throws ActionException;

	public CableForecastDeviationDay getLastCableForecastDeviationDay(Cable cable) throws ActionException;

	public List<CableForecastDeviationDay> getNotDeviatedCableForecastDeviationDays(Cable cable, int numberOfDays)
			throws ActionException;

	public void storeCableForecastDeviationDay(CableForecastDeviationDay cfdd) throws ActionException;

	public boolean isCableForecastDeviationDay(Cable cable, Date day) throws ActionException;

	public List<CspForecast> getLastCspForecastsForCable(Cable cable) throws ActionException;

	public void updateCspForecastValue(CspForecastValue cfv) throws ActionException;

	public CspForecastValue getLastCspForecastValueForCspUsageValue(CspUsageValue cspUsageValue) throws ActionException;;
}
