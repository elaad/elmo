package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.DsoCsp;
import nl.enexis.scip.model.LocationMeasurement;
import nl.enexis.scip.model.LocationMeasurementValue;

public interface CspMessagingService {

	public void pushCspForecast(CspForecastMessage cspForecastMessage, Csp csp) throws ActionException;

	public void pushWeatherUpdate(int blockMinutes, LocationMeasurement locationMeasurement,
			List<LocationMeasurementValue> locationMeasurementValues, Csp csp, Dso dso) throws ActionException;

	public void heartbeat(DsoCsp dsoCsp) throws ActionException;

}
