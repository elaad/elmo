package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.InputStream;
import java.util.List;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.CspLocationMeasurement;
import nl.enexis.scip.model.LocationMeasurement;

public interface LocationMeasurementsService {

	public List<LocationMeasurement> getAllLocationMeasurements();

	public List<CspLocationMeasurement> getAllCspLocationMeasurements();

	public void fetchMeasurementValues(LocationMeasurement locationMeasurement) throws ActionException;

	public void processLocationMeasurementFile(LocationMeasurement locationMeasurement, InputStream is)
			throws ActionException;

	public void sendMeasurementValuesToCsp(CspLocationMeasurement clm) throws ActionException;
}
