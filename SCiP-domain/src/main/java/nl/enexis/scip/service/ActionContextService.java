package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessage.Level;

public interface ActionContextService {

	public void saveOrUpdateActionContext();

	public List<String> getLoggedActionTargets();

	public List<ActionMessage> searchActionMessages(String actionId, Date fromDate, Date toDate, String targetType,
			String targetId, List<ActionEvent> selectedEvents, List<Level> selectedLevels,
			List<ActionExceptionType> selectedExceptionTypes, String actor);

	public List<String> getLoggedActors();

	public List<Action> getNotificationActions(ActionEvent event, Date fromDate, Date toDate, List<Level> mailLevels);

}
