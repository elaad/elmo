package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.AdminUser;
import nl.enexis.scip.model.AdminUserNotification;
import nl.enexis.scip.model.AuditLogItem;
import nl.enexis.scip.model.ConfigurationParameter;
import nl.enexis.scip.model.enumeration.NotificationInterval;

public interface GenericStorageService {

	public ConfigurationParameter findConfigurationParameter(String name) throws ActionException;

	public List<ConfigurationParameter> getAllConfigurationParameters() throws ActionException;

	public ConfigurationParameter updateConfigurationParameter(ConfigurationParameter config) throws ActionException;

	public AdminUser findAdminUserByUserId(String userId) throws ActionException;

	public List<AdminUser> getAllAdminUsers() throws ActionException;

	public AdminUser updateAdminUser(AdminUser user) throws ActionException;

	public AdminUserNotification createAdminUserNotification(AdminUserNotification notification) throws ActionException;

	public AdminUserNotification updateAdminUserNotification(AdminUserNotification editedNotification)
			throws ActionException;

	public void deleteAdminUserNotification(AdminUserNotification editedNotification) throws ActionException;

	public List<AdminUserNotification> findActiveDirectAdminUserNotifications(ActionEvent type) throws ActionException;

	public void deleteAdminUser(AdminUser editedUser) throws ActionException;

	public List<AdminUserNotification> findActiveIntervalAdminUserNotifications(NotificationInterval interval)
			throws ActionException;

	List<AuditLogItem> getAllAuditLogItems() throws ActionException;

	List<AuditLogItem> getAuditLogItemsBetweenDates(Date startDate, Date endDate) throws ActionException;

	List<String> getDistinctAuditLogEntities() throws ActionException;

}
