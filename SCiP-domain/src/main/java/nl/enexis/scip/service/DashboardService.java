package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.Map;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.DsoCsp;

public interface DashboardService {

	public Map<String, Date[]> getLastSuForecastInputDates();

	public Map<Cable, Date> getLastScForecastInputDates();

	public Action getLastHeartbeatEvent(DsoCsp dsoCsp);

	public Map<ActionEvent, Long> getActionEventErrorCounts(Integer lastNumberOfDays);

	public Map<Cable, Long> getIncompleteMeasurmentCounts(Integer lastNumberOfDays);

	public Map<CableCsp, Long> getCspUsageDeviationCounts(Integer lastNumberOfDays, Long deviationMargin);

	public Map<String, Date> getLastSuccessSendLocationMeasurmentForCsps();

	public Map<CableCsp, Date> getLastSuccessForecastMessageForCableCsps();

	public Map<String, Long> getConnectivityErrorCounts(Integer lastNumberOfDays);

	public Map<String, Double> getActionEventTurnaroundTimes(Integer lastNumberOfDays);

	public Map<String, Double> getTimerEventTurnaroundTimes(Integer lastNumberOfDays);

	public Date getLastDbCleanupDate();

}
