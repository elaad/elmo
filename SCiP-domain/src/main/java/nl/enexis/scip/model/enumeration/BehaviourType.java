package nl.enexis.scip.model.enumeration;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

public enum BehaviourType {
	SC("Smart Charging"), SU("Smart Usage");

	private final String name;

	private BehaviourType(String s) {
		name = s;
	}

	public boolean equalsName(String otherName) {
		return otherName == null ? false : name.equals(otherName);
	}

	@Override
	public String toString() {
		return name;
	}

	public static boolean contains(String test) {

		for (BehaviourType mn : BehaviourType.values()) {
			if (mn.name().equals(test)) {
				return true;
			}
		}

		return false;
	}

	public static List<SelectItem> toSelectItems() {
		BehaviourType[] behaviours = values();
		List<SelectItem> behaviourOptions = new ArrayList<SelectItem>();

		for (BehaviourType behaviour : behaviours) {
			SelectItem newItem = new SelectItem(behaviour, behaviour.toString());
			behaviourOptions.add(newItem);
		}

		return behaviourOptions;
	}

}
