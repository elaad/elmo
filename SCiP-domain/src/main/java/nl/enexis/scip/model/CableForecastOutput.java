package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SC_CABLE_FORECAST_OUTPUT")
//@NamedQueries({ @NamedQuery(name = "CableForecastOutput.getLastCableForecastOutput", query = "SELECT cfo FROM CableForecastOutput cfo WHERE cfo.cableForecast.cable = :cable AND cfo.day = :day AND cfo.hour = :hour AND cfo.block = :block ORDER BY cfo.id DESC") })
@NamedQueries({ @NamedQuery(name = "CableForecastOutput.getLastCableForecastOutput", query = "SELECT cfo FROM CableForecastOutput cfo WHERE cfo.cableForecast = :cableForecast AND cfo.day = :day AND cfo.hour = :hour AND cfo.block = :block ORDER BY cfo.id DESC") })
public class CableForecastOutput implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7015255740339123689L;
	
	private Long id;
	private CableForecast cableForecast;
	private Date day;
	private int dayOfWeek;
	private int hour;
	private int block;
	private BigDecimal usageExclCars1 = new BigDecimal("0.00");
	private BigDecimal usageExclCars2 = new BigDecimal("0.00");
	private BigDecimal usageExclCars3 = new BigDecimal("0.00");
	private BigDecimal availableCapacity = new BigDecimal("0.00");

	public CableForecastOutput() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
		public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABLE_FORECAST", nullable = false)
	public CableForecast getCableForecast() {
		return this.cableForecast;
	}

	public void setCableForecast(CableForecast cableForecast) {
		this.cableForecast = cableForecast;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "DAY", nullable = false, length = 7)
	public Date getDay() {
		return this.day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	@Column(name = "DAY_OF_WEEK", nullable = false, precision = 2, scale = 0)
	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	@Column(name = "HOUR", nullable = false, precision = 2, scale = 0)
	public int getHour() {
		return this.hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	@Column(name = "BLOCK", nullable = false, precision = 2, scale = 0)
	public int getBlock() {
		return this.block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	@Column(name = "AVAILABLE_CAPACITY", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAvailableCapacity() {
		return this.availableCapacity;
	}

	public void setAvailableCapacity(BigDecimal availableCapacity) {
		this.availableCapacity = availableCapacity;
	}

	@Column(name = "USAGE_EX_CARS_1", nullable = false, precision = 22, scale = 2)
	public BigDecimal getUsageExclCars1() {
		return this.usageExclCars1;
	}

	public void setUsageExclCars1(BigDecimal usageExclCars1) {
		this.usageExclCars1 = usageExclCars1;
	}

	@Column(name = "USAGE_EX_CARS_2", nullable = false, precision = 22, scale = 2)
	public BigDecimal getUsageExclCars2() {
		return this.usageExclCars2;
	}

	public void setUsageExclCars2(BigDecimal usageExclCars2) {
		this.usageExclCars2 = usageExclCars2;
	}

	@Column(name = "USAGE_EX_CARS_3", nullable = false, precision = 22, scale = 2)
	public BigDecimal getUsageExclCars3() {
		return this.usageExclCars3;
	}

	public void setUsageExclCars3(BigDecimal usageExclCars3) {
		this.usageExclCars3 = usageExclCars3;
	}

}
