package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlTransient;

import nl.enexis.scip.action.ActionTarget;
import nl.enexis.scip.util.xml.JaxbUtil;

@MappedSuperclass
public abstract class AuditTrailEntity implements ActionTarget {
	
	protected long optlock;
	protected String comments;    // Transient, not stored in database
	protected String storedState; // Transient, not stored in database
    protected String userId;      // Transient, not stored in database
	
	@PostLoad
	private void storeState(){
		this.setStoredState(JaxbUtil.toXmlString(this));
    }
	
	@XmlTransient // skipped by JAXB so not stored in de AuditTrail
    @Transient
	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@XmlTransient
	@Version
	@Column(name = "OPTLOCK", nullable = true)
	public long getOptlock() {
		return optlock;
	}

	public void setOptlock(long optlock) {
		this.optlock = optlock;
	}
	
    @XmlTransient
	@Transient
	public String getStoredState() {
		return this.storedState;
	}
	
	public void setStoredState(String storedState) {
		this.storedState = storedState;
	}	
	
    @XmlTransient
    @Transient
	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    // Abstract function for retrieving Record identifying key. 
	@Transient
    abstract public String getRecordKey();
	
	@Override
	@XmlTransient
	@Transient
	public String getActionTargetName() {
		return this.getClass().getName();
	}

}
