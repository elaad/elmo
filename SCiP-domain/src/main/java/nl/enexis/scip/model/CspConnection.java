package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "SC_CSP_CONNECTION", uniqueConstraints = @UniqueConstraint(columnNames = "EAN18"))
public class CspConnection extends ActiveTopologyEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5735113294098582086L;

	private CableCsp cableCsp;
	private String ean18;
	private BigDecimal capacity;

	public CspConnection() {
	}

    @XmlTransient
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABLE_CSP", nullable = false)
	public CableCsp getCableCsp() {
		return this.cableCsp;
	}

	public void setCableCsp(CableCsp cableCsp) {
		this.cableCsp = cableCsp;
	}

	@Id
	@XmlID
	@XmlAttribute 
    @NotEmpty(message="EAN18 is required")
    @Size(min = 18, max = 18, message="EAN18 has to be {max} digits")
	@Column(name = "EAN18", unique = true, nullable = false, length = 18)
	public String getEan18() {
		return this.ean18;
	}

	public void setEan18(String ean18) {
		this.ean18 = ean18;
	}

	@NotNull(message = "Capacity is required")
	@Range(min = 0, message = "Capacity cannot be less then {min}")
	@Column(name = "CAPACITY", unique = false, nullable = false, precision = 18, scale = 2)
	public BigDecimal getCapacity() {
		return capacity;
	}

	public void setCapacity(BigDecimal capacity) {
		this.capacity = capacity;
	}

    @Transient
    @Override
    public String getRecordKey() {
        return this.ean18;
    }
    
//	@Override
//	public boolean equals(Object object) {
//		if (object instanceof CspConnection) {
//			if (((CspConnection) object).getEan18().equals(this.getEan18())) {
//				return true;
//			} else {
//				return false;
//			}
//		} else {
//			return false;
//		}
//	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.getEan18();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.getEan18() == null) ? 0 : this.getEan18().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CspConnection)) {
			return false;
		}
		CspConnection other = (CspConnection) obj;
		if (this.getEan18() == null) {
			if (other.getEan18() != null) {
				return false;
			}
		} else if (!this.getEan18().equals(other.getEan18())) {
			return false;
		}
		return true;
	}

}
