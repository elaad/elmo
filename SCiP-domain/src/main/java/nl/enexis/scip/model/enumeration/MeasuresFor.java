package nl.enexis.scip.model.enumeration;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

public enum MeasuresFor {
	TOTALS, CARS, OTHERS;

	public static List<SelectItem> toSelectItems() {
		MeasuresFor[] mf = MeasuresFor.values();
		List<SelectItem> measuresForOptions = new ArrayList<SelectItem>();

		for (MeasuresFor element : mf) {
			SelectItem newItem = new SelectItem(element, element.toString());
			measuresForOptions.add(newItem);
		}

		return measuresForOptions;
	}

}
