package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "SC_CABLE_FORECAST_INPUT", uniqueConstraints = @UniqueConstraint(columnNames = { "CABLE", "BLOCK_START" }))
@NamedQueries({
		@NamedQuery(name = "CableForecastInput.getAll", query = "SELECT cfi FROM CableForecastInput cfi"),
		@NamedQuery(name = "CableForecastInput.getLastInputForCables", query = "SELECT cfi.cable, max(cfi.blockStart) FROM CableForecastInput cfi WHERE cfi.cable.active = true GROUP BY cfi.cable.cableId"),
		@NamedQuery(name = "CableForecastInput.findLastCableForecastInputAfterDate", query = "SELECT cfi FROM CableForecastInput cfi WHERE cfi.cable = :cable AND cfi.blockStart >= :fromDate ORDER BY cfi.blockStart DESC"),
		@NamedQuery(name = "CableForecastInput.findCableForecastInputBetweenDates", query = "SELECT cfi FROM CableForecastInput cfi WHERE cfi.cable = :cable AND cfi.blockStart BETWEEN :fromDate AND :toDate ORDER BY cfi.blockStart ASC"),
		@NamedQuery(name = "CableForecastInput.getForForecastInput", query = "SELECT cfi FROM CableForecastInput cfi WHERE cfi.cable = :cable ORDER BY cfi.blockStart DESC"),
		@NamedQuery(name = "CableForecastInput.findCableForecastInputForBlock", query = "SELECT cfi FROM CableForecastInput cfi WHERE cfi.cable = :cable AND cfi.day = :day AND cfi.hour = :hour AND cfi.block = :block") })
public class CableForecastInput implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4480105748228181460L;

	private Long id;
	private Cable cable;
	private Date day;
	private int hour;
	private int block;
	private Date blockStart;
	private Date blockEnd;
	private BigDecimal ampNow1;
	private BigDecimal ampNow2;
	private BigDecimal ampNow3;
	private BigDecimal ampMinusOneDay1;
	private BigDecimal ampMinusOneDay2;
	private BigDecimal ampMinusOneDay3;
	private BigDecimal ampMinusSixDay1;
	private BigDecimal ampMinusSixDay2;
	private BigDecimal ampMinusSixDay3;
	private BigDecimal ampMinusOneWeek1;
	private BigDecimal ampMinusOneWeek2;
	private BigDecimal ampMinusOneWeek3;
	private boolean corrected = false;

	public CableForecastInput() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, length = 50)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABLE")
	public Cable getCable() {
		return cable;
	}

	public void setCable(Cable cable) {
		this.cable = cable;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BLOCK_START", nullable = false)
	public Date getBlockStart() {
		return blockStart;
	}

	public void setBlockStart(Date blockStart) {
		this.blockStart = blockStart;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BLOCK_END", nullable = false)
	public Date getBlockEnd() {
		return blockEnd;
	}

	public void setBlockEnd(Date blockEnd) {
		this.blockEnd = blockEnd;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "DAY", nullable = false, length = 7)
	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	@Column(name = "HOUR", nullable = false, precision = 2, scale = 0)
	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	@Column(name = "BLOCK", nullable = false, precision = 2, scale = 0)
	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	@Column(name = "CORRECTED", nullable = false)
	public boolean isCorrected() {
		return corrected;
	}

	public void setCorrected(boolean corrected) {
		this.corrected = corrected;
	}

	@Column(name = "AMP_Y_1", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpNow1() {
		return ampNow1;
	}

	public void setAmpNow1(BigDecimal ampNow1) {
		this.ampNow1 = ampNow1;
	}

	@Column(name = "AMP_Y_2", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpNow2() {
		return ampNow2;
	}

	public void setAmpNow2(BigDecimal ampY2) {
		this.ampNow2 = ampY2;
	}

	@Column(name = "AMP_Y_3", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpNow3() {
		return ampNow3;
	}

	public void setAmpNow3(BigDecimal ampY3) {
		this.ampNow3 = ampY3;
	}

	@Column(name = "AMP_X1_1", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusOneDay1() {
		return ampMinusOneDay1;
	}

	public void setAmpMinusOneDay1(BigDecimal ampMinusOneDayX1) {
		this.ampMinusOneDay1 = ampMinusOneDayX1;
	}

	@Column(name = "AMP_X1_2", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusOneDay2() {
		return ampMinusOneDay2;
	}

	public void setAmpMinusOneDay2(BigDecimal ampMinusOneDayX2) {
		this.ampMinusOneDay2 = ampMinusOneDayX2;
	}

	@Column(name = "AMP_X1_3", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusOneDay3() {
		return ampMinusOneDay3;
	}

	public void setAmpMinusOneDay3(BigDecimal ampMinusOneDayX3) {
		this.ampMinusOneDay3 = ampMinusOneDayX3;
	}

	@Column(name = "AMP_X7_1", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusOneWeek1() {
		return ampMinusOneWeek1;
	}

	public void setAmpMinusOneWeek1(BigDecimal ampMinusOneWeekX1) {
		this.ampMinusOneWeek1 = ampMinusOneWeekX1;
	}

	@Column(name = "AMP_X7_2", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusOneWeek2() {
		return ampMinusOneWeek2;
	}

	public void setAmpMinusOneWeek2(BigDecimal ampMinusOneWeekX2) {
		this.ampMinusOneWeek2 = ampMinusOneWeekX2;
	}

	@Column(name = "AMP_X7_3", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusOneWeek3() {
		return ampMinusOneWeek3;
	}

	public void setAmpMinusOneWeek3(BigDecimal ampMinusOneWeekX3) {
		this.ampMinusOneWeek3 = ampMinusOneWeekX3;
	}

	@Column(name = "AMP_X6_1", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusSixDay1() {
		return ampMinusSixDay1;
	}

	public void setAmpMinusSixDay1(BigDecimal ampMinusSixDayX1) {
		this.ampMinusSixDay1 = ampMinusSixDayX1;
	}

	@Column(name = "AMP_X6_2", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusSixDay2() {
		return ampMinusSixDay2;
	}

	public void setAmpMinusSixDay2(BigDecimal ampMinusSixDayX2) {
		this.ampMinusSixDay2 = ampMinusSixDayX2;
	}

	@Column(name = "AMP_X6_3", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAmpMinusSixDay3() {
		return ampMinusSixDay3;
	}

	public void setAmpMinusSixDay3(BigDecimal ampMinusSixDayX3) {
		this.ampMinusSixDay3 = ampMinusSixDayX3;
	}

}
