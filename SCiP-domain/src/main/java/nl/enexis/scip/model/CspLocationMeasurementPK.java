package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class CspLocationMeasurementPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5353778268087612998L;

	private long locationMeasurementId;
	private String cspEan;

	public CspLocationMeasurementPK() {
	}

	@Column(name = "LOCATION_MEASUREMENT", nullable = false)
	public long getLocationMeasurementId() {
		return this.locationMeasurementId;
	}

	public void setLocationMeasurementId(long locationMeasurementId) {
		this.locationMeasurementId = locationMeasurementId;
	}

	@Column(name = "CSP", unique = false, nullable = false, length = 13)
	public String getCspEan() {
		return cspEan;
	}

	public void setCspEan(String cspEan) {
		this.cspEan = cspEan;
	}

	@Override
	public int hashCode() {
		String val = "" + locationMeasurementId + cspEan;
		return val.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof CspLocationMeasurementPK)) {
			return false;
		}
		CspLocationMeasurementPK pk = (CspLocationMeasurementPK) obj;
		return pk.locationMeasurementId == this.locationMeasurementId
				&& pk.cspEan.equals(this.cspEan);
	}
}