package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class LocationMeasurementValuePK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2835228001636131819L;

	private long locationMeasurementId;
	private Date datetime;

	public LocationMeasurementValuePK() {
	}

	@Column(name = "LOCATION_MEASUREMENT", nullable = false)
	public long getLocationMeasurementId() {
		return this.locationMeasurementId;
	}

	public void setLocationMeasurementId(long locationMeasurementId) {
		this.locationMeasurementId = locationMeasurementId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATETIME", nullable = false)
	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	@Override
	public int hashCode() {
		String val = "" + locationMeasurementId + datetime.getTime();
		return val.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof LocationMeasurementValuePK)) {
			return false;
		}
		LocationMeasurementValuePK pk = (LocationMeasurementValuePK) obj;
		return pk.locationMeasurementId == this.locationMeasurementId
				&& pk.datetime.getTime() == this.datetime.getTime();
	}
}