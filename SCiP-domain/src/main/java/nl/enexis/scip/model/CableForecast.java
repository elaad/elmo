package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "SC_CABLE_FORECAST")
@NamedQueries({
		@NamedQuery(name = "CableForecast.getAll", query = "SELECT cf FROM CableForecast cf"),
		@NamedQuery(name = "CableForecast.search", query = "SELECT cf FROM CableForecast cf WHERE cf.cable = :cable AND cf.datetime BETWEEN :dateFrom AND :dateTo ORDER BY cf.datetime DESC"),
		@NamedQuery(name = "CableForecast.getAllForCableOrderByDatetimeDesc", query = "SELECT cf FROM CableForecast cf WHERE cf.cable = :cable ORDER BY cf.datetime DESC"),
		@NamedQuery(name = "CableForecast.getCableForecastsOfDay", query = "SELECT cf FROM CableForecast cf WHERE cf.cable = :cable AND cf.successful = :success AND cf.datetime BETWEEN :fromDate AND :toDate ORDER BY cf.datetime ASC") })
public class CableForecast implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1742733254933691223L;

	private String id;
	private Cable cable;
	private Date datetime;
	private Integer blockMinutes;
	private BigDecimal remainingCapacityFactor;
	private BigDecimal forecastSafetyFactor;
	private boolean isSuccessful = false;
	private boolean isFixed = false;
	private List<CableForecastOutput> cableForecastOutputs = new ArrayList<CableForecastOutput>(0);
	private List<CableForecastInput> cableForecastInputs = new ArrayList<CableForecastInput>(0);

	public CableForecast() {
	}

	@Id
	@Column(name = "ID", unique = true, nullable = false, length = 50)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CABLE", nullable = true)
	public Cable getCable() {
		return this.cable;
	}

	public void setCable(Cable cable) {
		this.cable = cable;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATETIME", nullable = false)
	public Date getDatetime() {
		return this.datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	@Column(name = "BLOCK_MINUTES", nullable = true)
	public Integer getBlockMinutes() {
		return blockMinutes;
	}

	public void setBlockMinutes(Integer blockMinutes) {
		this.blockMinutes = blockMinutes;
	}

	@Column(name = "REMAINING_CAPACITY_FACTOR", nullable = true, precision = 22, scale = 2)
	public BigDecimal getRemainingCapacityFactor() {
		return remainingCapacityFactor;
	}

	public void setRemainingCapacityFactor(BigDecimal remainingCapacityFactor) {
		this.remainingCapacityFactor = remainingCapacityFactor;
	}

	@Column(name = "FORECAST_SAFETY_FACTOR", nullable = true, precision = 22, scale = 2)
	public BigDecimal getForecastSafetyFactor() {
		return forecastSafetyFactor;
	}

	public void setForecastSafetyFactor(BigDecimal forecastSafetyFactor) {
		this.forecastSafetyFactor = forecastSafetyFactor;
	}

	@Column(name = "SUCCESS", nullable = false)
	public boolean isSuccessful() {
		return isSuccessful;
	}

	public void setSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	@Column(name = "FIXED", nullable = false)
	public boolean isFixed() {
		return isFixed;
	}

	public void setFixed(boolean isFixed) {
		this.isFixed = isFixed;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "cableForecast")
	@OrderBy("day, hour, block")
	public List<CableForecastOutput> getCableForecastOutputs() {
		return cableForecastOutputs;
	}

	public void setCableForecastOutputs(List<CableForecastOutput> cableForecastOutputs) {
		this.cableForecastOutputs = cableForecastOutputs;
	}

	@Transient
	public List<CableForecastInput> getCableForecastInputs() {
		return cableForecastInputs;
	}

	public void setCableForecastInputs(List<CableForecastInput> cableForecastInputs) {
		this.cableForecastInputs = cableForecastInputs;
	}

}
