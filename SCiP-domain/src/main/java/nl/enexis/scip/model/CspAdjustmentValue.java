package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "SC_CSP_ADJUSTMENT_VALUE")
@NamedQueries({ 
	@NamedQuery(name = "CspAdjustmentValue.getByCspAdjustmentId", query = "SELECT cav FROM CspAdjustmentValue cav WHERE cav.cspAdjustment.id = :adId") ,
	@NamedQuery(name = "CspAdjustmentValue.getLastCspAdjustmentsValueForBlock", query = "SELECT cav FROM CspAdjustmentValue cav WHERE cav.cableCsp.cable = :cable AND cav.cableCsp.csp = :csp AND cav.day = :day AND cav.hour = :hour AND cav.block = :block ORDER BY cav.cspAdjustment.cspAdjustmentMessage.dateTime DESC") ,
})
public class CspAdjustmentValue implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8611428463991678713L;

	private Long id;
	private boolean extraCapacity;
	private CspAdjustment cspAdjustment;
	private CableCsp cableCsp;
	private Date startedAt;
	private Date endedAt;
	private Date day;
	private int dayOfWeek;
	private int hour;
	private int block;
	private BigDecimal requested;
	private BigDecimal assigned;
	private String unitOfMeasure;
	private CspForecastValue cspForecastValue;

	public CspAdjustmentValue() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "EXTRA_CAPACITY", nullable = false)
	public boolean isExtraCapacity() {
		return extraCapacity;
	}

	public void setExtraCapacity(boolean extraCapacity) {
		this.extraCapacity = extraCapacity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CSP_ADJUSTMENT", nullable = false)
	public CspAdjustment getCspAdjustment() {
		return this.cspAdjustment;
	}

	public void setCspAdjustment(CspAdjustment cspAdjustment) {
		this.cspAdjustment = cspAdjustment;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABLE_CSP", nullable = false)
	public CableCsp getCableCsp() {
		return cableCsp;
	}

	public void setCableCsp(CableCsp cableCsp) {
		this.cableCsp = cableCsp;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "STARTED_AT", nullable = false)
	public Date getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ENDED_AT", nullable = false)
	public Date getEndedAt() {
		return endedAt;
	}

	public void setEndedAt(Date endedAt) {
		this.endedAt = endedAt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "DAY", nullable = false)
	public Date getDay() {
		return this.day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	@Column(name = "DAY_OF_WEEK", nullable = false, precision = 2, scale = 0)
	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	@Column(name = "HOUR", nullable = false, precision = 2, scale = 0)
	public int getHour() {
		return this.hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	@Column(name = "BLOCK", nullable = false, precision = 2, scale = 0)
	public int getBlock() {
		return this.block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	@Column(name = "REQUESTED", nullable = false, precision = 22, scale = 2)
	public BigDecimal getRequested() {
		return this.requested;
	}

	public void setRequested(BigDecimal requested) {
		this.requested = requested;
	}

	@Column(name = "ASSIGNED", nullable = true, precision = 22, scale = 2)
	public BigDecimal getAssigned() {
		return this.assigned;
	}

	public void setAssigned(BigDecimal assigned) {
		this.assigned = assigned;
	}

	@Transient
	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CSP_FORECAST_VALUE", nullable = false)
	public CspForecastValue getCspForecastValue() {
		return cspForecastValue;
	}

	public void setCspForecastValue(CspForecastValue cspForecastValue) {
		this.cspForecastValue = cspForecastValue;
	}

}
