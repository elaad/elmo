package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import nl.enexis.scip.action.ActionTarget;

@Entity
@Table(name = "SC_ADMIN_USER")
@NamedQueries({ @NamedQuery(name = "AdminUser.getAll", query = "SELECT u FROM AdminUser u") })
public class AdminUser implements java.io.Serializable, ActionTarget {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8648735416901785216L;

	private String userId;
	private String firstName;
	private String lastName;
	private String password;
	private String email;
	private boolean receiveEmail;
	private boolean active;
	private List<AdminUserNotification> notifications;
	private String roles;

	public AdminUser() {
	}

	@Id
	@Column(name = "USER_ID", unique = true, nullable = false, length = 10)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId.toUpperCase();
	}

	@Column(name = "PASSWORD", unique = false, nullable = false, length = 10)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "MAIL", unique = false, nullable = false, length = 100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "FIRSTNAME", nullable = false, length = 100)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "LASTNAME", nullable = false, length = 100)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "RECEIVE_EMAIL", nullable = false)
	public boolean isReceiveEmail() {
		return receiveEmail;
	}

	public void setReceiveEmail(boolean receiveEmail) {
		this.receiveEmail = receiveEmail;
	}

	@Column(name = "ACTIVE", nullable = false)
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = { CascadeType.REMOVE })
	public List<AdminUserNotification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<AdminUserNotification> notifications) {
		this.notifications = notifications;
	}

	@Column(name = "ROLES", nullable = true, length = 100)
	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (userId == null ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AdminUser)) {
			return false;
		}
		AdminUser other = (AdminUser) obj;
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AdminUser (userId=" + userId + ", active=" + active + ")";
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return userId;
	}

	@Override
	@Transient
	public String getActionTargetName() {
		return this.getClass().getName();
	}
}
