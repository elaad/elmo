package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import nl.enexis.scip.util.xml.LongXmlAdapter;

@Entity
@Table(name = "SC_CABLE_CSP", uniqueConstraints = @UniqueConstraint(columnNames = { "CABLE", "CSP" }))
@NamedQueries({
		@NamedQuery(name = "CableCsp.getAll", query = "SELECT cce FROM CableCsp cce"),
		@NamedQuery(name = "CableCsp.getAllScCompleteActiveCableCsps", query = "SELECT DISTINCT cc FROM CableCsp cc JOIN cc.cspConnections ccon WHERE cc.active = true AND cc.cable.behaviour = nl.enexis.scip.model.enumeration.BehaviourType.SC AND cc.cable.active = true AND cc.csp.active = true AND ccon.active = true"),
		@NamedQuery(name = "CableCsp.getAllSuCompleteActiveCableCsps", query = "SELECT cc FROM CableCsp cc WHERE cc.active = true AND cc.cable.behaviour = nl.enexis.scip.model.enumeration.BehaviourType.SU AND cc.cable.active = true AND cc.csp.active = true"),
		@NamedQuery(name = "CableCsp.getCableCspByCableAndCsp", query = "SELECT cce FROM CableCsp cce WHERE cce.cable = :cable AND cce.csp = :csp") })
public class CableCsp extends ActiveTopologyEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5864772744234859220L;

	private Long id;
	private Integer numberOfBlocks;
	private Csp csp;
	private Cable cable;
	private List<CspForecast> cspForecasts = new ArrayList<CspForecast>(0);
	private List<CspConnection> cspConnections = new ArrayList<CspConnection>(0);
	private List<CspUsage> cspUsages = new ArrayList<CspUsage>(0);

	public CableCsp() {
	}

	@Id
	@XmlID
	@XmlAttribute
	@XmlJavaTypeAdapter(type = long.class, value = LongXmlAdapter.class)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	// @Max(value=96, message="Number of Blocks cannot be more then 96")
	@NotNull(message = "Number of Blocks is required")
	@Min(value = 1, message = "Number of Blocks cannot be less then 1")
	@Column(name = "NUMBER_OF_BLOCKS", nullable = false)
	public Integer getNumberOfBlocks() {
		return numberOfBlocks;
	}

	public void setNumberOfBlocks(Integer numberOfBlocks) {
		this.numberOfBlocks = numberOfBlocks;
	}

	@XmlIDREF
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CSP", nullable = false)
	public Csp getCsp() {
		return this.csp;
	}

	public void setCsp(Csp csp) {
		this.csp = csp;
	}

	@XmlIDREF
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CABLE", nullable = false)
	public Cable getCable() {
		return this.cable;
	}

	public void setCable(Cable cable) {
		this.cable = cable;
	}

	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cableCsp")
	public List<CspForecast> getCspForecasts() {
		return this.cspForecasts;
	}

	public void setCspForecasts(List<CspForecast> cspForecasts) {
		this.cspForecasts = cspForecasts;
	}

	@XmlTransient
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "cableCsp")
	public List<CspConnection> getCspConnections() {
		return this.cspConnections;
	}

	public void setCspConnections(List<CspConnection> cspConnections) {
		this.cspConnections = cspConnections;
	}

	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cableCsp")
	public List<CspUsage> getCspUsages() {
		return this.cspUsages;
	}

	public void setCspUsages(List<CspUsage> cspUsages) {
		this.cspUsages = cspUsages;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (cable == null ? 0 : cable.hashCode());
		result = prime * result + (csp == null ? 0 : csp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CableCsp)) {
			return false;
		}
		CableCsp other = (CableCsp) obj;
		if (cable == null) {
			if (other.cable != null) {
				return false;
			}
		} else if (!cable.equals(other.cable)) {
			return false;
		}
		if (csp == null) {
			if (other.csp != null) {
				return false;
			}
		} else if (!csp.equals(other.csp)) {
			return false;
		}
		return true;
	}

	@Transient
	public boolean hasActiveCspConnections() {		
		boolean result = false;
		for (CspConnection connection : this.cspConnections) {
			if (connection.isActive()) {
				result = true;
				break;
			}
		}
		return result;
	}

	@Transient
	@Override
	public String getRecordKey() {
		return Long.toString(this.id);
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.getCable().getCableId() + "-" + this.getCsp().getEan13();
	}

}
