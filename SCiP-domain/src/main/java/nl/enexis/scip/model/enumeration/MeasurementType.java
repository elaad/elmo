package nl.enexis.scip.model.enumeration;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public enum MeasurementType {
	IGEM1 ("IGem1"),
	IGEM2 ("IGem2"), 
	IGEM3 ("IGem3"),
	PMAX0 ("PMax0"),
	PMAX1 ("PMax1"), 
	PMAX2 ("PMax2"), 
	PMAX3 ("PMax3"), 
	IMAX ("IMax1"), 
	IMAX2 ("IMax2"), 
	IMAX3 ("IMax3"), 
	SMAX0 ("SMax0"),
	SMAX1 ("SMax1"), 
	SMAX2 ("SMax2"), 
	SMAX3 ("SMax3"), 
	COS1 ("Cos1"), 
	COS2 ("Cos2"), 
	COS3 ("Cos3"), 
	EP0 ("EP0"), 
	EP1 ("EP1"), 
	EP2 ("EP2"), 
	EP3 ("EP3"), 
	UGEM1 ("UGem1"), 
	UGEM2 ("UGem2"), 
	UGEM3 ("UGem3"), 
	IRR("irradiation");

	private final String name;

	private MeasurementType(String s) {
		name = s;
	}

	public boolean equalsName(String otherName) {
		return otherName == null ? false : name.equals(otherName);
	}

	@Override
	public String toString() {
		return name;
	}

	public static boolean contains(String test) {

		for (MeasurementType mn : MeasurementType.values()) {
			if (mn.name().equals(test)) {
				return true;
			}
		}
		return false;
	}
}
