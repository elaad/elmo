package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "SC_CSP_FORECAST_VALUE")
@NamedQueries({
		@NamedQuery(name = "CspForecastValue.getAll", query = "SELECT cfv FROM CspForecastValue cfv ORDER BY cfv.id"),
		@NamedQuery(name = "CspForecastValue.getByCspForecastId", query = "SELECT cfv FROM CspForecastValue cfv WHERE cfv.cspForecast.id = :fcastId ORDER BY cfv.id"),
		@NamedQuery(name = "CspForecastValue.getLastCspForecastValueForCableForecastOutput", query = "SELECT cfv FROM CspForecastValue cfv WHERE cfv.cableForecastOutput.id = :cableForecastOutputId AND cfv.cspForecast.cableCsp.csp.ean13 = :cspEan ORDER BY cfv.cspForecast.calculatedAt DESC"),
		@NamedQuery(name = "CspForecastValue.getLastCspForecastValuesForCspUsageValue", query = "SELECT cfv FROM CspForecastValue cfv WHERE cfv.cspForecast.cableCsp = :cableCsp AND cfv.cableForecastOutput.day = :day AND cfv.cableForecastOutput.hour = :hour AND cfv.cableForecastOutput.block = :block ORDER BY cfv.cspForecast.calculatedAt DESC") })
public class CspForecastValue implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7691052328970752841L;

	private Long id;
	private CspForecast cspForecast;
	private CableForecastOutput cableForecastOutput;
	private BigDecimal assigned;
	private BigDecimal originalAssigned;
	private BigDecimal remaining;
	private CspAdjustmentValue cspAdjustmentValue;

	public CspForecastValue() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CSP_FORECAST", nullable = false)
	public CspForecast getCspForecast() {
		return this.cspForecast;
	}

	public void setCspForecast(CspForecast cspForecast) {
		this.cspForecast = cspForecast;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CABLE_FORECAST_OUTPUT", nullable = false)
	public CableForecastOutput getCableForecastOutput() {
		return cableForecastOutput;
	}

	public void setCableForecastOutput(CableForecastOutput cableForecastOutput) {
		this.cableForecastOutput = cableForecastOutput;
	}

	@Column(name = "ASSIGNED", nullable = false, precision = 22, scale = 2)
	public BigDecimal getAssigned() {
		return this.assigned;
	}

	public void setAssigned(BigDecimal assigned) {
		this.assigned = assigned;
	}

	@Column(name = "ORIGINAL_ASSIGNED", nullable = true, precision = 22, scale = 2)
	public BigDecimal getOriginalAssigned() {
		return originalAssigned;
	}

	public void setOriginalAssigned(BigDecimal originalAssigned) {
		this.originalAssigned = originalAssigned;
	}

	@Column(name = "REMAINING", nullable = false, precision = 22, scale = 2)
	public BigDecimal getRemaining() {
		return this.remaining;
	}

	public void setRemaining(BigDecimal remaining) {
		this.remaining = remaining;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CSP_ADJUSTMENT_VALUE", nullable = true)
	public CspAdjustmentValue getCspAdjustmentValue() {
		return cspAdjustmentValue;
	}

	public void setCspAdjustmentValue(CspAdjustmentValue cspAdjustmentValue) {
		this.cspAdjustmentValue = cspAdjustmentValue;
	}

	private boolean changeCheckBlock = false;

	@Transient
	public boolean isChangeCheckBlock() {
		return changeCheckBlock;
	}

	public void setChangeCheckBlock(boolean changeCheckBlock) {
		this.changeCheckBlock = changeCheckBlock;
	}

}
