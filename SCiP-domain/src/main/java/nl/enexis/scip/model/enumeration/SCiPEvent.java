package nl.enexis.scip.model.enumeration;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public enum SCiPEvent {
	FETCH_LOCATION_VALUES, SEND_LOCATION_VALUES, FETCH_LOCATION_VALUES_TIMER, SEND_LOCATION_VALUES_TIMER, UNKNOWN, TOPOLOGY, RECEIVE_USAGE, SEND_FORECAST, REQUEST_FORECAST, SEND_FORECAST_TIMER, CALCULATE_FORECAST, PREPARE_FORECAST_INPUT, PREPARE_FORECAST_INPUT_TIMER, CALCULATE_CSP_FORECAST, CALCULATE_FORECAST_TIMER, FETCH_MEASUREMENTS_TIMER, FETCH_MEASUREMENTS, RECEIVE_ADJUSTMENT;
}
