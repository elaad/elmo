package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "SC_MEASUREMENT_VALUE")
@NamedQueries({ @NamedQuery(name = "MeasurementValue.getAll", query = "SELECT m FROM MeasurementValue m") })
public class MeasurementValue implements java.io.Serializable {

	private static final long serialVersionUID = -4251663566699782568L;

	private Long id;
	private Meter meter;
	private Measurement measurement;
	private BigDecimal iGem1;
	private BigDecimal iGem2;
	private BigDecimal iGem3;
	private BigDecimal iMax1;
	private BigDecimal iMax2;
	private BigDecimal iMax3;
	private BigDecimal pMax1;
	private BigDecimal pMax2;
	private BigDecimal pMax3;
	private BigDecimal pMax0;
	private BigDecimal sMax1;
	private BigDecimal sMax2;
	private BigDecimal sMax3;
	private BigDecimal sMax0;
	private BigDecimal cos1;
	private BigDecimal cos2;
	private BigDecimal cos3;
	private BigDecimal ep1;
	private BigDecimal ep2;
	private BigDecimal ep3;
	private BigDecimal ep0;
	private BigDecimal uGem1;
	private BigDecimal uGem2;
	private BigDecimal uGem3;

	public MeasurementValue() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "METER", nullable = false)
	public Meter getMeter() {
		return this.meter;
	}

	public void setMeter(Meter meter) {
		this.meter = meter;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MEASUREMENT", nullable = false)
	public Measurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

	@Column(name = "IGEM1", nullable = true, precision = 18, scale = 2)
	public BigDecimal getIGem1() {
		return iGem1;
	}

	public void setIGem1(BigDecimal iGem1) {
		this.iGem1 = iGem1;
	}

	@Column(name = "IGEM2", nullable = true, precision = 18, scale = 2)
	public BigDecimal getIGem2() {
		return iGem2;
	}

	public void setIGem2(BigDecimal iGem2) {
		this.iGem2 = iGem2;
	}

	@Column(name = "IGEM3", nullable = true, precision = 18, scale = 2)
	public BigDecimal getIGem3() {
		return iGem3;
	}

	public void setIGem3(BigDecimal iGem3) {
		this.iGem3 = iGem3;
	}

	@Column(name = "IMAX1", nullable = true, precision = 18, scale = 2)
	public BigDecimal getIMax1() {
		return iMax1;
	}

	public void setIMax1(BigDecimal iMax1) {
		this.iMax1 = iMax1;
	}

	@Column(name = "IMAX2", nullable = true, precision = 18, scale = 2)
	public BigDecimal getIMax2() {
		return iMax2;
	}

	public void setIMax2(BigDecimal iMax2) {
		this.iMax2 = iMax2;
	}

	@Column(name = "IMAX3", nullable = true, precision = 18, scale = 2)
	public BigDecimal getIMax3() {
		return iMax3;
	}

	public void setIMax3(BigDecimal iMax3) {
		this.iMax3 = iMax3;
	}

	@Column(name = "PMAX1", nullable = true, precision = 18, scale = 2)
	public BigDecimal getPMax1() {
		return pMax1;
	}

	public void setPMax1(BigDecimal pMax1) {
		this.pMax1 = pMax1;
	}

	@Column(name = "PMAX2", nullable = true, precision = 18, scale = 2)
	public BigDecimal getPMax2() {
		return pMax2;
	}

	public void setPMax2(BigDecimal pMax2) {
		this.pMax2 = pMax2;
	}

	@Column(name = "PMAX3", nullable = true, precision = 18, scale = 2)
	public BigDecimal getPMax3() {
		return pMax3;
	}

	public void setPMax3(BigDecimal pMax3) {
		this.pMax3 = pMax3;
	}

	@Column(name = "PMAX0", nullable = true, precision = 18, scale = 2)
	public BigDecimal getPMax0() {
		return pMax0;
	}

	public void setPMax0(BigDecimal pMax0) {
		this.pMax0 = pMax0;
	}

	@Column(name = "SMAX1", nullable = true, precision = 18, scale = 2)
	public BigDecimal getSMax1() {
		return sMax1;
	}

	public void setSMax1(BigDecimal sMax1) {
		this.sMax1 = sMax1;
	}

	@Column(name = "SMAX2", nullable = true, precision = 18, scale = 2)
	public BigDecimal getSMax2() {
		return sMax2;
	}

	public void setSMax2(BigDecimal sMax2) {
		this.sMax2 = sMax2;
	}

	@Column(name = "SMAX3", nullable = true, precision = 18, scale = 2)
	public BigDecimal getSMax3() {
		return sMax3;
	}

	public void setSMax3(BigDecimal sMax3) {
		this.sMax3 = sMax3;
	}

	@Column(name = "SMAX0", nullable = true, precision = 18, scale = 2)
	public BigDecimal getSMax0() {
		return sMax0;
	}

	public void setSMax0(BigDecimal sMax0) {
		this.sMax0 = sMax0;
	}

	@Column(name = "COS1", nullable = true, precision = 18, scale = 2)
	public BigDecimal getCos1() {
		return cos1;
	}

	public void setCos1(BigDecimal cos1) {
		this.cos1 = cos1;
	}

	@Column(name = "COS2", nullable = true, precision = 18, scale = 2)
	public BigDecimal getCos2() {
		return cos2;
	}

	public void setCos2(BigDecimal cos2) {
		this.cos2 = cos2;
	}

	@Column(name = "COS3", nullable = true, precision = 18, scale = 2)
	public BigDecimal getCos3() {
		return cos3;
	}

	public void setCos3(BigDecimal cos3) {
		this.cos3 = cos3;
	}

	@Column(name = "EP1", nullable = true, precision = 18, scale = 2)
	public BigDecimal getEp1() {
		return ep1;
	}

	public void setEp1(BigDecimal ep1) {
		this.ep1 = ep1;
	}

	@Column(name = "EP2", nullable = true, precision = 18, scale = 2)
	public BigDecimal getEp2() {
		return ep2;
	}

	public void setEp2(BigDecimal ep2) {
		this.ep2 = ep2;
	}

	@Column(name = "EP3", nullable = true, precision = 18, scale = 2)
	public BigDecimal getEp3() {
		return ep3;
	}

	public void setEp3(BigDecimal ep3) {
		this.ep3 = ep3;
	}

	@Column(name = "EP0", nullable = true, precision = 18, scale = 2)
	public BigDecimal getEp0() {
		return ep0;
	}

	public void setEp0(BigDecimal ep0) {
		this.ep0 = ep0;
	}

	@Column(name = "UGEM1", nullable = true, precision = 18, scale = 2)
	public BigDecimal getUGem1() {
		return uGem1;
	}

	public void setUGem1(BigDecimal uGem1) {
		this.uGem1 = uGem1;
	}

	@Column(name = "UGEM2", nullable = true, precision = 18, scale = 2)
	public BigDecimal getUGem2() {
		return uGem2;
	}

	public void setUGem2(BigDecimal uGem2) {
		this.uGem2 = uGem2;
	}

	@Column(name = "UGEM3", nullable = true, precision = 18, scale = 2)
	public BigDecimal getUGem3() {
		return uGem3;
	}

	public void setUGem3(BigDecimal uGem3) {
		this.uGem3 = uGem3;
	}

}
