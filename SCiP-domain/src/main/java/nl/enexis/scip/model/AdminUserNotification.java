package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.model.enumeration.NotificationInterval;
import nl.enexis.scip.model.id.AdminUserNotificationId;

@Entity
@Table(name = "SC_ADMIN_USER_NOTIFICATION")
@NamedQueries({
		@NamedQuery(name = "AdminUserNotification.getAll", query = "SELECT un FROM AdminUserNotification un"),
		@NamedQuery(name = "AdminUserNotification.findActiveDirectActionAdminUserNotifications", query = "SELECT un FROM AdminUserNotification un WHERE un.enabled = :enabled AND un.event = :event AND un.interval = :interval"),
		@NamedQuery(name = "AdminUserNotification.findActiveIntervalAdminUserNotifications", query = "SELECT un FROM AdminUserNotification un WHERE un.enabled = :enabled AND un.interval = :interval") })
@IdClass(AdminUserNotificationId.class)
public class AdminUserNotification implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8810154357817189936L;

	private AdminUser user;
	private ActionEvent event;
	private NotificationInterval interval;
	private Level level;
	private boolean enabled = true;

	public AdminUserNotification() {
	}

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER", nullable = false)
	public AdminUser getUser() {
		return user;
	}

	public void setUser(AdminUser user) {
		this.user = user;
	}

	@Id
	@NotNull(message = "Event is required")
	@Enumerated(EnumType.STRING)
	@Column(name = "EVENT", nullable = false, length = 30)
	public ActionEvent getEvent() {
		return event;
	}

	public void setEvent(ActionEvent event) {
		this.event = event;
	}

	@Id
	@NotNull(message = "Interval is required")
	@Enumerated(EnumType.STRING)
	@Column(name = "NOTIFY_INTERVAL", nullable = false, length = 6)
	public NotificationInterval getInterval() {
		return interval;
	}

	public void setInterval(NotificationInterval interval) {
		this.interval = interval;
	}

	@Id
	@NotNull(message = "Level is required")
	@Enumerated(EnumType.STRING)
	@Column(name = "LEVEL", nullable = false, length = 5)
	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	@Column(name = "ENABLED", nullable = false)
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (event == null ? 0 : event.hashCode());
		result = prime * result + (interval == null ? 0 : interval.hashCode());
		result = prime * result + (level == null ? 0 : level.hashCode());
		result = prime * result + (user == null ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AdminUserNotification)) {
			return false;
		}
		AdminUserNotification other = (AdminUserNotification) obj;
		if (event != other.event) {
			return false;
		}
		if (interval != other.interval) {
			return false;
		}
		if (level != other.level) {
			return false;
		}
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AdminUserNotification (user=" + user + ", event=" + event + ", interval=" + interval + ", level="
				+ level + ", enabled=" + enabled + ")";
	}

}
