package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "SC_LOCATION_MEASUREMENT_VALUE")
@NamedQueries({
		@NamedQuery(name = "LocationMeasurementValue.getAll", query = "SELECT lmv FROM LocationMeasurementValue lmv"),
		@NamedQuery(name = "LocationMeasurementValue.getLastIrrValuesForSu", query = "SELECT cl.cable.cableId, lmv.locationMeasurement.forecast, max(lmv.key.datetime) FROM LocationMeasurementValue lmv JOIN lmv.locationMeasurement.location.cableLocations cl WHERE cl.cable.behaviour = nl.enexis.scip.model.enumeration.BehaviourType.SU AND cl.cable.active = true AND lmv.locationMeasurement.measurementType = nl.enexis.scip.model.enumeration.MeasurementType.IRR GROUP BY cl.cable.cableId, lmv.locationMeasurement.forecast"),
		@NamedQuery(name = "LocationMeasurementValue.getLocationMeasurmentValuesBeforeDate", query = "SELECT lmv FROM LocationMeasurementValue lmv WHERE lmv.locationMeasurement = :locationMeasurement AND lmv.key.datetime <= :toDate order by lmv.key.datetime DESC "),
		@NamedQuery(name = "LocationMeasurementValue.getLocationMeasurmentValuesFromDate", query = "SELECT lmv FROM LocationMeasurementValue lmv WHERE lmv.locationMeasurement = :locationMeasurement AND lmv.key.datetime >= :fromDate order by lmv.key.datetime ASC") })
public class LocationMeasurementValue implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5864772744234859220L;

	private LocationMeasurementValuePK key;
	private BigDecimal value;
	private LocationMeasurement locationMeasurement;

	public LocationMeasurementValue() {
	}

	@EmbeddedId
	public LocationMeasurementValuePK getKey() {
		return key;
	}

	public void setKey(LocationMeasurementValuePK key) {
		this.key = key;
	}

	@Column(name = "VALUE", nullable = true, precision = 18, scale = 2)
	public BigDecimal getValue() {
		return this.value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@MapsId("locationMeasurementId")
	// references EmbeddedId's property
	@JoinColumn(name = "LOCATION_MEASUREMENT", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.EAGER)
	public LocationMeasurement getLocationMeasurement() {
		return this.locationMeasurement;
	}

	public void setLocationMeasurement(LocationMeasurement locationMeasurement) {
		this.locationMeasurement = locationMeasurement;
	}

}
