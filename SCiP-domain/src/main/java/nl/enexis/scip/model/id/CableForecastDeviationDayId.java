package nl.enexis.scip.model.id;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.Date;

import nl.enexis.scip.model.Cable;

public class CableForecastDeviationDayId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4376896902972590033L;

	private Cable cable;
	private Date day;

	public CableForecastDeviationDayId() {
	}

	public Cable getCable() {
		return cable;
	}

	public void setCable(Cable cable) {
		this.cable = cable;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (cable == null ? 0 : cable.hashCode());
		result = prime * result + (day == null ? 0 : day.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CableForecastDeviationDayId)) {
			return false;
		}
		CableForecastDeviationDayId other = (CableForecastDeviationDayId) obj;
		if (cable == null) {
			if (other.cable != null) {
				return false;
			}
		} else if (!cable.equals(other.cable)) {
			return false;
		}
		if (day == null) {
			if (other.day != null) {
				return false;
			}
		} else if (!day.equals(other.day)) {
			return false;
		}
		return true;
	}

}