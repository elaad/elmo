package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import nl.enexis.scip.model.enumeration.DatabaseAction;

@Entity
@Table(name = "SC_AUDIT_LOG_ITEM")
@NamedQueries({ @NamedQuery(name = "AuditLogItem.getBetweenDates", query = "SELECT c FROM AuditLogItem c WHERE c.addedAt BETWEEN :startDate AND :endDate"), })
public class AuditLogItem implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -176225417904946505L;
	
	private long id;
	private String tableName;
	private String recordKey;
	private String dbAction;
	private String userId;
	private String newValues;
	private String oldValues;
	private String comments;
	private Date   addedAt;
	
	public AuditLogItem() {

	}
	
	public AuditLogItem(String tableName, String recordKey, DatabaseAction dbAction,
			String user, String newValues, String oldValues, String comments) {
		super();
		this.tableName = tableName;
		this.recordKey = recordKey;
		this.dbAction = dbAction.value();
		this.newValues = newValues;
		this.oldValues = oldValues;
		this.comments = comments;
		this.userId = user;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, length = 20)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "TABLE_NAME", nullable = false)
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Column(name = "RECORD_KEY", nullable = true)
	public String getRecordKey() {
		return recordKey;
	}

	public void setRecordKey(String recordKey) {
		this.recordKey = recordKey;
	}

	@Column(name = "DB_ACTION", nullable = true)
	public String getDbAction() {
		return dbAction;
	}

	public void setDbAction(String dbAction) {
		this.dbAction = dbAction;
	}

	@Column(name = "USER_ID", nullable = true, length = 10)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "NEW_VALUES", nullable = true, length = 4096)
	public String getNewValues() {
		return newValues;
	}

	public void setNewValues(String newValues) {
		this.newValues = newValues;
	}

	@Column(name = "OLD_VALUES", nullable = true, length = 4096)
	public String getOldValues() {
		return oldValues;
	}

	public void setOldValues(String oldValues) {
		this.oldValues = oldValues;
	}
	
    @Column(name = "COMMENTS", nullable = true, length = 2048)
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Basic(optional = false)
	@Temporal(TemporalType.TIMESTAMP)	
	@Column(name = "ADDED_AT", nullable = true, insertable = false, updatable = false)
	public Date getAddedAt() {
		return addedAt;
	}

	public void setAddedAt(Date addedAt) {
		this.addedAt = addedAt;
	}
}
