package nl.enexis.scip.model.id;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.model.AdminUser;
import nl.enexis.scip.model.enumeration.NotificationInterval;

public class AdminUserNotificationId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -426112847448616967L;

	private AdminUser user;
	private ActionEvent event;
	private NotificationInterval interval;
	private Level level;

	public AdminUserNotificationId() {
	}

	public AdminUser getUser() {
		return user;
	}

	public void setUser(AdminUser user) {
		this.user = user;
	}

	public ActionEvent getEvent() {
		return event;
	}

	public void setEvent(ActionEvent event) {
		this.event = event;
	}

	public NotificationInterval getInterval() {
		return interval;
	}

	public void setInterval(NotificationInterval interval) {
		this.interval = interval;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (event == null ? 0 : event.hashCode());
		result = prime * result + (interval == null ? 0 : interval.hashCode());
		result = prime * result + (level == null ? 0 : level.hashCode());
		result = prime * result + (user == null ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AdminUserNotificationId)) {
			return false;
		}
		AdminUserNotificationId other = (AdminUserNotificationId) obj;
		if (event != other.event) {
			return false;
		}
		if (interval != other.interval) {
			return false;
		}
		if (level != other.level) {
			return false;
		}
		if (user == null) {
			if (other.user != null) {
				return false;
			}
		} else if (!user.equals(other.user)) {
			return false;
		}
		return true;
	}

}