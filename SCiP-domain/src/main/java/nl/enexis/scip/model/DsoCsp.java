package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import nl.enexis.scip.util.xml.LongXmlAdapter;

@Entity
@Table(name = "SC_DSO_CSP", uniqueConstraints = @UniqueConstraint(columnNames = { "DSO", "CSP" }))
@NamedQueries({
		@NamedQuery(name = "DsoCsp.getAll", query = "SELECT dce FROM DsoCsp dce"),
		@NamedQuery(name = "DsoCsp.getDsoCspByDsoAndCsp", query = "SELECT dce FROM DsoCsp dce WHERE dce.dso = :dso AND dce.csp = :csp") })
public class DsoCsp extends ActiveTopologyEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5864772744234859220L;

	private Long id;
	private Dso dso;
	private Csp csp;
	
	public DsoCsp() {
	}

	@Id
	@XmlID
	@XmlAttribute 
    @XmlJavaTypeAdapter(type = long.class, value = LongXmlAdapter.class)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@XmlIDREF
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DSO", nullable = false)
	public Dso getDso() {
		return this.dso;
	}

	public void setDso(Dso dso) {
		this.dso = dso;
	}

    @XmlIDREF
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CSP", nullable = false)
	public Csp getCsp() {
		return this.csp;
	}

	public void setCsp(Csp csp) {
		this.csp = csp;
	}

    @Transient
    @Override
    public String getRecordKey() {
        return Long.toString(this.id);
    }
    
	@Override
	@Transient
	public String getActionTargetId() {
		return this.getDso().getEan13() + "-" + this.getCsp().getEan13();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.getCsp() == null) ? 0 : this.getCsp().hashCode());
		result = prime * result + ((this.getDso() == null) ? 0 : this.getDso().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DsoCsp)) {
			return false;
		}
		DsoCsp other = (DsoCsp) obj;
		if (this.getCsp() == null) {
			if (other.getCsp() != null) {
				return false;
			}
		} else if (!this.getCsp().equals(other.getCsp())) {
			return false;
		}
		if (this.getDso() == null) {
			if (other.getDso() != null) {
				return false;
			}
		} else if (!this.getDso().equals(other.getDso())) {
			return false;
		}
		return true;
	}

}
