package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import nl.enexis.scip.action.ActionTarget;

@Entity
@Table(name = "SC_CONFIG_PARAMETER")
@NamedQueries({ @NamedQuery(name = "ConfigurationParameter.getAll", query = "SELECT c FROM ConfigurationParameter c"), })
public class ConfigurationParameter extends AuditTrailEntity implements java.io.Serializable, ActionTarget {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8805952726478739078L;

	private String name;
	private String value;
	private boolean changeable;
	
	public ConfigurationParameter() {
		
	}

	@Id
	@Column(name = "NAME", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "VALUE", unique = false, nullable = false, length = 250)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "CHANGEABLE", nullable = true)
	public boolean isChangeable() {
		return changeable;
	}

	public void setChangeable(boolean changeable) {
		this.changeable = changeable;
	}
	
	@Transient
    @Override
	public String getRecordKey() {
	    return this.getName();
	}

	@Override
	public String toString() {
		return "ConfigurationParameter (name=" + name + ", value=" + value + ", changeable=" + changeable + ")";
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.getName();
	}

	@Override
	@Transient
	public String getActionTargetName() {
		return this.getClass().getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (name == null ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ConfigurationParameter)) {
			return false;
		}
		ConfigurationParameter other = (ConfigurationParameter) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}
}
