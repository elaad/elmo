package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SC_CSP_USAGE_VALUE")
@NamedQueries({
		@NamedQuery(name = "CspUsageValue.getLastUsageValuesForSu", query = "SELECT c.cableId, max(cuv.endedAt) FROM CspUsageValue cuv JOIN cuv.cableCsp.cable c WHERE c.behaviour = nl.enexis.scip.model.enumeration.BehaviourType.SU AND c.active = true AND cuv.last = true GROUP BY c.cableId"),
		@NamedQuery(name = "CspUsageValue.getCspUsagesForCableForecastBlock", query = "SELECT cuv FROM CspUsageValue cuv WHERE cuv.cableCsp.cable = :cable AND cuv.last = :last AND cuv.dayOfWeek = :dayOfWeek AND cuv.hour = :hour AND cuv.block = :block AND cuv.day >= :fromDate"),
		@NamedQuery(name = "CspUsageValue.getCspUsagesForCableCspBeforeDate", query = "SELECT cuv FROM CspUsageValue cuv WHERE cuv.cableCsp = :cableCsp AND cuv.startedAt <= :toDate AND cuv.last = :last order by cuv.startedAt desc"),
		@NamedQuery(name = "CspUsageValue.getCspUsagesByDate", query = "SELECT cuv FROM CspUsageValue cuv WHERE cuv.last = true AND cuv.startedAt BETWEEN :fromDate AND :toDate order by cuv.startedAt DESC") })
public class CspUsageValue implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8488001014148167460L;

	private Long id;
	private CspUsage cspUsage;
	private CableCsp cableCsp;
	private Date startedAt;
	private Date endedAt;
	private Date day;
	private int dayOfWeek;
	private int hour;
	private int block;
	private BigDecimal energyConsumption;
	private BigDecimal averageCurrent;
	private BigDecimal deviation = new BigDecimal("0.00");
	private boolean last = true;

	public CspUsageValue() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CSP_USAGE", nullable = false)
	public CspUsage getCspUsage() {
		return this.cspUsage;
	}

	public void setCspUsage(CspUsage cspUsage) {
		this.cspUsage = cspUsage;
	}

	@Column(name = "ENERGY_CONSUMPTION", nullable = false, precision = 12, scale = 2)
	public BigDecimal getEnergyConsumption() {
		return this.energyConsumption;
	}

	public void setEnergyConsumption(BigDecimal energyConsumption) {
		this.energyConsumption = energyConsumption;
	}

	@Column(name = "AVERAGE_CURRENT", nullable = false, precision = 12, scale = 2)
	public BigDecimal getAverageCurrent() {
		return averageCurrent;
	}

	public void setAverageCurrent(BigDecimal averageCurrent) {
		this.averageCurrent = averageCurrent;
	}

	@Column(name = "DEVIATION", nullable = false, precision = 12, scale = 2)
	public BigDecimal getDeviation() {
		return deviation;
	}

	public void setDeviation(BigDecimal deviation) {
		this.deviation = deviation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABLE_CSP", nullable = false)
	public CableCsp getCableCsp() {
		return cableCsp;
	}

	public void setCableCsp(CableCsp cableCsp) {
		this.cableCsp = cableCsp;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "STARTED_AT", nullable = false)
	public Date getStartedAt() {
		return startedAt;
	}

	public void setStartedAt(Date startedAt) {
		this.startedAt = startedAt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ENDED_AT", nullable = false)
	public Date getEndedAt() {
		return endedAt;
	}

	public void setEndedAt(Date endedAt) {
		this.endedAt = endedAt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "DAY", nullable = false, length = 7)
	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	@Column(name = "DAY_OF_WEEK", nullable = false, precision = 2, scale = 0)
	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	@Column(name = "HOUR", nullable = false, precision = 2, scale = 0)
	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	@Column(name = "BLOCK", nullable = false, precision = 2, scale = 0)
	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	@Column(name = "LAST", nullable = true)
	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}
}
