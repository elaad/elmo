package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlID;

@Entity
@Table(name = "SC_LOCATION")
@NamedQueries({ @NamedQuery(name = "Location.getAll", query = "SELECT l FROM Location l") })
public class Location implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8805952726478739078L;

	private String locationId;
	private String locationName;
	private List<CableLocation> cableLocations = new ArrayList<CableLocation>(0);
	private List<LocationMeasurement> locationMeasurements = new ArrayList<LocationMeasurement>(0);

	public Location() {
	}

	@Id
    @XmlID
	@Column(name = "LOCATION_ID", unique = true, nullable = false, length = 10)
	public String getLocationId() {
		return this.locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	@Column(name = "LOCATION_NAME", unique = false, nullable = false, length = 50)
	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "location")
	public List<CableLocation> getCableLocations() {
		return this.cableLocations;
	}

	public void setCableLocations(List<CableLocation> cableLocations) {
		this.cableLocations = cableLocations;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "location")
	public List<LocationMeasurement> getLocationMeasurements() {
		return locationMeasurements;
	}

	public void setLocationMeasurements(
			List<LocationMeasurement> locationMeasurements) {
		this.locationMeasurements = locationMeasurements;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.getLocationId() == null) ? 0 : this.getLocationId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Location)) {
			return false;
		}
		Location other = (Location) obj;
		if (this.getLocationId() == null) {
			if (other.getLocationId() != null) {
				return false;
			}
		} else if (!this.getLocationId().equals(other.getLocationId())) {
			return false;
		}
		return true;
	}
}
