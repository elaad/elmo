package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "SC_DSO")
@NamedQueries({ @NamedQuery(name = "Dso.getAll", query = "SELECT d FROM Dso d") })
public class Dso extends ActiveTopologyEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -411721892076990567L;

	private String ean13;
	private String name;
	private List<DsoCsp> dsoCsps = new ArrayList<DsoCsp>(0);
	private List<Cable> cables = new ArrayList<Cable>(0);

	public Dso() {
	}

	@Id
    @XmlID
    @XmlAttribute 
    @NotEmpty(message="EAN13 is required")
    @Size(min = 13, max = 13, message = "EAN13 has to be {max} digits")
	@Column(name = "EAN13", unique = true, nullable = false, length = 13)
	public String getEan13() {
		return this.ean13;
	}

	public void setEan13(String ean13) {
		this.ean13 = ean13;
	}

    @NotEmpty(message="Name is required")
    @Size(max = 100, message = "Name cannot be more then {max} characters long")
	@Column(name = "NAME", nullable = false, length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dso")
	public List<DsoCsp> getDsoCsps() {
		return dsoCsps;
	}

	public void setDsoCsps(List<DsoCsp> dsoCsps) {
		this.dsoCsps = dsoCsps;
	}

	@XmlTransient
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dso")
	public List<Cable> getCables() {
		return this.cables;
	}

	public void setCables(List<Cable> cables) {
		this.cables = cables;
	}

    @Transient
    @Override
    public String getRecordKey() {
        return this.ean13;
    }

	@Override
	@Transient
	public String getActionTargetId() {
		return this.getEan13();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.getEan13() == null) ? 0 : this.getEan13().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Dso)) {
			return false;
		}
		Dso other = (Dso) obj;
		if (this.getEan13() == null) {
			if (other.getEan13() != null) {
				return false;
			}
		} else if (!this.getEan13().equals(other.getEan13())) {
			return false;
		}
		return true;
	}

}
