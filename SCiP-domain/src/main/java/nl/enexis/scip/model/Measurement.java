package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "SC_MEASUREMENT", uniqueConstraints = @UniqueConstraint(columnNames = { "CABLE", "DATE" }))
@NamedQueries({
		@NamedQuery(name = "Measurement.getAll", query = "SELECT m FROM Measurement m"),
		@NamedQuery(name = "Measurement.getPeriodIncompleteMeasurmentPerCable", query = "SELECT m.cable, COUNT(m) as counted FROM Measurement m WHERE m.complete = false AND m.cable.transformer.active = true AND m.cable.transformer.deleted = false AND m.cable.transformer.measurementProvider.active = true AND m.cable.transformer.measurementProvider.deleted = false AND m.date BETWEEN :from AND :to GROUP BY m.cable ORDER BY counted DESC"),
		@NamedQuery(name = "Measurement.getMeasurementForCableAndDate", query = "SELECT m FROM Measurement m WHERE m.date = :date AND m.cable = :cable"),
		@NamedQuery(name = "Measurement.getClosestNextCompleteMeasurement", query = "SELECT m FROM Measurement m WHERE m.date > :date AND m.cable = :cable AND m.complete = :complete ORDER BY m.date ASC"),
		@NamedQuery(name = "Measurement.getClosestPrevCompleteMeasurement", query = "SELECT m FROM Measurement m WHERE m.date < :date AND m.cable = :cable AND m.complete = :complete ORDER BY m.date DESC"),
		@NamedQuery(name = "Measurement.getMeasurementForCableBetweenDates", query = "SELECT m FROM Measurement m WHERE  m.cable = :cable AND m.date BETWEEN :startDate AND :endDate"),
		@NamedQuery(name = "Measurement.getMeasurementForProviderIdBetweenDates", query = "SELECT m FROM Measurement m WHERE  m.cable.transformer.measurementProvider.id = :mpid AND m.date BETWEEN :startDate AND :endDate"),
		@NamedQuery(name = "Measurement.getMeasurementBetweenDates", query = "SELECT m FROM Measurement m WHERE m.date BETWEEN :startDate AND :endDate") })
public class Measurement implements java.io.Serializable {

	private static final long serialVersionUID = -4251663566699782568L;

	private Long id;
	private Date date;
	private Cable cable;
	private boolean complete = false;
	private List<MeasurementValue> measurementValues = new ArrayList<MeasurementValue>(0);

	public Measurement() {
	}

	public Measurement(Cable cable, Date date) {
		this.cable = cable;
		this.date = date;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CABLE", nullable = false)
	public Cable getCable() {
		return cable;
	}

	public void setCable(Cable cable) {
		this.cable = cable;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE", nullable = false)
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Column(name = "COMPLETE", nullable = false)
	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "measurement")
	public List<MeasurementValue> getMeasurementValues() {
		return measurementValues;
	}

	public void setMeasurementValues(List<MeasurementValue> measurementValues) {
		this.measurementValues = measurementValues;
	}

	@Override
	public String toString() {
		return "Measurement [date=" + date + ", cable=" + cable + "]";
	}

}
