package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import nl.enexis.scip.model.enumeration.DeviationDayType;
import nl.enexis.scip.model.id.CableForecastDeviationDayId;

@Entity
@Table(name = "SC_CABLE_FORECAST_DEVIATION_DAY")
@NamedQueries({
		@NamedQuery(name = "CableForecastDeviationDay.getAll", query = "SELECT cfdd FROM CableForecastDeviationDay cfdd"),
		@NamedQuery(name = "CableForecastDeviationDay.getLastNotDeviatedDays", query = "SELECT cfdd FROM CableForecastDeviationDay cfdd WHERE cfdd.cable = :cable AND cfdd.type = :deviationType ORDER BY cfdd.day DESC"),
		@NamedQuery(name = "CableForecastDeviationDay.getForCableAndDay", query = "SELECT cfdd FROM CableForecastDeviationDay cfdd WHERE cfdd.cable = :cable AND cfdd.day = :day"),
		@NamedQuery(name = "CableForecastDeviationDay.getLastForCable", query = "SELECT cfdd FROM CableForecastDeviationDay cfdd WHERE cfdd.cable = :cable ORDER BY cfdd.day DESC") })
@IdClass(CableForecastDeviationDayId.class)
public class CableForecastDeviationDay implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3617158066016154305L;

	private Cable cable;
	private Date day;
	private BigDecimal deviation;
	private BigDecimal averageDeviation;
	private DeviationDayType type = DeviationDayType.NO_DEVIATION;

	public CableForecastDeviationDay() {
	}

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CABLE", nullable = true)
	public Cable getCable() {
		return this.cable;
	}

	public void setCable(Cable cable) {
		this.cable = cable;
	}

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name = "DAY", nullable = false, length = 7)
	public Date getDay() {
		return this.day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	@Column(name = "DEVIATION", nullable = false, precision = 18, scale = 2)
	public BigDecimal getDeviation() {
		return deviation;
	}

	public void setDeviation(BigDecimal deviation) {
		this.deviation = deviation;
	}

	@Column(name = "AVERAGE_DEVIATION", nullable = false, precision = 18, scale = 2)
	public BigDecimal getAverageDeviation() {
		return averageDeviation;
	}

	public void setAverageDeviation(BigDecimal averageDeviation) {
		this.averageDeviation = averageDeviation;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "DEVIATION_TYPE", unique = false, nullable = false, length = 16)
	public DeviationDayType getType() {
		return type;
	}

	public void setType(DeviationDayType type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (cable == null ? 0 : cable.hashCode());
		result = prime * result + (day == null ? 0 : day.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CableForecastDeviationDay)) {
			return false;
		}
		CableForecastDeviationDay other = (CableForecastDeviationDay) obj;
		if (cable == null) {
			if (other.cable != null) {
				return false;
			}
		} else if (!cable.equals(other.cable)) {
			return false;
		}
		if (day == null) {
			if (other.day != null) {
				return false;
			}
		} else if (!day.equals(other.day)) {
			return false;
		}
		return true;
	}
}
