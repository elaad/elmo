package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SC_CSP_USAGE")
public class CspUsage implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5692622920971783743L;

	private Long id;
	private CableCsp cableCsp;
	private List<CspUsageValue> cspUsageValues = new ArrayList<CspUsageValue>(0);
	private CspUsageMessage cspUsageMessage;

	public CspUsage() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABLE_CSP", nullable = false)
	public CableCsp getCableCsp() {
		return this.cableCsp;
	}

	public void setCableCsp(CableCsp cableCsp) {
		this.cableCsp = cableCsp;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cspUsage")
	public List<CspUsageValue> getCspUsageValues() {
		return this.cspUsageValues;
	}

	public void setCspUsageValues(List<CspUsageValue> cspUsageValues) {
		this.cspUsageValues = cspUsageValues;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CSP_USAGE_MESSAGE", nullable = false)
	public CspUsageMessage getCspUsageMessage() {
		return cspUsageMessage;
	}

	public void setCspUsageMessage(CspUsageMessage cspUsageMessage) {
		this.cspUsageMessage = cspUsageMessage;
	}

}
