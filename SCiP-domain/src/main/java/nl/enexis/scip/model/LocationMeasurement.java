package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import nl.enexis.scip.action.ActionTarget;
import nl.enexis.scip.model.enumeration.MeasurementType;
import nl.enexis.scip.model.enumeration.UnitOfMeasure;

@Entity
@Table(name = "SC_LOCATION_MEASUREMENT", uniqueConstraints = @UniqueConstraint(columnNames = {
		"LOCATION", "MEASUREMENT_TYPE" }))
@NamedQueries({ @NamedQuery(name = "LocationMeasurement.getAll", query = "SELECT lm FROM LocationMeasurement lm") })
public class LocationMeasurement implements java.io.Serializable, ActionTarget {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5864772744234859220L;

	private Long id;
	private Location location;
	private UnitOfMeasure uom;
	private boolean forecast;
	private MeasurementType measurementType;
	private List<CspLocationMeasurement> cspLocationMeasurements = new ArrayList<CspLocationMeasurement>(0);

	public LocationMeasurement() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "LOCATION", nullable = false)
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "UNIT_OF_MEASURE", unique = false, nullable = false, length = 10)
	public UnitOfMeasure getUom() {
		return uom;
	}

	public void setUom(UnitOfMeasure uom) {
		this.uom = uom;
	}

	@Column(name = "FORECAST", nullable = false)
	public boolean isForecast() {
		return forecast;
	}

	public void setForecast(boolean forecast) {
		this.forecast = forecast;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "MEASUREMENT_TYPE", unique = false, nullable = false, length = 20)
	public MeasurementType getMeasurementType() {
		return measurementType;
	}

	public void setMeasurementType(MeasurementType measurementType) {
		this.measurementType = measurementType;
	}
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "locationMeasurement")
	public List<CspLocationMeasurement> getCspLocationMeasurements() {
		return cspLocationMeasurements;
	}

	public void setCspLocationMeasurements(List<CspLocationMeasurement> cspLocationMeasurements) {
		this.cspLocationMeasurements = cspLocationMeasurements;
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.getLocation().getLocationId() + "-" + this.getMeasurementType().name()  + "-" + (this.isForecast() ? "F" : "O");
	}

	@Override
	@Transient
	public String getActionTargetName() {
		return this.getClass().getName();
	}

}
