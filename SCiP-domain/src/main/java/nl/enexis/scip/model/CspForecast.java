package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import nl.enexis.scip.action.ActionTarget;

@Entity
@Table(name = "SC_CSP_FORECAST")
@NamedQueries({
		@NamedQuery(name = "CspForecast.getAll", query = "SELECT cfe FROM CspForecast cfe"),
		@NamedQuery(name = "CspForecast.getByDate", query = "SELECT cf FROM CspForecast cf WHERE cf.calculatedAt BETWEEN :dateFrom AND :dateTo ORDER BY cf.id"),
		@NamedQuery(name = "CspForecast.getByCableForecast", query = "SELECT cf FROM CspForecast cf WHERE cf.cableForecast.id = :cableForecastId"),
		@NamedQuery(name = "CspForecast.getByCspForecastMessageId", query = "SELECT cf FROM CspForecastMessage cfm INNER JOIN cfm.cspForecasts cf WHERE cfm.id = :msgId"),
		@NamedQuery(name = "CspForecast.getCspForcastForCableCsp", query = "SELECT cfe FROM CspForecast cfe WHERE cfe.cableCsp = :cableCsp ORDER BY cfe.calculatedAt DESC") })
public class CspForecast implements java.io.Serializable, ActionTarget {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7962440764667505236L;

	public enum AssignmentChange {
		UP, DOWN, EQUAL
	};

	private Long id;
	private Integer numberOfBlocks;
	private CableForecast cableForecast;
	private CableCsp cableCsp;
	private Date calculatedAt;
	private AssignmentChange change = null;
	private BigDecimal changeValue;
	private CspForecastValue changeBlock;
	private List<CspForecastValue> cspForecastValues = new ArrayList<CspForecastValue>(0);

	public CspForecast() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NUMBER_OF_BLOCKS", nullable = true)
	public Integer getNumberOfBlocks() {
		return numberOfBlocks;
	}

	public void setNumberOfBlocks(Integer numberOfBlocks) {
		this.numberOfBlocks = numberOfBlocks;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABLE_FORECAST", nullable = false)
	public CableForecast getCableForecast() {
		return cableForecast;
	}

	public void setCableForecast(CableForecast cableForecast) {
		this.cableForecast = cableForecast;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CABLE_CSP", nullable = false)
	public CableCsp getCableCsp() {
		return this.cableCsp;
	}

	public void setCableCsp(CableCsp cableCsp) {
		this.cableCsp = cableCsp;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CALCULATED_AT", nullable = false)
	public Date getCalculatedAt() {
		return this.calculatedAt;
	}

	public void setCalculatedAt(Date calculatedAt) {
		this.calculatedAt = calculatedAt;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cspForecast")
	@OrderBy("id ASC")
	public List<CspForecastValue> getCspForecastValues() {
		return cspForecastValues;
	}

	public void setCspForecastValues(List<CspForecastValue> cspForecastValues) {
		this.cspForecastValues = cspForecastValues;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "CHANGE_DIRECTION", nullable = true, length = 5)
	public AssignmentChange getChange() {
		return change;
	}

	public void setChange(AssignmentChange change) {
		this.change = change;
	}

	@Column(name = "CHANGE_VALUE", nullable = true, precision = 22, scale = 2)
	public BigDecimal getChangeValue() {
		return changeValue;
	}

	public void setChangeValue(BigDecimal changeValue) {
		this.changeValue = changeValue;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANGE_BLOCK", nullable = true)
	public CspForecastValue getChangeBlock() {
		return changeBlock;
	}

	public void setChangeBlock(CspForecastValue changeBlock) {
		this.changeBlock = changeBlock;
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.cableCsp.getActionTargetId() + "-" + this.getId();
	}

	@Override
	@Transient
	public String getActionTargetName() {
		return this.getClass().getName();
	}

}
