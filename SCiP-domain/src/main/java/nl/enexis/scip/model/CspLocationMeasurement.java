package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import nl.enexis.scip.action.ActionTarget;

@Entity
@Table(name = "SC_CSP_LOCATION_MEASUREMENT")
@NamedQueries({ @NamedQuery(name = "CspLocationMeasurement.getAll", query = "SELECT clm FROM CspLocationMeasurement clm") })
public class CspLocationMeasurement implements java.io.Serializable, ActionTarget {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5864772744234859220L;

	private CspLocationMeasurementPK key;
	private LocationMeasurement locationMeasurement;
	private Csp csp;
	private boolean doSent;
	private Date sendFrom;

	public CspLocationMeasurement() {
	}

	@EmbeddedId
	public CspLocationMeasurementPK getKey() {
		return key;
	}

	public void setKey(CspLocationMeasurementPK key) {
		this.key = key;
	}

	@MapsId("locationMeasurementId")
	// references EmbeddedId's property
	@JoinColumn(name = "LOCATION_MEASUREMENT", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.EAGER)
	public LocationMeasurement getLocationMeasurement() {
		return this.locationMeasurement;
	}

	public void setLocationMeasurement(LocationMeasurement locationMeasurement) {
		this.locationMeasurement = locationMeasurement;
	}

	@MapsId("cspEan")
	// references EmbeddedId's property
	@JoinColumn(name = "CSP", referencedColumnName = "EAN13")
	@ManyToOne(fetch = FetchType.EAGER)
	public Csp getCsp() {
		return csp;
	}

	public void setCsp(Csp csp) {
		this.csp = csp;
	}

	@Column(name = "DO_SENT", nullable = true)
	public boolean isDoSent() {
		return doSent;
	}

	public void setDoSent(boolean doSent) {
		this.doSent = doSent;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SEND_FROM", nullable = true)
	public Date getSendFrom() {
		return sendFrom;
	}

	public void setSendFrom(Date sendFrom) {
		this.sendFrom = sendFrom;
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.csp.getEan13() + "-" + this.locationMeasurement.getActionTargetId();
	}

	@Override
	@Transient
	public String getActionTargetName() {
		return this.getClass().getName();
	}

}
