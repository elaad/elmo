package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SC_CSP_GET_FORECAST_MESSAGE")
@NamedQueries({
		@NamedQuery(name = "CspGetForecastMessage.getAll", query = "SELECT cgfme FROM CspGetForecastMessage cgfme"),
		@NamedQuery(name = "CspGetForecastMessage.findByEventId", query = "SELECT cgfme FROM CspGetForecastMessage cgfme WHERE cgfme.eventId = :eventId") })
public class CspGetForecastMessage extends Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7388118511611882753L;

	public CspGetForecastMessage() {
	}

	@Override
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return super.getId();
	}

	@Override
	public void setId(Long id) {
		super.setId(id);
	}

	@Override
	@Column(name = "EVENT_ID", unique = true, nullable = false, length = 50)
	public String getEventId() {
		return super.getEventId();
	}

	@Override
	public void setEventId(String eventId) {
		super.setEventId(eventId);
	}

	@Override
	@Column(name = "CSP", unique = false, nullable = false, length = 13)
	public String getCsp() {
		return super.getCsp();
	}

	@Override
	public void setCsp(String csp) {
		super.setCsp(csp);
	}

	@Override
	@Column(name = "DSO", unique = false, nullable = false, length = 13)
	public String getDso() {
		return super.getDso();
	}

	@Override
	public void setDso(String dso) {
		super.setDso(dso);
	}

	@Override
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE_TIME", nullable = false)
	public Date getDateTime() {
		return super.getDateTime();
	}

	@Override
	public void setDateTime(Date dateTime) {
		super.setDateTime(dateTime);
	}

	@Override
	@Column(name = "PRIORITY", unique = false, nullable = false, precision = 22, scale = 0)
	public int getPriority() {
		return super.getPriority();
	}

	@Override
	public void setPriority(int priority) {
		super.setPriority(priority);
	}

	@Override
	@Column(name = "SUCCESS", nullable = true)
	public boolean isSuccessful() {
		return super.isSuccessful();
	}

	@Override
	public void setSuccessful(boolean isSuccessful) {
		super.setSuccessful(isSuccessful);
	}

}
