package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

@Entity
@Table(name = "SC_CSP", uniqueConstraints = @UniqueConstraint(columnNames = "EAN13"))
@NamedQueries({ 
	@NamedQuery(name = "Csp.getAll", query = "SELECT c FROM Csp c"),
	@NamedQuery(name = "Csp.getByCableId", query = "SELECT cc.csp FROM CableCsp cc WHERE cc.cable.cableId = :cableId"),
})
public class Csp extends ActiveTopologyEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -411721892076990567L;

	private String ean13;
	private String name;
	private String endpoint;
	private String protocolVersion;
	private List<DsoCsp> dsoCsps = new ArrayList<DsoCsp>(0);
	private List<CableCsp> cableCsps = new ArrayList<CableCsp>(0);

	public Csp() {
	}

	@Id
	@XmlID
	@XmlAttribute
	@NotEmpty(message = "EAN13 is required")
	@Size(min = 13, max = 13, message = "EAN13 has to be 13 digits")
	@Column(name = "EAN13", unique = true, nullable = false, length = 13)
	public String getEan13() {
		return this.ean13;
	}

	public void setEan13(String ean13) {
		this.ean13 = ean13;
	}

	@NotEmpty(message = "Name is required")
	@Size(max = 100, message = "Name cannot be more then 100 characters")
	@Column(name = "NAME", nullable = false, length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Size(max = 255, message = "End Point URL cannot be more then {max} characters long")
	@URL(message = "End point URL needs to have a valid URL syntax")
	// TODO Cross field check: //Active=true AND @NotEmpty: then
	// "End point URL needs to be configured" RDL TODO
	@Column(name = "ENDPOINT", nullable = true, length = 255)
	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	@Size(max = 5, message = "OSCP portocol version cannot be more then {max} characters long")
	@Pattern(regexp = "^\\d{1,2}\\.\\d{1,2}$", message = "OSCP portocol version must have format '99.99'. E.g.: '1.2' or '0.12'")
	// TODO Cross field check: //Active=true AND @NotEmpty: then
	// "OSCP portocol version needs to be configured" RDL TODO
	@Column(name = "PROTOCOL_VERSION", nullable = true, length = 5)
	public String getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "csp")
	public List<DsoCsp> getDsoCsps() {
		return dsoCsps;
	}

	public void setDsoCsps(List<DsoCsp> dsoCsps) {
		this.dsoCsps = dsoCsps;
	}

	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "csp")
	public List<CableCsp> getCableCsps() {
		return this.cableCsps;
	}

	public void setCableCsps(List<CableCsp> cableCsps) {
		this.cableCsps = cableCsps;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ean13 == null ? 0 : ean13.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Csp)) {
			return false;
		}
		Csp other = (Csp) obj;
		if (ean13 == null) {
			if (other.ean13 != null) {
				return false;
			}
		} else if (!ean13.equals(other.ean13)) {
			return false;
		}
		return true;
	}

	@Transient
	@Override
	public String getRecordKey() {
		return this.ean13;
	}

	@Override
	public String toString() {
		return "Csp (ean13=" + ean13 + ", name=" + name + ")";
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.getEan13();
	}

}
