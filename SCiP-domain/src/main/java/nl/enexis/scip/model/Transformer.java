package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "SC_TRANSFORMER", uniqueConstraints = @UniqueConstraint(columnNames = "TRAFO_ID"))
@NamedQueries({ @NamedQuery(name = "Transformer.getAll", query = "SELECT t FROM Transformer t") })
public class Transformer extends ActiveTopologyEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6902758432342349730L;

	private String trafoId;
	private Date lastMeasurement;
	private List<Meter> meters = new ArrayList<Meter>(0);
	private Cable cable;
	private Date changedDate;
	private MeasurementProvider measurementprovider;

	public Transformer() {
	}

	@Id
	@XmlID
    @XmlAttribute 
    @NotEmpty(message = "Trafo ID is required")
    @Size(max = 20, message = "Trafo ID cannot be more then {max} characters long")
	@Column(name = "TRAFO_ID", unique = true, nullable = false, length = 20)
	public String getTrafoId() {
		return this.trafoId;
	}

	public void setTrafoId(String trafoId) {
		this.trafoId = trafoId;
	}

	@XmlIDREF
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "transformer")
	public Cable getCable() {
		return this.cable;
	}

	public void setCable(Cable cable) {
		this.cable = cable;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_MEASUREMENT", nullable = true)
	public Date getLastMeasurement() {
		return lastMeasurement;
	}

	public void setLastMeasurement(Date lastMeasurement) {
		this.lastMeasurement = lastMeasurement;
	}

	@XmlTransient
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "transformer")
	public List<Meter> getMeters() {
		return meters;
	}

	public void setMeters(List<Meter> meters) {
		this.meters = meters;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "MEASUREMENT_PROVIDER", nullable = false)
	public MeasurementProvider getMeasurementProvider() {
		return measurementprovider;
	}

	public void setMeasurementProvider(MeasurementProvider measurementprovider) {
		this.measurementprovider = measurementprovider;
	}

	@Transient
	public int getActiveMetersCount() {
		int count = 0;
		for (Meter meter : this.getMeters()) {
			if (meter.isActive()) {
				count++;
			}
		}
		return count;
	}

    @Transient
    @Override
    public String getRecordKey() {
        return this.trafoId;
    }
    
	@Override
	@Transient
	public String getActionTargetId() {
		return this.getTrafoId();
	}

	@Override
	public String toString() {
		return "Transformer (trafoId=" + trafoId + ", lastMeasurement=" + lastMeasurement + ", active=" + active + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (trafoId == null ? 0 : trafoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Transformer)) {
			return false;
		}
		Transformer other = (Transformer) obj;
		if (trafoId == null) {
			if (other.trafoId != null) {
				return false;
			}
		} else if (!trafoId.equals(other.trafoId)) {
			return false;
		}
		return true;
	}

}
