package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;

import nl.enexis.scip.model.enumeration.MeasuresFor;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "SC_METER", uniqueConstraints = @UniqueConstraint(columnNames = "METER_ID"))
@NamedQueries({ @NamedQuery(name = "Meter.getAll", query = "SELECT m FROM Meter m") })
public class Meter extends ActiveTopologyEntity implements java.io.Serializable {

	private static final long serialVersionUID = -4639023892962381737L;

	private String meterId;
	private String meterName;
	private MeasuresFor measures;
	private Transformer transformer;

	public Meter() {
	}

	@Id
    @XmlID
    @XmlAttribute 
    @NotEmpty(message = "Meter ID is required")
    @Size(max = 20, message = "Meter ID cannot be more then {max} characters long")
	@Column(name = "METER_ID", unique = true, nullable = false, length = 20)
	public String getMeterId() {
		return this.meterId;
	}

	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

    @NotEmpty(message="Name is required")
    @Size(max = 100, message = "Name cannot be more then {max} characters long")
	@Column(name = "METER_NAME", unique = false, nullable = true, length = 100)
	public String getMeterName() {
		return this.meterName;
	}

	public void setMeterName(String meterName) {
		this.meterName = meterName;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "MEASURES", unique = false, nullable = false, length = 8)
	public MeasuresFor getMeasures() {
		return this.measures;
	}

	public void setMeasures(MeasuresFor measures) {
		this.measures = measures;
	}
	
    //TODO: workaround for Java 1.7 JAXB bug: https://java.net/jira/browse/JAXB-870, replace @XmlTransient with @XmlIDREF
    @XmlTransient       
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TRANSFORMER", nullable = false)
	public Transformer getTransformer() {
		return this.transformer;
	}

	public void setTransformer(Transformer transformer) {
		this.transformer = transformer;
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.getMeterId();
	}

	@Override
	public String toString() {
		return "Meter (meterId=" + meterId + ", meterName=" + meterName + ", measures=" + measures + ", active="
				+ active + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (meterId == null ? 0 : meterId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Meter)) {
			return false;
		}
		Meter other = (Meter) obj;
		if (meterId == null) {
			if (other.meterId != null) {
				return false;
			}
		} else if (!meterId.equals(other.meterId)) {
			return false;
		}
		return true;
	}

    @Transient
    @Override
    public String getRecordKey() {
        return this.meterId;
    }
}
