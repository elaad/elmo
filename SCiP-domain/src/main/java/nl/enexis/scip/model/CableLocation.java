package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import nl.enexis.scip.util.xml.LongXmlAdapter;

@Entity
@Table(name = "SC_CABLE_LOCATION", uniqueConstraints = @UniqueConstraint(columnNames = {
		"CABLE", "LOCATION" }))
@NamedQueries({ @NamedQuery(name = "CableLocation.getAll", query = "SELECT cl FROM CableLocation cl") })
public class CableLocation implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5864772744234859220L;

	private Long id;
	private Location location;
	private Cable cable;
	private Date addedDate;
	private Date changedDate;

	public CableLocation() {
	}

	@Id
    @XmlID
    @XmlJavaTypeAdapter(type = long.class, value = LongXmlAdapter.class)
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "LOCATION", nullable = false)
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

    @XmlAttribute @XmlIDREF
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CABLE", nullable = false)
	public Cable getCable() {
		return this.cable;
	}

	public void setCable(Cable cable) {
		this.cable = cable;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ADDED_AT", nullable = false)
	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHANGED_AT", nullable = false)
	public Date getChangedDate() {
		return changedDate;
	}

	public void setChangedDate(Date changedDate) {
		this.changedDate = changedDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.getCable() == null) ? 0 : this.getCable().hashCode());
		result = prime * result + ((this.getLocation() == null) ? 0 : this.getLocation().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CableLocation)) {
			return false;
		}
		CableLocation other = (CableLocation) obj;
		if (this.getCable() == null) {
			if (other.getCable() != null) {
				return false;
			}
		} else if (!this.getCable().equals(other.getCable())) {
			return false;
		}
		if (this.getLocation() == null) {
			if (other.getLocation() != null) {
				return false;
			}
		} else if (!this.getLocation().equals(other.getLocation())) {
			return false;
		}
		return true;
	}
}
