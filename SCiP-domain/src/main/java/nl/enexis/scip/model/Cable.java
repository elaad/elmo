package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlTransient;

import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.model.enumeration.UsageMeasurementResponsible;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "SC_CABLE", uniqueConstraints = @UniqueConstraint(columnNames = "CABLE_ID"))
@NamedQueries({ @NamedQuery(name = "Cable.getAll", query = "SELECT c FROM Cable c"),
		@NamedQuery(name = "Cable.getByCspEan13", query = "SELECT c.cable FROM CableCsp c WHERE c.csp.ean13 = :ean13") })
public class Cable extends ActiveTopologyEntity implements java.io.Serializable, Cloneable {

	/**
	 *     
	 */
	private static final long serialVersionUID = -8805952726478739078L;

	private String cableId;
	private BigDecimal safetyFactor;
	private BigDecimal remainingFactor;
	private BigDecimal maxCapacity;
	private BigDecimal maxCarCapacity;
	private Date lastCspCapacityChange;
	private Dso dso;
	private Transformer transformer;
	private UsageMeasurementResponsible usageMeasurementResponsible;
	private List<CableCsp> cableCsps = new ArrayList<CableCsp>(0);
	private List<CableLocation> cableLocations = new ArrayList<CableLocation>(0);
	private BehaviourType behaviour;

	// for testing
	private boolean fixedForecast;
	private BigDecimal fixedForecastMax = new BigDecimal(0);
	private BigDecimal fixedForecastDeviation = new BigDecimal(0);
	private boolean fixedForecastRandom;
	private String fixedForecastDeviationBlocks;

	public Cable() {
	}

	@Id
	@XmlID
	@XmlAttribute
	@NotEmpty(message = "Cable ID is required")
	@Size(max = 20, message = "Cable ID cannot be more then {max} characters long")
	@Column(name = "CABLE_ID", unique = true, nullable = false, length = 20)
	public String getCableId() {
		return this.cableId;
	}

	public void setCableId(String cableId) {
		this.cableId = cableId;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "BEHAVIOUR", unique = false, nullable = false, length = 20)
	public BehaviourType getBehaviour() {
		return behaviour;
	}

	public void setBehaviour(BehaviourType behaviour) {
		this.behaviour = behaviour;
	}

	@NotNull(message = "Safety factor is required")
	@Range(min = 0, max = 1, message = "Safety factor should be between 0 and 1")
	@Column(name = "SAFETY_FACTOR", unique = false, nullable = false, precision = 18, scale = 2)
	public BigDecimal getSafetyFactor() {
		return safetyFactor;
	}

	public void setSafetyFactor(BigDecimal safetyFactor) {
		this.safetyFactor = safetyFactor;
	}

	@NotNull(message = "Remaining factor is required")
	@Range(min = 0, max = 1, message = "Remaining factor should be between 0 and 1")
	@Column(name = "REMAINING_FACTOR", unique = false, nullable = false, precision = 18, scale = 2)
	public BigDecimal getRemainingFactor() {
		return remainingFactor;
	}

	public void setRemainingFactor(BigDecimal remainingFactor) {
		this.remainingFactor = remainingFactor;
	}

	@NotNull(message = "Maximum capacity is required")
	@Min(value = 0, message = "Maximum capacity should be more then 0")
	@Column(name = "MAX_CAPACITY", unique = false, nullable = false, precision = 18, scale = 2)
	public BigDecimal getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(BigDecimal maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	@NotNull(message = "Maximum car capacity is required")
	@Min(value = 0, message = "Maximum car capacity should be more then 0")
	@Column(name = "MAX_CAR_CAPACITY", unique = false, nullable = false, precision = 18, scale = 2)
	public BigDecimal getMaxCarCapacity() {
		return maxCarCapacity;
	}

	public void setMaxCarCapacity(BigDecimal maxCarCapacity) {
		this.maxCarCapacity = maxCarCapacity;
	}

	@XmlIDREF
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DSO", nullable = false)
	public Dso getDso() {
		return dso;
	}

	public void setDso(Dso dso) {
		this.dso = dso;
	}

	@XmlIDREF
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TRANSFORMER", nullable = false)
	public Transformer getTransformer() {
		return this.transformer;
	}

	public void setTransformer(Transformer transformer) {
		this.transformer = transformer;
	}

	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cable")
	public List<CableCsp> getCableCsps() {
		return this.cableCsps;
	}

	public void setCableCsps(List<CableCsp> cableCsps) {
		this.cableCsps = cableCsps;
	}

	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cable")
	public List<CableLocation> getCableLocations() {
		return this.cableLocations;
	}

	public void setCableLocations(List<CableLocation> cableLocations) {
		this.cableLocations = cableLocations;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "USAGE_MEASURED_BY", unique = false, nullable = false, length = 8)
	public UsageMeasurementResponsible getUsageMeasurementResponsible() {
		return usageMeasurementResponsible;
	}

	public void setUsageMeasurementResponsible(UsageMeasurementResponsible usageMeasurementResponsible) {
		this.usageMeasurementResponsible = usageMeasurementResponsible;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CSP_CAPACITY_CHANGED_AT", nullable = false)
	public Date getCspLastCapacityChange() {
		return lastCspCapacityChange;
	}

	public void setCspLastCapacityChange(Date changedDate) {
		this.lastCspCapacityChange = changedDate;
	}

	/*
	 * Not only for test purposes anymore  but also used as fall-back when no forecast can be calculated
	 */
	@Column(name = "FIXED_FORECAST", nullable = false)
	public boolean isFixedForecast() {
		return fixedForecast;
	}

	public void setFixedForecast(boolean fixedForecast) {
		this.fixedForecast = fixedForecast;
	}

	@NotNull(message = "Fixed forcast max is required")
	@Min(value = 0, message = "Fixed forecast max should be bigger than 0")
	@Column(name = "FIXED_FORECAST_MAX", unique = false, nullable = true, precision = 18, scale = 2)
	public BigDecimal getFixedForecastMax() {
		return fixedForecastMax;
	}

	public void setFixedForecastMax(BigDecimal fixedForecastMax) {
		this.fixedForecastMax = fixedForecastMax;
	}

	@NotNull(message = "Fixed forecast deviation is required")
	@Min(value = 0, message = "Fixed forecast deviation should be bigger than or equal to 0")
	@Column(name = "FIXED_FORECAST_DEVIATION", unique = false, nullable = true, precision = 18, scale = 2)
	public BigDecimal getFixedForecastDeviation() {
		return fixedForecastDeviation;
	}

	public void setFixedForecastDeviation(BigDecimal fixedForecastDeviation) {
		this.fixedForecastDeviation = fixedForecastDeviation;
	}

	@Column(name = "FIXED_FORECAST_RANDOM", nullable = true)
	public boolean isFixedForecastRandom() {
		return fixedForecastRandom;
	}

	public void setFixedForecastRandom(boolean fixedForecastRandom) {
		this.fixedForecastRandom = fixedForecastRandom;
	}

	@Column(name = "FIXED_FORECAST_DEVIATION_BLOCKS", unique = false, nullable = true, length = 20)
	public String getFixedForecastDeviationBlocks() {
		return fixedForecastDeviationBlocks;
	}

	public void setFixedForecastDeviationBlocks(String fixedForecastDeviationBlocks) {
		this.fixedForecastDeviationBlocks = fixedForecastDeviationBlocks;
	}

	@Transient
	@Override
	public String getRecordKey() {
		return this.cableId;
	}

	@Transient
	@XmlTransient
	@AssertTrue(message = "Fixed forecast max should be bigger than 0")
	public boolean isFixedForecastSetCorrectly() {
//		if (!isFixedForecast()) {
//			return true;
//		}
//		if (isFixedForecast() && getFixedForecastMax().compareTo(new BigDecimal(0)) > 0) {
		if (this.getFixedForecastMax().compareTo(new BigDecimal(0)) > 0) {
			return true;
		}

		return false;
	}

	@Transient
	@XmlTransient
	@AssertTrue(message = "Maximum car capacity cannot be more than the maximum capacity")
	public boolean isMaximumCarCapacityNotMoreThanMaximumCapacity() {
		if (this.getMaxCarCapacity().compareTo(this.getMaxCapacity()) <= 0) {
			return true;
		}

		return false;
	}

	@Transient
	@XmlTransient
	@AssertTrue(message = "Fixed forcast deviation cannot be more than the Fixed forecast maximum")
	public boolean isFixedForecastDeviationNotMoreThanFixedForecastMaximum() {
		if (this.getFixedForecastDeviation().compareTo(this.getFixedForecastMax()) <= 0) {
			return true;
		}

		return false;
	}

	@Transient
	@XmlTransient
	@AssertTrue(message = "Fixed forcast maximum cannot be more than maximum car capacity.")
	public boolean isFixedForecastMaximumNotMoreThanMaxCarCapacity() {
		// this check return true if max car capacity == 0, only check when max
		// car capacity is set
		if (this.getMaxCarCapacity().compareTo(new BigDecimal(0)) == 0) {
			return true;
		}
		if (this.getMaxCarCapacity().compareTo(new BigDecimal(0)) > 0
				&& this.getFixedForecastMax().compareTo(this.getMaxCarCapacity()) <= 0) {
			return true;
		}

		return false;
	}

	@Transient
	@XmlTransient
	@AssertTrue(message = "Fixed forcast maximum cannot be more than maximum capacity")
	public boolean isFixedForecastMaximumNotMoreThanMaxCapacity() {
		// this check return true if max car capacity > 0, only check when max
		// car capacity is not set
		if (this.getMaxCarCapacity().compareTo(new BigDecimal(0)) > 0) {
			return true;
		}
		if (this.getMaxCarCapacity().compareTo(new BigDecimal(0)) == 0
				&& this.getFixedForecastMax().compareTo(this.getMaxCapacity()) <= 0) {
			return true;
		}

		return false;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	@Transient
	public String getActionTargetId() {
		return this.getCableId();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (cableId == null ? 0 : cableId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Cable)) {
			return false;
		}
		Cable other = (Cable) obj;
		if (cableId == null) {
			if (other.cableId != null) {
				return false;
			}
		} else if (!cableId.equals(other.cableId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Cable [cableId=" + cableId + "]";
	}

}
