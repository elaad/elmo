package nl.enexis.scip.interceptor;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import nl.enexis.scip.action.ActionContext;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.interceptor.annotation.Log;
import nl.enexis.scip.interceptor.annotation.enumeration.LogLevel;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

@Interceptor
@Log
public class LoggingInterceptor implements Serializable {

	private static final long serialVersionUID = -739823315771192726L;

	@AroundInvoke
	public Object logInvocation(InvocationContext ctx) throws Exception {
		Log log = ctx.getMethod().getAnnotation(Log.class);
		Level level = Level.INFO;
		if (log != null) {
			if (log.logLevel().equals(LogLevel.DEBUG)) {
				level = Level.DEBUG;
			} else if (log.logLevel().equals(LogLevel.TRACE)) {
				level = Level.TRACE;
			} 
		}
		return this.log(ctx, level, log == null ? false : log.logParams(), log == null ? false : log.logStackTrace());
	}

	private Object log(InvocationContext ctx, Level logLevel, boolean doLogParams, boolean doLogStackTrace) throws Exception {
		Action action = ActionContext.getAction();
		String actionId = action.getId();
		String event = action.getType().toString();
		String actor = action.getActor();
		String methodName = ctx.getMethod().getName();

		@SuppressWarnings("rawtypes")
		Class clazz = ctx.getMethod().getDeclaringClass();

		Logger log = Logger.getLogger(clazz);

		if (log.isEnabled(logLevel)) {
			log.log(clazz.getName(), logLevel, "[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]", new Object[] { methodName, actionId, event, actor,
					">>> ENTERING" }, null);

			if (doLogParams) {
				Object[] params = ctx.getParameters();
				for (int i = 0; i < params.length; i++) {
					Object object = params[i];
					log.log(clazz.getName(), logLevel, "[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}] [{5}] [{6}] [{7}]", new Object[] {
							methodName, actionId, event, actor, ">>> IN", i, object, object.getClass().getName() }, null);
				}
			}
		}

		try {

			Object result = ctx.proceed();

			if (log.isEnabled(logLevel)) {
				if (doLogParams && result != null) {
					log.log(clazz.getName(), logLevel, "[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}] [{5}] [{6}]", new Object[] { methodName,
							actionId, event, actor, "<<< OUT", result, result.getClass().getName() }, null);

				}
				log.log(clazz.getName(), logLevel, "[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]", new Object[] { methodName, actionId, event,
						actor, "<<< EXITING" }, null);
			}

			return result;
		} catch (Exception e) {
			if (log.isEnabled(Level.ERROR)) {
				log.log(clazz.getName(), Level.ERROR, "[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}] [{5}]", new Object[] { methodName, actionId,
						event, actor, "<<< EXCEPTION", e.getMessage() }, doLogStackTrace ? e : null);
			}

			throw e;
		}
	}

}