package nl.enexis.scip.action.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.model.ActionMessage.Level;

@Entity
@Table(name = "SC_ACTION")
@NamedQueries({
	@NamedQuery(name = "Action.getActionEventTurnaroundTimes", 
query = "SELECT a.type, MAX(a.end) FROM Action a WHERE a.type <> nl.enexis.scip.action.ActionEvent.TIMER AND a.end IS NOT NULL AND a.start BETWEEN :fromDate AND :toDate GROUP BY a.type"),
	@NamedQuery(name = "Action.getLastSuccessSendLocationMeasurmentForCsps", query = "SELECT at.value, MAX(a.end) as lastSuccessDate FROM Action a JOIN a.targets at WHERE a.type = nl.enexis.scip.action.ActionEvent.SEND_LOCATION_MEASUREMENTS AND a.success = true AND at.direction = nl.enexis.scip.action.ActionTargetInOut.IN AND at.type = 'nl.enexis.scip.model.CspLocationMeasurement' GROUP BY at.value ORDER BY lastSuccessDate"),
		@NamedQuery(name = "Action.getLastHeartbeatActionDsoCsp", query = "SELECT a FROM Action a JOIN a.targets at WHERE a.type = nl.enexis.scip.action.ActionEvent.HEARTBEAT AND a.end IS NOT NULL AND at.direction = nl.enexis.scip.action.ActionTargetInOut.IN AND at.type = 'nl.enexis.scip.model.DsoCsp' AND at.value = :dsoCspTargetId ORDER BY a.start DESC"),
		@NamedQuery(name = "Action.getDistinctActors", query = "SELECT DISTINCT a.actor FROM Action a ORDER BY a.actor ASC"),
		@NamedQuery(name = "Action.getPeriodErrorCountsPerEvent", query = "SELECT a.type, COUNT(a) as counted FROM Action a WHERE a.success = :success AND a.end IS NOT NULL AND a.start BETWEEN :from AND :to GROUP BY a.type ORDER BY counted DESC"),
		@NamedQuery(name = "Action.getNotificationEvents", query = "SELECT DISTINCT a FROM Action a LEFT JOIN a.messages am WHERE a.type = :event AND a.start BETWEEN :from AND :to AND am.level IN :levels ORDER BY a.start ASC") })
public class Action implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -832134442895080341L;

	private String id = UUID.randomUUID().toString();
	private Date start = new Date();
	private Date end;
	private ActionEvent type = ActionEvent.UNKNOWN;
	private boolean success = false;
	private String actor = "UNKNOWN";
	private Action parent;
	private List<ActionTarget> targets = new ArrayList<ActionTarget>(0);
	private List<ActionMessage> messages = new ArrayList<ActionMessage>(0);
	private boolean doDirectNotification = true;

	public Action() {
	}

	@Transient
	public boolean containsErrors() {
		if (!this.isSuccess()) {
			return true;
		} else {
			for (ActionMessage message : this.getMessages()) {
				if (Level.ERROR.equals(message.getLevel())) {
					return true;
				}
			}
		}
		return false;
	}

	@Transient
	public boolean containsWarnings(boolean includeErrors) {
		if (includeErrors && this.containsErrors()) {
			return true;
		} else {
			for (ActionMessage message : this.getMessages()) {
				if (Level.WARN.equals(message.getLevel())) {
					return true;
				}
			}
		}

		return false;
	}

	@Id
	@Column(name = "ID", nullable = false, length = 50)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START", nullable = false)
	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "END", nullable = true)
	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "EVENT", nullable = false, length = 30)
	public ActionEvent getType() {
		return type;
	}

	public void setType(ActionEvent type) {
		this.type = type;
	}

	@Column(name = "SUCCESS", nullable = false)
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	@Column(name = "ACTOR", nullable = false, length = 20)
	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "PARENT")
	public Action getParent() {
		return parent;
	}

	public void setParent(Action parent) {
		this.parent = parent;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "action", cascade = { CascadeType.ALL })
	public List<ActionTarget> getTargets() {
		return targets;
	}

	public void setTargets(List<ActionTarget> targets) {
		this.targets = targets;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "action", cascade = { CascadeType.ALL })
	public List<ActionMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<ActionMessage> messages) {
		this.messages = messages;
	}

	@Transient
	public boolean doDirectNotification() {
		return doDirectNotification;
	}

	@Transient
	public void setDirectNotification(boolean doDirectNotification) {
		this.doDirectNotification = doDirectNotification;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (id == null ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Action)) {
			return false;
		}
		Action other = (Action) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}
