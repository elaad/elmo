package nl.enexis.scip.action.model.id;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;

import nl.enexis.scip.action.model.ActionMessage;

public class ActionMessageTargetId  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3031241493607757786L;
	
	private Integer number;
	private ActionMessage actionMessage;

	public ActionMessageTargetId() {
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public ActionMessage getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(ActionMessage actionMessage) {
		this.actionMessage = actionMessage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (actionMessage == null ? 0 : actionMessage.hashCode());
		result = prime * result + (number == null ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ActionMessageTargetId)) {
			return false;
		}
		ActionMessageTargetId other = (ActionMessageTargetId) obj;
		if (actionMessage == null) {
			if (other.actionMessage != null) {
				return false;
			}
		} else if (!actionMessage.equals(other.actionMessage)) {
			return false;
		}
		if (number == null) {
			if (other.number != null) {
				return false;
			}
		} else if (!number.equals(other.number)) {
			return false;
		}
		return true;
	}

}