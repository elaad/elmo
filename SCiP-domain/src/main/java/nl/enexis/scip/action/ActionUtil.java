package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessageTarget;

import org.apache.commons.lang3.exception.ExceptionUtils;

public class ActionUtil {

	public static ActionMessage setActionMessageExceptionDetails(ActionMessage msg, Throwable e) {
		if (e != null) {
			String message = msg.getMessage();

			if (e instanceof ActionException) {
				ActionException ex = (ActionException) e;
				if (message == null || message.trim().length() == 0) {
					msg.setMessage(ex.getMessage());
				}
				msg.setExceptionType(ex.getType());
			} else if (e instanceof SecurityException) { //TODO: similar to equals else
				if (message == null || message.trim().length() == 0) {
					msg.setMessage(ActionExceptionType.UNEXPECTED.toString());
				}
				msg.setExceptionType(ActionExceptionType.UNEXPECTED);
			} else {
				if (message == null || message.trim().length() == 0) {
					msg.setMessage(ActionExceptionType.UNEXPECTED.toString());
				}
				msg.setExceptionType(ActionExceptionType.UNEXPECTED);
			}

			msg.setCause(e.getClass().getName());
			msg.setCauseMessage(e.getMessage());

			Throwable rootCause = ExceptionUtils.getRootCause(e);
			if (rootCause != null) {
				msg.setRootCause(rootCause.getClass().getName());
				msg.setRootCauseMessage(rootCause.getMessage());
			}
		}

		return msg;

	}

	public static ActionMessage setActionMessageTarget(ActionMessage msg, ActionTarget targetObject) {

		ActionMessageTarget target = new ActionMessageTarget();

		target.setActionMessage(msg);
		target.setType(targetObject.getActionTargetName());
		target.setValue(targetObject.getActionTargetId());

		msg.getMessageTargets().add(target);
		target.setNumber(msg.getMessageTargets().size());
		
		return msg;
	}

	public static Action setActionTarget(Action action, ActionTarget targetObject, ActionTargetInOut inOut) {

		nl.enexis.scip.action.model.ActionTarget target = new nl.enexis.scip.action.model.ActionTarget();

		target.setAction(action);
		target.setDirection(inOut);
		target.setType(targetObject.getActionTargetName());
		target.setValue(targetObject.getActionTargetId());
		
		action.getTargets().add(target);
		target.setNumber(action.getTargets().size());

		return action;
	}
}
