package nl.enexis.scip.action.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import nl.enexis.scip.action.model.id.ActionMessageTargetId;

@Entity
@Table(name = "SC_ACTION_MESSAGE_TARGET")
@NamedQueries({ @NamedQuery(name = "ActionMessageTarget.getDistinctTypes", query = "SELECT DISTINCT amt.type FROM ActionMessageTarget amt") })
@IdClass(ActionMessageTargetId.class)
public class ActionMessageTarget implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8241310932684850175L;
	
	
	private Integer number;
	private ActionMessage actionMessage;
	private String type;
	private String value;
	
	public ActionMessageTarget() {
	}
	
	@Id
	@Column(name = "NUMBER", nullable = false)
	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "MSG_NUMBER", referencedColumnName = "NUMBER"),
			@JoinColumn(name = "ACTION", referencedColumnName = "ACTION") })
	public ActionMessage getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(ActionMessage actionMessage) {
		this.actionMessage = actionMessage;
	}

	@Column(name = "TYPE", nullable = false, length = 256)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "VALUE", nullable = false, length = 1024)
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}