package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


public enum ActionEvent implements ActionTarget {
	UNKNOWN, DASHBOARD, FETCH_LOCATION_MEASUREMENTS, SEND_LOCATION_MEASUREMENTS, CSP_FORECAST_DIVISION, CALCULATE_FORECAST, PREPARE_FORECAST_INPUT, USER_MANAGEMENT, USER_PROFILE, DEVIATION_DAY_DETERMINATION, CSP_DEVIATION_DETERMINATION, FETCH_MEASUREMENTS, RECEIVE_MEASUREMENTS, SEND_CAPACITY_FORECAST, UPDATE_AGGREGATED_USAGE, GET_CAPACITY_FORECAST, REQUEST_ADJUSTED_CAPACITY, HEARTBEAT, TIMER, CONFIGURATION, TOPOLOGY, NOTIFICATION;

	@Override
	public String getActionTargetId() {
		return this.name();
	}

	@Override
	public String getActionTargetName() {
		return this.getClass().getName();
	}
}