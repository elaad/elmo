package nl.enexis.scip.action.interceptor;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.AroundInvoke;
import javax.interceptor.AroundTimeout;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.ActionTarget;
import nl.enexis.scip.action.ActionTargetInOut;
import nl.enexis.scip.action.ActionUtil;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.service.ActionContextService;
import nl.enexis.scip.service.MailService;
import nl.enexis.scip.timer.TimerInterface;

@Interceptor
@ActionContext
public class ActionContextInterceptor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7141983950461767935L;

	@Inject
	@Named("actionContextService")
	transient ActionContextService actionContextService;

	@Inject
	transient MailService mailService;

	@AroundTimeout
	@AroundInvoke
	public Object handleActionContext(InvocationContext ctx) throws Exception {

		Action action = nl.enexis.scip.action.ActionContext.getAction();

		ActionContext setActionContext = ctx.getMethod().getAnnotation(ActionContext.class);
		action.setType(setActionContext.event());
		action.setDirectNotification(setActionContext.doDirectNotification());

		if (ctx.getTarget() instanceof TimerInterface) {
			ActionEvent timerEvent = ((TimerInterface) ctx.getTarget()).getTimerEvent();
			ActionUtil.setActionTarget(action, timerEvent, ActionTargetInOut.IN);
		}

		Object[] params = ctx.getParameters();
		for (Object object : params) {
			if (object instanceof ActionTarget) {
				ActionUtil.setActionTarget(action, (ActionTarget) object, ActionTargetInOut.IN);
			}
		}

		ActionLogger.info("Event started", new Object[] { ctx }, null, true);

		actionContextService.saveOrUpdateActionContext();

		return this.proceedInvocationContext(ctx, action, setActionContext);
	}
	
	private Object proceedInvocationContext(InvocationContext ctx, Action action, ActionContext setActionContext) throws Exception {
		Object result = null;
		try {
			result = ctx.proceed();

			action = nl.enexis.scip.action.ActionContext.getAction();
			action.setSuccess(true);

			if (result instanceof ActionTarget) {
				ActionUtil.setActionTarget(action, (ActionTarget) result, ActionTargetInOut.OUT);
			}

			List<ActionMessage> actionMessages = action.getMessages();
			for (ActionMessage actionMessage : actionMessages) {
				if (nl.enexis.scip.action.model.ActionMessage.Level.ERROR.equals(actionMessage.getLevel())) {
					action.setSuccess(false);
					break;
				}
			}
		} catch (Throwable e) {
			action = nl.enexis.scip.action.ActionContext.getAction();
			action.setSuccess(false);

			List<Object> msgParams = new ArrayList<Object>();
			msgParams.add(ctx);
			if (e instanceof ActionException) {
				msgParams.addAll(((ActionException) e).getTargets());
			}

			ActionLogger.error("Action exception: " + e.getMessage(), msgParams.toArray(), e, true);

			if (setActionContext.doRethrow()) {
				throw e;
			}
		} finally {
			ActionLogger.info("Event ended", new Object[] { ctx }, null, true);

			action = nl.enexis.scip.action.ActionContext.getAction();
			action.setEnd(new Date());

			if (action.doDirectNotification()) {
				// call notification
				try {
					mailService.sendDirectActionNotification(action);
				} catch (Exception e) {
					ActionLogger.error("Notification exception: " + e.getMessage(), new Object[] { ctx }, e, true);
				}
			}

			actionContextService.saveOrUpdateActionContext();

			nl.enexis.scip.action.ActionContext.removeAction();
		}
		return result;
	}
}