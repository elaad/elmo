package nl.enexis.scip.action.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.model.id.ActionMessageId;

@Entity
@Table(name = "SC_ACTION_MESSAGE")
@NamedQueries({
		@NamedQuery(name = "ActionMessage.getConnectivityErrorCountsPerParty", query = "SELECT amt.type, amt.value, COUNT(am) FROM ActionMessage am JOIN am.messageTargets amt WHERE am.date BETWEEN :fromDate AND :toDate AND am.exceptionType = nl.enexis.scip.action.ActionExceptionType.CONNECTION AND amt.type IN ('nl.enexis.scip.model.Csp','nl.enexis.scip.model.MeasurementProvider') GROUP BY amt.type, amt.value"),
		@NamedQuery(name = "ActionMessage.searchMessagesWithTarget", query = "SELECT am FROM ActionMessage am LEFT JOIN fetch am.action LEFT JOIN am.messageTargets amt LEFT JOIN am.action.targets at WHERE am.date BETWEEN :fromDate AND :toDate AND am.level IN :selectedLevels AND am.exceptionType IN :selectedExceptionTypes AND am.action.type IN :selectedEvents AND am.action.id LIKE :actionId AND am.action.actor LIKE :actor AND ((amt.type LIKE :targetType AND amt.value LIKE :targetId) OR (at.type LIKE :targetType AND at.value LIKE :targetId)) ORDER BY am.date DESC, am.number DESC"),
		@NamedQuery(name = "ActionMessage.searchMessages", query = "SELECT am FROM ActionMessage am LEFT JOIN fetch am.action WHERE am.date BETWEEN :fromDate AND :toDate AND am.level IN :selectedLevels AND am.exceptionType IN :selectedExceptionTypes AND am.action.type IN :selectedEvents AND am.action.id LIKE :actionId AND am.action.actor LIKE :actor ORDER BY am.date DESC, am.number DESC") })
@IdClass(ActionMessageId.class)
public class ActionMessage implements java.io.Serializable {
	// amt.type LIKE :targetType OR
	/**
	 * 
	 */
	private static final long serialVersionUID = 8241310932684850175L;

	public enum Level {
		TRACE, DEBUG, INFO, WARN, ERROR
	};

	private Integer number;
	private Action action;
	private Date date;
	private Level level;
	private ActionExceptionType exceptionType = ActionExceptionType.NO_EXCEPTION;
	private String message;
	private String cause;
	private String causeMessage;
	private String rootCause;
	private String rootCauseMessage;
	private List<ActionMessageTarget> messageTargets = new ArrayList<ActionMessageTarget>();

	public ActionMessage() {
	}

	@Id
	@Column(name = "NUMBER", nullable = false)
	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Id
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ACTION", nullable = false)
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATETIME", nullable = false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "LEVEL", nullable = false, length = 5)
	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	@Enumerated(EnumType.STRING)
	@Column(name = "EXCEPTION_TYPE", nullable = false, length = 30)
	public ActionExceptionType getExceptionType() {
		return exceptionType;
	}

	public void setExceptionType(ActionExceptionType exceptionType) {
		this.exceptionType = exceptionType;
	}

	@Column(name = "MESSAGE", nullable = false, length = 2048)
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		if (message != null && message.length() > 2048) {
			this.message = message.substring(0, 2048);
		} else {
			this.message = message;
		}
	}

	@Column(name = "CAUSE", nullable = true, length = 256)
	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	@Column(name = "CAUSE_MESSAGE", nullable = true, length = 2048)
	public String getCauseMessage() {
		return causeMessage;
	}

	public void setCauseMessage(String causeMessage) {
		if (causeMessage != null && causeMessage.length() > 2048) {
			this.causeMessage = causeMessage.substring(0, 2048);
		} else {
			this.causeMessage = causeMessage;
		}
	}

	@Column(name = "ROOT_CAUSE", nullable = true, length = 256)
	public String getRootCause() {
		return rootCause;
	}

	public void setRootCause(String rootCause) {
		this.rootCause = rootCause;
	}

	@Column(name = "ROOT_CAUSE_MESSAGE", nullable = true, length = 2048)
	public String getRootCauseMessage() {
		return rootCauseMessage;
	}

	public void setRootCauseMessage(String rootCauseMessage) {
		if (rootCauseMessage != null && rootCauseMessage.length() > 2048) {
			this.rootCauseMessage = rootCauseMessage.substring(0, 2048);
		} else {
			this.rootCauseMessage = rootCauseMessage;
		}
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "actionMessage", cascade = { CascadeType.ALL })
	public List<ActionMessageTarget> getMessageTargets() {
		return messageTargets;
	}

	public void setMessageTargets(List<ActionMessageTarget> messageTargets) {
		this.messageTargets = messageTargets;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (action == null ? 0 : action.hashCode());
		result = prime * result + (number == null ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ActionMessage)) {
			return false;
		}
		ActionMessage other = (ActionMessage) obj;
		if (action == null) {
			if (other.action != null) {
				return false;
			}
		} else if (!action.equals(other.action)) {
			return false;
		}
		if (number == null) {
			if (other.number != null) {
				return false;
			}
		} else if (!number.equals(other.number)) {
			return false;
		}
		return true;
	}

}