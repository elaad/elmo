package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

public class ActionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9191381767390285376L;

	private List<ActionTarget> targets = new ArrayList<ActionTarget>();

	private ActionExceptionType type = ActionExceptionType.UNEXPECTED;

	public ActionException() {
		super();
	}

	public ActionException(String message) {
		super(message);
	}

	public ActionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ActionException(ActionExceptionType type) {
		this.type = type;
	}

	public ActionException(ActionExceptionType type, String message) {
		super(message);
		this.type = type;
	}

	public ActionException(ActionExceptionType type, Throwable cause) {
		super(cause);
		this.type = type;
	}

	public ActionException(ActionExceptionType type, String message, Throwable cause) {
		super(message, cause);
		this.type = type;
	}

	public ActionExceptionType getType() {
		return this.type;
	}

	public List<ActionTarget> getTargets() {
		return targets;
	}

	public void setTargets(List<ActionTarget> targets) {
		this.targets = targets;
	}

}
