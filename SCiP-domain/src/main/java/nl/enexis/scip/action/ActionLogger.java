package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.text.MessageFormat;
import java.util.Date;

import javax.interceptor.InvocationContext;

import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;

public final class ActionLogger {

	
	private ActionLogger(){		
	}
	
	public static void trace(String message, Object[] messageParams, Throwable throwable, boolean doPersist) {
		if (doPersist) {
			persist(nl.enexis.scip.action.model.ActionMessage.Level.TRACE, message, messageParams, throwable);
		}

		Logger log = getLogger(messageParams);

		if (!log.isTraceEnabled()) {
			return;
		}

		doLog(log, Level.TRACE, message, messageParams, throwable);
	}

	public static void debug(String message, Object[] messageParams, Throwable throwable, boolean doPersist) {
		if (doPersist) {
			persist(nl.enexis.scip.action.model.ActionMessage.Level.DEBUG, message, messageParams, throwable);
		}

		Logger log = getLogger(messageParams);

		if (!log.isDebugEnabled()) {
			return;
		}

		doLog(log, Level.DEBUG, message, messageParams, throwable);
	}

	public static void info(String message, Object[] messageParams, Throwable throwable, boolean doPersist) {
		if (doPersist) {
			persist(nl.enexis.scip.action.model.ActionMessage.Level.INFO, message, messageParams, throwable);
		}

		Logger log = getLogger(messageParams);

		if (!log.isInfoEnabled()) {
			return;
		}

		doLog(log, Level.INFO, message, messageParams, throwable);
	}

	public static void warn(String message, Object[] messageParams, Throwable throwable, boolean doPersist) {
		if (doPersist) {
			persist(nl.enexis.scip.action.model.ActionMessage.Level.WARN, message, messageParams, throwable);
		}

		Logger log = getLogger(messageParams);

		if (!log.isEnabled(Level.WARN)) {
			return;
		}

		doLog(log, Level.WARN, message, messageParams, throwable);
	}

	public static void error(String message, Object[] messageParams, Throwable throwable, boolean doPersist) {
		if (doPersist) {
			persist(nl.enexis.scip.action.model.ActionMessage.Level.ERROR, message, messageParams, throwable);
		}

		Logger log = getLogger(messageParams);

		if (!log.isEnabled(Level.ERROR)) {
			return;
		}

		doLog(log, Level.ERROR, message, messageParams, throwable);
	}

	private static void persist(ActionMessage.Level level, String message, Object[] messageParams, Throwable throwable) {
		Action action = ActionContext.getAction();

		if (message != null) {
			message = MessageFormat.format(message, messageParams);
		} else {
			message = "";
		}

		ActionMessage msg = new ActionMessage();
		msg.setAction(action);
		msg.setMessage(message);
		msg.setLevel(level);
		msg.setDate(new Date());
		int number = action.getMessages().size();
		msg.setNumber(++number);
		ActionUtil.setActionMessageExceptionDetails(msg, throwable);

		for (Object object : messageParams) {
			if (object instanceof ActionTarget) {
				ActionUtil.setActionMessageTarget(msg, (ActionTarget) object);
			}
		}

		action.getMessages().add(msg);
	}

	private static void doLog(Logger log, Level logLevel, String message, Object[] messageParams, Throwable throwable) {
		String methodName = getCallerMethodName(messageParams);
		String className = getCallerClassName(messageParams);
		Action action = ActionContext.getAction();
		String actionId = action.getId();
		String event = action.getType().toString();
		String actor = action.getActor();

		if (message != null) {
			message = MessageFormat.format(message, messageParams);
		} else {
			message = "NO MESSAGE";
		}

		log.log(className, logLevel, "[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]", new Object[] { methodName, actionId,
				event, actor, message }, throwable);
	}

	private static Logger getLogger(Object[] messageParams) {
		String className = getCallerClassName(messageParams);
		Logger log = Logger.getLogger(className);
		return log;
	}

	private static String getCallerMethodName(Object[] messageParams) {
		if (messageParams != null) {
			for (Object object : messageParams) {
				if (object instanceof InvocationContext) {
					return ((InvocationContext) object).getMethod().getName();
				}
			}
		}
		return Thread.currentThread().getStackTrace()[4].getMethodName();
	}

	private static String getCallerClassName(Object[] messageParams) {
		if (messageParams != null) {
			for (Object object : messageParams) {
				if (object instanceof InvocationContext) {
					return ((InvocationContext) object).getMethod().getDeclaringClass().getName();
				}
			}
		}
		return Thread.currentThread().getStackTrace()[4].getClassName();
	}

}
