package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import nl.enexis.scip.action.model.Action;

public class ActionContext {

	private static ThreadLocal<Action> context = new ThreadLocal<Action>() {
		@Override
		protected Action initialValue() {
			return new Action();
		}

		// @Override
		// protected Action childValue(Action parentValue) {
		// Action action = new Action();
		// action.setParent(parentValue);
		// return action;
		// };
	};

	private ActionContext() {
	};

	public static Action getAction() {
		return context.get();
	}

	public static void removeAction() {
		context.remove();
	}

	public static String getId() {
		return ActionContext.getAction().getId();
	}

	public static void setId(String newId) {
		ActionContext.getAction().setId(newId);
	}

	public static boolean doDirectNotification() {
		return ActionContext.getAction().doDirectNotification();
	}

	public static void setDirectNotification(boolean doMail) {
		ActionContext.getAction().setDirectNotification(doMail);
	}

	public static String getActor() {
		return ActionContext.getAction().getActor();
	}

	public static void setActor(String newActor) {
		ActionContext.getAction().setActor(newActor);
	}

	public static ActionEvent getEvent() {
		return ActionContext.getAction().getType();
	}

	public static void setEvent(ActionEvent event) {
		ActionContext.getAction().setType(event);
	}

	public static void addActionTarget(ActionTarget target) {
		if (target != null) {
			ActionUtil.setActionTarget(ActionContext.getAction(), target, ActionTargetInOut.IN);
		}
	}

	public static void addActionTarget(ActionTarget target, ActionTargetInOut inOrOut) {
		if (target != null) {
			ActionUtil.setActionTarget(ActionContext.getAction(), target, inOrOut == null ? ActionTargetInOut.IN
					: inOrOut);
		}
	}

}
