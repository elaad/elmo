package nl.enexis.scip.action.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;

import nl.enexis.scip.util.ForecastBlockUtil;

public class TimeBlock {
	private final Date day;
	private final int hour;
	private final int blockOfHour;

	private TimeBlock(Date day, int hour, int blockOfHour) {
		this.day = ForecastBlockUtil.getDate(day);
		this.hour = hour;
		this.blockOfHour = blockOfHour;
	}

	public static TimeBlock createForm(Date dateTime, int blockMinutes) {
		return new TimeBlock(dateTime, ForecastBlockUtil.getHour(dateTime), ForecastBlockUtil.getBlock(dateTime,
				blockMinutes));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + blockOfHour;
		result = prime * result + (day == null ? 0 : day.hashCode());
		result = prime * result + hour;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		TimeBlock other = (TimeBlock) obj;
		if (blockOfHour != other.blockOfHour) {
			return false;
		}
		if (day == null) {
			if (other.day != null) {
				return false;
			}
		} else if (!day.equals(other.day)) {
			return false;
		}
		if (hour != other.hour) {
			return false;
		}
		return true;
	}

	public Date getDay() {
		return day;
	}

	public int getHour() {
		return hour;
	}

	public int getBlockOfHour() {
		return blockOfHour;
	}	
}
