package nl.enexis.scip.util.security;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.bind.DatatypeConverter;


public abstract class PasswordUtil {
	
	private static final String keyalgorithm = "PBKDF2WithHmacSHA1";
	private static final String rngalgorithm = "SHA1PRNG";
	private static final int keylength = 160; /* size in bits */
	private static final int iterations = 10000;
	
	
	/* returns an encrypted key based on a clear text and a salt */
	private static byte[] getEncryptedKey(String clearText, byte[] salt) throws InvalidKeySpecException, NoSuchAlgorithmException {
		
		KeySpec spec = new PBEKeySpec(clearText.toCharArray(), salt, iterations, keylength);
		return SecretKeyFactory.getInstance(keyalgorithm).generateSecret(spec).getEncoded();
	}
	
	
	public static String encrypt(String clearText) throws InvalidKeySpecException, NoSuchAlgorithmException {
		
		/* generate a random salt and use it to encrypt the clear text */
		byte[] salt = new byte[8]; SecureRandom.getInstance(rngalgorithm).nextBytes(salt);
		byte[] key = getEncryptedKey(clearText, salt);
		
		/* return the salt (first 16 chars) and the key (40 chars) as a hexadecimal string */
		return DatatypeConverter.printHexBinary(salt) + DatatypeConverter.printHexBinary(key);
	}
	

	public static boolean authenticate(String clearText, String cipherText) throws InvalidKeySpecException, NoSuchAlgorithmException {
		
		/* get the salt from the cipher text and use it to encrypt the clear text */
		byte[] salt = DatatypeConverter.parseHexBinary(cipherText.substring(0, 16));
		byte[] key = getEncryptedKey(clearText, salt);
		
		/* return true if the encrypted clear text and the known key are equal */
		return cipherText.substring(16).equals(DatatypeConverter.printHexBinary(key));
	}

}
