package nl.enexis.scip.util;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ForecastBlockUtil {
	public static String DAY = "DAY";
	public static String HOUR = "HOUR";
	public static String BLOCK = "BLOCK";

	public ForecastBlockUtil() {
	}

	public static int getBlocksPerHour(int blockMinutes) {
		return 60 / blockMinutes;
	}

	public static int getBlock(Date date, int blockMinutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MINUTE) / blockMinutes + 1;
	}

	public static int getHour(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.HOUR_OF_DAY);
	}

	public static Date getDate(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		try {
			return df.parse(df.format(date));
		} catch (ParseException e) {
			return null;
		}
	}

	public static Date addDaysToDate(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, days);
		return new Date(cal.getTime().getTime());
	}

	public static Date addMinutesToDate(Date date, int minutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, minutes);
		return new Date(cal.getTime().getTime());
	}

	public static int getDayOfWeek(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	public static Date getStartDateOfBlock(Map<String, Object> blockValues, int blockMinutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(ForecastBlockUtil.getDate((Date) blockValues.get(ForecastBlockUtil.DAY)));
		cal.set(Calendar.HOUR_OF_DAY, (Integer) blockValues.get(ForecastBlockUtil.HOUR));
		cal.set(Calendar.MINUTE, ((Integer) blockValues.get(ForecastBlockUtil.BLOCK) - 1) * blockMinutes);
		return new Date(cal.getTime().getTime());
	}

	public static Date getEndDateOfBlock(Map<String, Object> blockValues, int blockMinutes) {
		Date startDate = ForecastBlockUtil.getStartDateOfBlock((Date) blockValues.get(ForecastBlockUtil.DAY),
				(Integer) blockValues.get(ForecastBlockUtil.HOUR), (Integer) blockValues.get(ForecastBlockUtil.BLOCK), blockMinutes);
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.add(Calendar.MINUTE, blockMinutes);
		cal.add(Calendar.SECOND, -1);
		return new Date(cal.getTime().getTime());
	}

	public static Date getStartDateOfBlock(Date date, int hour, int block, int blockMinutes) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(ForecastBlockUtil.getDate(date));
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, (block - 1) * blockMinutes);
		return new Date(cal.getTime().getTime());
	}

	public static Date getEndDateOfBlock(Date date, int hour, int block, int blockMinutes) {
		Date startDate = ForecastBlockUtil.getStartDateOfBlock(date, hour, block, blockMinutes);
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.add(Calendar.MINUTE, blockMinutes);
		cal.add(Calendar.SECOND, -1);
		return new Date(cal.getTime().getTime());
	}

	public static Date getEndDateOfBlock(Date datetime, int blockMinutes) {
		Date date = ForecastBlockUtil.getDate(datetime);
		int block = ForecastBlockUtil.getBlock(datetime, blockMinutes);
		int hour = ForecastBlockUtil.getHour(datetime);
		return getEndDateOfBlock(date, hour, block, blockMinutes);
	}

	public static Map<String, Object> getNextBlockValues(Date date, int hour, int block, int blocksPerHour) {
		Map<String, Object> blockValues = new HashMap<String, Object>();

		int nextBlock = block + 1;
		int nextHour = new Integer(hour).intValue();
		Date nextDate = ForecastBlockUtil.getDate(date);

		if (nextBlock > blocksPerHour) {
			nextBlock = 1;
			nextHour++;
			if (nextHour > 23) {
				nextHour = 0;
				nextDate = ForecastBlockUtil.addDaysToDate(nextDate, 1);
			}
		}

		blockValues.put(ForecastBlockUtil.DAY, nextDate);
		blockValues.put(ForecastBlockUtil.HOUR, nextHour);
		blockValues.put(ForecastBlockUtil.BLOCK, nextBlock);

		return blockValues;
	}

	public static Map<String, Object> getNextBlockValues(Map<String, Object> blockValues, int blockMinutes) {
		int blocksPerHour = ForecastBlockUtil.getBlocksPerHour(blockMinutes);
		return ForecastBlockUtil.getNextBlockValues((Date) blockValues.get(ForecastBlockUtil.DAY),
				(Integer) blockValues.get(ForecastBlockUtil.HOUR), (Integer) blockValues.get(ForecastBlockUtil.BLOCK), blocksPerHour);

	}

	public static Map<String, Object> getPrevBlockValues(Date date, int hour, int block, int blocksPerHour) {
		Map<String, Object> blockValues = new HashMap<String, Object>();

		int prevBlock = block - 1;
		int prevHour = new Integer(hour).intValue();
		Date prevDate = ForecastBlockUtil.getDate(date);

		if (prevBlock < 1) {
			prevBlock = blocksPerHour;
			prevHour--;
			if (prevHour < 0) {
				prevHour = 23;
				prevDate = ForecastBlockUtil.addDaysToDate(prevDate, -1);
			}
		}

		blockValues.put(ForecastBlockUtil.DAY, prevDate);
		blockValues.put(ForecastBlockUtil.HOUR, prevHour);
		blockValues.put(ForecastBlockUtil.BLOCK, prevBlock);

		return blockValues;
	}

	public static Map<String, Object> getPrevBlockValues(Map<String, Object> blockValues, int blockMinutes) {
		int blocksPerHour = ForecastBlockUtil.getBlocksPerHour(blockMinutes);
		return ForecastBlockUtil.getPrevBlockValues((Date) blockValues.get(ForecastBlockUtil.DAY),
				(Integer) blockValues.get(ForecastBlockUtil.HOUR), (Integer) blockValues.get(ForecastBlockUtil.BLOCK), blocksPerHour);
	}

	public static Map<String, Object> getBlockValues(Date datetime, int blockMinutes) {
		Map<String, Object> blockValues = new HashMap<String, Object>();

		blockValues.put(ForecastBlockUtil.DAY, ForecastBlockUtil.getDate(datetime));
		blockValues.put(ForecastBlockUtil.HOUR, ForecastBlockUtil.getHour(datetime));
		blockValues.put(ForecastBlockUtil.BLOCK, ForecastBlockUtil.getBlock(datetime, blockMinutes));

		return blockValues;
	}

	public static Map<String, Object> getCurrentBlockValues(int blockMinutes) {
		Map<String, Object> blockValues = new HashMap<String, Object>();

		Date datetime = new Date();

		blockValues.put(ForecastBlockUtil.DAY, ForecastBlockUtil.getDate(datetime));
		blockValues.put(ForecastBlockUtil.HOUR, ForecastBlockUtil.getHour(datetime));
		blockValues.put(ForecastBlockUtil.BLOCK, ForecastBlockUtil.getBlock(datetime, blockMinutes));

		return blockValues;
	}

	public static Map<String, Object> addBlocksToBlockValues(Map<String, Object> blockValues, int blockMinutes, int numberOfBlocks) {
		Date orgDate = ForecastBlockUtil.getStartDateOfBlock(blockValues, blockMinutes);
		int minutes = blockMinutes * numberOfBlocks;
		Date newDate = ForecastBlockUtil.addMinutesToDate(orgDate, minutes);
		return ForecastBlockUtil.getBlockValues(newDate, blockMinutes);
	}

}
