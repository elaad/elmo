package nl.enexis.scip.util.xml;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ActionLogger.class })
public class JaxbUtilTest {

	// Assert that the xml to string functionality contains the data it should contain, indicating that it is indeed xml
	@Test
	public void testToXmlString_Simple() {
		String returnedValue = JaxbUtil.toXmlString(this);

		assertEquals("Jaxb fails to create XML generated text.", true,
				returnedValue.contains("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"));
		assertEquals("Current class is not incorporated in Jaxb's XML generated text.", true, returnedValue.contains("<JaxbUtilTest/>"));
	}

	// Assert that the xml to string functionality contains the data it should contain, indicating that it is indeed xml
	@Test
	public void testToXmlString_Extended() {
		String returnedValue = JaxbUtil.toXmlString(new Object[] { "11", 22, new Integer("8") });
		assertEquals(
				true,
				returnedValue
						.contains("<item xsi:type=\"xs:string\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">11</item>"));
		assertEquals(
				true,
				returnedValue
						.contains("<item xsi:type=\"xs:int\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">22</item>"));
		assertEquals(
				true,
				returnedValue
						.contains("<item xsi:type=\"xs:int\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">8</item>"));
	}

	// Assert that when data cannot be converted to the xml, raise an exception
	@Test
	public void testToXmlString_Exception() {
		PowerMockito.mockStatic(ActionLogger.class);
		assertEquals("XML not generated!", JaxbUtil.toXmlString(new Object[] { "11", 22, new ActionException("8") }));

		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(eq("Cloudn't marshall JAXB object {0} to string!"), any(Object[].class), any(Throwable.class), eq(false));
	}
}
