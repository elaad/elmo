package nl.enexis.scip.util;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import nl.enexis.scip.UnitTestHelper;

import org.junit.Test;

public class ForecastBlockUtilTest {

	// Assert that each forecast block is correctly computed if converted to hours
	@Test
	public void testGetBlocksPerHour() {
		// TODO: division by zero, check this
		// assertEquals(0, ForecastBlockUtil.getBlocksPerHour(0));
		assertEquals(60, ForecastBlockUtil.getBlocksPerHour(1));
		assertEquals(2, ForecastBlockUtil.getBlocksPerHour(30));
		assertEquals(0, ForecastBlockUtil.getBlocksPerHour(9999));
	}

	// Asserts that the correct block is taken
	@Test
	public void testGetBlock() {
		assertEquals("Expected to be in the 3rd block considering the 33rd minute with blockMinutes of 15", 3,
				ForecastBlockUtil.getBlock(UnitTestHelper.getDate23Dec2015at11h33m27s(), 15));

		assertEquals("Expected to be in the 4th block considering the 46th minute with blockMinutes of 15", 4,
				ForecastBlockUtil.getBlock(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputMinutes(46), 15));

		assertEquals("Expected to be in the 1st block considering the 0th minute with blockMinutes of 3", 1,
				ForecastBlockUtil.getBlock(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputMinutes(0), 3));

		assertEquals("Expected to be in the 60th block considering the 59th minute with blockMinutes of 1", 60,
				ForecastBlockUtil.getBlock(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputMinutes(59), 1));
	}

	// Assert that the correct hour is computed when a date is given
	@Test
	public void testGetHour() {
		assertEquals(11, ForecastBlockUtil.getHour(UnitTestHelper.getDate23Dec2015at11h33m27s()));
		assertEquals(13, ForecastBlockUtil.getHour(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputHours(2)));
		assertEquals(9, ForecastBlockUtil.getHour(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputHours(22)));
	}

	// Assert that the date is formatted to "yyyyMMdd"
	@Test
	public void testGetDate() {
		assertEquals(UnitTestHelper.getDayOfDateWithoutTime(UnitTestHelper.getDate23Dec2015at11h33m27s()),
				ForecastBlockUtil.getDate(UnitTestHelper.getDate23Dec2015at11h33m27s()));
		assertEquals(UnitTestHelper.getDayOfDateWithoutTime(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(1)),
				ForecastBlockUtil.getDate(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(1)));
		assertEquals(UnitTestHelper.getDayOfDateWithoutTime(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(8)),
				ForecastBlockUtil.getDate(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(8)));
	}

	// Assures that days are added to the given date
	@Test
	public void testAddDaysToDate() {
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(-100),
				ForecastBlockUtil.addDaysToDate(UnitTestHelper.getDate23Dec2015at11h00m27s(), -100));
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(0),
				ForecastBlockUtil.addDaysToDate(UnitTestHelper.getDate23Dec2015at11h00m27s(), 0));
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(5),
				ForecastBlockUtil.addDaysToDate(UnitTestHelper.getDate23Dec2015at11h00m27s(), 5));
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(54343),
				ForecastBlockUtil.addDaysToDate(UnitTestHelper.getDate23Dec2015at11h00m27s(), 54343));
	}

	// Assures that minutes are added to the given date
	@Test
	public void testAddMinutesToDate() {
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputMinutes(-1),
				ForecastBlockUtil.addMinutesToDate(UnitTestHelper.getDate23Dec2015at11h00m27s(), -1));
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputMinutes(0),
				ForecastBlockUtil.addMinutesToDate(UnitTestHelper.getDate23Dec2015at11h00m27s(), 0));
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputMinutes(432324),
				ForecastBlockUtil.addMinutesToDate(UnitTestHelper.getDate23Dec2015at11h00m27s(), 432324));
	}

	// Assert that the day of the week is returned considering a given date
	@Test
	public void testGetDayOfWeek() {
		assertEquals(7, ForecastBlockUtil.getDayOfWeek(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(-4)));
		assertEquals(4, ForecastBlockUtil.getDayOfWeek(UnitTestHelper.getDate23Dec2015at11h00m27s()));
		assertEquals(3, ForecastBlockUtil.getDayOfWeek(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(6)));
	}

	// Verifies that the start date is correctly retrieved from a block value
	@Test
	public void testGetStartDateOfBlock_BlockValues() {
		int blockMinutes = 15;
		Map<String, Object> blockValues = new HashMap<String, Object>();
		blockValues.put(ForecastBlockUtil.DAY, ForecastBlockUtil.getDate(UnitTestHelper.getDate23Dec2015at11h00m27s()));
		blockValues.put(ForecastBlockUtil.HOUR, ForecastBlockUtil.getHour(UnitTestHelper.getDate23Dec2015at11h00m27s()));
		blockValues.put(ForecastBlockUtil.BLOCK, ForecastBlockUtil.getBlock(UnitTestHelper.getDate23Dec2015at11h00m27s(), blockMinutes));

		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m00s(), ForecastBlockUtil.getStartDateOfBlock(blockValues, blockMinutes));

		blockMinutes = 13;
		Map<String, Object> blockValues2 = new HashMap<String, Object>();
		blockValues2.put(ForecastBlockUtil.DAY, ForecastBlockUtil.getDate(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(-1)));
		blockValues2.put(ForecastBlockUtil.HOUR,
				ForecastBlockUtil.getHour(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputHours(-1)));
		blockValues2.put(ForecastBlockUtil.BLOCK, ForecastBlockUtil.getBlock(UnitTestHelper.getDate23Dec2015at11h00m27s(), blockMinutes));

		assertEquals(UnitTestHelper.getDate(2015, Calendar.DECEMBER, 22, 10, 0, 0),
				ForecastBlockUtil.getStartDateOfBlock(blockValues2, blockMinutes));
	}

	// Verifies that the end date is correctly retrieved from a block value
	@Test
	public void testGetEndDateOfBlock_BlockValues() {
		int blockMinutes = 15;
		Map<String, Object> blockValues = new HashMap<String, Object>();
		blockValues.put(ForecastBlockUtil.DAY, ForecastBlockUtil.getDate(UnitTestHelper.getDate23Dec2015at11h00m27s()));
		blockValues.put(ForecastBlockUtil.HOUR, ForecastBlockUtil.getHour(UnitTestHelper.getDate23Dec2015at11h00m27s()));
		blockValues.put(ForecastBlockUtil.BLOCK, ForecastBlockUtil.getBlock(UnitTestHelper.getDate23Dec2015at11h00m27s(), blockMinutes));

		assertEquals(UnitTestHelper.getDate(2015, Calendar.DECEMBER, 23, 11, 14, 59),
				ForecastBlockUtil.getEndDateOfBlock(blockValues, blockMinutes));

		blockMinutes = 13;
		Map<String, Object> blockValues2 = new HashMap<String, Object>();
		blockValues2.put(ForecastBlockUtil.DAY, ForecastBlockUtil.getDate(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputDays(-1)));
		blockValues2.put(ForecastBlockUtil.HOUR,
				ForecastBlockUtil.getHour(UnitTestHelper.getDate23Dec2015at11h00m27sAndExtraInputHours(-1)));
		blockValues2.put(ForecastBlockUtil.BLOCK, ForecastBlockUtil.getBlock(UnitTestHelper.getDate23Dec2015at11h00m27s(), blockMinutes));

		assertEquals(UnitTestHelper.getDate(2015, Calendar.DECEMBER, 22, 10, 12, 59),
				ForecastBlockUtil.getEndDateOfBlock(blockValues2, blockMinutes));
	}

	// Assert that the start date of a block is correctly processed
	@Test
	public void testGetStartDateOfBlock_DateAndInts() {
		int blockMinutes = 15;
		int block = 3;
		int hour = 8;
		Date date = UnitTestHelper.getDate(2010, Calendar.NOVEMBER, 1, 16, 9, 32);
		assertEquals(UnitTestHelper.getDate(2010, Calendar.NOVEMBER, 1, 8, 30, 00),
				ForecastBlockUtil.getStartDateOfBlock(date, hour, block, blockMinutes));

		blockMinutes = 27;
		block = 1;
		hour = 20;
		date = UnitTestHelper.getDate(2001, Calendar.JUNE, 6, 16, 9, 32);
		assertEquals(UnitTestHelper.getDate(2001, Calendar.JUNE, 6, 20, 00, 00),
				ForecastBlockUtil.getStartDateOfBlock(date, hour, block, blockMinutes));
	}

	// Assert that the end date of a block is correctly processed
	@Test
	public void testGetEndDateOfBlock_DateAndInts() {
		int blockMinutes = 15;
		int block = 2;
		int hour = 5;
		Date date = UnitTestHelper.getDate(2008, Calendar.APRIL, 10, 3, 9, 32);
		assertEquals(UnitTestHelper.getDate(2008, Calendar.APRIL, 10, 5, 29, 59),
				ForecastBlockUtil.getEndDateOfBlock(date, hour, block, blockMinutes));

		blockMinutes = 9;
		block = 5;
		hour = 9;
		date = UnitTestHelper.getDate(2004, Calendar.AUGUST, 27, 7, 9, 32);
		assertEquals(UnitTestHelper.getDate(2004, Calendar.AUGUST, 27, 9, 44, 59),
				ForecastBlockUtil.getEndDateOfBlock(date, hour, block, blockMinutes));
	}

	// Assert that the next block value is correctly returned
	@Test
	public void testGetNextBlockValues() {
		Date date = UnitTestHelper.getDate(1984, Calendar.AUGUST, 6, 9, 44, 59);
		int hour = 3;
		int block = 9;
		int blocksPerHour = 10;
		Map<String, Object> blockValues = ForecastBlockUtil.getNextBlockValues(date, hour, block, blocksPerHour);

		assertEquals(UnitTestHelper.getDate(1984, Calendar.AUGUST, 6, 0, 0, 0), blockValues.get(ForecastBlockUtil.DAY));
		assertEquals(3, blockValues.get(ForecastBlockUtil.HOUR));
		assertEquals(10, blockValues.get(ForecastBlockUtil.BLOCK));

		date = UnitTestHelper.getDate(1888, Calendar.JANUARY, 11, 7, 34, 53);
		hour = 23;
		block = 10;
		blocksPerHour = 10;
		blockValues = ForecastBlockUtil.getNextBlockValues(date, hour, block, blocksPerHour);
		assertEquals(UnitTestHelper.getDate(1888, Calendar.JANUARY, 12, 0, 0, 0), blockValues.get(ForecastBlockUtil.DAY));
		assertEquals(0, blockValues.get(ForecastBlockUtil.HOUR));
		assertEquals(1, blockValues.get(ForecastBlockUtil.BLOCK));

	}

	// Assert that the previous block value is correctly returned
	@Test
	public void testGetPrevBlockValuesDateIntIntInt() {
		Date date = UnitTestHelper.getDate(1984, Calendar.AUGUST, 6, 9, 44, 59);
		int hour = 3;
		int block = 9;
		int blocksPerHour = 10;
		Map<String, Object> blockValues = ForecastBlockUtil.getPrevBlockValues(date, hour, block, blocksPerHour);

		assertEquals(UnitTestHelper.getDate(1984, Calendar.AUGUST, 6, 0, 0, 0), blockValues.get(ForecastBlockUtil.DAY));
		assertEquals(3, blockValues.get(ForecastBlockUtil.HOUR));
		assertEquals(8, blockValues.get(ForecastBlockUtil.BLOCK));

		date = UnitTestHelper.getDate(1888, Calendar.JANUARY, 11, 7, 34, 53);
		hour = 0;
		block = 1;
		blocksPerHour = 10;
		blockValues = ForecastBlockUtil.getPrevBlockValues(date, hour, block, blocksPerHour);
		assertEquals(UnitTestHelper.getDate(1888, Calendar.JANUARY, 10, 0, 0, 0), blockValues.get(ForecastBlockUtil.DAY));
		assertEquals(23, blockValues.get(ForecastBlockUtil.HOUR));
		assertEquals(10, blockValues.get(ForecastBlockUtil.BLOCK));
	}

	// Assert that a block is correctly added to a block value map
	@Test
	public void testAddBlocksToBlockValues() {
		Map<String, Object> blockValues = new HashMap<String, Object>();
		blockValues.put(ForecastBlockUtil.DAY, UnitTestHelper.getDate(2015, Calendar.DECEMBER, 23, 00, 00, 00));
		blockValues.put(ForecastBlockUtil.HOUR, 11);
		blockValues.put(ForecastBlockUtil.BLOCK, 1);

		int blockMinutes = 15;
		int numberOfBlocks = 3;
		Map<String, Object> newBlockValues = new HashMap<String, Object>();
		newBlockValues.put(ForecastBlockUtil.DAY, UnitTestHelper.getDate(2015, Calendar.DECEMBER, 23, 00, 00, 00));
		newBlockValues.put(ForecastBlockUtil.HOUR, 11);
		newBlockValues.put(ForecastBlockUtil.BLOCK, 4);

		Map<String, Object> returnedBlockValues = ForecastBlockUtil.addBlocksToBlockValues(blockValues, blockMinutes, numberOfBlocks);
		assertEquals(newBlockValues, returnedBlockValues);

		blockValues = new HashMap<String, Object>();
		blockValues.put(ForecastBlockUtil.DAY, UnitTestHelper.getDate(1337, Calendar.MAY, 31, 00, 00, 00));
		blockValues.put(ForecastBlockUtil.HOUR, 23);
		blockValues.put(ForecastBlockUtil.BLOCK, 2);

		blockMinutes = 30;
		numberOfBlocks = 1;
		newBlockValues = new HashMap<String, Object>();
		newBlockValues.put(ForecastBlockUtil.DAY, UnitTestHelper.getDate(1337, Calendar.JUNE, 1, 00, 00, 00));
		newBlockValues.put(ForecastBlockUtil.HOUR, 0);
		newBlockValues.put(ForecastBlockUtil.BLOCK, 1);

		returnedBlockValues = ForecastBlockUtil.addBlocksToBlockValues(blockValues, blockMinutes, numberOfBlocks);
		assertEquals(newBlockValues, returnedBlockValues);
	}
}
