package nl.enexis.scip;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * 
 * 
 * @version 0.0.6
 */

public class UnitTestHelper {

	private static void setDefaultTimeZone() {
		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
	}

	public static Date getCurrentDateMinusOneYear() {
		setDefaultTimeZone();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, -1);
		return calendar.getTime();
	}

	public static Date getCurrentDateMinusOneHour() {
		setDefaultTimeZone();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR_OF_DAY, -1);
		return calendar.getTime();
	}

	public static Date getCurrentDateIncreasedWithOneYear() {
		setDefaultTimeZone();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, 1);
		return calendar.getTime();
	}

	public static Date getDate(long milli) {
		setDefaultTimeZone();
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		calendar.setTime(new Date(milli));
		return calendar.getTime();
	}

	public static Date getDate(int year, int month, int day, int hour, int minutes, int seconds) {
		setDefaultTimeZone();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(1000000000000L);
		calendar.set(year, month, day, hour, minutes, seconds);
		return getDate(calendar.getTime().getTime());
	}

	public static Date getDate23Dec2015at11h00m27sAndExtraInputDays(long days) {
		return getDate23Dec2015at11h00m27sAndExtraInputHours(days * 24);
	}

	public static Date getDate23Dec2015at11h00m27sAndExtraInputHours(long hours) {
		return getDate23Dec2015at11h00m27sAndExtraInputMinutes(hours * 60);
	}

	public static Date getDate23Dec2015at11h00m27sAndExtraInputMinutes(long minutes) {
		return getDate23Dec2015at11h00m27sAndExtraInputSeconds(minutes * 60);
	}

	public static Date getDate23Dec2015at11h00m27sAndExtraInputSeconds(long seconds) {
		return getDate(1450868427000L + seconds * 1000);
	}

	public static Date getDate23Dec2015at11h33m27s() {
		return getDate(1450870407000L);
	}

	public static Date getDate23Dec2015at11h00m27s() {
		return getDate(1450868427000L);
	}

	public static Date getDate23Dec2015at11h00m00s() {
		return getDate(1450868400000L);
	}

	public static Date getDayOfDateWithoutTime(Date inputDate) {
		setDefaultTimeZone();
		Date date = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
			date = sdf.parse(sdf.format(inputDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static Date getDateWithoutTime(int year, int month, int day) {
		setDefaultTimeZone();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(1000000000000L);
		calendar.set(year, month, day);
		return getDate(calendar.getTime().getTime());
	}
}
