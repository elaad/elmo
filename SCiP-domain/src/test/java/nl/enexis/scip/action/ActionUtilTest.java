package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessageTarget;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ExceptionUtils.class)
public class ActionUtilTest {

	@Before
	public void setup() {
		PowerMockito.mockStatic(ExceptionUtils.class);
	}

	// Verify that the correcty exception is thrown considering the parameter it is provided
	@Test
	public void testSetActionMessageExceptionDetails() {
		// prepare
		ActionMessage actionMessage = new ActionMessage();
		IOException iOException = new IOException();
		SecurityException securityException = new SecurityException();
		ActionException actionException = new ActionException();
		when(ExceptionUtils.getRootCause(any())).thenReturn(new ArrayIndexOutOfBoundsException());

		// execute1
		ActionUtil.setActionMessageExceptionDetails(actionMessage, null);
		// verify nullmeting
		assertEquals(null, actionMessage.getCause());

		// execute2
		ActionUtil.setActionMessageExceptionDetails(actionMessage, iOException);
		// verify2
		assertEquals(new IOException().toString(), actionMessage.getCause());
		assertEquals(ActionExceptionType.UNEXPECTED.toString(), actionMessage.getMessage());
		assertEquals(ActionExceptionType.UNEXPECTED, actionMessage.getExceptionType());

		// prepare3
		actionMessage = new ActionMessage();
		// execute3
		ActionUtil.setActionMessageExceptionDetails(actionMessage, securityException);
		// verify3
		assertEquals(new SecurityException().toString(), actionMessage.getCause());
		assertEquals(ActionExceptionType.UNEXPECTED.toString(), actionMessage.getMessage());
		assertEquals(ActionExceptionType.UNEXPECTED, actionMessage.getExceptionType());

		// prepare4
		actionMessage = new ActionMessage();
		// execute4
		ActionUtil.setActionMessageExceptionDetails(actionMessage, actionException);
		// verify4
		assertEquals(new ActionException().toString(), actionMessage.getCause());
		assertEquals(null, actionMessage.getMessage());
		assertEquals(ActionExceptionType.UNEXPECTED, actionMessage.getExceptionType());

		// verify rootclause
		assertEquals(new ArrayIndexOutOfBoundsException().toString(), actionMessage.getRootCause());
		assertEquals(null, actionMessage.getRootCauseMessage());
	}

	// Assert that the action message target is correctly retrieved when set
	@Test
	public void testSetActionMessageTarget() {
		// prepare
		ActionMessage actionMessage = new ActionMessage();
		ActionTarget mockActionTarget = mock(ActionTarget.class);
		when(mockActionTarget.getActionTargetId()).thenReturn("GETACTIONTARGETID_ACTIONMESSAGETARGET");
		when(mockActionTarget.getActionTargetName()).thenReturn("GETACTIONTARGETNAME_ACTIONMESSAGETARGET");
		// execute
		ActionMessage returnedActionMessage = ActionUtil.setActionMessageTarget(actionMessage, mockActionTarget);
		// verify
		ActionMessageTarget target = returnedActionMessage.getMessageTargets().get(0);
		assertEquals(returnedActionMessage, target.getActionMessage());
		assertEquals("GETACTIONTARGETNAME_ACTIONMESSAGETARGET", target.getType());
		assertEquals("GETACTIONTARGETID_ACTIONMESSAGETARGET", target.getValue());
		assertSame(target.getNumber(), returnedActionMessage.getMessageTargets().size());
		assertSame(1, returnedActionMessage.getMessageTargets().size());
	}

	// Assure that setActionTarget sets input data correctly
	@Test
	public void testSetActionTarget() {
		// prepare
		ActionTarget mockActionTarget = mock(ActionTarget.class);
		when(mockActionTarget.getActionTargetId()).thenReturn("GETACTIONTARGETID");
		when(mockActionTarget.getActionTargetName()).thenReturn("GETACTIONTARGETNAME");
		// execute
		Action action = ActionUtil.setActionTarget(ActionContext.getAction(), mockActionTarget, ActionTargetInOut.IN);

		// note target is model.ActionTarget
		// verify
		nl.enexis.scip.action.model.ActionTarget target = action.getTargets().get(0);

		assertEquals(action, target.getAction());
		assertEquals(ActionTargetInOut.IN, target.getDirection());
		assertEquals("GETACTIONTARGETNAME", target.getType());
		assertEquals("GETACTIONTARGETID", target.getValue());
		assertSame(target.getNumber(), action.getTargets().size());
		assertSame(1, action.getTargets().size());
	}
}
