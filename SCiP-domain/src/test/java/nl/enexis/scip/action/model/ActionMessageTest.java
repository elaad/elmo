package nl.enexis.scip.action.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public class ActionMessageTest {

	private ActionMessage actionMessage;
	private String textNull;
	private String textZero;
	private String textNormal;
	private String textTooLarge;

	@Before
	public void setup() {
		actionMessage = new ActionMessage();
		textNull = null;
		textZero = this.generateStringOfSize(0);
		textNormal = this.generateStringOfSize(1000);
		textTooLarge = this.generateStringOfSize(5000);
	}

	// assert that messages are correctly set and get
	@Test
	public void testMessage() {
		actionMessage.setMessage(textNull);
		assertEquals(textNull, actionMessage.getMessage());

		actionMessage.setMessage(textNormal);
		assertEquals(textNormal, actionMessage.getMessage());

		actionMessage.setMessage(textZero);
		assertEquals(textZero, actionMessage.getMessage());

		actionMessage.setMessage(textTooLarge);
		assertEquals(this.generateStringOfSize(2048), actionMessage.getMessage());
	}

	// assert that cause messages are correctly set and get
	@Test
	public void testCauseMessage() {
		actionMessage.setCauseMessage(textNull);
		assertEquals(textNull, actionMessage.getCauseMessage());

		actionMessage.setCauseMessage(textNull);
		assertEquals(textNull, actionMessage.getCauseMessage());

		actionMessage.setCauseMessage(textNormal);
		assertEquals(textNormal, actionMessage.getCauseMessage());

		actionMessage.setCauseMessage(textZero);
		assertEquals(textZero, actionMessage.getCauseMessage());

		actionMessage.setCauseMessage(textTooLarge);
		assertEquals(this.generateStringOfSize(2048), actionMessage.getCauseMessage());
	}

	// assert that root cause messages are correctly set and get
	@Test
	public void testRootCauseMessage() {
		actionMessage.setRootCauseMessage(textNull);
		assertEquals(textNull, actionMessage.getRootCauseMessage());

		actionMessage.setRootCauseMessage(textNormal);
		assertEquals(textNormal, actionMessage.getRootCauseMessage());

		actionMessage.setRootCauseMessage(textZero);
		assertEquals(textZero, actionMessage.getRootCauseMessage());

		actionMessage.setRootCauseMessage(textTooLarge);
		assertEquals(this.generateStringOfSize(2048), actionMessage.getRootCauseMessage());
	}

	private String generateStringOfSize(int size) {
		char[] chars = new char[size];
		Arrays.fill(chars, 'X');
		String text = new String(chars);
		return text;
	}
}
