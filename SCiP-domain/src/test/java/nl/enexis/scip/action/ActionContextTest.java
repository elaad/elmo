package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import nl.enexis.scip.action.model.Action;

import org.junit.After;
import org.junit.Test;

public class ActionContextTest {

	@After
	public void tearDown() {
		ActionContext.removeAction();
	}

	// Assert that getting getAction is equal
	@Test
	public void testGetAction() {
		assertEquals(ActionContext.getAction(), ActionContext.getAction());
		assertNotNull(ActionContext.getAction());
	}

	// Assert that after deletion of an Action object, a new action object is created and not equal to the deleted
	// Action object
	@Test
	public void testRemoveAction() {
		Action action1 = ActionContext.getAction();
		ActionContext.removeAction();
		assertNotEquals(action1, ActionContext.getAction());
	}

	// Assert that getId is not null and equals
	@Test
	public void testGetId() {
		assertNotNull(ActionContext.getId());
		assertEquals(ActionContext.getId(), ActionContext.getId());
		assertNotNull(ActionContext.getId());
	}

	// Assert that after setting an id it equals getId
	@Test
	public void testSetId() {
		ActionContext.setId("SETID");
		assertEquals("SETID", ActionContext.getId());
	}

	// Assure that this function has the default of true
	@Test
	public void testDoDirectNotification() {
		assertEquals(true, ActionContext.doDirectNotification());
	}

	// Assure that after setting the value it is set
	@Test
	public void testSetDirectNotification() {
		ActionContext.setDirectNotification(false);
		assertEquals(false, ActionContext.doDirectNotification());
	}

	// Verify that setting the actor is correcly done
	@Test
	public void testActor() {
		assertEquals("UNKNOWN", ActionContext.getActor());
		ActionContext.setActor("ACTOR");
		assertEquals("ACTOR", ActionContext.getActor());
		// TODO: null should turn into UNKNOWN?
		// ActionContext.setActor(null);
		// assertEquals("UNKOWN", ActionContext.getActor());
	}

	// Assert that this functions gets the correct event
	@Test
	public void testGetEvent() {
		assertEquals(ActionEvent.UNKNOWN, ActionContext.getEvent());
	}

	// Assert that this functions sets the event and get it afterwards
	@Test
	public void testSetEvent() {
		ActionContext.setEvent(ActionEvent.NOTIFICATION);
		assertEquals(ActionEvent.NOTIFICATION, ActionContext.getEvent());
	}

	// Verify that adding an ActionTarget is processed
	@Test
	public void testAddActionTarget() {
		assertEquals("expected empty target", 0, ActionContext.getAction().getTargets().size());
		ActionContext.addActionTarget(null);
		assertEquals("expected empty target", 0, ActionContext.getAction().getTargets().size());
		ActionTarget actionTarget = mock(ActionTarget.class);
		ActionContext.addActionTarget(actionTarget);
		assertEquals(1, ActionContext.getAction().getTargets().size());
	}

	// Verify that adding an ActionTarget with ActionTargetInOut is processed
	@Test
	public void testAddActionTargetInOut() {
		assertEquals("expected empty target", 0, ActionContext.getAction().getTargets().size());
		ActionContext.addActionTarget(null, ActionTargetInOut.IN);
		ActionContext.addActionTarget(null, ActionTargetInOut.OUT);
		assertEquals("expected empty target", 0, ActionContext.getAction().getTargets().size());
		ActionTarget actionTarget = mock(ActionTarget.class);
		ActionContext.addActionTarget(actionTarget, ActionTargetInOut.OUT);
		assertEquals(1, ActionContext.getAction().getTargets().size());
		assertEquals(ActionTargetInOut.OUT, ActionContext.getAction().getTargets().get(0).getDirection());
	}

}
