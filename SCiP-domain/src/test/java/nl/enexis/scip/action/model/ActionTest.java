package nl.enexis.scip.action.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import nl.enexis.scip.action.model.ActionMessage.Level;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

//Note that this class implemented by using hamcrest
public class ActionTest {

	@Spy
	private Action spyAction;

	private ActionMessage actionMessage;

	private Action action;

	private List<ActionMessage> actionMessageList;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		actionMessageList = new ArrayList<ActionMessage>();
		action = new Action();
		actionMessage = new ActionMessage();
	}

	private void addLevelToActionMessage(Level levelType) {
		actionMessage.setLevel(levelType);
		actionMessageList.add(actionMessage);
	}

	// Assure that in case of no success an exception is raised
	@Test
	public void when_NotSucces_Expect_ContainsErrors() {
		when(spyAction.isSuccess()).thenReturn(false);
		assertThat(spyAction.containsErrors(), is(true));
	}

	// Assure that in case of success no exception is raised
	@Test
	public void when_IsSuccesAndNoErrorMessages_Expect_NotContainsErrors() {
		when(spyAction.isSuccess()).thenReturn(true);
		this.addLevelToActionMessage(Level.INFO);
		when(spyAction.getMessages()).thenReturn(actionMessageList);

		assertThat(spyAction.containsErrors(), is(false));
	}

	// Assure that in case of success while containing errors an exception is raised
	@Test
	public void when_IsSuccesAndErrorMessages_Expect_ContainsErrors() {
		when(spyAction.isSuccess()).thenReturn(true);
		this.addLevelToActionMessage(Level.ERROR);
		when(spyAction.getMessages()).thenReturn(actionMessageList);

		assertThat(spyAction.containsErrors(), is(true));
	}

	// Assure that in case containing errors, a warning is expected
	@Test
	public void when_ContainsErrors_Expect_ContainsWarnings() {
		when(spyAction.containsErrors()).thenReturn(true);

		assertThat(spyAction.containsWarnings(true), is(true));
	}

	// Assert that, if no warnings and errors, none of them are expected
	@Test
	public void when_NotIncludeErrorsAndNoWarningMessages_Expect_NotContainsWarnings() {
		this.addLevelToActionMessage(Level.DEBUG);
		when(spyAction.getMessages()).thenReturn(actionMessageList);

		assertThat(spyAction.containsWarnings(false), is(false));
	}

	// Assert that, if no warnings and errors, none of them are expected
	@Test
	public void When_NotIncludeErrorsAndAddWarningMessages_Expect_ContainsWarnings() {
		this.addLevelToActionMessage(Level.WARN);
		when(spyAction.getMessages()).thenReturn(actionMessageList);

		assertThat(spyAction.containsWarnings(false), is(true));
	}

	// Assert that, if no warnings and errors, none of them are expected
	@Test
	public void When_ReflexiveHashCode_Expect_HashCodeMatches() {
		action.setId("123abc");

		assertThat(action.hashCode(), is(equalTo(action.hashCode())));
	}

	// Note that these hashcode and equal unit tests are also shown as example

	// Assert that the hashcode, as intended, is indeed symmetric
	@Test
	public void When_SymmetricHashCode_Expect_HashCodeMatches() {
		Action actionX = new Action();
		actionX.setId("xyz789");
		Action actionY = new Action();
		actionY.setId("xyz789");

		assertThat(actionX.hashCode(), is(equalTo(actionY.hashCode())));
		assertThat(actionY.hashCode(), is(equalTo(actionX.hashCode())));
	}

	// Assert that the different action ids are not equal
	@Test
	public void When_ActionIdDiffers_Expect_ObjectsDoNotEqual() {
		Action actionX = new Action();
		actionX.setId("123abc");
		Action actionY = new Action();
		actionY.setId("not123abc");

		assertFalse(actionX.equals(actionY));
	}

	// Assert that the hashcode, as intended is indeed reflexive
	@Test
	public void When_ReflexiveHashCode_Expect_EqualsMatches() {
		action.setId("123abc");

		assertTrue(action.equals(action));
	}

	// Assert that when Action's are equal the hashcode's are also equal
	@Test
	public void When_SymmetricHashCode_Expect_EqualsMatches() {
		Action actionX = new Action();
		actionX.setId("xyz789");
		Action actionY = new Action();
		actionY.setId("xyz789");

		assertTrue(actionX.equals(actionY));
		assertTrue(actionY.equals(actionX));

		assertEquals(actionX.hashCode(), actionY.hashCode());
	}
}
