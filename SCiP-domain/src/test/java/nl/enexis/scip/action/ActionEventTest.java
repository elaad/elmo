package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ActionEventTest {

	// Assert that this function gets the correct action target id
	@Test
	public void testGetActionTargetId() {
		assertEquals("CALCULATE_FORECAST", ActionEvent.CALCULATE_FORECAST.getActionTargetId());
	}

	// Assert that the correct action target name is retrieved
	@Test
	public void testGetActionTargetName() {
		assertEquals("nl.enexis.scip.action.ActionEvent", ActionEvent.CONFIGURATION.getActionTargetName());
	}

}
