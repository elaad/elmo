package nl.enexis.scip.action;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Logger.class)
public class ActionLoggerTest {

	@Before
	public void setup() {
		PowerMockito.mockStatic(Logger.class);
	}

	@After
	public void tearDown() {
		ActionContext.removeAction();
	}

	// Verify that ActionLogger.trace logs the message correctly
	@Test
	public void testTrace() {
		// prepare
		Logger mockLogger = mock(Logger.class);
		when(mockLogger.isTraceEnabled()).thenReturn(false).thenReturn(true);
		when(Logger.getLogger(anyString())).thenReturn(mockLogger);
		ActionException actionException = new ActionException();
		// pre-verify ("nul(l)-meting")
		assertEquals(0, ActionContext.getAction().getMessages().size());

		// execute
		ActionLogger.trace("MESSAGE", new Object[] {}, actionException, false);

		// verify
		assertEquals(0, ActionContext.getAction().getMessages().size());
		ActionLogger.trace("MESSAGE", new Object[] {}, actionException, true);
		assertEquals(1, ActionContext.getAction().getMessages().size());
		assertEquals("MESSAGE", ActionContext.getAction().getMessages().get(0).getMessage());

		PowerMockito.verifyStatic(times(2));
		Logger.getLogger(eq("nl.enexis.scip.action.ActionLoggerTest"));

		verify(mockLogger).log(eq("nl.enexis.scip.action.ActionLoggerTest"), eq(Level.TRACE), eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]"),
				any(Object[].class), eq(actionException));
	}

	// Verify that ActionLogger.debug logs the message correctly
	@Test
	public void testDebug() {
		// prepare
		Logger mockLogger = Mockito.mock(Logger.class);
		when(mockLogger.isDebugEnabled()).thenReturn(false).thenReturn(true);
		when(Logger.getLogger(anyString())).thenReturn(mockLogger);
		ActionException actionException = new ActionException();
		// pre-verify ("nul(l)-meting")
		assertEquals(0, ActionContext.getAction().getMessages().size());
		// execute
		ActionLogger.debug("MESSAGE", new Object[] {}, actionException, false);
		// verify
		assertEquals(0, ActionContext.getAction().getMessages().size());
		ActionLogger.debug("MESSAGE", new Object[] {}, actionException, true);
		assertEquals(1, ActionContext.getAction().getMessages().size());
		assertEquals("MESSAGE", ActionContext.getAction().getMessages().get(0).getMessage());

		PowerMockito.verifyStatic(times(2));
		Logger.getLogger(eq("nl.enexis.scip.action.ActionLoggerTest"));

		verify(mockLogger).log(eq("nl.enexis.scip.action.ActionLoggerTest"), eq(Level.DEBUG), eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]"),
				any(Object[].class), eq(actionException));
	}

	// Verify that ActionLogger.info logs the message correctly
	@Test
	public void testInfo() {
		// prepare
		Logger mockLogger = Mockito.mock(Logger.class);
		when(mockLogger.isInfoEnabled()).thenReturn(false).thenReturn(true);
		when(Logger.getLogger(anyString())).thenReturn(mockLogger);
		ActionException actionException = new ActionException();
		// pre-verify ("nul(l)-meting")
		assertEquals(0, ActionContext.getAction().getMessages().size());
		// execute
		ActionLogger.info("MESSAGE", new Object[] {}, actionException, false);
		// verify
		assertEquals(0, ActionContext.getAction().getMessages().size());
		ActionLogger.info("MESSAGE", new Object[] {}, actionException, true);
		assertEquals(1, ActionContext.getAction().getMessages().size());
		assertEquals("MESSAGE", ActionContext.getAction().getMessages().get(0).getMessage());

		PowerMockito.verifyStatic(times(2));
		Logger.getLogger(eq("nl.enexis.scip.action.ActionLoggerTest"));

		verify(mockLogger).log(eq("nl.enexis.scip.action.ActionLoggerTest"), eq(Level.INFO), eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]"),
				any(Object[].class), eq(actionException));
	}

	// Verify that ActionLogger.warn logs the message correctly
	@Test
	public void testWarn() {
		// prepare
		Logger mockLogger = Mockito.mock(Logger.class);
		when(mockLogger.isEnabled(any())).thenReturn(false).thenReturn(true);
		when(Logger.getLogger(anyString())).thenReturn(mockLogger);
		ActionException actionException = new ActionException();
		// pre-verify ("nul(l)-meting")
		assertEquals(0, ActionContext.getAction().getMessages().size());
		// execute
		ActionLogger.warn("MESSAGE", new Object[] {}, actionException, false);
		// verify
		assertEquals(0, ActionContext.getAction().getMessages().size());
		ActionLogger.warn("MESSAGE", new Object[] {}, actionException, true);
		assertEquals(1, ActionContext.getAction().getMessages().size());
		assertEquals("MESSAGE", ActionContext.getAction().getMessages().get(0).getMessage());

		PowerMockito.verifyStatic(times(2));
		Logger.getLogger(eq("nl.enexis.scip.action.ActionLoggerTest"));

		verify(mockLogger).log(eq("nl.enexis.scip.action.ActionLoggerTest"), eq(Level.WARN), eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]"),
				any(Object[].class), eq(actionException));
	}

	// Verify that ActionLogger.error logs the message correctly
	@Test
	public void testError() {
		// prepare
		Logger mockLogger = Mockito.mock(Logger.class);
		when(mockLogger.isEnabled(any())).thenReturn(false).thenReturn(true);
		when(Logger.getLogger(anyString())).thenReturn(mockLogger);
		ActionException actionException = new ActionException();
		// pre-verify ("nul(l)-meting")
		assertEquals(0, ActionContext.getAction().getMessages().size());
		// execute
		ActionLogger.error("MESSAGE", new Object[] {}, actionException, false);
		// verify
		assertEquals(0, ActionContext.getAction().getMessages().size());
		ActionLogger.error("MESSAGE", new Object[] {}, actionException, true);
		assertEquals(1, ActionContext.getAction().getMessages().size());
		assertEquals("MESSAGE", ActionContext.getAction().getMessages().get(0).getMessage());

		PowerMockito.verifyStatic(times(2));
		Logger.getLogger(eq("nl.enexis.scip.action.ActionLoggerTest"));

		verify(mockLogger).log(eq("nl.enexis.scip.action.ActionLoggerTest"), eq(Level.ERROR), eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]"),
				any(Object[].class), eq(actionException));
	}

}
