package nl.enexis.scip.action.interceptor;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.interceptor.InvocationContext;

import nl.enexis.scip.action.ActionContext;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.ActionTarget;
import nl.enexis.scip.action.ActionTargetInOut;
import nl.enexis.scip.action.ActionUtil;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.service.ActionContextService;
import nl.enexis.scip.service.MailService;
import nl.enexis.scip.timer.TimerInterface;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ActionUtil.class, ActionLogger.class, ActionContext.class })
public class ActionContextInterceptorTest {

	@InjectMocks
	private ActionContextInterceptor actionContextInterceptor;

	@Mock
	private ActionContextService actionContextService;

	@Mock
	private MailService mailService;

	private InvocationContext ctx;

	private Action action;

	@Before
	public void setup() throws NoSuchMethodException {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ActionUtil.class);
		PowerMockito.mockStatic(ActionLogger.class);
		this.ctx = this.prepareTestOfHandleActionContext();
	}

	// Used to avoid common prepares
	private InvocationContext prepareTestOfHandleActionContext() throws NoSuchMethodException {
		InvocationContext ctx = mock(InvocationContext.class);
		Method method = this.getClass().getMethod("dummyMethod");
		TimerInterface timerEvent = mock(TimerInterface.class);

		when(ctx.getMethod()).thenReturn(method);
		when(ctx.getParameters()).thenReturn(new Object[] {});
		when(ctx.getTarget()).thenReturn(timerEvent);

		PowerMockito.mockStatic(ActionContext.class);

		mock(ActionContext.class);

		action = spy(Action.class);
		ActionMessage actionMessage = new ActionMessage();
		actionMessage.setLevel(ActionMessage.Level.ERROR);
		List<ActionMessage> listOfActionMessages = new ArrayList<ActionMessage>();
		listOfActionMessages.add(actionMessage);
		when(action.getMessages()).thenReturn(listOfActionMessages);

		PowerMockito.when(ActionContext.getAction()).thenReturn(action);

		return ctx;
	}

	// Verifies an occurrence of a correctly handled ActionContextInterceptor without including an action target
	@Test
	public void testHandleActionContext_BreakAndDirectNotification() throws Exception {
		// execute
		actionContextInterceptor.handleActionContext(ctx);

		// verify
		verify(action).setSuccess(eq(false));

		// finally
		PowerMockito.verifyStatic(times(1));
		ActionLogger.info(eq("Event ended"), any(Object[].class), any(Throwable.class), eq(true));

		verify(mailService).sendDirectActionNotification(action);
	}

	// Verifies an occurrence of a correctly handled ActionContextInterceptor including an action target
	@Test
	public void testHandleActionContext_BreakAndNoDirectNotification() throws Exception {
		// prepare
		Meter meter = new Meter();
		meter.setMeterId("METERID");
		when(ctx.getParameters()).thenReturn(new Object[] { meter });

		Meter dSO = new Meter();
		dSO.setMeterId("DSO");
		when(ctx.proceed()).thenReturn(dSO);

		// nullmeting
		assertEquals(0, ActionContext.getAction().getTargets().size());

		// execute
		actionContextInterceptor.handleActionContext(ctx);

		// verify
		PowerMockito.verifyStatic(times(1));
		ActionUtil.setActionTarget(any(Action.class), eq((ActionTarget) meter), eq(ActionTargetInOut.IN));
		PowerMockito.verifyStatic(times(2));
		ActionUtil.setActionTarget(any(Action.class), any(ActionTarget.class), eq(ActionTargetInOut.IN));
		PowerMockito.verifyStatic(times(1));
		ActionLogger.info(eq("Event started"), any(Object[].class), any(Throwable.class), eq(true));
		PowerMockito.verifyStatic(times(1));
		ActionUtil.setActionTarget(any(Action.class), eq((ActionTarget) dSO), eq(ActionTargetInOut.OUT));

		// finally
		PowerMockito.verifyStatic(times(1));
		ActionLogger.info(eq("Event ended"), any(Object[].class), any(Throwable.class), eq(true));
	}

	// Assures that ActionExceptions are raised in case of failures and verifies them
	// @Test(expected=ActionException.class)
	@Test
	public void testHandleActionContext_CatchInProceedInvocationContext() throws Exception {
		// prepare
		when(ctx.proceed()).thenThrow(new ActionException("PROCEEDACTIONEXCEPTION"));

		// execute
		try {
			actionContextInterceptor.handleActionContext(ctx);
			fail("Expected PROCEEDACTIONEXCEPTION, but no exception was thrown.");
		} catch (ActionException e) {
			assertEquals("PROCEEDACTIONEXCEPTION", e.getMessage());
		}

		// verify
		verify(action).setSuccess(eq(false));
		PowerMockito.verifyStatic(times(1));
		ActionLogger.error(eq("Action exception: PROCEEDACTIONEXCEPTION"), any(Object[].class), any(Throwable.class), eq(true));

		// finally
		PowerMockito.verifyStatic(times(1));
		ActionLogger.info(eq("Event ended"), any(Object[].class), any(Throwable.class), eq(true));
	}

	// Do no throw away, used in tests as replacement for Method
	@nl.enexis.scip.action.interceptor.ActionContext
	public void dummyMethod() {
	}
}