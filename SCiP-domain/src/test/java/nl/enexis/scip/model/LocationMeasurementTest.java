package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import nl.enexis.scip.model.enumeration.MeasurementType;

import org.junit.Test;

public class LocationMeasurementTest {

	// Assert that getting the action target id consists of the correct combination of data
	@Test
	public void testGetActionTargetId() {
		Location location = new Location();
		location.setLocationId("LOCATIONID");
		MeasurementType measurementType = MeasurementType.COS1;
		LocationMeasurement locationMeasurement = new LocationMeasurement();
		locationMeasurement.setLocation(location);
		locationMeasurement.setMeasurementType(measurementType);
		locationMeasurement.setForecast(true);

		assertEquals("LOCATIONID-COS1-F", locationMeasurement.getActionTargetId());

		locationMeasurement.setForecast(false);

		assertEquals("LOCATIONID-COS1-O", locationMeasurement.getActionTargetId());
	}
}
