package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CableCspTest {
	private CableCsp cableCsp;

	@Before
	public void setup() {
		cableCsp = new CableCsp();
	}

	// Assert the correct boolean in case of active or no active csp connection
	@Test
	public void testHasActiveCspConnections() {

		CspConnection cspConnection1 = new CspConnection();
		cspConnection1.setActive(false);
		CspConnection cspConnection2 = new CspConnection();
		cspConnection2.setActive(true);

		List<CspConnection> cspConnectionList = new ArrayList<CspConnection>();
		cspConnectionList.add(cspConnection1);

		cableCsp.setCspConnections(cspConnectionList);

		assertEquals(false, cableCsp.hasActiveCspConnections());

		cspConnectionList.add(cspConnection2);

		assertEquals(true, cableCsp.hasActiveCspConnections());
	}

	// Assert that the correct action target id is retrieved
	@Test
	public void testGetActionTargetId() {
		Cable cable = new Cable();
		cable.setCableId("CABLEID");
		Csp csp = new Csp();
		csp.setEan13("EAN13");

		cableCsp.setCsp(csp);
		cableCsp.setCable(cable);

		assertEquals("CABLEID-EAN13", cableCsp.getActionTargetId());
	}
}
