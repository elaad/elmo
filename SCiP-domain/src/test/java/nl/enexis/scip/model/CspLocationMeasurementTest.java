package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

public class CspLocationMeasurementTest {

	// Assert that getting the action target id equals the csp ean13 and its the location measurement id
	@Test
	public void testGetActionTargetId() {
		CspLocationMeasurement cspLocationMeasurement = new CspLocationMeasurement();
		Csp csp = new Csp();
		csp.setEan13("EAN13");
		LocationMeasurement locationMeasurement = mock(LocationMeasurement.class);
		when(locationMeasurement.getActionTargetId()).thenReturn("ACTIONTARGETID");
		cspLocationMeasurement.setCsp(csp);
		cspLocationMeasurement.setLocationMeasurement(locationMeasurement);

		assertEquals("EAN13-ACTIONTARGETID", cspLocationMeasurement.getActionTargetId());
	}
}
