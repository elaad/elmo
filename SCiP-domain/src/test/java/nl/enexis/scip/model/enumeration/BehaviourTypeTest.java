package nl.enexis.scip.model.enumeration;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.faces.model.SelectItem;

import org.junit.Test;

public class BehaviourTypeTest {

	// Assert that this function correctly implements contains
	@Test
	public void testContains() {
		assertEquals(true, BehaviourType.contains("SU"));
		assertEquals(true, BehaviourType.contains("SC"));
		assertEquals(false, BehaviourType.contains("NOTEXISTS"));
	}

	// Assert that the label and value are correctly linked to the behaviour
	@Test
	public void testToSelectItems() {
		List<SelectItem> behaviourList = BehaviourType.toSelectItems();
		assertEquals("Smart Charging", behaviourList.get(0).getLabel().toString());
		assertEquals("Smart Charging", behaviourList.get(0).getValue().toString());

		assertEquals("Smart Usage", behaviourList.get(1).getLabel().toString());
		assertEquals("Smart Usage", behaviourList.get(1).getValue().toString());
	}
}
