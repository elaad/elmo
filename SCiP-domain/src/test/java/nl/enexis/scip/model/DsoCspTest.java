package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DsoCspTest {

	// Assert that getting the action target id equals the dso ean13 and the csp ean13
	@Test
	public void testGetActionTargetId() {
		Dso dso = new Dso();
		dso.setEan13("EAN13DSO");
		Csp csp = new Csp();
		csp.setEan13("EAN13CSP");
		DsoCsp dsoCsp = new DsoCsp();
		dsoCsp.setDso(dso);
		dsoCsp.setCsp(csp);

		assertEquals("EAN13DSO-EAN13CSP", dsoCsp.getActionTargetId());
	}
}
