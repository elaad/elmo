package nl.enexis.scip.model.enumeration;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class DatabaseActionTest {

	// Assert that the each database action is correctly linked to its abbreviation
	@Test
	public void testFromValue() {
		assertEquals(DatabaseAction.UPDATE, DatabaseAction.fromValue("U"));
		assertEquals(DatabaseAction.DELETE, DatabaseAction.fromValue("D"));
		assertEquals(DatabaseAction.INSERT, DatabaseAction.fromValue("I"));
		try {
			DatabaseAction.fromValue("NOTHING");
			fail("No exception thrown");
		} catch (IllegalArgumentException iae) {
			assertEquals("NOTHING", iae.getMessage());
		}
	}
}
