package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class TransformerTest {

	// Assert that the count of active meters is correctly implemented
	@Test
	public void testGetActiveMetersCount() {
		Meter meter1 = new Meter();
		meter1.setActive(true);
		Meter meter2 = new Meter();
		meter2.setActive(true);
		Meter meter3 = new Meter();
		meter3.setActive(true);
		List<Meter> meterList = new ArrayList<Meter>();
		Transformer transformer = new Transformer();

		assertEquals(0, transformer.getActiveMetersCount());

		meterList.add(meter1);
		meterList.add(meter2);
		meterList.add(meter3);
		transformer.setMeters(meterList);

		assertEquals(3, transformer.getActiveMetersCount());
	}

}
