package nl.enexis.scip.model;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class CableTest {

	private Cable cable;

	@Before
	public void setup() {
		cable = new Cable();
	}

	// Assert that the value is larger than 0
	@Test
	public void testIsFixedForecastSetCorrectly() {
		// nullmeting, default is false
		// assertTrue(cable.isFixedForecastSetCorrectly());

		cable.setFixedForecastMax(new BigDecimal(0));
		assertEquals(false, cable.isFixedForecastSetCorrectly());

		cable.setFixedForecastMax(new BigDecimal(-1));
		assertEquals(false, cable.isFixedForecastSetCorrectly());

		cable.setFixedForecastMax(new BigDecimal(1));
		assertEquals(true, cable.isFixedForecastSetCorrectly());
	}

	// Assert that the maximum car capacity is not more than the maximum capacity
	@Test
	public void testIsMaximumCarCapacityNotMoreThanMaximumCapacity() {
		cable.setMaxCarCapacity(new BigDecimal(-1));
		cable.setMaxCapacity(new BigDecimal(1));
		assertEquals(true, cable.isMaximumCarCapacityNotMoreThanMaximumCapacity());

		cable.setMaxCarCapacity(new BigDecimal(1));
		cable.setMaxCapacity(new BigDecimal(-1));
		assertEquals(false, cable.isMaximumCarCapacityNotMoreThanMaximumCapacity());

		cable.setMaxCarCapacity(new BigDecimal(0));
		cable.setMaxCapacity(new BigDecimal(0));
		assertEquals(true, cable.isMaximumCarCapacityNotMoreThanMaximumCapacity());
	}

	// Assert that the fixed forecast deviation is not more than the fixed forecast maximum
	@Test
	public void testIsFixedForecastDeviationNotMoreThanFixedForecastMaximum() {
		cable.setFixedForecastDeviation(new BigDecimal(-1));
		cable.setFixedForecastMax(new BigDecimal(1));
		assertEquals(true, cable.isFixedForecastDeviationNotMoreThanFixedForecastMaximum());

		cable.setFixedForecastDeviation(new BigDecimal(1));
		cable.setFixedForecastMax(new BigDecimal(-1));
		assertEquals(false, cable.isFixedForecastDeviationNotMoreThanFixedForecastMaximum());

		cable.setFixedForecastDeviation(new BigDecimal(0));
		cable.setFixedForecastMax(new BigDecimal(0));
		assertEquals(true, cable.isFixedForecastDeviationNotMoreThanFixedForecastMaximum());
	}

	// Assert that the fixed forecast maximum is not more than the maximum car capacity
	@Test
	public void testIsFixedForecastMaximumNotMoreThanMaxCarCapacity() {
		cable.setMaxCarCapacity(new BigDecimal(0)); // maxCarCapacity set on 0
													// returns true
		assertEquals(true, cable.isFixedForecastMaximumNotMoreThanMaxCarCapacity());

		cable.setFixedForecastMax(new BigDecimal(1));
		cable.setMaxCarCapacity(new BigDecimal(1));
		assertEquals(true, cable.isFixedForecastMaximumNotMoreThanMaxCarCapacity());

		cable.setFixedForecastMax(new BigDecimal(1));
		cable.setMaxCarCapacity(new BigDecimal(2));
		assertEquals(true, cable.isFixedForecastMaximumNotMoreThanMaxCarCapacity());

		cable.setFixedForecastMax(new BigDecimal(2));
		cable.setMaxCarCapacity(new BigDecimal(1));
		assertEquals(false, cable.isFixedForecastMaximumNotMoreThanMaxCarCapacity());
	}

	// Assert that the fixed forecast maximum is not more than the maximum capacity
	@Test
	public void testIsFixedForecastMaximumNotMoreThanMaxCapacity() {
		cable.setMaxCarCapacity(new BigDecimal(-1));
		assertEquals(false, cable.isFixedForecastMaximumNotMoreThanMaxCapacity());

		cable.setMaxCarCapacity(new BigDecimal(0));
		cable.setFixedForecastMax(new BigDecimal(1));
		cable.setMaxCapacity(new BigDecimal(1));
		assertEquals(true, cable.isFixedForecastMaximumNotMoreThanMaxCapacity());

		cable.setFixedForecastMax(new BigDecimal(1));
		cable.setMaxCapacity(new BigDecimal(2));
		assertEquals(true, cable.isFixedForecastMaximumNotMoreThanMaxCapacity());

		cable.setFixedForecastMax(new BigDecimal(1));
		cable.setMaxCapacity(new BigDecimal(1));
		assertEquals(true, cable.isFixedForecastMaximumNotMoreThanMaxCapacity());

		cable.setFixedForecastMax(new BigDecimal(2));
		cable.setMaxCapacity(new BigDecimal(1));
		assertEquals(false, cable.isFixedForecastMaximumNotMoreThanMaxCapacity());
	}

}
