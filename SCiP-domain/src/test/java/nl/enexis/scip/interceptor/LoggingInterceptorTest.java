package nl.enexis.scip.interceptor;

/*
 * #%L
 * SCiP-domain
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;

import javax.interceptor.InvocationContext;

import nl.enexis.scip.interceptor.annotation.Log;
import nl.enexis.scip.interceptor.annotation.enumeration.LogLevel;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Logger.class, Log.class })
public class LoggingInterceptorTest {

	@InjectMocks
	private LoggingInterceptor loggingInterceptor;

	private InvocationContext ctx;
	private Logger mockLogger;

	@Before
	public void setup() throws NoSuchMethodException {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(Logger.class);
		this.ctx = this.prepare();
	}

	private InvocationContext prepare() throws NoSuchMethodException {
		InvocationContext ctx = mock(InvocationContext.class);
		this.mockLogger = mock(Logger.class);

		when(ctx.getParameters()).thenReturn(new Object[] { this });
		when(Logger.getLogger(any(Class.class))).thenReturn(mockLogger);
		when(mockLogger.isEnabled(any(Level.class))).thenReturn(true);

		return ctx;
	}

	// Assert that the logInterceptor logs the corresponding class on debug level
	@Test
	public void testLogInvocationAndLog_Debug() throws Exception {
		Method method = this.getClass().getMethod("dummyMethodLogDEBUG");
		when(ctx.getMethod()).thenReturn(method);
		when(ctx.proceed()).thenReturn("PROCEED");

		String result = loggingInterceptor.logInvocation(ctx).toString();

		verify(mockLogger, times(2)).log(eq("nl.enexis.scip.interceptor.LoggingInterceptorTest"), eq(Level.DEBUG),
				eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]"), any(Object[].class), any(Throwable.class));
		verify(mockLogger, times(1)).log(eq("nl.enexis.scip.interceptor.LoggingInterceptorTest"), eq(Level.DEBUG),
				eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}] [{5}] [{6}] [{7}]"), any(Object[].class), any(Throwable.class));
		verify(mockLogger, times(1)).log(eq("nl.enexis.scip.interceptor.LoggingInterceptorTest"), eq(Level.DEBUG),
				eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}] [{5}] [{6}]"), any(Object[].class), any(Throwable.class));

		assertEquals("PROCEED", result);
	}

	// Assert that the logInterceptor logs the corresponding class on exception level
	@Test
	public void testLogInvocationAndLog_Exception() throws Exception {
		Method method = this.getClass().getMethod("dummyMethodLogTRACE");
		when(ctx.getMethod()).thenReturn(method);
		Exception proceedException = new Exception("PROCEEDEXCEPTION");
		when(ctx.proceed()).thenThrow(proceedException);

		try {
			loggingInterceptor.logInvocation(ctx);
			fail("Expected PROCEEDEXCEPTION, but no exception was thrown.");
		} catch (Exception e) {
			assertEquals("PROCEEDEXCEPTION", e.getMessage());
		}

		// verify exception
		verify(mockLogger).log(eq("nl.enexis.scip.interceptor.LoggingInterceptorTest"), eq(Level.TRACE),
				eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]"), any(Object[].class), any(Throwable.class));
		verify(mockLogger).log(eq("nl.enexis.scip.interceptor.LoggingInterceptorTest"), eq(Level.ERROR),
				eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}] [{5}]"), any(Object[].class), eq(proceedException));
	}

	// Assert that the logInterceptor logs the corresponding class on info level
	@Test
	public void testLogInvocationAndLog_Info() throws Exception {
		Method method = this.getClass().getMethod("dummyMethodLogINFO");
		when(ctx.getMethod()).thenReturn(method);

		loggingInterceptor.logInvocation(ctx);

		verify(mockLogger, times(2)).log(eq("nl.enexis.scip.interceptor.LoggingInterceptorTest"), eq(Level.INFO),
				eq("[{0}] [ELMO] [{1}] [{2}] [{3}] [{4}]"), any(Object[].class), any(Throwable.class));
	}

	// function mocks
	@nl.enexis.scip.interceptor.annotation.Log
	public void dummyMethodLog() {
	}

	@nl.enexis.scip.interceptor.annotation.Log(logLevel = LogLevel.DEBUG, logParams = true, logStackTrace = false)
	public void dummyMethodLogDEBUG() {
	}

	@nl.enexis.scip.interceptor.annotation.Log(logLevel = LogLevel.TRACE, logParams = false, logStackTrace = true)
	public void dummyMethodLogTRACE() {
	}

	@nl.enexis.scip.interceptor.annotation.Log(logLevel = LogLevel.INFO, logParams = false, logStackTrace = false)
	public void dummyMethodLogINFO() {
	}

}
