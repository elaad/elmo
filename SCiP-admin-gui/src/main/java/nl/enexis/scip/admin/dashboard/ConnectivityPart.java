package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.service.DashboardService;

@Named
@ApplicationScoped
public class ConnectivityPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = 726565705877246049L;

	private List<ConnectivityErrorCounts> counts;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	public ConnectivityPart() {
	}

	//@PostConstruct
	@Override
	public void reset() throws ActionException {
		Map<String, Long> fourWeek = dashboardService.getConnectivityErrorCounts(28);
		Map<String, Long> week = dashboardService.getConnectivityErrorCounts(7);
		Map<String, Long> day = dashboardService.getConnectivityErrorCounts(1);

		counts = new ArrayList<ConnectivityErrorCounts>();

		for (String party : fourWeek.keySet()) {
			ConnectivityErrorCounts eventCounts = new ConnectivityErrorCounts(party);
			int index = counts.indexOf(eventCounts);
			if (index < 0) {
				eventCounts.setFourWeek(fourWeek.get(party));
				counts.add(eventCounts);
			} else {
				counts.get(index).setFourWeek(fourWeek.get(party));
			}
		}

		for (String party : week.keySet()) {
			ConnectivityErrorCounts eventCounts = new ConnectivityErrorCounts(party);
			int index = counts.indexOf(eventCounts);
			counts.get(index).setWeek(week.get(party));
		}

		for (String party : day.keySet()) {
			ConnectivityErrorCounts eventCounts = new ConnectivityErrorCounts(party);
			int index = counts.indexOf(eventCounts);
			counts.get(index).setDay(day.get(party));
		}

		Collections.sort(counts);
	}

	public List<ConnectivityErrorCounts> getCounts() {
		return this.counts;
	}

	public class ConnectivityErrorCounts implements Serializable, Comparable<ConnectivityErrorCounts> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3674054838209952987L;

		private String party;
		private Long day = (long) 0;
		private Long week = (long) 0;
		private Long fourWeek = (long) 0;

		public ConnectivityErrorCounts(String party) {
			this.party = party;
		}

		public String getParty() {
			return party;
		}

		public void setParty(String party) {
			this.party = party;
		}

		public Long getDay() {
			return day;
		}

		public void setDay(Long day) {
			this.day = day;
		}

		public Long getWeek() {
			return week;
		}

		public void setWeek(Long week) {
			this.week = week;
		}

		public Long getFourWeek() {
			return fourWeek;
		}

		public void setFourWeek(Long fourWeek) {
			this.fourWeek = fourWeek;
		}

		private ConnectivityPart getOuterType() {
			return ConnectivityPart.this;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (party == null ? 0 : party.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof ConnectivityErrorCounts)) {
				return false;
			}
			ConnectivityErrorCounts other = (ConnectivityErrorCounts) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (party == null) {
				if (other.party != null) {
					return false;
				}
			} else if (!party.equals(other.party)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(ConnectivityErrorCounts o) {
			int compare = this.day.compareTo(o.day);
			if (compare == 0) {
				compare = this.week.compareTo(o.week);
				if (compare == 0) {
					compare = this.fourWeek.compareTo(o.fourWeek);
				}
			}
			return -1 * compare;
		}
	}
}
