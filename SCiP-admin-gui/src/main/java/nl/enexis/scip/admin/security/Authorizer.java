package nl.enexis.scip.admin.security;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.admin.security.interceptor.AuthorizationInterceptor;

import org.picketlink.Identity;

@Named
@SessionScoped
public class Authorizer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4761263822746051534L;

	@Inject
	Identity identity;

	public Authorizer() {
	}

	public boolean isAuthorized(String role) {
		return AuthorizationInterceptor.checkRole(identity, role);
	}
}