package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.Collection;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionContext;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.service.TopologyStorageService;

import org.richfaces.component.UIExtendedDataTable;

@Named
@SessionScoped
@FacesConverter("nl.enexis.scip.admin.Topology")
public class Topology implements Serializable {
	/**
     * 
     */
	private static final long serialVersionUID = 5610058691750631142L;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	@Named("forecastChart")
	transient ForecastChart forecastChart;

	@Inject
	@Named("topologyCable")
	transient TopologyCable topologyCable;

	private Collection<Object> selectionCable;
	private Collection<Object> selectionCableCsp;
	private Cable selectedCable;
	private CableCsp selectedCableCsp;
	private String actionId = "";

	public Topology() {
	}

	public void refresh() {
		this.selectedCableCsp = null;
		this.selectionCableCsp = null;

		this.selectionCable = null;
		this.selectedCable = null;

		topologyCable.resetAll();
	}

	public String forecastChart() {
		if (this.selectedCable == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage("No cable selected!");
			context.addMessage(null, message);
			return "";
		} else {
			forecastChart.search();
			return "forecastChart";
		}
	}

	public String measurementExport() {
		if (this.selectedCable == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage("No cable selected!");
			context.addMessage(null, message);
			return "";
		} else {
			return "measurementExport";
		}
	}

	public String masterData() {
		return "topologyMasterData";
	}

	public void cableSelectionListener(AjaxBehaviorEvent event) {
		selectedCableCsp = null;
		selectionCableCsp = null;

		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		for (Object selectionKey : selectionCable) {
			dataTable.setRowKey(selectionKey);
			if (dataTable.isRowAvailable()) {
				selectedCable = (Cable) dataTable.getRowData();
				String cableId = selectedCable.getCableId();
				try {
					selectedCable = topologyStorageService.getCompleteCableTopology(cableId);
				} catch (ActionException e) {
					e.printStackTrace();
				}
			}
		}
		dataTable.setRowKey(originalKey);
	}

	public void cableCspSelectionListener(AjaxBehaviorEvent event) {
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		for (Object selectionKey : selectionCableCsp) {
			dataTable.setRowKey(selectionKey);
			if (dataTable.isRowAvailable()) {
				selectedCableCsp = (CableCsp) dataTable.getRowData();
			}
		}
		dataTable.setRowKey(originalKey);
	}

	public Collection<Object> getSelectionCable() {
		return selectionCable;
	}

	public void setSelectionCable(Collection<Object> selectionCable) {
		this.selectionCable = selectionCable;
	}

	public Collection<Object> getSelectionCableCsp() {
		return selectionCableCsp;
	}

	public void setSelectionCableCsp(Collection<Object> selectionCableCsp) {
		this.selectionCableCsp = selectionCableCsp;
	}

	public Cable getSelectedCable() {
		if (selectedCable == null) {
			return null;
		}

		if (!ActionContext.getId().equals(this.actionId)) {
			String cableId = selectedCable.getCableId();
			try {
				selectedCable = topologyStorageService.getCompleteCableTopology(cableId);
			} catch (ActionException e) {
				e.printStackTrace();
			}
			
			this.actionId = ActionContext.getId();
		} 

		return selectedCable;
	}

	public void setSelectedCable(Cable selectedCable) {
		this.selectedCable = selectedCable;
	}

	public CableCsp getSelectedCableCsp() {
		if (selectedCableCsp == null) {
			return null;
		}

		for (CableCsp cableCsp : selectedCable.getCableCsps()) {
			if (selectedCableCsp.equals(cableCsp)) {
				selectedCableCsp = cableCsp;
				break;
			}
		}

		return selectedCableCsp;
	}

	public void setSelectedCableCsp(CableCsp selectedCableCsp) {
		this.selectedCableCsp = selectedCableCsp;
	}
}
