package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.admin.xls.XlsUtil;
import nl.enexis.scip.service.GeneralService;
import nl.enexis.scip.service.MeasurementStorageService;

@Named
@SessionScoped
public class ExportMeasurements implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5610058691750631142L;

    @Inject
    @Named("measurementStorageService")
    transient  MeasurementStorageService measurementStorageService;

	@Inject
	@Named("topology")
	transient Topology topology;

	@Inject
	@Named("generalService")
	transient GeneralService generalService;

	private Date fromDate;
	private Date toDate;
	
	public ExportMeasurements() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		this.toDate = new Date();
		try {
			this.fromDate = df.parse(df.format(this.toDate));
			Calendar c = Calendar.getInstance();
			c.setTime(new Date(this.fromDate.getTime()));
			c.add(Calendar.DATE, 1); // number of days to add
			this.toDate = c.getTime();
		} catch (ParseException e) {
			ActionLogger.error(e.getMessage(), new Object[] { }, e, true);
		}
		// this.search();
	}
	
	public String back() {
		return "topologyCable";
	}

	public void export() {
		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			String filename = "measurements-" + topology.getSelectedCable().getCableId() + ".xls";
			String template = generalService.getConfigurationParameter("ExportMeasurementsTemplate").getValue();
			Map<String, Object> beanmap = new HashMap<String, Object>();
			beanmap.put("measurements", measurementStorageService.getMeasurementForCableBetweenDates(
					this.fromDate, this.toDate, topology.getSelectedCable()));
			beanmap.put("cableId", topology.getSelectedCable().getCableId());
			beanmap.put("fromDate", this.fromDate);
			beanmap.put("toDate", this.toDate);
			
			XlsUtil.exportXlsContent(response, filename, template, beanmap);
			FacesContext.getCurrentInstance().responseComplete();
			
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getClass().getName() + ": " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage("popupmessage", message);
		}

	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}