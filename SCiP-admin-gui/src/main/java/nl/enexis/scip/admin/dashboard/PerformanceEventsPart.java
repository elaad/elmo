package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.service.DashboardService;

@Named
@ApplicationScoped
public class PerformanceEventsPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4715308817859525015L;

	private List<EventTurnaroundTimes> data;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	public PerformanceEventsPart() {
	}

	//@PostConstruct
	@Override
	public void reset() throws ActionException {
		Map<String, Double> week = dashboardService.getActionEventTurnaroundTimes(7);
		Map<String, Double> day = dashboardService.getActionEventTurnaroundTimes(1);

		data = new ArrayList<EventTurnaroundTimes>();

		for (String event : week.keySet()) {
			EventTurnaroundTimes eventTurnarounds = new EventTurnaroundTimes();
			eventTurnarounds.setEvent(event);
			int index = data.indexOf(eventTurnarounds);
			if (index < 0) {
				eventTurnarounds.setWeek(week.get(event));
				data.add(eventTurnarounds);
			} else {
				data.get(index).setWeek(week.get(event));
			}
		}

		for (String event : day.keySet()) {
			EventTurnaroundTimes eventTurnarounds = new EventTurnaroundTimes();
			eventTurnarounds.setEvent(event);
			int index = data.indexOf(eventTurnarounds);
			data.get(index).setDay(day.get(event));
		}

		Collections.sort(data);
	}

	public List<EventTurnaroundTimes> getData() {
		return this.data;
	}

	public class EventTurnaroundTimes implements Serializable, Comparable<EventTurnaroundTimes> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3674054838209952987L;

		private String event;
		private Double day;
		private Double week;

		public EventTurnaroundTimes() {
		}

		public String getEvent() {
			return event;
		}

		public void setEvent(String event) {
			this.event = event;
		}

		public Double getDay() {
			return day;
		}

		public void setDay(Double day) {
			this.day = day;
		}

		public Double getWeek() {
			return week;
		}

		public void setWeek(Double week) {
			this.week = week;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (event == null ? 0 : event.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof EventTurnaroundTimes)) {
				return false;
			}
			EventTurnaroundTimes other = (EventTurnaroundTimes) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (event == null) {
				if (other.event != null) {
					return false;
				}
			} else if (!event.equals(other.event)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(EventTurnaroundTimes o) {
			int compare = this.day == null && o.day == null ? 0 : this.day == null ? -1 : o.day == null ? 1
					: this.day.compareTo(o.day);
			if (compare == 0) {
				compare = this.week == null && o.week == null ? 0 : this.week == null ? -1 : o.week == null ? 1
						: this.week.compareTo(o.week);
			}
			return -1 * compare;
		}

		private PerformanceEventsPart getOuterType() {
			return PerformanceEventsPart.this;
		}
	}
}
