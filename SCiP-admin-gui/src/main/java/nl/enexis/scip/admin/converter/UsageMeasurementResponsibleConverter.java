package nl.enexis.scip.admin.converter;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.SelectItem;

import nl.enexis.scip.model.enumeration.UsageMeasurementResponsible;

@FacesConverter(value = "usageMeasurementResponsibleConverter")
public class UsageMeasurementResponsibleConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}
		return UsageMeasurementResponsible.valueOf(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object obj) {
		if (obj == null) {
			return null;
		}
		return ((UsageMeasurementResponsible) obj).toString();
	}

	public static List<SelectItem> toSelectItems() {
		UsageMeasurementResponsible[] list = UsageMeasurementResponsible.values();
		List<SelectItem> listOptions = new ArrayList<SelectItem>();

		for (UsageMeasurementResponsible element : list) {
			SelectItem newItem = new SelectItem(element, element.toString());
			listOptions.add(newItem);
		}

		return listOptions;
	}
}
