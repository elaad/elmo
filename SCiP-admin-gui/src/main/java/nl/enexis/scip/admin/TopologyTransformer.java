package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.MeasurementProvider;
import nl.enexis.scip.model.Transformer;
import nl.enexis.scip.service.TopologyStorageService;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class TopologyTransformer extends TopologyGenericEntity<Transformer> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 2023729348130632437L;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	private Identity identity;

	private String editedMeasurementProviderId;
	private String newMeasurementProviderId;

	public TopologyTransformer() {
		this.type = Transformer.class;
	}

	@Override
	public void createNew() {
		super.createNew();
		this.newMeasurementProviderId = null;
	}

	@Override
	public void setEdited(Transformer edited) {
		super.setEdited(edited);
		this.editedMeasurementProviderId = edited.getMeasurementProvider().getId();
	}

	@Override
	protected void saveEntity() throws ActionException {
		Transformer transformer = this.getEdited();
		if (transformer != null) {
			MeasurementProvider measurementProvider = topologyStorageService
					.findMeasurementProviderById(this.editedMeasurementProviderId);
			if (measurementProvider != null) {
				transformer.setMeasurementProvider(measurementProvider);
			}
			topologyStorageService.updateTransformer(transformer);
		}
	}

	@Override
	protected void insertEntity() throws ActionException {
		Transformer transformer = this.getNewEntity();
		if (transformer != null) {
			MeasurementProvider measurementProvider = topologyStorageService
					.findMeasurementProviderById(this.newMeasurementProviderId);
			if (measurementProvider != null) {
				transformer.setMeasurementProvider(measurementProvider);
			}
			topologyStorageService.createTransformer(transformer);
		}
	}

	@Override
	protected void deleteEntity() throws ActionException {
		topologyStorageService.deleteTransformer(this.getDeleted());
	}

	@Override
	protected List<Transformer> getAllEntities() throws ActionException {
		return topologyStorageService.getAllTransformers();
	}

	@Override
	protected User getUser() {
		if (identity == null) {
			return null;
		}
		return (User) identity.getAccount();
	}

	public String getEditedMeasurementProviderId() {
		return editedMeasurementProviderId;
	}

	public void setEditedMeasurementProviderId(String editedMeasurementProviderId) {
		this.editedMeasurementProviderId = editedMeasurementProviderId;
	}

	public String getNewMeasurementProviderId() {
		return newMeasurementProviderId;
	}

	public void setNewMeasurementProviderId(String newMeasurementProviderId) {
		this.newMeasurementProviderId = newMeasurementProviderId;
	}

	public List<SelectItem> getAllTransformerOptions() {
		List<SelectItem> allTransformerOptions = new ArrayList<SelectItem>();

		for (Transformer trafo : this.getAll()) {
			allTransformerOptions.add(new SelectItem(trafo.getTrafoId(), trafo.getTrafoId()));
		}

		return allTransformerOptions;
	}
}
