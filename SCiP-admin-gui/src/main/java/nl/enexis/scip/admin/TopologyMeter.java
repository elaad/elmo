package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.model.Transformer;
import nl.enexis.scip.model.enumeration.MeasuresFor;
import nl.enexis.scip.service.TopologyStorageService;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class TopologyMeter extends TopologyGenericEntity<Meter> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -5347765789852781432L;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	private Identity identity;

	private String editedTransformerId;
	private String newTransformerId;

	public TopologyMeter() {
		this.type = Meter.class;
	}

	@Override
	public void createNew() {
		super.createNew();
		this.newTransformerId = "";
	}

	@Override
	public void setEdited(Meter edited) {
		super.setEdited(edited);
		this.editedTransformerId = edited.getTransformer().getTrafoId();
	}

	@Override
	protected void saveEntity() throws ActionException {
		Meter meter = this.getEdited();
		if (meter != null) {
			Transformer trafo = topologyStorageService.findTransformerByTrafoId(this.editedTransformerId);
			if (trafo != null) {
				meter.setTransformer(trafo);
			}
			topologyStorageService.updateMeter(meter);
		}
	}

	@Override
	protected void insertEntity() throws ActionException {
		Meter meter = this.getNewEntity();
		if (meter != null) {
			Transformer trafo = topologyStorageService.findTransformerByTrafoId(this.newTransformerId);
			if (trafo != null) {
				meter.setTransformer(trafo);
			}
			topologyStorageService.createMeter(meter);
		}
	}

	@Override
	protected void deleteEntity() throws ActionException {
		topologyStorageService.deleteMeter(this.getDeleted());
	}

	@Override
	protected List<Meter> getAllEntities() throws ActionException {
		return topologyStorageService.getAllMeters();
	}

	@Override
	protected User getUser() {
		if (identity == null) {
			return null;
		}
		return (User) identity.getAccount();
	}

	public String getEditedTransformerId() {
		return editedTransformerId;
	}

	public void setEditedTransformerId(String editedTransformerId) {
		this.editedTransformerId = editedTransformerId;
	}

	public String getNewTransformerId() {
		return newTransformerId;
	}

	public void setNewTransformerId(String newTransformerId) {
		this.newTransformerId = newTransformerId;
	}

    public List<SelectItem> getMeasuresForOptions() {
        return MeasuresFor.toSelectItems();
    }
}
