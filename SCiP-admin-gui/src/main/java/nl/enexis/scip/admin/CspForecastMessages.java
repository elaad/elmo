package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.richfaces.component.UIExtendedDataTable;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.service.ForecastStorageService;
import nl.enexis.scip.service.TopologyStorageService;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named
@SessionScoped
public class CspForecastMessages implements Serializable {
	/**
	 * This is the backing bean of the CSP forecast message GUI.
	 * The user chooses a CSP and a cable. Choosing one limits the selection of the other to sensible values.
	 * A search button will bring up the list of forecast messages found in that period.
	 * A detail view lists the forecasts (there can be more than one in a message).
	 * Inspecting a forecast will pop up an pane with forecast values. 
	 */
	private static final long serialVersionUID = 4284509460928219553L;
    
    @Inject
    @Named("forecastStorageService")
    transient ForecastStorageService forecastStorageService;
    
    @Inject
    @Named("topologyStorageService")
    transient TopologyStorageService topologyStorageService;

	private Date fromDate, toDate;
	private List<Csp> csps, allcsps; // CSPs in drop-down
	private String selCspId; // Id (EAN) of selected CSP
	private List<Cable> cables, allcables; // Cables in drop-down
	private String selCableId; // Id of selected cable
	private List<CspForecastMessage> cspFcMsgs; // forecast messages obtained by search
	private Collection<Object> selectionFcMsg; // forecast message selection
	private CspForecastMessage selectedFcMsg; // selected forecast message
	private CspForecast inspectedCspForecast; // inspected CSP forecast
	private CableForecast inspectedCableForecast; // associated cable forecast
	private CspForecastValue inspectedRestBlock; // rest block of inspected forecast

	private static final BigDecimal TWO = BigDecimal.valueOf(2); 

	
	public CspForecastMessages() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		toDate = new Date();
		try {
			fromDate = df.parse(df.format(toDate));
			Calendar c = Calendar.getInstance();
			c.setTime(new Date(fromDate.getTime()));
			c.add(Calendar.DATE, 1); // number of days to add
			toDate = c.getTime();
		} catch (ParseException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
		}
		selCspId = ""; selCableId = "";
	}

	@PostConstruct
	public void initialise() {

		try {
			allcsps = topologyStorageService.getAllCsps();
			allcables = topologyStorageService.getAllCables();
		} catch (ActionException e) {			
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain CSP/Cable records", e.getMessage()));
		}
		csps = allcsps; cables = allcables;
	}
	
	
    public void changeCsp(ValueChangeEvent event) {
    	
    	selCspId = event.getNewValue().toString();
    	//ActionLogger.debug("CSP selection: {0}", new Object[]{selCspId}, null, false);
    	
    	try {
			cables = topologyStorageService.getCablesByCspEan13(selCspId);
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain cable records", e.getMessage()));
		}
    	
    	cspFcMsgs = null;  // will clear the table of forecast messages
    	selectedFcMsg = null;  // and by extension the table of forecasts
    }
    
    
    public void changeCable(ValueChangeEvent event) {
    	
    	selCableId = event.getNewValue().toString();
    	//ActionLogger.debug("Cable selection: {0}", new Object[]{selCableId}, null, false);
    	
    	try {
    		csps = topologyStorageService.getCspsByCableId(selCableId);
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain CSP records", e.getMessage()));
		}
    	
    	cspFcMsgs = null;  // will clear the table of forecast messages
    	selectedFcMsg = null;  // and by extension the table of forecasts
    }

    
	public void search() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		selectionFcMsg = null; // ensure no forecast messages are preselected
		selectedFcMsg = null;  // will clear the table of forecasts
		
		try {
			// find selected cable object, and with that, find forecast messages
			Cable cable = topologyStorageService.findCableByCableId(selCableId);
			cspFcMsgs = forecastStorageService.getCspForecastMessagesByDateCspAndCable(selCspId, fromDate, toDate, cable);
		} catch (ActionException e) {
			context.addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain forecast messages", e.getMessage()));
		}
		
		if (cspFcMsgs.isEmpty()) {
			context.addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_INFO, "No forecast messages found.", null));
		}
		ActionLogger.debug("Loaded: " + cspFcMsgs.size() + " forecast messages.", null, null, false);
	}
	
	
	public void selectionListenerFcMsg(AjaxBehaviorEvent event) {
		
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		for (Object key : selectionFcMsg) {
			dataTable.setRowKey(key);
			if (dataTable.isRowAvailable()) {
				selectedFcMsg = (CspForecastMessage) dataTable.getRowData();
			}
		}
		dataTable.setRowKey(originalKey);
		
		// get forecasts for selected forecast message
		try {
			selectedFcMsg.setCspForecasts(
				forecastStorageService.getCspForcastByCspForecastMessageId(selectedFcMsg.getId()));
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain forecasts", e.getMessage()));
		}
		ActionLogger.debug("Loaded: " + selectedFcMsg.getCspForecasts().size() + " forecasts.", null, null, false);
	}
	
	
	// helper method to return the start of a forecast block
	public Date getStartDateOfBlock(CableForecastOutput output) {
		
		return ForecastBlockUtil.getStartDateOfBlock(
				output.getDay(), output.getHour() , output.getBlock(), inspectedCableForecast.getBlockMinutes());
	}
	
	
	
	// getters and setters below this line

	
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<Csp> getCsps() {
		return csps;
	}

	public void setCsps(List<Csp> csps) {
		this.csps = csps;
	}

	public String getSelCspId() {
		return selCspId;
	}

	// if the selected CSP is cleared, reset the cable drop-down
	public void setSelCspId(String selCspId) {
		this.selCspId = selCspId;
		if (selCspId.isEmpty()) {
			cables = allcables;
		}
	}

	public List<Cable> getCables() {
		return cables;
	}

	public void setCables(List<Cable> cables) {
		this.cables = cables;
	}

	public String getSelCableId() {
		return selCableId;
	}

	// if the selected cable is cleared, reset the CSP drop-down
	public void setSelCableId(String selCableId) {
		this.selCableId = selCableId;
		if (selCableId.isEmpty()) {
			csps = allcsps;
		}
	}

	public List<CspForecastMessage> getCspFcMsgs() {
		return cspFcMsgs;
	}

	public void setCspFcMsgs(List<CspForecastMessage> cspFcMsgs) {
		this.cspFcMsgs = cspFcMsgs;
	}

	public Collection<Object> getSelectionFcMsg() {
		return selectionFcMsg;
	}

	public void setSelectionFcMsg(Collection<Object> selectionFcMsg) {
		this.selectionFcMsg = selectionFcMsg;
	}

	public CspForecastMessage getSelectedFcMsg() {
		return selectedFcMsg;
	}

	
	public void setSelectedFcMsg(CspForecastMessage selectedFcMsg) {
		this.selectedFcMsg = selectedFcMsg;
	}

	public CspForecast getInspectedCspForecast() {
		return inspectedCspForecast;
	}


	public void setInspectedForecast(CspForecast cspForecast) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		inspectedCspForecast = cspForecast;
		String cfid = inspectedCspForecast.getCableForecast().getId();

		// we need the cable forecast later (to get to the block size in minutes)
		try {
			inspectedCableForecast = forecastStorageService.findCableForecastById(cfid);
		} catch (ActionException e) {
			context.addMessage("popupmessage", new FacesMessage(
				FacesMessage.SEVERITY_ERROR, "Failed to obtain cable forecast", e.getMessage()));
		}
		
		// get forecast values for selected forecast
		try {
			inspectedCspForecast.setCspForecastValues(
					forecastStorageService.getCspForecastValuesByCspForecastId(inspectedCspForecast.getId(), true, false));
		} catch (ActionException e) {
			context.addMessage("popupmessage", new FacesMessage(
				FacesMessage.SEVERITY_ERROR, "Failed to obtain forecast values", e.getMessage()));
		}
		
		int numblocks = inspectedCspForecast.getCspForecastValues().size();
		ActionLogger.debug("Loaded: " + numblocks + " out of " 
			+ inspectedCspForecast.getNumberOfBlocks() + " expected forecast values.", null, null, false);
		
		// calculate rest block of the forecast, if a rest block is present at all
		// the assigned rest is the minimum assigned value of all regular blocks
		// the remaining rest is half of the remaining value in the minimum block	
		
		inspectedRestBlock = null;  // reset to null; we may not have a rest block
		
		int blocksize = inspectedCableForecast.getBlockMinutes(), 
			maxblocks = 1440 / blocksize; // number blocks per day
		
		if (numblocks > 0 && numblocks < maxblocks) { // we have a rest block, calculate it
			
			CspForecastValue currentBlock = inspectedCspForecast.getCspForecastValues().get(0); // first block
			inspectedRestBlock = new CspForecastValue();
			inspectedRestBlock.setAssigned(currentBlock.getAssigned());
			inspectedRestBlock.setRemaining(currentBlock.getRemaining().divide(TWO));

			for (int i = 1; i < numblocks; ++i) {
				currentBlock = inspectedCspForecast.getCspForecastValues().get(i);
				if (currentBlock.getAssigned().compareTo(inspectedRestBlock.getAssigned()) < 0) {
					inspectedRestBlock.setAssigned(currentBlock.getAssigned());
					inspectedRestBlock.setRemaining(currentBlock.getRemaining().divide(TWO));
				}
			}
		}
		
	}

	
	public CspForecastValue getInspectedRestBlock() {
		return inspectedRestBlock;
	}

}