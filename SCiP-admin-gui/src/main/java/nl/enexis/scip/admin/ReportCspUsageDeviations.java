package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.admin.xls.XlsUtil;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.service.CspStorageService;
import nl.enexis.scip.service.ForecastStorageService;
import nl.enexis.scip.service.GeneralService;
import nl.enexis.scip.service.TopologyStorageService;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named
@SessionScoped
public class ReportCspUsageDeviations implements Serializable {
	/**
	 * This is the backing bean of the report of deviations between forecasts and CSP usage.
	 * The user chooses a period and a search button will bring up the list of deviations in that period.
	 * An export button is provided to export the deviations (if there are any). 
	 */
	private static final long serialVersionUID = 2369962893006314803L;
    
	@Inject
	@Named("forecastStorageService")
	transient ForecastStorageService forecastStorageService;
    
    @Inject
    @Named("topologyStorageService")
    transient TopologyStorageService topologyStorageService;
    
    @Inject
    @Named("cspStorageService")
    transient CspStorageService cspStorageService;
    
	@Inject
	@Named("generalService")
	transient GeneralService generalService;

	private Date fromDate, toDate;
	private double upperMargin = 10, lowerMargin = 10;
	private boolean upperPercentage = true, lowerPercentage = true, showAll = false;
	private List<Cable> cables, allcables; // Cables in drop-down
	private String selectedCableId; // Id of selected cable
	private String cableBehavior; // Cable behavior type (SC/SU)
	private List<Csp> csps, allcsps; // Cables in drop-down
	private String selectedCspId; // Id of selected CSP
	private List<CspForecast> forecasts; // forecasts obtained by search
	private Map<String, BigDecimal> usageMap; // map to store CSP usage

	
	public ReportCspUsageDeviations() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		
		try {
			c.setTime(df.parse(df.format(new Date())));  
			fromDate = c.getTime(); // today at 00:00
		} catch (ParseException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
		}
		c.add(Calendar.DATE, 1); toDate = c.getTime(); // tomorrow at 00:00
		selectedCspId = ""; selectedCableId = ""; cableBehavior = "SC";
	}
	
	
	@PostConstruct
	public void initialise() {

		try {
			allcsps = topologyStorageService.getAllCsps();
			allcables = topologyStorageService.getAllCables();
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain cable records", e.getMessage()));
		}
		csps = allcsps; cables = allcables;
	}

	
    public void changeCsp(ValueChangeEvent event) {
    	
    	if (event.getNewValue() == null)
		 {
			return;  // in case we select no CSP
		}
    	selectedCspId = event.getNewValue().toString();
    	//ActionLogger.debug("CSP selection: {0}", new Object[]{selectedCspId}, null, false);
    	
    	try {
			cables = topologyStorageService.getCablesByCspEan13(selectedCspId);
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain cable records", e.getMessage()));
		}
    	
    	forecasts = null;  // will clear the table of forecasts
    }
    
    
    public void changeCable(ValueChangeEvent event) {
    	
    	if (event.getNewValue() == null)
		 {
			return;  // in case we select no cable
		}
    	selectedCableId = event.getNewValue().toString();
    	
    	try {
    		csps = topologyStorageService.getCspsByCableId(selectedCableId);
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain CSP records", e.getMessage()));
		}
    	
    	forecasts = null;  // clears the table of forecasts
    }
    
	
	public void search() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		// load eligible forecasts from storage
		try {
				forecasts = forecastStorageService.getCspForcastByDate(fromDate, toDate, true);
		} catch (ActionException e) {
			context.addMessage("popupmessage", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Failed to obtain forecasts", e.getMessage()));
		}

		if (forecasts.isEmpty()) {
			context.addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_INFO, "No forecasts found.", null));
			return;
		}
		ActionLogger.debug("Found " + forecasts.size() + " forecasts between " + fromDate + " and " + toDate, null, null, false);

		// optionally filter on selected CSP / Cable / Behavior
		if ( ! (selectedCspId + selectedCableId + cableBehavior).isEmpty() ) {
			
			Iterator<CspForecast> iterator = forecasts.iterator();		
			while (iterator.hasNext()) {
				
				CableCsp cablecsp = iterator.next().getCableCsp();
				boolean remove = ! selectedCableId.isEmpty() && ! selectedCableId.equals(cablecsp.getCable().getCableId());
				remove = remove || ! cableBehavior.isEmpty() && ! BehaviourType.valueOf(cableBehavior).equals(cablecsp.getCable().getBehaviour());
				remove = remove || ! selectedCspId.isEmpty() && ! selectedCspId.equals(cablecsp.getCsp().getEan13());
				if ( remove ) {
					iterator.remove();
				}
			}
			ActionLogger.debug("After filtering " + forecasts.size() + " forecasts remain (CSP="+ selectedCspId 
					+ ", Cable=" + selectedCableId + ", Behavior=" + cableBehavior + ")", null, null, false);
		}
		
		// load the forecast values from storage; we need only the first (saves memory too)
		for (CspForecast cf : forecasts) {
			try {
				List<CspForecastValue> lcfv = forecastStorageService.getCspForecastValuesByCspForecastId(cf.getId(), true, true);	
				if (!lcfv.isEmpty()) {
					cf.setCspForecastValues(lcfv.subList(0,1));
				}
			} catch (ActionException e) {
				context.addMessage("popupmessage", new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Failed to obtain forecast values", e.getMessage()));
			}
		}
		
		// load eligible CSP usage values from storage and create the usage map
		List<CspUsageValue> usagevalues = null;
		try {
				usagevalues = cspStorageService.getCspUsageValuesByDate(fromDate, toDate);
		} catch (ActionException e) {
			context.addMessage("popupmessage", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Failed to obtain csp usage values", e.getMessage()));
		}
		ActionLogger.debug("Found " + usagevalues.size() + " usage values between " + fromDate + " and " + toDate, null, null, false);

		usageMap = new HashMap<String, BigDecimal>();
		for (CspUsageValue cuv : usagevalues) {
			String key = cuv.getCableCsp().getId() + "/" + cuv.getDay() + "/" + cuv.getHour() + "/" + cuv.getBlock();
			if (cuv.getCableCsp().getCable().getBehaviour().equals(BehaviourType.SC)) {
				usageMap.put(key, cuv.getAverageCurrent());
			} else {
				usageMap.put(key, cuv.getEnergyConsumption());
			}
		}
		
		// remove forecasts that we do not want to display/export if the showAll option is false
		if (! showAll) {
			Iterator<CspForecast> cfit = forecasts.iterator();
			while (cfit.hasNext()) {
				if (! this.isDeviationOutsideMargins(cfit.next())) {
			    	cfit.remove(); continue;
			    }
			}
		}
		
		if (forecasts.isEmpty()) {
			context.addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_INFO, "No csp deviations found.", null));
			return;
		}
		ActionLogger.debug("Showing " + forecasts.size() + " forecasts (Showall = " + showAll + ").", null, null, false);
	}
	
	
	public void export() {

		try {
			HttpServletResponse response = (HttpServletResponse) 
				FacesContext.getCurrentInstance().getExternalContext().getResponse();

			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
			String filename = "csp-usage-deviations-" + df.format(fromDate) + "-" + df.format(toDate) + ".xls";
			String template = generalService.getConfigurationParameter("ExportCspUsageDeviationsTemplate").getValue();
			Map<String, Object> beanmap = new HashMap<String, Object>(); beanmap.put("this", this);
			
			XlsUtil.exportXlsContent(response, filename, template, beanmap);
			FacesContext.getCurrentInstance().responseComplete();
			
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getClass().getName() + ": " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage("popupmessage", message);
		}
	}
	
	
	// helper method to construct the CSP usage key
	private String getUsageKey(CspForecast cf) {
		
		CspForecastValue cfv = cf.getCspForecastValues().get(0);
		return cf.getCableCsp().getId() + "/" + cfv.getCableForecastOutput().getDay() + "/" 
				+ cfv.getCableForecastOutput().getHour() + "/" + cfv.getCableForecastOutput().getBlock();
	}
	
	// helper method to return the CSP usage from the map
	public BigDecimal getUsage(CspForecast cf) {
		return usageMap.get(this.getUsageKey(cf));
	}
	
	// helper method to return the first forecast value
	public CspForecastValue getForecastValue(CspForecast cf) {
		return cf.getCspForecastValues().get(0);
	}
	
	// helper method to return the total (plus adjustment) assigned capacity
	public BigDecimal getTotalAssigned(CspForecast cf) {
		CspForecastValue cfv = getForecastValue(cf);
		CspAdjustmentValue cav = cfv.getCspAdjustmentValue();
		BigDecimal extra = (cav == null) ? BigDecimal.ZERO : cav.getAssigned();
		return cfv.getAssigned().add(extra);
	}

	// helper method to return the start of the first forecast value block
	public Date getStartDateOfBlock(CspForecast cf) {
		
		CableForecastOutput cfo = cf.getCspForecastValues().get(0).getCableForecastOutput();
		return ForecastBlockUtil.getStartDateOfBlock(
			cfo.getDay(), cfo.getHour() , cfo.getBlock(), cfo.getCableForecast().getBlockMinutes());
	}
	
	// helper method to return the deviation between usage and forecast
	public BigDecimal getDeviation(CspForecast cf) {
		
		BigDecimal usage = usageMap.get(this.getUsageKey(cf));
		if (usage == null)
		 {
			return null; // no usage for this forecast
		}
		return usage.subtract(cf.getCspForecastValues().get(0).getAssigned());
	}
	
	// helper method to return the deviation as a percentage of the forecast
	public BigDecimal getDeviationPercentage(CspForecast cf) {
		
		BigDecimal deviation = this.getDeviation(cf);
		BigDecimal assigned = cf.getCspForecastValues().get(0).getAssigned();
		//ActionLogger.debug("Assigned " + assigned + " ID " + cf.getCspForecastValues().get(0).getId(), null, null, false);
		if (deviation == null || assigned == BigDecimal.ZERO)
		 {
			return null; // prevent division by zero!
		}
		//ActionLogger.debug("DIVIDE " + deviation + " by " + assigned, null, null, false);
		try { 
			return deviation.multiply(BigDecimal.valueOf(100)).divide(assigned, 1, RoundingMode.HALF_UP);
		} catch (Exception e) { 
			ActionLogger.error("Caught impossible exception " + e + " for CspForecastValue " +
				cf.getCspForecastValues().get(0).getId() + "  and usage key " + this.getUsageKey(cf), null, null, false);
			return null;
		}
	}
	
	// helper method to determine if a deviation is outside the selected margins
	public boolean isDeviationOutsideMargins(CspForecast cf) {
		
		BigDecimal deviation = this.getDeviation(cf);
		if (deviation == null)
		 {
			return false; // indeterminate deviations are by definition within margins
		}
		
		BigDecimal assigned = cf.getCspForecastValues().get(0).getAssigned();
		// if nothing is assigned but there is usage, it is by definition outside the upper percentage margin
		if (assigned == BigDecimal.ZERO) {
			return upperPercentage && deviation.compareTo(BigDecimal.ZERO) == 1;
		}
		
	    return lowerPercentage && this.getDeviationPercentage(cf).compareTo(BigDecimal.valueOf(-lowerMargin)) == -1 || 
		upperPercentage && this.getDeviationPercentage(cf).compareTo(BigDecimal.valueOf(upperMargin)) == 1 || 
		! lowerPercentage && deviation.compareTo(BigDecimal.valueOf(-lowerMargin)) == -1 || 
		! upperPercentage && deviation.compareTo(BigDecimal.valueOf(upperMargin)) == 1;
	}
	
	
	// standard (more or less) getters and setters below this line

	
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public double getUpperMargin() {
		return upperMargin;
	}

	public void setUpperMargin(double upperMargin) {
		this.upperMargin = upperMargin;
	}

	public double getLowerMargin() {
		return lowerMargin;
	}

	public void setLowerMargin(double lowerMargin) {
		this.lowerMargin = lowerMargin;
	}

	public boolean isUpperPercentage() {
		return upperPercentage;
	}

	public void setUpperPercentage(boolean upperPercentage) {
		this.upperPercentage = upperPercentage;
	}

	public boolean isLowerPercentage() {
		return lowerPercentage;
	}

	public void setLowerPercentage(boolean lowerPercentage) {
		this.lowerPercentage = lowerPercentage;
	}

	public boolean isShowAll() {
		return showAll;
	}

	public void setShowAll(boolean showAll) {
		this.showAll = showAll;
	}

	public List<Csp> getCsps() {
		return csps;
	}

	public void setCsps(List<Csp> csps) {
		this.csps = csps;
	}

	public String getSelectedCspId() {
		return selectedCspId;
	}
	
	// if the selected CSP is cleared, reset the cable drop-down
	public void setSelectedCspId(String selectedCspId) {
		this.selectedCspId = selectedCspId != null ? selectedCspId : "";
		if (this.selectedCspId.isEmpty()) {
			cables = allcables;
		}
	}
	
	public List<Cable> getCables() {
		return cables;
	}

	public void setCables(List<Cable> cables) {
		this.cables = cables;
	}

	public String getSelectedCableId() {
		return selectedCableId;
	}
	
	// if the selected cable is cleared, reset the CSP drop-down
	public void setSelectedCableId(String selectedCableId) {
		this.selectedCableId = selectedCableId != null ? selectedCableId : "";
		if (this.selectedCableId.isEmpty()) {
			csps = allcsps;
		}
	}

	public String getCableBehavior() {
		return cableBehavior;
	}
    
	public void setCableBehavior(String cableBehavior) {
		this.cableBehavior = cableBehavior != null ? cableBehavior : "";
	}

	// helper method to return behaviour types
    public List<SelectItem> getBehaviourOptions() {
        return BehaviourType.toSelectItems();
    }
    
	public List<CspForecast> getForecasts() {
		return forecasts;
	}

	public void setForecasts(List<CspForecast> forecasts) {
		this.forecasts = forecasts;
	}

}