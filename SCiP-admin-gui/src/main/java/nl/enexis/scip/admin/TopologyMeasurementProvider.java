package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.MeasurementProvider;
import nl.enexis.scip.service.TopologyStorageService;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class TopologyMeasurementProvider extends TopologyGenericEntity<MeasurementProvider> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 310582116413020154L;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	private Identity identity;

	public TopologyMeasurementProvider() {
		this.type = MeasurementProvider.class;
	}

	@Override
	protected void saveEntity() throws ActionException {
		topologyStorageService.updateMP(this.getEdited());
	}

	@Override
	protected void insertEntity() throws ActionException {
		topologyStorageService.createMeasurementProvider(this.getNewEntity());
	}

	@Override
	protected void deleteEntity() throws ActionException {
		topologyStorageService.deleteMeasurementProvider(this.getDeleted());
	}

	@Override
	protected List<MeasurementProvider> getAllEntities() throws ActionException {
		return topologyStorageService.getAllMPs();
	}

	@Override
	protected User getUser() {
		if (identity == null) {
			return null;
		}
		return (User) identity.getAccount();
	}

	public List<SelectItem> getAllMeasurementProviderOptions() {
		List<SelectItem> allMeasurementProviderOptions = new ArrayList<SelectItem>();

		for (MeasurementProvider mp : this.getAll()) {
			allMeasurementProviderOptions.add(new SelectItem(mp.getId(), mp.getName()));
		}

		return allMeasurementProviderOptions;
	}

}
