package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.model.DsoCsp;
import nl.enexis.scip.service.DashboardService;
import nl.enexis.scip.service.TopologyStorageService;

@Named
@ApplicationScoped
public class LastHeartbeatPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4715308817859525015L;

	private List<LastHeartbeat> data;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	public LastHeartbeatPart() {
	}

	// @PostConstruct
	@Override
	public void reset() throws ActionException {
		List<DsoCsp> dsoCsps = topologyStorageService.getAllDsoCsps(false);

		// filter dsoCsp list on not active
		List<DsoCsp> removedDsoCsps = new ArrayList<DsoCsp>();
		for (int i = 0; i < dsoCsps.size(); i++) {
			DsoCsp dsoCsp = dsoCsps.get(i);
			if (Double.parseDouble(dsoCsp.getCsp().getProtocolVersion()) < 1.0) {
				removedDsoCsps.add(dsoCsp);
			} else if (!(dsoCsp.isActive() && !dsoCsp.isDeleted() && dsoCsp.getDso().isActive()
					&& !dsoCsp.getDso().isDeleted() && dsoCsp.getCsp().isActive() && !dsoCsp.getCsp().isDeleted())) {
				removedDsoCsps.add(dsoCsp);
			}
		}
		for (DsoCsp dsoCsp : removedDsoCsps) {
			dsoCsps.remove(dsoCsp);
		}

		data = new ArrayList<LastHeartbeat>();

		for (DsoCsp dsoCsp : dsoCsps) {
			LastHeartbeat heartbeat = new LastHeartbeat();
			heartbeat.setCspEan(dsoCsp.getCsp().getEan13());
			heartbeat.setDsoEan(dsoCsp.getDso().getEan13());
			heartbeat.setSuccess(false);

			Action action = dashboardService.getLastHeartbeatEvent(dsoCsp);
			if (action != null) {
				heartbeat.setLast(action.getStart());
				heartbeat.setSuccess(action.isSuccess());
			}

			data.add(heartbeat);
		}

		Collections.sort(data);
	}

	public List<LastHeartbeat> getData() {
		return this.data;
	}

	public class LastHeartbeat implements Serializable, Comparable<LastHeartbeat> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6673113468776135376L;

		private String dsoEan;
		private String cspEan;
		private Date last;
		private boolean success;

		public LastHeartbeat() {
		}

		public String getDsoEan() {
			return dsoEan;
		}

		public void setDsoEan(String dsoEan) {
			this.dsoEan = dsoEan;
		}

		public String getCspEan() {
			return cspEan;
		}

		public void setCspEan(String cspEan) {
			this.cspEan = cspEan;
		}

		public Date getLast() {
			return last;
		}

		public void setLast(Date last) {
			this.last = last;
		}

		public boolean isSuccess() {
			return success;
		}

		public void setSuccess(boolean success) {
			this.success = success;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (cspEan == null ? 0 : cspEan.hashCode());
			result = prime * result + (dsoEan == null ? 0 : dsoEan.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof LastHeartbeat)) {
				return false;
			}
			LastHeartbeat other = (LastHeartbeat) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (cspEan == null) {
				if (other.cspEan != null) {
					return false;
				}
			} else if (!cspEan.equals(other.cspEan)) {
				return false;
			}
			if (dsoEan == null) {
				if (other.dsoEan != null) {
					return false;
				}
			} else if (!dsoEan.equals(other.dsoEan)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(LastHeartbeat o) {
			int compare = this.success == o.success ? 0 : this.success == false ? -1 : 1;
			if (compare == 0) {
				if (this.last != null && o.last != null) {
					compare = this.last.compareTo(o.last);
				} else {
					if (this.last == null) {
						compare = -1;
					} else if (o.last == null) {
						compare = 1;
					} else {
						compare = 0;
					}
				}
			}
			return compare;
		}

		private LastHeartbeatPart getOuterType() {
			return LastHeartbeatPart.this;
		}
	}
}
