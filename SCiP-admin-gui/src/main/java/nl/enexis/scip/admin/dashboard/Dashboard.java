package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionLogger;

@Named
@ApplicationScoped
public class Dashboard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3740297842520952L;

	@Inject
	CspUsageDeviationsPart cspUsageDeviationsPart;

	@Inject
	EventErrorsPart eventErrorsPart;

	@Inject
	IncompleteMeasurementsPart incompleteMeasurementsPart;

	@Inject
	LastOutboundMessageSuccessPart lastOutboundMessageSuccessPart;

	@Inject
	LastHeartbeatPart lastHeartbeatPart;

	@Inject
	ConnectivityPart connectivityPart;

	@Inject
	ScLastForecastInputPart scLastForecastInputPart;

	@Inject
	SuLastForecastInputPart suLastForecastInputPart;

	@Inject
	PerformanceEventsPart performanceEventsPart;

	@Inject
	PerformanceTimersPart performanceTimersPart;

	@Inject
	MiscellaneousPart miscellaneousPart;

	private Date lastRefresh = new Date();

	public Dashboard() {
	}

	public void observe(@Observes ActionEvent event) {

		if (ActionEvent.DASHBOARD.equals(event)) {
			this.refresh();
		}
	}

	public void refresh() {
		ActionLogger.info("***** DASHBOARD START *****", new Object[] {}, null, false);
		try {
			this.setLastRefresh(new Date());
			connectivityPart.reset();
			eventErrorsPart.reset();
			incompleteMeasurementsPart.reset();
			lastHeartbeatPart.reset();
			lastOutboundMessageSuccessPart.reset();
			scLastForecastInputPart.reset();
			suLastForecastInputPart.reset();
			performanceEventsPart.reset();
			performanceTimersPart.reset();
			miscellaneousPart.reset();
			cspUsageDeviationsPart.reset();
		} catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage("popupmessage", message);
		}
		ActionLogger.info("***** DASHBOARD END *****", new Object[] {}, null, false);

	}

	public Date getLastRefresh() {
		return lastRefresh;
	}

	public void setLastRefresh(Date lastRefresh) {
		this.lastRefresh = lastRefresh;
	}
}
