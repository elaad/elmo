package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.admin.xls.XlsUtil;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Measurement;
import nl.enexis.scip.model.MeasurementProvider;
import nl.enexis.scip.model.MeasurementValue;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.service.GeneralService;
import nl.enexis.scip.service.MeasurementStorageService;
import nl.enexis.scip.service.TopologyStorageService;

import org.richfaces.component.UIExtendedDataTable;

@Named
@SessionScoped
public class ReportMeasurements implements Serializable {
	/**
	 * This is the backing bean of the report of incomplete measurements. The
	 * user chooses a period and a measurement provider, or a cable, or neither
	 * (but not both). A search button will bring up the list of incomplete
	 * measurements in that period. A detail view lists the meters associated
	 * with the cable and whether they were in the measurement. An export button
	 * is provided to export incomplete measurements (if there are any).
	 */
	private static final long serialVersionUID = -7650039072937097258L;

	@Inject
	@Named("measurementStorageService")
	transient MeasurementStorageService measurementStorageService;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	@Named("generalService")
	transient GeneralService generalService;

	private Date fromDate, toDate;
	private List<MeasurementProvider> MPs; // Measurement providers in drop-down
	private String selectedMPId; // Id of selected measurement provider
	private List<Cable> cables; // Cables in drop-down
	private String selectedCableId; // Id of selected cable
	private List<Measurement> measurements; // measurements obtained by search
	private Collection<Object> selectionMeasurement; // measurement selection
	private Measurement selectedMeasurement; // selected measurement

	public ReportMeasurements() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();

		try {
			c.setTime(df.parse(df.format(new Date())));
			fromDate = c.getTime(); // today at 00:00
		} catch (ParseException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
		}
		c.add(Calendar.DATE, 1);
		toDate = c.getTime(); // tomorrow at 00:00
		selectedMPId = "";
		selectedCableId = "";
	}

	@PostConstruct
	public void initialise() {

		try {
			MPs = topologyStorageService.getAllMPs();
			cables = topologyStorageService.getAllCables();
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage(
					"popupmessage",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain MP and/or Cable records", e
							.getMessage()));
		}
	}

	public void search() {

		FacesContext context = FacesContext.getCurrentInstance();

		selectionMeasurement = null; // ensure no measurements are preselected
		selectedMeasurement = null; // will clear the table of measurement
									// values

		List<Measurement> searchresult = new ArrayList<Measurement>();
		try {
			if (selectedMPId.length() > 0) { // find by provider (all cables)
				searchresult = measurementStorageService.getMeasurementForProviderIdBetweenDates(fromDate, toDate,
						selectedMPId);
			} else if (selectedCableId.length() > 0) {
				// find selected cable object, and with that, find the
				// measurements
				Cable cable = topologyStorageService.findCableByCableId(selectedCableId);
				searchresult = measurementStorageService.getMeasurementForCableBetweenDates(fromDate, toDate, cable);
			} else { // just find all measurements in the selected period
				searchresult = measurementStorageService.getMeasurementBetweenDates(fromDate, toDate);
			}
		} catch (ActionException e) {
			context.addMessage("popupmessage", new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Failed to obtain measurements for MP '" + selectedMPId + "' and cable '" + selectedCableId + "'",
					e.getMessage()));
		}

		if (searchresult.isEmpty()) {
			context.addMessage("popupmessage", new FacesMessage(FacesMessage.SEVERITY_INFO, "No measurements found.",
					null));
			measurements = searchresult;
			return;
		}

		// filter out complete measurements
		measurements = new ArrayList<Measurement>();
		for (Measurement m : searchresult) {
			if (!m.isComplete()) {
				measurements.add(m);
			}
		}

		if (measurements.isEmpty()) {
			context.addMessage("popupmessage", new FacesMessage(FacesMessage.SEVERITY_INFO,
					"No incomplete measurements found.", null));
		}
	}

	public void export() {

		try {
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();

			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
			String filename = "incomplete-measurements-" + df.format(fromDate) + "-" + df.format(toDate) + ".xls";
			String template = generalService.getConfigurationParameter("ExportIncompleteMeasurementsTemplate")
					.getValue();
			Map<String, Object> beanmap = new HashMap<String, Object>();
			beanmap.put("this", this);

			XlsUtil.exportXlsContent(response, filename, template, beanmap);
			FacesContext.getCurrentInstance().responseComplete();

		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getClass().getName() + ": " + e.getMessage());
			FacesContext.getCurrentInstance().addMessage("popupmessage", message);
		}
	}

	public void selectionListenerMeasurement(AjaxBehaviorEvent event) {

		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		for (Object key : selectionMeasurement) {
			dataTable.setRowKey(key);
			if (dataTable.isRowAvailable()) {
				selectedMeasurement = (Measurement) dataTable.getRowData();
			}
		}
		dataTable.setRowKey(originalKey);

	}

	// helper method to check if a meter is present in a measurement
	public boolean hasMeter(Measurement measurement, Meter meter) {

		for (MeasurementValue mval : measurement.getMeasurementValues()) {
			if (mval.getMeter().equals(meter)) {
				return true;
			}
		}
		return false;
	}

	// getters and setters below this line

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<MeasurementProvider> getMPs() {
		return MPs;
	}

	public void setMPs(List<MeasurementProvider> MPs) {
		this.MPs = MPs;
	}

	public String getSelectedMPId() {
		return selectedMPId;
	}

	public void setSelectedMPId(String selectedMPId) {
		this.selectedMPId = selectedMPId != null ? selectedMPId : "";
		if (this.selectedMPId.isEmpty()) {
			selectedCableId = "";
		}
	}

	public List<Cable> getCables() {
		return cables;
	}

	public void setCables(List<Cable> cables) {
		this.cables = cables;
	}

	public String getSelectedCableId() {
		return selectedCableId;
	}

	public void setSelectedCableId(String selectedCableId) {
		this.selectedCableId = selectedCableId != null ? selectedCableId : "";
		if (this.selectedCableId.isEmpty()) {
			selectedMPId = "";
		}
	}

	public List<Measurement> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<Measurement> measurements) {
		this.measurements = measurements;
	}

	public Collection<Object> getSelectionMeasurement() {
		return selectionMeasurement;
	}

	public void setSelectionMeasurement(Collection<Object> selectionMeasurement) {
		this.selectionMeasurement = selectionMeasurement;
	}

	public Measurement getSelectedMeasurement() {
		return selectedMeasurement;
	}

	public void setSelectedMeasurement(Measurement selectedMeasurement) {
		this.selectedMeasurement = selectedMeasurement;
	}

}