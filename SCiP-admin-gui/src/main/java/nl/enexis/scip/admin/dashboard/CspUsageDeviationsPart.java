package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.service.DashboardService;

@Named
@ApplicationScoped
public class CspUsageDeviationsPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = 726565705877246049L;

	private List<UsageDeviationCounts> counts;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	private Long deviationMargin = (long) 10;

	public CspUsageDeviationsPart() {
	}

	//@PostConstruct
	@Override
	public void reset() throws ActionException {
		Map<CableCsp, Long> fourWeek = dashboardService.getCspUsageDeviationCounts(28, deviationMargin);
		Map<CableCsp, Long> week = dashboardService.getCspUsageDeviationCounts(7, deviationMargin);
		Map<CableCsp, Long> day = dashboardService.getCspUsageDeviationCounts(1, deviationMargin);

		counts = new ArrayList<UsageDeviationCounts>();

		for (CableCsp cableCsp : fourWeek.keySet()) {
			UsageDeviationCounts eventCounts = new UsageDeviationCounts(cableCsp.getCable().getCableId(), cableCsp
					.getCsp().getEan13());
			int index = counts.indexOf(eventCounts);
			if (index < 0) {
				eventCounts.setFourWeek(fourWeek.get(cableCsp));
				counts.add(eventCounts);
			} else {
				counts.get(index).setFourWeek(fourWeek.get(cableCsp));
			}
		}

		for (CableCsp cableCsp : week.keySet()) {
			UsageDeviationCounts eventCounts = new UsageDeviationCounts(cableCsp.getCable().getCableId(), cableCsp
					.getCsp().getEan13());
			int index = counts.indexOf(eventCounts);
			counts.get(index).setWeek(week.get(cableCsp));
		}

		for (CableCsp cableCsp : day.keySet()) {
			UsageDeviationCounts eventCounts = new UsageDeviationCounts(cableCsp.getCable().getCableId(), cableCsp
					.getCsp().getEan13());
			int index = counts.indexOf(eventCounts);
			counts.get(index).setDay(day.get(cableCsp));
		}

		Collections.sort(counts);
	}

	public Long getDeviationMargin() {
		return deviationMargin;
	}

	public void setDeviationMargin(Long deviationMargin) {
		this.deviationMargin = deviationMargin;
	}

	public List<UsageDeviationCounts> getCounts() {
		return this.counts;
	}

	public class UsageDeviationCounts implements Serializable, Comparable<UsageDeviationCounts> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3459038530471940640L;

		private String cable;
		private String csp;
		private Long day = (long) 0;
		private Long week = (long) 0;
		private Long fourWeek = (long) 0;

		public UsageDeviationCounts(String cable, String csp) {
			this.cable = cable;
			this.csp = csp;
		}

		public String getCable() {
			return cable;
		}

		public void setCable(String cable) {
			this.cable = cable;
		}

		public String getCsp() {
			return csp;
		}

		public void setCsp(String csp) {
			this.csp = csp;
		}

		public Long getDay() {
			return day;
		}

		public void setDay(Long day) {
			this.day = day;
		}

		public Long getWeek() {
			return week;
		}

		public void setWeek(Long week) {
			this.week = week;
		}

		public Long getFourWeek() {
			return fourWeek;
		}

		public void setFourWeek(Long fourWeek) {
			this.fourWeek = fourWeek;
		}

		private CspUsageDeviationsPart getOuterType() {
			return CspUsageDeviationsPart.this;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (cable == null ? 0 : cable.hashCode());
			result = prime * result + (csp == null ? 0 : csp.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof UsageDeviationCounts)) {
				return false;
			}
			UsageDeviationCounts other = (UsageDeviationCounts) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (cable == null) {
				if (other.cable != null) {
					return false;
				}
			} else if (!cable.equals(other.cable)) {
				return false;
			}
			if (csp == null) {
				if (other.csp != null) {
					return false;
				}
			} else if (!csp.equals(other.csp)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(UsageDeviationCounts o) {
			int compare = this.day.compareTo(o.day);
			if (compare == 0) {
				compare = this.week.compareTo(o.week);
				if (compare == 0) {
					compare = this.fourWeek.compareTo(o.fourWeek);
				}
			}
			return -1 * compare;
		}
	}
}
