package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.service.TopologyStorageService;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class TopologyCableCsp extends TopologyGenericEntity<CableCsp> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -9105755353141474686L;

	private String editedCableId;
	private String editedCspEan13;
	private boolean editedActive;
	private String newCableId;
	private String newCspEan13;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	private Identity identity;

	public TopologyCableCsp() {
		this.type = CableCsp.class;
	}

	@Override
	public void createNew() {
		super.createNew();

		this.newCableId = "";
		this.newCspEan13 = "";
		this.newEntity.setNumberOfBlocks(96);
	}

	@Override
	public void setEdited(CableCsp edited) {
		super.setEdited(edited);

		this.editedCableId = edited.getCable().getCableId();
		this.editedCspEan13 = edited.getCsp().getEan13();
		this.editedActive = edited.isActive();
	}

	@Override
	protected void saveEntity() throws ActionException {
		CableCsp cableCsp = this.getEdited();
		if (cableCsp != null) {
			Cable cable = topologyStorageService.findCableByCableId(this.editedCableId);
			if (cable != null) {
				cableCsp.setCable(cable);
				// if the cableCsp was (in)activated we must update the CspLastCapacityChange on the cable.
				if (cableCsp.isActive() != this.editedActive) {
					cable.setCspLastCapacityChange(new Date());
					topologyStorageService.updateCable(cable);
				}
			}
			Csp csp = topologyStorageService.findCspByEan13(this.editedCspEan13);
			if (csp != null) {
				cableCsp.setCsp(csp);
			}

			topologyStorageService.updateCableCsp(cableCsp);
		}
	}

	@Override
	protected void insertEntity() throws ActionException {
		CableCsp cableCsp = this.getNewEntity();
		if (cableCsp != null) {
			Cable cable = topologyStorageService.findCableByCableId(this.newCableId);
			if (cable != null) {
				cableCsp.setCable(cable);
				// if the new cableCsp is active right away, 
				// we must update the CspLastCapacityChange on the cable.
				if (cableCsp.isActive()) {
					cable.setCspLastCapacityChange(new Date());
					topologyStorageService.updateCable(cable);
				}
			}
			Csp csp = topologyStorageService.findCspByEan13(this.newCspEan13);
			if (csp != null) {
				cableCsp.setCsp(csp);
			}
			topologyStorageService.createCableCsp(cableCsp);
		}
	}

	@Override
	protected void deleteEntity() throws ActionException {
		topologyStorageService.deleteCableCsp(this.getDeleted());
	}

	@Override
	protected List<CableCsp> getAllEntities() throws ActionException {
		return topologyStorageService.getAllCableCsps();
	}

	@Override
	protected User getUser() {
		if (identity == null) {
			return null;
		}
		return (User) identity.getAccount();
	}

	public String getEditedCableId() {
		return editedCableId;
	}

	public void setEditedCableId(String editedCableId) {
		this.editedCableId = editedCableId;
	}

	public String getEditedCspEan13() {
		return editedCspEan13;
	}

	public void setEditedCspEan13(String editedCspEan13) {
		this.editedCspEan13 = editedCspEan13;
	}

	public String getNewCableId() {
		return newCableId;
	}

	public void setNewCableId(String newCableId) {
		this.newCableId = newCableId;
	}

	public String getNewCspEan13() {
		return newCspEan13;
	}

	public void setNewCspEan13(String newCspEan13) {
		this.newCspEan13 = newCspEan13;
	}

	public List<SelectItem> getAllCableCspOptions() {
		List<SelectItem> allCableCspOptions = new ArrayList<SelectItem>();

		for (CableCsp cableCsp : this.getAll()) {
			allCableCspOptions.add(new SelectItem(cableCsp.getId(), cableCsp.getId() + " ("
					+ cableCsp.getCable().getCableId() + " - " + cableCsp.getCsp().getName() + ")"));
		}

		return allCableCspOptions;
	}
}
