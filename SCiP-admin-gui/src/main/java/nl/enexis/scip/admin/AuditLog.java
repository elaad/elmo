package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.AdminUser;
import nl.enexis.scip.model.AuditLogItem;
import nl.enexis.scip.model.enumeration.DatabaseAction;
import nl.enexis.scip.service.GenericStorageService;

import org.richfaces.component.UIExtendedDataTable;

@Named
@SessionScoped
public class AuditLog implements Serializable {
	/**
	 * This is the backing bean of the audit log viewer. 
	 * Select records based on a date period, and optionally a user and/or an entity.
	 */
	private static final long serialVersionUID = 8745518748378635379L;

	@Inject
	@Named("genericStorageService")
	transient GenericStorageService genericStorageService;

	private Date fromDate, toDate;
	private List<AdminUser> users; // Users in drop-down
	private String selectedUserId; // Id of selected user
	private List<String> entities; // Entity in drop-down
	private String selectedEntity; // Selected entity
	private List<AuditLogItem> logitems; // log items obtained by search
	private Collection<Object> selectionLogItem; // log item selection
	private AuditLogItem selectedLogItem; // selected log item
	private AuditLogItem inspectedLogItem; // inspected log item

	public AuditLog() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();

		try {
			c.setTime(df.parse(df.format(new Date()))); // today at 00:00
		} catch (ParseException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage",
					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
		}
		c.add(Calendar.DATE, 1); toDate = c.getTime();    // tomorrow
		c.add(Calendar.DATE, -7); fromDate = c.getTime(); // one week ago
		selectedUserId = "";
		selectedEntity = "";
	}

	@PostConstruct
	public void initialise() {

		try {
			users = genericStorageService.getAllAdminUsers();
			entities = genericStorageService.getDistinctAuditLogEntities();
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage",
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain users and/or entities", e.getMessage()));
		}
	}

	public void search() {

		FacesContext context = FacesContext.getCurrentInstance();

		selectionLogItem = null; // ensure no items are preselected
		selectedLogItem = null; // will clear the table of log items

		try {
			logitems = genericStorageService.getAuditLogItemsBetweenDates(fromDate, toDate);
			if (logitems.isEmpty()) {
				context.addMessage("popupmessage", 
						new FacesMessage(FacesMessage.SEVERITY_INFO, "No records found.", null));
				return;
			}
		} catch (ActionException e) {
			context.addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain log items", e.getMessage()));
			return;
		}

		// filter by user if selected
		if (selectedUserId.length() > 0) {
			Iterator<AuditLogItem> it = logitems.iterator();
			while (it.hasNext()) {
				if (! it.next().getUserId().equals(selectedUserId)) {
					it.remove();
				}
			}
		}
		
		// filter by entity if selected
		if (selectedEntity.length() > 0) {
			Iterator<AuditLogItem> it = logitems.iterator();
			while (it.hasNext()) {
				if (! it.next().getTableName().equals(selectedEntity)) {
					it.remove();
				}
			}
		}
		
		if (logitems.isEmpty()) {
			context.addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_INFO, "No matching records found.", null));
		}
	}


	public void selectionListenerLogItem(AjaxBehaviorEvent event) {

		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		for (Object key : selectionLogItem) {
			dataTable.setRowKey(key);
			if (dataTable.isRowAvailable()) {
				selectedLogItem = (AuditLogItem) dataTable.getRowData();
			}
		}
		dataTable.setRowKey(originalKey);

	}

	// helper method to return dbAction as a human readable string
	public String getDbActionName(String v) {
		return DatabaseAction.fromValue(v).name();
	}

	// getters and setters below this line

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<AdminUser> getUsers() {
		return users;
	}

	public void setUsers(List<AdminUser> users) {
		this.users = users;
	}

	public String getSelectedUserId() {
		return selectedUserId;
	}

	public void setSelectedUserId(String selectedUserId) {
		this.selectedUserId = selectedUserId != null ? selectedUserId : "";
	}

	public List<String> getEntities() {
		return entities;
	}

	public void setEntities(List<String> entities) {
		this.entities = entities;
	}

	public String getSelectedEntity() {
		return selectedEntity;
	}

	public void setSelectedEntity(String selectedEntity) {
		this.selectedEntity = selectedEntity != null ? selectedEntity : "";
	}

	public List<AuditLogItem> getLogitems() {
		return logitems;
	}

	public void setLogitems(List<AuditLogItem> logitems) {
		this.logitems = logitems;
	}

	public Collection<Object> getSelectionLogItem() {
		return selectionLogItem;
	}

	public void setSelectionLogItem(Collection<Object> selectionLogItem) {
		this.selectionLogItem = selectionLogItem;
	}

	public AuditLogItem getSelectedLogItem() {
		return selectedLogItem;
	}

	public void setSelectedLogItem(AuditLogItem selectedLogItem) {
		this.selectedLogItem = selectedLogItem;
	}

	public AuditLogItem getInspectedLogItem() {
		return inspectedLogItem;
	}

	public void setInspectedLogItem(AuditLogItem inspectedLogItem) {
		this.inspectedLogItem = inspectedLogItem;
	}

}