package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.service.TopologyStorageService;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class TopologyDso extends TopologyGenericEntity<Dso> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -9143158733879147829L;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	private Identity identity;

	public TopologyDso() {
		this.type = Dso.class;
	}

	@Override
	protected void saveEntity() throws ActionException {
		topologyStorageService.updateDso(this.getEdited());
	}

	@Override
	protected void insertEntity() throws ActionException {
		topologyStorageService.createDso(this.getNewEntity());
	}

	@Override
	protected void deleteEntity() throws ActionException {
		topologyStorageService.deleteDso(this.getDeleted());
	}

	@Override
	protected List<Dso> getAllEntities() throws ActionException {
		return topologyStorageService.getAllDsos();
	}

	@Override
	protected User getUser() {
		if (identity == null) {
			return null;
		}
		return (User) identity.getAccount();
	}

	public List<SelectItem> getAllDsoOptions() {
		List<SelectItem> allDsoOptions = new ArrayList<SelectItem>();

		for (Dso dso : this.getAll()) {
			allDsoOptions.add(new SelectItem(dso.getEan13(), dso.getName()));
		}

		return allDsoOptions;
	}
}
