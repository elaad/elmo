package nl.enexis.scip.admin.security;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.model.AdminUser;
import nl.enexis.scip.service.GenericStorageService;
import nl.enexis.scip.util.security.PasswordUtil;

import org.picketlink.annotations.PicketLink;
import org.picketlink.authentication.BaseAuthenticator;
import org.picketlink.credential.DefaultLoginCredentials;
import org.picketlink.idm.model.Attribute;
import org.picketlink.idm.model.basic.User;

@PicketLink
public class Authenticator extends BaseAuthenticator {

	@Inject
	private DefaultLoginCredentials credentials;

	@Inject
	@Named("genericStorageService")
	GenericStorageService genericStorageService;

	@Override
	public void authenticate() {
		FacesContext context = FacesContext.getCurrentInstance();
		//context.getExternalContext().invalidateSession(); 
	
		try {
			String userId = credentials.getUserId().toUpperCase();
			String enteredpassword = credentials.getPassword();

			AdminUser adminUser = genericStorageService.findAdminUserByUserId(userId);
			String storedpassword = adminUser.getPassword();

			boolean authenticated, encrypted;
			encrypted = storedpassword.length() == 56 && storedpassword.matches("\\p{XDigit}+"); // probably
																									// encrypted
			if (!encrypted) {
				// for now, accept clear text password, but encrypt and store it
				// right away
				authenticated = storedpassword.equals(enteredpassword);
				if (authenticated) {
					adminUser.setPassword(PasswordUtil.encrypt(storedpassword));
					genericStorageService.updateAdminUser(adminUser);
				}
			} else {
				authenticated = PasswordUtil.authenticate(enteredpassword, storedpassword);
			}

			if (adminUser != null && authenticated) {
				if (adminUser.isActive()) {
					this.setStatus(AuthenticationStatus.SUCCESS);
					User user = new User(userId);
					user.setEmail(adminUser.getEmail());
					user.setFirstName(adminUser.getFirstName());
					user.setLastName(adminUser.getLastName());
					String roles = adminUser.getRoles();
					if (roles != null) {
						Attribute<String> attribute = new Attribute<String>("roles", roles);
						user.setAttribute(attribute);
					}
					this.setAccount(user);
				} else {
					this.setStatus(AuthenticationStatus.FAILURE);
					context.addMessage(
							null,
							new FacesMessage(
									"Authentication Failure - The credentials you provided were valid, but the user is inactive."));
				}
			} else {
				this.setStatus(AuthenticationStatus.FAILURE);
				context.addMessage(null, new FacesMessage(
						"Authentication Failure - The username or password you provided were invalid."));
			}
		} catch (Exception e) {
			this.setStatus(AuthenticationStatus.FAILURE);
			context.addMessage(null,
					new FacesMessage("Unexpected authentication/authorization failure: " + e.getMessage()));
		}
		
        context.getApplication().getNavigationHandler().handleNavigation(context, null, "login");
	}
}