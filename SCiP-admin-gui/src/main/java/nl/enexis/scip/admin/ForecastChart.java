package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.service.ForecastStorageService;
import nl.enexis.scip.service.GeneralService;
import nl.enexis.scip.util.ForecastBlockUtil;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleInsets;
import org.richfaces.component.UIExtendedDataTable;

@Named
@SessionScoped
public class ForecastChart implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5610058691750631142L;

    @Inject
    @Named("forecastStorageService")
    transient ForecastStorageService forecastStorageService;
	
	@Inject
	@Named("topology")
	transient Topology topology;

	@Inject
	@Named("generalService")
	transient GeneralService generalService;

	private Collection<Object> selection;
	private List<CableForecast> cableForecasts;
	private List<CableForecastInput> cableForecastsInputs;
	private CableForecast selectedCableForecast;
	private Date fromDate;
	private Date toDate;

	public ForecastChart() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		this.toDate = new Date();
		try {
			this.fromDate = df.parse(df.format(this.toDate));
			Calendar c = Calendar.getInstance();
			c.setTime(new Date(this.fromDate.getTime()));
			c.add(Calendar.DATE, 1); // number of days to add
			this.toDate = c.getTime();
		} catch (ParseException e) {
			ActionLogger.error(e.getMessage(), new Object[] { }, e, true);
		}
		// this.search();
	}

	public String back() {
		return "topologyCable";
	}

	public void paint(OutputStream out, Object data) throws IOException {
		JFreeChart chart = this.createChart(this.createDataset());
		BufferedImage image = chart.createBufferedImage(1200, 800);
		ImageIO.write(image, "png", out);
	}

	private JFreeChart createChart(XYDataset dataset) {
		String title = "Forecast usage excl. cars cable "
				+ selectedCableForecast.getCable().getCableId() + " for "
				+ selectedCableForecast.getDatetime();

		JFreeChart chart = ChartFactory.createTimeSeriesChart(title, // title
				"Hour", // x-axis label
				"Amp", // y-axis label
				dataset, // data
				true, // create legend?
				true, // generate tooltips?
				false // generate URLs?
				);

		chart.setBackgroundPaint(Color.white);

		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);
		plot.setWeight(5);

		XYItemRenderer r = plot.getRenderer();
		if (r instanceof XYLineAndShapeRenderer) {
			XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
			renderer.setBaseShapesVisible(false);
			renderer.setBaseShapesFilled(true);
			renderer.setDrawSeriesLineAsPath(true);
			BasicStroke dashed = new BasicStroke(1.0f, BasicStroke.CAP_ROUND,
					BasicStroke.JOIN_ROUND, 1.0f, new float[] { 6.0f, 6.0f },
					0.0f);
			for (int i = 0; i < 6; i++) {
				renderer.setSeriesStroke(i, i > 2 ? dashed : new BasicStroke(
						1.0f));
				if (i == 0 || i == 3) {
					renderer.setSeriesPaint(i, Color.RED);
				} else if (i == 1 || i == 4) {
					renderer.setSeriesPaint(i, Color.BLUE);
				} else {
					renderer.setSeriesPaint(i, Color.YELLOW);
				}
			}
		}

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("HH:mm"));

		return chart;

	}

	/**
	 * Creates a dataset.
	 */
	private XYDataset createDataset() {

		List<CableForecastOutput> cfos = this.selectedCableForecast
				.getCableForecastOutputs();

		TimeSeries s1 = new TimeSeries("forecast-fase1");
		TimeSeries s2 = new TimeSeries("forecast-fase2");
		TimeSeries s3 = new TimeSeries("forecast-fase3");

		for (CableForecastOutput cfo : cfos) {
			Date date = ForecastBlockUtil.getStartDateOfBlock(cfo.getDay(),
					cfo.getHour(), cfo.getBlock(),
					this.selectedCableForecast.getBlockMinutes());
			s1.add(new Minute(date), cfo.getUsageExclCars1().doubleValue());
			s2.add(new Minute(date), cfo.getUsageExclCars2().doubleValue());
			s3.add(new Minute(date), cfo.getUsageExclCars3().doubleValue());
		}

		TimeSeries s4 = new TimeSeries("measured-fase1");
		TimeSeries s5 = new TimeSeries("measured-fase2");
		TimeSeries s6 = new TimeSeries("measured-fase3");

		for (CableForecastInput cfi : this.cableForecastsInputs) {
			Date date = cfi.getBlockStart();
			s4.add(new Minute(date), cfi.getAmpNow1());
			s5.add(new Minute(date), cfi.getAmpNow2());
			s6.add(new Minute(date), cfi.getAmpNow3());

		}

		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(s1);
		dataset.addSeries(s2);
		dataset.addSeries(s3);
		dataset.addSeries(s4);
		dataset.addSeries(s5);
		dataset.addSeries(s6);

		return dataset;
	}

	public void selectionListener(AjaxBehaviorEvent event) {
		FacesContext context = FacesContext.getCurrentInstance();

		UIExtendedDataTable dataTable = (UIExtendedDataTable) event
				.getComponent();
		Object originalKey = dataTable.getRowKey();
		for (Object selectionKey : selection) {
			dataTable.setRowKey(selectionKey);
			if (dataTable.isRowAvailable()) {
				selectedCableForecast = (CableForecast) dataTable.getRowData();
				try {
					this.findMeasurements();
				} catch (ActionException e) {
					e.printStackTrace();
					FacesMessage message = new FacesMessage(e.getMessage());
					context.addMessage(null, message);
				}
			}
		}
		dataTable.setRowKey(originalKey);
	}

	private void findMeasurements() throws ActionException {
		this.cableForecastsInputs = forecastStorageService.getCableForecastInputs(selectedCableForecast);
	}

	public void search() {
		FacesContext context = FacesContext.getCurrentInstance();

		Cable cable = topology.getSelectedCable();

		this.selection = null;
		this.selectedCableForecast = null;
		this.cableForecastsInputs = new ArrayList<CableForecastInput>();
		this.cableForecasts = new ArrayList<CableForecast>();

		try {
			this.cableForecasts = forecastStorageService.searchCableForecasts(cable,
					fromDate, toDate);
		} catch (ActionException e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage(null, message);
		}
	}

	public List<CableForecast> getCableForecasts() {
		return cableForecasts;
	}

	public void setCableForecasts(List<CableForecast> cableForecasts) {
		this.cableForecasts = cableForecasts;
	}

	public CableForecast getSelectedCableForecast() {
		return selectedCableForecast;
	}

	public void setSelectedCableForecast(CableForecast selectedCableForecast) {
		this.selectedCableForecast = selectedCableForecast;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

}