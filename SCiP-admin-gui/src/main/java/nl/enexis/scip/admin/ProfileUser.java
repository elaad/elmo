package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.admin.converter.ActionEventConverter;
import nl.enexis.scip.admin.converter.MessageLevelConverter;
import nl.enexis.scip.admin.converter.NotificationIntervalConverter;
import nl.enexis.scip.model.AdminUser;
import nl.enexis.scip.model.AdminUserNotification;
import nl.enexis.scip.service.GenericStorageService;
import nl.enexis.scip.util.security.PasswordUtil;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class ProfileUser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5610058691750631142L;

	@Inject
	@Named("genericStorageService")
	transient GenericStorageService genericStorageService;

	@Inject
	private Identity identity;

	private AdminUser user;

	private String password;
	private String passwordConfirm;

	private AdminUserNotification toDeleteNotification;
	private AdminUserNotification newNotification = new AdminUserNotification();

	public ProfileUser() {
	}

	@PostConstruct
	public void init() throws FacesException {
		String userId = ((User) identity.getAccount()).getLoginName();
		try {
			this.user = genericStorageService.findAdminUserByUserId(userId);
		} catch (ActionException e) {
			throw new FacesException(e.getMessage(), e);
		}
		this.password = "";
		this.passwordConfirm = "";
	}

	public void save() {
		FacesContext context = FacesContext.getCurrentInstance();

		if (!(this.password.trim().isEmpty() && this.passwordConfirm.trim().isEmpty())) {
			if (!this.password.trim().equals(this.passwordConfirm.trim())) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"The password and the confirmation password do not match!", "");
				context.addMessage("popupmessage", message);
				this.password = "";
				this.passwordConfirm = "";
				return;

			} else {

				try {
					/* encrypt and store the password */
					this.user.setPassword(PasswordUtil.encrypt(this.password));

				} catch (Exception e) {
					FacesMessage message = new FacesMessage(e.getMessage());
					context.addMessage("popupmessage", message);
				}
			}
		}

		try {
			this.user = genericStorageService.updateAdminUser(this.user);
			FacesMessage message = new FacesMessage("User profile saved successfully.");
			context.addMessage("popupmessage", message);

		} catch (ActionException e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage("popupmessage", message);
		}
	}

	public void reset() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.init();
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage("popupmessage", message);
		}
	}

	public void resetNewNotification() {
		newNotification = new AdminUserNotification();
	}

	public void insertNewNotification() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			newNotification.setUser(user);
			newNotification = genericStorageService.createAdminUserNotification(newNotification);
			user.getNotifications().add(newNotification);
			this.resetNewNotification();
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getCause().getMessage());
			context.addMessage("popupmessage", message);
		}
	}

	public void toggleActive(AdminUserNotification notification) {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			notification.setEnabled(!notification.isEnabled());
			genericStorageService.updateAdminUserNotification(notification);
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getCause().getMessage());
			context.addMessage("popupmessage", message);
		}
	}
	
	public void deleteNotification() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			genericStorageService.deleteAdminUserNotification(toDeleteNotification);
			user.getNotifications().remove(toDeleteNotification);
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getCause().getMessage());
			context.addMessage("popupmessage", message);
		}
	}

	public void cancelDeleteNotification() {
		toDeleteNotification = null;
	}

	public AdminUser getUser() {
		return this.user;
	}

	public void setUser(AdminUser user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public AdminUserNotification getToDeleteNotification() {
		return toDeleteNotification;
	}

	public void setToDeleteNotification(AdminUserNotification toDeleteNotification) {
		this.toDeleteNotification = toDeleteNotification;
	}

	public AdminUserNotification getNewNotification() {
		return newNotification;
	}

	public void setNewNotification(AdminUserNotification newNotification) {
		this.newNotification = newNotification;
	}

	public List<SelectItem> getActionEventSelectItems() {
		return ActionEventConverter.toSelectItems();
	}

	public List<SelectItem> getNotificationIntervalSelectItems() {
		return NotificationIntervalConverter.toSelectItems();
	}

	public List<SelectItem> getMessageLevelSelectItems() {
		return MessageLevelConverter.toSelectItems();
	}

}