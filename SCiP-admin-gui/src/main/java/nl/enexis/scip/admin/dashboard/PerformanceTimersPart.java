package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.service.DashboardService;

@Named
@ApplicationScoped
public class PerformanceTimersPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4715308817859525015L;

	private List<TimerTurnaroundTimes> data;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	public PerformanceTimersPart() {
	}

	//@PostConstruct
	@Override
	public void reset() throws ActionException {
		Map<String, Double> week = dashboardService.getTimerEventTurnaroundTimes(7);
		Map<String, Double> day = dashboardService.getTimerEventTurnaroundTimes(1);

		data = new ArrayList<TimerTurnaroundTimes>();

		for (String event : week.keySet()) {
			TimerTurnaroundTimes eventTurnarounds = new TimerTurnaroundTimes();
			eventTurnarounds.setEvent(event);
			int index = data.indexOf(eventTurnarounds);
			if (index < 0) {
				eventTurnarounds.setWeek(week.get(event));
				data.add(eventTurnarounds);
			} else {
				data.get(index).setWeek(week.get(event));
			}
		}

		for (String event : day.keySet()) {
			TimerTurnaroundTimes eventTurnarounds = new TimerTurnaroundTimes();
			eventTurnarounds.setEvent(event);
			int index = data.indexOf(eventTurnarounds);
			data.get(index).setDay(day.get(event));
		}

		Collections.sort(data);
	}

	public List<TimerTurnaroundTimes> getData() {
		return this.data;
	}

	public class TimerTurnaroundTimes implements Serializable, Comparable<TimerTurnaroundTimes> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1711533754899351878L;
		
		private String timer;
		private Double day;
		private Double week;

		public TimerTurnaroundTimes() {
		}

		public String getEvent() {
			return timer;
		}

		public void setEvent(String event) {
			this.timer = event;
		}

		public Double getDay() {
			return day;
		}

		public void setDay(Double day) {
			this.day = day;
		}

		public Double getWeek() {
			return week;
		}

		public void setWeek(Double week) {
			this.week = week;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (timer == null ? 0 : timer.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof TimerTurnaroundTimes)) {
				return false;
			}
			TimerTurnaroundTimes other = (TimerTurnaroundTimes) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (timer == null) {
				if (other.timer != null) {
					return false;
				}
			} else if (!timer.equals(other.timer)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(TimerTurnaroundTimes o) {
			int compare = this.day == null && o.day == null ? 0 : this.day == null ? -1 : o.day == null ? 1
					: this.day.compareTo(o.day);
			if (compare == 0) {
				compare = this.week == null && o.week == null ? 0 : this.week == null ? -1 : o.week == null ? 1
						: this.week.compareTo(o.week);
			}
			return -1 * compare;
		}

		private PerformanceTimersPart getOuterType() {
			return PerformanceTimersPart.this;
		}
	}
}
