package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.service.DashboardService;

@Named
@ApplicationScoped
public class MiscellaneousPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2362464951834507246L;

	private List<MiscellaneousValue> data;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	public MiscellaneousPart() {
	}

	//@PostConstruct
	@Override
	public void reset() throws ActionException {
		this.data = new ArrayList<MiscellaneousValue>();

		MiscellaneousValue mv = new MiscellaneousValue();
		mv.setName("Last Database Cleanup");
		mv.setDateValue(dashboardService.getLastDbCleanupDate());

		this.data.add(mv);
	}

	public List<MiscellaneousValue> getData() {
		return this.data;
	}

	public class MiscellaneousValue implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 399005983039965837L;

		private String name;
		private Double doubleValue;
		private Integer intValue;
		private String stringValue;
		private Date dateValue;

		public MiscellaneousValue() {
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Double getDoubleValue() {
			return doubleValue;
		}

		public void setDoubleValue(Double doubleValue) {
			this.doubleValue = doubleValue;
		}

		public Integer getIntValue() {
			return intValue;
		}

		public void setIntValue(Integer intValue) {
			this.intValue = intValue;
		}

		public String getStringValue() {
			return stringValue;
		}

		public void setStringValue(String stringValue) {
			this.stringValue = stringValue;
		}

		public Date getDateValue() {
			return dateValue;
		}

		public void setDateValue(Date dateValue) {
			this.dateValue = dateValue;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (name == null ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof MiscellaneousValue)) {
				return false;
			}
			MiscellaneousValue other = (MiscellaneousValue) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (name == null) {
				if (other.name != null) {
					return false;
				}
			} else if (!name.equals(other.name)) {
				return false;
			}
			return true;
		}

		private MiscellaneousPart getOuterType() {
			return MiscellaneousPart.this;
		}
	}
}
