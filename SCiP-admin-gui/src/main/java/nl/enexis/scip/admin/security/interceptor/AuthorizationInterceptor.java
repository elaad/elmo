package nl.enexis.scip.admin.security.interceptor;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;

import org.picketlink.Identity;
import org.picketlink.idm.model.Attribute;

@Interceptor
@AuthorizationCheck
public class AuthorizationInterceptor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7141983950461767935L;

	@Inject
	private Identity identity;

	@AroundInvoke
	public Object handleActionContext(InvocationContext ctx) throws Exception {
		AuthorizationCheck authorization = ctx.getMethod().getAnnotation(AuthorizationCheck.class);
		String requiredRole = authorization.role();

		boolean isAuthorized = AuthorizationInterceptor.checkRole(identity, requiredRole);

		if (!isAuthorized) {
			String msg = "User is not authorized for this action which requires role '" + requiredRole + "'!";

			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, msg, "");
			context.addMessage(null, message);
			context.addMessage("popupmessage", message);

			throw new ActionException(ActionExceptionType.AUTHORIZATION, msg);
		}

		return ctx.proceed();
	}

	public static boolean checkRole(Identity identity, String requiredRole) {
		boolean isAuthorized = false;
		if (identity.isLoggedIn()) {
			Attribute<String> roles = identity.getAccount().getAttribute("roles");
			if (roles != null && roles.getValue() != null && !roles.getValue().trim().isEmpty()) {
				String[] roleArray = roles.getValue().trim().split(",");
				if (roleArray.length > 0) {
					for (String role : roleArray) {
						if (role.trim().equalsIgnoreCase(requiredRole)) {
							isAuthorized = true;
							break;
						}
					}
				}
			}
		}
		return isAuthorized;
	}
}