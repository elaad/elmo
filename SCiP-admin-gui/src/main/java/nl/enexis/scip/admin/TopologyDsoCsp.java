package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.DsoCsp;
import nl.enexis.scip.service.TopologyStorageService;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class TopologyDsoCsp extends TopologyGenericEntity<DsoCsp> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 8827225139321362834L;

	private String editedDsoEan13;
	private String editedCspEan13;
	private String newDsoEan13;
	private String newCspEan13;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService; 

	@Inject
	private Identity identity;

	public TopologyDsoCsp() {
		this.type = DsoCsp.class;
	}

	@Override
	public void createNew() {
		super.createNew();

		this.newDsoEan13 = "";
		this.newCspEan13 = "";
	}

	@Override
	public void setEdited(DsoCsp edited) {
		super.setEdited(edited);

		this.editedDsoEan13 = edited.getDso().getEan13();
		this.editedCspEan13 = edited.getCsp().getEan13();
	}

	@Override
	protected void saveEntity() throws ActionException {
		DsoCsp dsoCsp = this.getEdited();
		if (dsoCsp != null) {
			Dso dso = topologyStorageService.findDsoByEan13(this.editedDsoEan13);
			if (dso != null) {
				dsoCsp.setDso(dso);
			}
			Csp csp = topologyStorageService.findCspByEan13(this.editedCspEan13);
			if (csp != null) {
				dsoCsp.setCsp(csp);
			}
			topologyStorageService.updateDsoCsp(dsoCsp);
		}
	}

	@Override
	protected void insertEntity() throws ActionException {
		DsoCsp dsoCsp = this.getNewEntity();
		if (dsoCsp != null) {
			Dso dso = topologyStorageService.findDsoByEan13(this.newDsoEan13);
			if (dso != null) {
				dsoCsp.setDso(dso);
			}
			Csp csp = topologyStorageService.findCspByEan13(this.newCspEan13);
			if (csp != null) {
				dsoCsp.setCsp(csp);
			}
			topologyStorageService.createDsoCsp(dsoCsp);
		}
	}

	@Override
	protected void deleteEntity() throws ActionException {
		topologyStorageService.deleteDsoCsp(this.getDeleted());
	}

	@Override
	protected List<DsoCsp> getAllEntities() throws ActionException {
		return topologyStorageService.getAllDsoCsps();
	}

	@Override
	protected User getUser() {
		if (identity == null) {
			return null;
		}
		return (User) identity.getAccount();
	}

	public String getEditedDsoEan13() {
		return editedDsoEan13;
	}

	public void setEditedDsoEan13(String editedDsoEan13) {
		this.editedDsoEan13 = editedDsoEan13;
	}

	public String getEditedCspEan13() {
		return editedCspEan13;
	}

	public void setEditedCspEan13(String editedCspEan13) {
		this.editedCspEan13 = editedCspEan13;
	}

	public String getNewDsoEan13() {
		return newDsoEan13;
	}

	public void setNewDsoEan13(String newDsoEan13) {
		this.newDsoEan13 = newDsoEan13;
	}

	public String getNewCspEan13() {
		return newCspEan13;
	}

	public void setNewCspEan13(String newCspEan13) {
		this.newCspEan13 = newCspEan13;
	}
}
