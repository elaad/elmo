package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.service.DashboardService;
import nl.enexis.scip.service.TopologyStorageService;

@Named
@ApplicationScoped
public class ScLastForecastInputPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4715308817859525015L;

	private List<ScLastInputDates> data;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	public ScLastForecastInputPart() {
	}

	// @PostConstruct
	@Override
	public void reset() throws ActionException {
		Map<Cable, Date> dates = dashboardService.getLastScForecastInputDates();

		List<Cable> activeCables = topologyStorageService.getAllActiveCables(true);

		data = new ArrayList<ScLastInputDates>();

		for (Cable cable : activeCables) {
			if (BehaviourType.SC.equals(cable.getBehaviour())  && cable.getTransformer().isActive()) {
				ScLastInputDates cableInputDates = new ScLastInputDates();
				cableInputDates.setCableId(cable.getCableId());
				if (dates.containsKey(cable)) {
					cableInputDates.setLastInput(dates.get(cable));
				}
				cableInputDates.setFixed(cable.isFixedForecast());
				data.add(cableInputDates);
			}
		}

		// for (Cable cable : dates.keySet()) {
		// Date date = dates.get(cable);
		// ScLastInputDates cableInputDates = new ScLastInputDates();
		// cableInputDates.setCableId(cable.getCableId());
		// cableInputDates.setLastInput(date);
		// cableInputDates.setFixed(cable.isFixedForecast());
		// data.add(cableInputDates);
		// }

		Collections.sort(data);
	}

	public List<ScLastInputDates> getData() {
		return this.data;
	}

	public class ScLastInputDates implements Serializable, Comparable<ScLastInputDates> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5673113468776135376L;

		private String cableId;
		private boolean fixed;
		private Date lastInput;

		public ScLastInputDates() {
		}

		public String getCableId() {
			return cableId;
		}

		public void setCableId(String cableId) {
			this.cableId = cableId;
		}

		public Date getLastInput() {
			return lastInput;
		}

		public void setLastInput(Date lastInput) {
			this.lastInput = lastInput;
		}

		public boolean isFixed() {
			return fixed;
		}

		public void setFixed(boolean fixed) {
			this.fixed = fixed;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (cableId == null ? 0 : cableId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof ScLastInputDates)) {
				return false;
			}
			ScLastInputDates other = (ScLastInputDates) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (cableId == null) {
				if (other.cableId != null) {
					return false;
				}
			} else if (!cableId.equals(other.cableId)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(ScLastInputDates o) {
			int compare = 0;

			if (this.lastInput != null && o.lastInput != null) {
				this.lastInput.compareTo(o.lastInput);
			} else {
				if (this.lastInput == null) {
					compare = -1;
				} else if (o.lastInput == null) {
					compare = 1;
				} else {
					compare = 0;
				}
			}

			return compare;
		}

		private ScLastForecastInputPart getOuterType() {
			return ScLastForecastInputPart.this;
		}
	}
}
