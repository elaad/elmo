package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.service.DashboardService;
import nl.enexis.scip.service.TopologyStorageService;

@Named
@ApplicationScoped
public class LastOutboundMessageSuccessPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6566978213409989112L;

	private List<OutboundLocMeasurementMessageSuccess> dataLocMeasurements;
	private List<OutboundForecastMessageSuccess> dataForecastMsg;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	public LastOutboundMessageSuccessPart() {
	}

	// @PostConstruct
	@Override
	public void reset() throws ActionException {
		// Location measurements
		dataLocMeasurements = new ArrayList<OutboundLocMeasurementMessageSuccess>();
		Map<String, Date> lastSuccesses = dashboardService.getLastSuccessSendLocationMeasurmentForCsps();
		for (String cspLocationMeasurementId : lastSuccesses.keySet()) {
			OutboundLocMeasurementMessageSuccess lastSuccess = new OutboundLocMeasurementMessageSuccess();
			String[] cspLocationMeasurementIdParts = cspLocationMeasurementId.split("-");
			lastSuccess.setCspEan(cspLocationMeasurementIdParts[0]);
			lastSuccess.setLocationId(cspLocationMeasurementIdParts[1]);
			lastSuccess.setMeasurementType(cspLocationMeasurementIdParts[2]);
			lastSuccess.setForecast("F".equalsIgnoreCase(cspLocationMeasurementIdParts[3]) ? true : false);
			lastSuccess.setLast(lastSuccesses.get(cspLocationMeasurementId));
			dataLocMeasurements.add(lastSuccess);
		}
		Collections.sort(dataLocMeasurements);

		// Forecast messages
		Map<CableCsp, Date> lastFmSuccesses = dashboardService.getLastSuccessForecastMessageForCableCsps();
		List<CableCsp> activeCableCsps = topologyStorageService.getAllCompleteActiveCableCsps();
		dataForecastMsg = new ArrayList<OutboundForecastMessageSuccess>();
		for (CableCsp cableCsp : activeCableCsps) {
			OutboundForecastMessageSuccess lastSuccess = new OutboundForecastMessageSuccess();
			lastSuccess.setCableCsp(cableCsp);
			if (lastFmSuccesses.containsKey(cableCsp)) {
				lastSuccess.setLast(lastFmSuccesses.get(cableCsp));
			}
			dataForecastMsg.add(lastSuccess);
		}

		// for (CableCsp cableCsp : lastFmSuccesses.keySet()) {
		// OutboundForecastMessageSuccess lastSuccess = new
		// OutboundForecastMessageSuccess();
		// lastSuccess.setCableCsp(cableCsp);
		// lastSuccess.setLast(lastFmSuccesses.get(cableCsp));
		// dataForecastMsg.add(lastSuccess);
		// }
		Collections.sort(dataForecastMsg);
	}

	public List<OutboundLocMeasurementMessageSuccess> getDataLocMeasurements() {
		return this.dataLocMeasurements;
	}

	public List<OutboundForecastMessageSuccess> getDataForecastMsg() {
		return this.dataForecastMsg;
	}

	public class OutboundLocMeasurementMessageSuccess implements Serializable,
			Comparable<OutboundLocMeasurementMessageSuccess> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6673113468776135376L;

		private String cspEan;
		private String locationId;
		private String measurementType;
		private boolean forecast;
		private Date last;

		public OutboundLocMeasurementMessageSuccess() {
		}

		public String getCspEan() {
			return cspEan;
		}

		public void setCspEan(String cspEan) {
			this.cspEan = cspEan;
		}

		public String getLocationId() {
			return locationId;
		}

		public void setLocationId(String locationId) {
			this.locationId = locationId;
		}

		public String getMeasurementType() {
			return measurementType;
		}

		public void setMeasurementType(String measurementType) {
			this.measurementType = measurementType;
		}

		public boolean isForecast() {
			return forecast;
		}

		public void setForecast(boolean forecast) {
			this.forecast = forecast;
		}

		public Date getLast() {
			return last;
		}

		public void setLast(Date last) {
			this.last = last;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (cspEan == null ? 0 : cspEan.hashCode());
			result = prime * result + (forecast ? 1231 : 1237);
			result = prime * result + (locationId == null ? 0 : locationId.hashCode());
			result = prime * result + (measurementType == null ? 0 : measurementType.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof OutboundLocMeasurementMessageSuccess)) {
				return false;
			}
			OutboundLocMeasurementMessageSuccess other = (OutboundLocMeasurementMessageSuccess) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (cspEan == null) {
				if (other.cspEan != null) {
					return false;
				}
			} else if (!cspEan.equals(other.cspEan)) {
				return false;
			}
			if (forecast != other.forecast) {
				return false;
			}
			if (locationId == null) {
				if (other.locationId != null) {
					return false;
				}
			} else if (!locationId.equals(other.locationId)) {
				return false;
			}
			if (measurementType == null) {
				if (other.measurementType != null) {
					return false;
				}
			} else if (!measurementType.equals(other.measurementType)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(OutboundLocMeasurementMessageSuccess o) {
			int compare;

			if (this.last != null && o.last != null) {
				compare = this.last.compareTo(o.last);
			} else {
				if (this.last == null) {
					compare = -1;
				} else if (o.last == null) {
					compare = 1;
				} else {
					compare = 0;
				}
			}

			return compare;
		}

		private LastOutboundMessageSuccessPart getOuterType() {
			return LastOutboundMessageSuccessPart.this;
		}
	}

	public class OutboundForecastMessageSuccess implements Serializable, Comparable<OutboundForecastMessageSuccess> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3683674174008313689L;

		private CableCsp cableCsp;
		private Date last;

		public OutboundForecastMessageSuccess() {
		}

		public CableCsp getCableCsp() {
			return cableCsp;
		}

		public void setCableCsp(CableCsp cableCsp) {
			this.cableCsp = cableCsp;
		}

		public Date getLast() {
			return last;
		}

		public void setLast(Date last) {
			this.last = last;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (cableCsp == null ? 0 : cableCsp.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof OutboundForecastMessageSuccess)) {
				return false;
			}
			OutboundForecastMessageSuccess other = (OutboundForecastMessageSuccess) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (cableCsp == null) {
				if (other.cableCsp != null) {
					return false;
				}
			} else if (!cableCsp.equals(other.cableCsp)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(OutboundForecastMessageSuccess o) {
			int compare;

			if (this.last != null && o.last != null) {
				compare = this.last.compareTo(o.last);
			} else {
				if (this.last == null) {
					compare = -1;
				} else if (o.last == null) {
					compare = 1;
				} else {
					compare = 0;
				}
			}

			return compare;
		}

		private LastOutboundMessageSuccessPart getOuterType() {
			return LastOutboundMessageSuccessPart.this;
		}
	}

}
