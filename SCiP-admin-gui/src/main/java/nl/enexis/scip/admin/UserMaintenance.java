package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.admin.security.interceptor.AuthorizationCheck;
import nl.enexis.scip.model.AdminUser;
import nl.enexis.scip.service.GenericStorageService;
import nl.enexis.scip.util.security.PasswordUtil;

import org.richfaces.component.UIExtendedDataTable;

@Named
@SessionScoped
public class UserMaintenance implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5610058691750631142L;

	@Inject
	@Named("genericStorageService")
	transient GenericStorageService genericStorageService;

	// @Inject
	// private Identity identity;

	private Collection<Object> selection;
	private List<AdminUser> users;

	private String password;
	private boolean change = true;

	private AdminUser editedUser;

	public UserMaintenance() {
	}

	@PostConstruct
	public void init() throws FacesException {
		try {
			this.users = genericStorageService.getAllAdminUsers();
		} catch (ActionException e) {
			throw new FacesException(e.getMessage(), e);
		}
		this.selection = null;
		this.editedUser = null;
		this.password = "";
	}

	public void refresh() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			this.init();
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage("popupmessage", message);
		}
	}

	public void selectionListener(AjaxBehaviorEvent event) {
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		for (Object selectionKey : selection) {
			dataTable.setRowKey(selectionKey);
			if (dataTable.isRowAvailable()) {
				editedUser = (AdminUser) dataTable.getRowData();
				change = true;
				this.password = "";
				break;
			} else {
				editedUser = null;
			}
		}
		dataTable.setRowKey(originalKey);
	}

	@ActionContext(event = ActionEvent.USER_MANAGEMENT)
	@AuthorizationCheck(role = "admin")
	public void save() {
		nl.enexis.scip.action.ActionContext.addActionTarget(this.editedUser);
		FacesContext context = FacesContext.getCurrentInstance();

		if (!this.password.trim().isEmpty()) {
			try {
				/* encrypt and store the password */
				this.editedUser.setPassword(PasswordUtil.encrypt(this.password));
			} catch (Exception e) {
				ActionLogger.error("Exception setting password for user {0}",
						new Object[] { this.editedUser.getUserId() }, e, true);
				FacesMessage message = new FacesMessage(e.getMessage());
				context.addMessage("popupmessage", message);
				return;
			}
		}

		try {
			this.editedUser = genericStorageService.updateAdminUser(this.editedUser);
			FacesMessage message = new FacesMessage("User saved successfully.");
			context.addMessage("popupmessage", message);
			this.init();
		} catch (ActionException e) {
			ActionLogger.error("Exception updating user {0}", new Object[] { this.editedUser.getUserId() }, e, true);
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage("popupmessage", message);
		}
	}

	@ActionContext(event = ActionEvent.USER_MANAGEMENT)
	@AuthorizationCheck(role = "admin")
	public void delete() {
		nl.enexis.scip.action.ActionContext.addActionTarget(this.editedUser);
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			genericStorageService.deleteAdminUser(editedUser);
			this.init();
		} catch (Exception e) {
			ActionLogger.error("Exception removing user {0}", new Object[] { this.editedUser.getUserId() }, e, true);
			FacesMessage message = new FacesMessage(e.getCause().getMessage());
			context.addMessage("popupmessage", message);
		}
	}

	public void add() {
		this.change = false;
		this.editedUser = new AdminUser();
	}

	@ActionContext(event = ActionEvent.USER_MANAGEMENT)
	@AuthorizationCheck(role = "admin")
	public void create() {
		nl.enexis.scip.action.ActionContext.addActionTarget(this.editedUser);
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			/* encrypt and store the password */
			this.editedUser.setPassword(PasswordUtil.encrypt(this.password));
		} catch (Exception e) {
			ActionLogger.error("Exception setting password for new user {0}",
					new Object[] { this.editedUser.getUserId() }, e, true);
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage("popupmessage", message);
			return;
		}

		try {
			this.editedUser = genericStorageService.updateAdminUser(this.editedUser);
			FacesMessage message = new FacesMessage("User created successfully.");
			context.addMessage("popupmessage", message);
			this.init();
		} catch (ActionException e) {
			ActionLogger
					.error("Exception creating new user {0}", new Object[] { this.editedUser.getUserId() }, e, true);
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage("popupmessage", message);
		}
	}

	public void cancel() {
		this.init();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public AdminUser getEditedUser() {
		return editedUser;
	}

	public void setEditedUser(AdminUser editedUser) {
		this.editedUser = editedUser;
	}

	public List<AdminUser> getUsers() {
		return users;
	}

	public void setUsers(List<AdminUser> users) {
		this.users = users;
	}

	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

	public boolean isChange() {
		return change;
	}

	public void setChange(boolean change) {
		this.change = change;
	}
}