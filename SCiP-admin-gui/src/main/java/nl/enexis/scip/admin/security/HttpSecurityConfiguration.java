package nl.enexis.scip.admin.security;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import javax.enterprise.event.Observes;

import org.picketlink.config.SecurityConfigurationBuilder;
import org.picketlink.event.SecurityConfigurationEvent;

public class HttpSecurityConfiguration {

    public void onInit(@Observes SecurityConfigurationEvent event) {
        SecurityConfigurationBuilder builder = event.getBuilder();

        builder.http()
        	.forPath("/pages/protected/*", "protected")
        	.forPath("/templates/*", "protected")
        	.forPath("/components/*", "protected")
        	.forPath("/resources/*", "protected")
            .forGroup("protected")
            	.authenticateWith()
            		.form()
                            .loginPage("/pages/login.jsf")
                            .errorPage("/pages/login.jsf")
            .forPath("/logout")
                    .logout()
                    .redirectTo("/pages/login.jsf");
    }

}