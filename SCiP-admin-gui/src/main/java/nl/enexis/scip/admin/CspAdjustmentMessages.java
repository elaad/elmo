package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustment;
import nl.enexis.scip.model.CspAdjustmentMessage;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.service.CspStorageService;
import nl.enexis.scip.service.GeneralService;
import nl.enexis.scip.service.TopologyStorageService;
import nl.enexis.scip.util.ForecastBlockUtil;

import org.richfaces.component.UIExtendedDataTable;

@Named
@SessionScoped
public class CspAdjustmentMessages implements Serializable {
	/**
	 * This is the backing bean of the CSP adjustment message GUI.
	 * The user chooses a cable and optionally a CSP. Choosing a cable limits the CSP selection to sensible values.
	 * A search button will bring up the list of forecast messages found in that period.
	 * A detail view lists the adjustments (there can be more than one in a message).
	 * Inspecting an adjustment will pop up an pane with adjustment and forecast values. 
	 */
	private static final long serialVersionUID = -7306096159213844149L;

    
    @Inject
    @Named("cspStorageService")
    transient CspStorageService cspStorageService;
    
    @Inject
    @Named("topologyStorageService")
    transient TopologyStorageService topologyStorageService;
    
    @Inject
    @Named("generalService")
    transient GeneralService generalService;

	private Date fromDate, toDate;
	private List<Cable> cables; // Cables in drop-down
	private List<Csp> csps; // CSPs in drop-down
	private String selCableId; // Id of selected cable
	private String selCspId; // Id (EAN) of selected CSP
	private List<CspAdjustmentMessage> cspAdMsgs; // adjustment messages obtained by search
	private Collection<Object> selectionAdMsg; // adjustment message selection
	private CspAdjustmentMessage selectedAdMsg; // selected adjustment message
	private CspAdjustment inspectedCspAdjustment; // inspected adjustment

	
	public CspAdjustmentMessages() {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		toDate = new Date();
		try {
			fromDate = df.parse(df.format(toDate));
			Calendar c = Calendar.getInstance();
			c.setTime(new Date(fromDate.getTime()));
			c.add(Calendar.DATE, 1); // number of days to add
			toDate = c.getTime();
		} catch (ParseException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null));
		}
		selCspId = ""; selCableId = "";
	}

	@PostConstruct
	public void initialise() {

		try {
			cables = topologyStorageService.getAllCables();
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to load cable records", e.getMessage()));
		}
		//ActionLogger.debug("Loaded: {0}", new Object[]{cables}, null, false);
	}
	
    
    public void changeCable(ValueChangeEvent event) {
    	
    	selCableId = event.getNewValue().toString();
    	
    	try {
			csps = topologyStorageService.getCspsByCableId(selCableId);
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain CSP records", e.getMessage()));
		}
    	//ActionLogger.debug("Loaded: {0}", new Object[]{csps}, null, false);
    	
    	cspAdMsgs = null;  // will clear the table of adjustment messages
    	selectedAdMsg = null;  // and by extension the table of adjustments
    }

    
	public void search() {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		selectionAdMsg = null; // ensure no adjustment messages are preselected
		selectedAdMsg = null;  // will clear the table of adjustments
		
		try {
			cspAdMsgs = cspStorageService.getCspAdjustmentMessageByCableIdBetweenDates(selCableId, fromDate, toDate);
		} catch (ActionException e) {
			context.addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain adjustment messages", e.getMessage()));
		}
		ActionLogger.debug("Found {0} adjustment messages for cable {1} between {2} and {3}.", 
			new Object[]{ cspAdMsgs.size(), selCableId, fromDate, toDate}, null, false);
		
		if (cspAdMsgs.isEmpty()) {
			context.addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_INFO, "No adjustment messages found.", null));
			return;
		}
		
		if (selCspId != null && ! selCspId.isEmpty()) {
			// if we selected a CSP, remove messages that we are not interested in 
			for (Iterator<CspAdjustmentMessage> it = cspAdMsgs.iterator(); it.hasNext(); ) {
				CspAdjustmentMessage m = it.next(); if (! m.getCsp().equals(selCspId)) {
					it.remove();
				}
			}
		}
		ActionLogger.debug("Returning {0} adjustment messages for csp {1}.", 
			new Object[]{ cspAdMsgs.size(), selCspId}, null, false);
	}
	
	
	public void selectionListenerAdMsg(AjaxBehaviorEvent event) {
		
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		Object originalKey = dataTable.getRowKey();
		for (Object key : selectionAdMsg) {
			dataTable.setRowKey(key);
			if (dataTable.isRowAvailable()) {
				selectedAdMsg = (CspAdjustmentMessage) dataTable.getRowData();
			}
		}
		dataTable.setRowKey(originalKey);
		
		// get adjustments for selected adjustment message
		List<CspAdjustment> adjustments = null;
		try {
			adjustments = cspStorageService.getCspAdjustmentByCspAdjustmentMessageId(selectedAdMsg.getId());
		} catch (ActionException e) {
			FacesContext.getCurrentInstance().addMessage("popupmessage", 
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to obtain adjustments", e.getMessage()));
		}
		ActionLogger.debug("Found {0} adjustments for adjustment message {1}.", 
				new Object[]{ adjustments.size(), selectedAdMsg.getId()}, null, false);
		
		selectedAdMsg.setCspAdjustments(adjustments);
		for (CspAdjustment adj : adjustments)
		 {
			adj.setCspAdjustmentMessage(selectedAdMsg); // link adjustment message
		}
	}
	
	
	// helper method to return the start of an adjustment value block
	public Date getStartDateOfBlock(CspAdjustmentValue adval) {
				
		Integer blockMinutes = adval.getCspForecastValue().getCableForecastOutput().getCableForecast().getBlockMinutes();
		return ForecastBlockUtil.getStartDateOfBlock(
			adval.getDay(), adval.getHour() , adval.getBlock(), blockMinutes);
	}
	
	
	// getters and setters below this line

	
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<Csp> getCsps() {
		return csps;
	}

	public void setCsps(List<Csp> csps) {
		this.csps = csps;
	}

	public String getSelCspId() {
		return selCspId;
	}

	public void setSelCspId(String selCspId) {
		this.selCspId = selCspId;
	}

	public List<Cable> getCables() {
		return cables;
	}

	public void setCables(List<Cable> cables) {
		this.cables = cables;
	}

	public String getSelCableId() {
		return selCableId;
	}

	// if the selected cable is cleared, reset the CSP drop-down
	public void setSelCableId(String selCableId) {
		this.selCableId = selCableId;
		if (selCableId.isEmpty()) {
			selCspId = ""; csps = null;
		}
	}

	public List<CspAdjustmentMessage> getCspAdMsgs() {
		return cspAdMsgs;
	}

	public void setCspAdMsgs(List<CspAdjustmentMessage> cspAdMsgs) {
		this.cspAdMsgs = cspAdMsgs;
	}

	public Collection<Object> getSelectionAdMsg() {
		return selectionAdMsg;
	}

	public void setSelectionAdMsg(Collection<Object> selectionAdMsg) {
		this.selectionAdMsg = selectionAdMsg;
	}

	public CspAdjustmentMessage getSelectedAdMsg() {
		return selectedAdMsg;
	}

	
	public void setSelectedAdMsg(CspAdjustmentMessage selectedAdMsg) {
		this.selectedAdMsg = selectedAdMsg;
	}

	public CspAdjustment getInspectedCspAdjustment() {
		return inspectedCspAdjustment;
	}

	public void setInspectedCspAdjustment(CspAdjustment inspectedCspAdjustment) {
		
		FacesContext context = FacesContext.getCurrentInstance();
		
		this.inspectedCspAdjustment = inspectedCspAdjustment;
		
		List<CspAdjustmentValue> values = null;
		// get adjustment values for selected adjustment
		try {
			values = cspStorageService.getCspAdjustmentValueByCspAdjustmentId(inspectedCspAdjustment.getId());
		} catch (ActionException e) {
			context.addMessage("popupmessage", new FacesMessage(
				FacesMessage.SEVERITY_ERROR, "Failed to obtain adjustment values", e.getMessage()));
		}
		ActionLogger.debug("Found {0} adjustment values for csp adjustment {1}.", 
			new Object[]{ values.size(), inspectedCspAdjustment.getId()}, null, false);
		
		this.inspectedCspAdjustment.setCspAdjustmentValues(values);	
	}

}