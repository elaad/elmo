package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.Transformer;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.model.enumeration.UsageMeasurementResponsible;
import nl.enexis.scip.service.TopologyStorageService;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class TopologyCable extends TopologyGenericEntity<Cable> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 5610058691750631142L;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	private Identity identity;

	private String editedDsoEan13;
	private String editedTrafoId;
	private String newDsoEan13;
	private String newTrafoId;

	public TopologyCable() {
		this.type = Cable.class;
	}

	@Override
	public void createNew() {
		super.createNew();

		this.newDsoEan13 = "";
		this.newTrafoId = "";
	}

	@Override
	public void setEdited(Cable edited) {
		super.setEdited(edited);

		this.editedDsoEan13 = edited.getDso().getEan13();
		this.editedTrafoId = edited.getTransformer().getTrafoId();
	}

	@Override
	protected void saveEntity() throws ActionException {
		Cable cable = this.getEdited();
		if (cable != null) {
			Dso dso = topologyStorageService.findDsoByEan13(this.editedDsoEan13);
			if (dso != null) {
				cable.setDso(dso);
			}
			Transformer trafo = topologyStorageService.findTransformerByTrafoId(this.editedTrafoId);
			if (trafo != null) {
				cable.setTransformer(trafo);
			}

			topologyStorageService.updateCable(cable);
		}
	}

	@Override
	protected void insertEntity() throws ActionException {
		Cable cable = this.getNewEntity();
		if (cable != null) {
			Dso dso = topologyStorageService.findDsoByEan13(this.newDsoEan13);
			if (dso != null) {
				cable.setDso(dso);
			}
			Transformer trafo = topologyStorageService.findTransformerByTrafoId(this.newTrafoId);
			if (trafo != null) {
				cable.setTransformer(trafo);
			}

			topologyStorageService.createCable(cable);
		}
	}

	@Override
	protected void deleteEntity() throws ActionException {
		topologyStorageService.deleteCable(this.getDeleted());
	}

	@Override
	protected List<Cable> getAllEntities() throws ActionException {
		return topologyStorageService.getAllCables();
	}

	@Override
	protected User getUser() {
		if (identity == null) {
			return null;
		}
		return (User) identity.getAccount();
	}

	public String getEditedDsoEan13() {
		return editedDsoEan13;
	}

	public void setEditedDsoEan13(String editedDsoEan13) {
		this.editedDsoEan13 = editedDsoEan13;
	}

	public String getEditedTrafoId() {
		return editedTrafoId;
	}

	public void setEditedTrafoId(String editedTrafoId) {
		this.editedTrafoId = editedTrafoId;
	}

	public String getNewDsoEan13() {
		return newDsoEan13;
	}

	public void setNewDsoEan13(String newDsoEan13) {
		this.newDsoEan13 = newDsoEan13;
	}

	public String getNewTrafoId() {
		return newTrafoId;
	}

	public void setNewTrafoId(String newTrafoId) {
		this.newTrafoId = newTrafoId;
	}

	public List<SelectItem> getAllCableOptions() {
		List<SelectItem> allCableOptions = new ArrayList<SelectItem>();

		for (Cable cable : this.getAll()) {
			allCableOptions.add(new SelectItem(cable.getCableId(), cable.getCableId()));
		}

		return allCableOptions;
	}
	
    public List<SelectItem> getBehaviourOptions() {
        return BehaviourType.toSelectItems();
    }

    public List<SelectItem> getUsageMeasurementResponsibleOptions() {
        return UsageMeasurementResponsible.toSelectItems();
    }

}
