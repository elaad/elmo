package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.service.DashboardService;

@Named
@ApplicationScoped
public class IncompleteMeasurementsPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = 726565705877246049L;

	private List<IncompleteMeasurments> counts;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	public IncompleteMeasurementsPart() {
	}

	// @PostConstruct
	@Override
	public void reset() throws ActionException {
		Map<Cable, Long> fourWeek = dashboardService.getIncompleteMeasurmentCounts(28);
		Map<Cable, Long> week = dashboardService.getIncompleteMeasurmentCounts(7);
		Map<Cable, Long> day = dashboardService.getIncompleteMeasurmentCounts(1);

		counts = new ArrayList<IncompleteMeasurments>();

		for (Cable cable : fourWeek.keySet()) {
			IncompleteMeasurments cableCounts = new IncompleteMeasurments();
			cableCounts.setCable(cable);
			int index = counts.indexOf(cableCounts);
			if (index < 0) {
				cableCounts.setFourWeek(fourWeek.get(cable));
				counts.add(cableCounts);
			} else {
				counts.get(index).setFourWeek(fourWeek.get(cable));
			}
		}

		for (Cable cable : week.keySet()) {
			IncompleteMeasurments cableCounts = new IncompleteMeasurments();
			cableCounts.setCable(cable);
			int index = counts.indexOf(cableCounts);
			counts.get(index).setWeek(week.get(cable));
		}

		for (Cable cable : day.keySet()) {
			IncompleteMeasurments cableCounts = new IncompleteMeasurments();
			cableCounts.setCable(cable);
			int index = counts.indexOf(cableCounts);
			counts.get(index).setDay(day.get(cable));
		}

		Collections.sort(counts);
	}

	public List<IncompleteMeasurments> getCounts() {
		return this.counts;
	}

	public class IncompleteMeasurments implements Serializable, Comparable<IncompleteMeasurments> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3674098838209952987L;

		private Cable cable;
		private Long day = (long) 0;
		private Long week = (long) 0;
		private Long fourWeek = (long) 0;

		public IncompleteMeasurments() {
		}

		public Cable getCable() {
			return cable;
		}

		public void setCable(Cable cable) {
			this.cable = cable;
		}

		public Long getDay() {
			return day;
		}

		public void setDay(Long day) {
			this.day = day;
		}

		public Long getWeek() {
			return week;
		}

		public void setWeek(Long week) {
			this.week = week;
		}

		public Long getFourWeek() {
			return fourWeek;
		}

		public void setFourWeek(Long fourWeek) {
			this.fourWeek = fourWeek;
		}

		private IncompleteMeasurementsPart getOuterType() {
			return IncompleteMeasurementsPart.this;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (cable == null ? 0 : cable.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof IncompleteMeasurments)) {
				return false;
			}
			IncompleteMeasurments other = (IncompleteMeasurments) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (cable == null) {
				if (other.cable != null) {
					return false;
				}
			} else if (!cable.equals(other.cable)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(IncompleteMeasurments o) {
			int compare = this.day.compareTo(o.day);
			if (compare == 0) {
				compare = this.week.compareTo(o.week);
				if (compare == 0) {
					compare = this.fourWeek.compareTo(o.fourWeek);
				}
			}
			return -1 * compare;
		}
	}
}
