package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Size;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.admin.security.interceptor.AuthorizationCheck;
import nl.enexis.scip.model.ConfigurationParameter;
import nl.enexis.scip.service.GenericStorageService;
import nl.enexis.scip.service.TimersService;

import org.hibernate.validator.constraints.NotEmpty;
import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class Configuration implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5610058691750631142L;
	
	@Inject
	@Named("genericStorageService")
	transient GenericStorageService genericStorageService;

	@Inject
	@Named("timersService")
	transient TimersService timersService;

	@Inject
	private Identity identity;

	private List<ConfigurationParameter> configs;
	private ConfigurationParameter editedConfig;
	
	@NotEmpty(message = "Comment is required")
	@Size(max = 2048, message = "Comment cannot be more then 2048 characters")
	private String newComments;

	public Configuration() {
	}

	@PostConstruct
	public void init() throws FacesException {
		this.editedConfig = null;
		try {
			this.configs = genericStorageService.getAllConfigurationParameters();
		} catch (ActionException e) {
			throw new FacesException(e.getMessage(), e);
		}
	}

	@ActionContext(event = ActionEvent.TIMER, doRethrow = false)
	@AuthorizationCheck(role = "admin")
	public void resetTimers() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			timersService.resetTimers();
		} catch (ActionException e) {
			ActionLogger.error("Error resetting timers", new Object[] {}, e, true);
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage(null, message);
		}

	}

	@ActionContext(event = ActionEvent.CONFIGURATION, doRethrow = false)
	@AuthorizationCheck(role = "admin")
	public void save() {
		nl.enexis.scip.action.ActionContext.addActionTarget(this.editedConfig);

		FacesContext context = FacesContext.getCurrentInstance();
		try {
			User user = (User) identity.getAccount();
			this.editedConfig.setUserId(user.getLoginName());
			this.editedConfig.setComments(newComments);
			genericStorageService.updateConfigurationParameter(this.editedConfig);
			this.init();
		} catch (Exception e) {
			ActionLogger.error("Error saving {0}", new Object[] { this.editedConfig.getName() }, e, true);
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage(null, message);
		}
	}

	public void reset() {
		FacesContext context = FacesContext.getCurrentInstance();

		try {
			this.init();
		} catch (Exception e) {
			FacesMessage message = new FacesMessage(e.getMessage());
			context.addMessage(null, message);
		}
	}

	public List<ConfigurationParameter> getConfigs() {
		// ensure up to date values from the database are shown;
		try {
			this.configs = genericStorageService.getAllConfigurationParameters();
		} catch (ActionException e) {
			throw new FacesException(e.getMessage(), e);
		}

		return configs;
	}

	public void setConfigs(List<ConfigurationParameter> configs) {
		this.configs = configs;
	}

	public ConfigurationParameter getEditedConfig() {
		return editedConfig;
	}

	public void setEditedConfig(ConfigurationParameter editedConfig) {
		this.editedConfig = editedConfig;
	}

	public String getNewComments() {
		newComments = "";
		return newComments;
	}

	public void setNewComments(String newComments) {
		this.newComments = newComments;
	}
}