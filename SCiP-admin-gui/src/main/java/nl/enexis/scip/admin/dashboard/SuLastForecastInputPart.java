package nl.enexis.scip.admin.dashboard;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.service.DashboardService;
import nl.enexis.scip.service.TopologyStorageService;

@Named
@ApplicationScoped
public class SuLastForecastInputPart extends GenericDashboardPart {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4715308817859525015L;

	private List<SuLastInputDates> data;

	@Inject
	@Named("dashboardService")
	DashboardService dashboardService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	public SuLastForecastInputPart() {
	}

	// @PostConstruct
	@Override
	public void reset() throws ActionException {
		Map<String, Date[]> dates = dashboardService.getLastSuForecastInputDates();

		List<Cable> activeCables = topologyStorageService.getAllActiveCables(true);

		data = new ArrayList<SuLastInputDates>();

		for (Cable cable : activeCables) {
			String cableId = cable.getCableId();
			if (BehaviourType.SU.equals(cable.getBehaviour()) && cable.isActive()) {
				SuLastInputDates cableInputDates = new SuLastInputDates();
				cableInputDates.setCableId(cableId);
				if (dates.containsKey(cableId)) {
					Date[] datesArr = dates.get(cableId);
					cableInputDates.setIrrForecast(datesArr[0]);
					cableInputDates.setIrrObservation(datesArr[1]);
					cableInputDates.setUsage(datesArr[2]);
				}
				data.add(cableInputDates);
			}
		}

		// for (String cableId : dates.keySet()) {
		// Date[] datesArr = dates.get(cableId);
		// SuLastInputDates cableInputDates = new SuLastInputDates();
		// cableInputDates.setCableId(cableId);
		// cableInputDates.setIrrForecast(datesArr[0]);
		// cableInputDates.setIrrObservation(datesArr[1]);
		// cableInputDates.setUsage(datesArr[2]);
		// data.add(cableInputDates);
		// }

		Collections.sort(data);
	}

	public List<SuLastInputDates> getData() {
		return this.data;
	}

	public class SuLastInputDates implements Serializable, Comparable<SuLastInputDates> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 3674054838209952987L;

		private String cableId;
		private Date irrForecast;
		private Date irrObservation;
		private Date usage;

		public SuLastInputDates() {
		}

		public String getCableId() {
			return cableId;
		}

		public void setCableId(String cableId) {
			this.cableId = cableId;
		}

		public Date getIrrForecast() {
			return irrForecast;
		}

		public void setIrrForecast(Date irrForecast) {
			this.irrForecast = irrForecast;
		}

		public Date getIrrObservation() {
			return irrObservation;
		}

		public void setIrrObservation(Date irrObservation) {
			this.irrObservation = irrObservation;
		}

		public Date getUsage() {
			return usage;
		}

		public void setUsage(Date usage) {
			this.usage = usage;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.getOuterType().hashCode();
			result = prime * result + (cableId == null ? 0 : cableId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof SuLastInputDates)) {
				return false;
			}
			SuLastInputDates other = (SuLastInputDates) obj;
			if (!this.getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (cableId == null) {
				if (other.cableId != null) {
					return false;
				}
			} else if (!cableId.equals(other.cableId)) {
				return false;
			}
			return true;
		}

		@Override
		public int compareTo(SuLastInputDates o) {
			int compare = 0;

			if (this.irrForecast != null && o.irrForecast != null) {
				compare = this.irrForecast.compareTo(o.irrForecast);
			} else {
				if (this.irrForecast == null) {
					compare = -1;
				} else if (o.irrForecast == null) {
					compare = 1;
				} else {
					compare = 0;
				}
			}

			if (compare == 0) {
				if (this.irrObservation != null && o.irrObservation != null) {
					compare = this.irrObservation.compareTo(o.irrObservation);
				} else {
					if (this.irrObservation == null) {
						compare = -1;
					} else if (o.irrObservation == null) {
						compare = 1;
					} else {
						compare = 0;
					}
				}
			}

			if (compare == 0) {
				if (this.usage != null && o.usage != null) {
					compare = this.usage.compareTo(o.usage);
				} else {
					if (this.usage == null) {
						compare = -1;
					} else if (o.usage == null) {
						compare = 1;
					} else {
						compare = 0;
					}
				}
			}

			return compare;
		}

		private SuLastForecastInputPart getOuterType() {
			return SuLastForecastInputPart.this;
		}
	}
}
