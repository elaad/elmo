package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CspConnection;
import nl.enexis.scip.service.TopologyStorageService;

import org.picketlink.Identity;
import org.picketlink.idm.model.basic.User;

@Named
@SessionScoped
public class TopologyCspConnection extends TopologyGenericEntity<CspConnection> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 8827225139321362834L;

	private long editedCableCspId;
	private boolean editedActive;
	private BigDecimal editedCapacity;
	private long newCableCspId;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	@Inject
	private Identity identity;

	public TopologyCspConnection() {
		this.type = CspConnection.class;
	}

	@Override
	public void createNew() {
		super.createNew();

		this.newCableCspId = -1;
	}

	@Override
	public void setEdited(CspConnection edited) {
		super.setEdited(edited);

		this.editedCableCspId = edited.getCableCsp().getId();
		this.editedActive = edited.isActive();
		this.editedCapacity = edited.getCapacity();
	}

	@Override
	protected void saveEntity() throws ActionException {
		CspConnection cspConnection = this.getEdited();
		if (cspConnection != null) {
			CableCsp cableCsp = topologyStorageService.findCableCspById(this.editedCableCspId);
			if (cableCsp != null) {
				cspConnection.setCableCsp(cableCsp);
				Cable cable = cableCsp.getCable(); 
				if (cable != null) {
					// if the cspConnection was (in)activated or if the capacity was changed,
					// we must update the CspLastCapacityChange on the cable.
					if ( cspConnection.isActive() != this.editedActive
						|| ! cspConnection.getCapacity().equals(this.editedCapacity)) {
						cable.setCspLastCapacityChange(new Date());
						topologyStorageService.updateCable(cable);
					}
				}
			}

			topologyStorageService.updateCspConnection(cspConnection);
		}
	}

	@Override
	protected void insertEntity() throws ActionException {
		CspConnection cspConnection = this.getNewEntity();
		if (cspConnection != null) {
			CableCsp cableCsp = topologyStorageService.findCableCspById(this.newCableCspId);
			if (cableCsp != null) {
				cspConnection.setCableCsp(cableCsp);
				Cable cable = cableCsp.getCable();
				if (cable != null) {
					// if the new cspConnection is active right away, 
					// we must update the CspLastCapacityChange on the cable.
					if (cspConnection.isActive()) {
						cable.setCspLastCapacityChange(new Date());
						topologyStorageService.updateCable(cable);
					}
				}
			}
			topologyStorageService.createCspConnection(cspConnection);
		}
	}

	@Override
	protected void deleteEntity() throws ActionException {
		topologyStorageService.deleteCspConnection(this.getDeleted());
	}

	@Override
	protected List<CspConnection> getAllEntities() throws ActionException {
		return topologyStorageService.getAllCspConnections();
	}

	@Override
	protected User getUser() {
		if (identity == null) {
			return null;
		}
		return (User) identity.getAccount();
	}

	public long getEditedCableCspId() {
		return editedCableCspId;
	}

	public void setEditedCableCspId(long editedCableCspId) {
		this.editedCableCspId = editedCableCspId;
	}

	public long getNewCableCspId() {
		return newCableCspId;
	}

	public void setNewCableCspId(long newCableCspId) {
		this.newCableCspId = newCableCspId;
	}
}
