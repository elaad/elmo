package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.Serializable;
import java.util.List;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Size;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.admin.security.interceptor.AuthorizationCheck;
import nl.enexis.scip.model.AuditTrailEntity;
import nl.enexis.scip.service.TopologyStorageService;

import org.hibernate.validator.constraints.NotEmpty;
import org.picketlink.idm.model.basic.User;

public abstract class TopologyGenericEntity<T extends AuditTrailEntity> implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 5446410272133959003L;

	@Inject
	@Named("topologyStorageService")
	transient TopologyStorageService topologyStorageService;

	protected Class<T> type;
	protected T newEntity;
	private T edited;
	private T deleted;
	private List<T> all;
	private boolean resetAll = false;

	@NotEmpty(message = "Comment is required")
	@Size(max = 2048, message = "Comment cannot be more then 2048 characters")
	private String newComments;

	public TopologyGenericEntity() {

	}

	public void refresh() {
		this.deleted = null;
		this.edited = null;
		this.newComments = "";
		this.newEntity = null;
	}

	public void resetAll() {
		this.resetAll = true;
		this.refresh();
	}

	@ActionContext(event = ActionEvent.TOPOLOGY)
	@AuthorizationCheck(role = "edit")
	public void save() {
		User user = this.getUser();
		if (user == null) {
			this.setFacesMessage("No identity found!");
		} else {
			try {
				this.edited.setUserId(user.getLoginName());
				this.edited.setComments(this.newComments);
				this.saveEntity();
			} catch (ActionException e) {
				ActionLogger.error("Error updating {0}", new Object[] { this.edited.getActionTargetName() }, e, true);
				this.setFacesMessage(e.getMessage());
			}
		}
		this.resetAll();
	}

	@ActionContext(event = ActionEvent.TOPOLOGY)
	@AuthorizationCheck(role = "edit")
	public void insert() {
		User user = this.getUser();
		if (user == null) {
			this.setFacesMessage("No identity found!");
		} else {
			try {
				this.newEntity.setUserId(user.getLoginName());
				this.newEntity.setComments(this.newComments);
				this.insertEntity();
			} catch (ActionException e) {
				ActionLogger.error("Error creating {0}", new Object[] { this.edited.getActionTargetName() }, e, true);
				this.setFacesMessage(e.getMessage());
			}
		}
		this.resetAll();
	}

	@ActionContext(event = ActionEvent.TOPOLOGY)
	@AuthorizationCheck(role = "edit")
	public void delete() {
		nl.enexis.scip.action.ActionContext.addActionTarget(this.deleted);
		User user = this.getUser();
		if (user == null) {
			this.setFacesMessage("No identity found!");
		} else {
			try {
				this.deleted.setUserId(user.getLoginName());
				this.deleted.setComments(this.newComments);
				this.deleteEntity();
			} catch (ActionException e) {
				ActionLogger.error("Error removing {0}", new Object[] { this.deleted.getActionTargetName() }, e, true);
				this.setFacesMessage(e.getMessage());
			}
		}
		this.resetAll();
	}

	public void createNew() {
		try {
			this.newEntity = this.type.newInstance();
		} catch (Exception e) {
			ActionLogger.error(e.getMessage(), new Object[] { }, e, true);
		}
	}

	public List<T> getAll() {
		// ensure up to date values from the database are shown;
		if (this.all == null || resetAll) {
			try {
				this.all = this.getAllEntities();
				this.resetAll = false;
			} catch (ActionException e) {
				throw new FacesException(e.getMessage(), e);
			}
		}

		return this.all;
	}

	protected boolean isResetAll() {
		return resetAll;
	}

	protected void setResetAll(boolean resetAll) {
		this.resetAll = resetAll;
	}

	public T getEdited() {
		return edited;
	}

	public void setEdited(T edited) {
		this.edited = edited;
	}

	public T getDeleted() {
		return deleted;
	}

	public void setDeleted(T deleted) {
		this.deleted = deleted;
	}

	public T getNewEntity() {
		return newEntity;
	}

	public void setNewEntity(T newEntity) {
		this.newEntity = newEntity;
	}

	public String getNewComments() {
		newComments = "";
		return newComments;
	}

	public void setNewComments(String newComments) {
		this.newComments = newComments;
	}

	protected void setFacesMessage(String msg) {
		FacesContext context = FacesContext.getCurrentInstance();

		if (context != null) {
			FacesMessage message = new FacesMessage(msg);
			context.addMessage("popupmessage", message);
		}
	}

	abstract protected void saveEntity() throws ActionException;

	abstract protected void insertEntity() throws ActionException;

	abstract protected void deleteEntity() throws ActionException;

	abstract protected List<T> getAllEntities() throws ActionException;

	abstract protected User getUser();
}
