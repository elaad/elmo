package nl.enexis.scip.admin;

/*
 * #%L
 * SCiP-admin-gui
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.ActionTargetInOut;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.action.model.ActionTarget;
import nl.enexis.scip.service.ActionContextService;

import org.richfaces.component.UIExtendedDataTable;

@Named("messageLog")
@SessionScoped
public class ActionMessageLog implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5610058691750631142L;

	private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	private Collection<Object> selection;
	private List<ActionMessage> messages;
	private ActionMessage selectedMessage;
	private String actionId = "";
	private Date fromDate;
	private Date toDate;
	private String targetType = "";
	private String actor = "";
	private String targetId = "";
	private List<Level> levels = Arrays.asList(Level.values());;
	private List<Level> selectedLevels = Arrays.asList(new Level[] { Level.ERROR });
	private List<ActionEvent> events = Arrays.asList(ActionEvent.values());
	private List<ActionEvent> selectedEvents = new ArrayList<ActionEvent>(0);
	private List<ActionExceptionType> exceptionTypes = Arrays.asList(ActionExceptionType.values());
	private List<ActionExceptionType> selectedExceptionTypes = new ArrayList<ActionExceptionType>(0);
	private List<Action> treeActions = new ArrayList<Action>();

	@Inject
	@Named("actionContextService")
	transient ActionContextService actionContextService;

	public ActionMessageLog() {
	}

	@PostConstruct
	public void init() {
		this.toDate = new Date();
		try {
			this.fromDate = df.parse(df.format(this.toDate));
			Calendar c = Calendar.getInstance();
			c.setTime(new Date(this.fromDate.getTime()));
			c.add(Calendar.DATE, 1); // number of days to add
			this.toDate = c.getTime();
		} catch (ParseException e) {
			ActionLogger.error(e.getMessage(), new Object[] { }, e, true);
		}

		//search();
	}

	public String getTargetForMessage(ActionMessage msg) {
		ActionTarget at = this.getActionTargetForMessage(msg);
		if (at != null) {
			return this.getShortTargetType(at.getType());
		}
		return "UNKNOWN";
	}

	public String getTargetIdForMessage(ActionMessage msg) {
		ActionTarget at = this.getActionTargetForMessage(msg);
		if (at != null) {
			return at.getValue();
		}
		return "UNKNOWN";
	}

	public String getShortTargetType(String targetType) {
		if (targetType == null || targetType.trim().length() == 0) {
			return "";
		} else {
			String[] parts = targetType.trim().split("\\.");
			return parts[parts.length - 1];
		}
	}

	private ActionTarget getActionTargetForMessage(ActionMessage msg) {
		ActionTarget target = null;

		for (ActionTarget t : msg.getAction().getTargets()) {
			if (t.getDirection().equals(ActionTargetInOut.IN) && t.getNumber() == 1) {
				target = t;
				break;
			}
		}

		return target;
	}

	@Produces
	@RequestScoped
	@Named("actors")
	public List<String> getActors() {
		List<String> actors = new ArrayList<String>();
		actors.add("");
		actors.addAll(actionContextService.getLoggedActors());
		return actors;
	}

	public List<ActionMessage> getMessages() {
		return this.messages;
	}

	public void selectionListener(AjaxBehaviorEvent event) {
		UIExtendedDataTable dataTable = (UIExtendedDataTable) event.getComponent();
		// dataTable.getEventNames();
		Object originalKey = dataTable.getRowKey();
		for (Object selectionKey : selection) {
			dataTable.setRowKey(selectionKey);
			if (dataTable.isRowAvailable()) {
				selectedMessage = (ActionMessage) dataTable.getRowData();
				treeActions = new ArrayList<Action>();
				treeActions.add(selectedMessage.getAction());
			}
		}
		dataTable.setRowKey(originalKey);

	}

	public void search() {
		this.selection = null;
		this.selectedMessage = null;
		this.treeActions = new ArrayList<Action>();

		this.messages = actionContextService.searchActionMessages(actionId, fromDate, toDate, targetType, targetId,
				selectedEvents, selectedLevels, selectedExceptionTypes, actor);
	}

	public ActionMessage getSelectedMessage() {
		return selectedMessage;
	}

	public void setSelectedMessage(ActionMessage selectedMessage) {
		this.selectedMessage = selectedMessage;
	}

	public Collection<Object> getSelection() {
		return selection;
	}

	public void setSelection(Collection<Object> selection) {
		this.selection = selection;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	@Produces
	@RequestScoped
	@Named("targetTypes")
	public List<String> getTargetTypes() {
		List<String> targetTypes = new ArrayList<String>();
		targetTypes.add("");
		targetTypes.addAll(actionContextService.getLoggedActionTargets());
		return targetTypes;
	}

	public String getSelectedTargetType() {
		return targetType;
	}

	public void setSelectedTargetType(String selectedTargetType) {
		this.targetType = selectedTargetType;
	}

	public String getSelectedActor() {
		return actor;
	}

	public void setSelectedActor(String actor) {
		this.actor = actor;
	}

	public List<ActionEvent> getEvents() {
		return events;
	}

	public void setEvents(List<ActionEvent> events) {
		this.events = events;
	}

	public List<ActionEvent> getSelectedEvents() {
		return selectedEvents;
	}

	public void setSelectedEvents(List<ActionEvent> selectedEvents) {
		this.selectedEvents = selectedEvents;
	}

	public List<ActionExceptionType> getExceptionTypes() {
		return exceptionTypes;
	}

	public void setExceptionTypes(List<ActionExceptionType> exceptionTypes) {
		this.exceptionTypes = exceptionTypes;
	}

	public List<ActionExceptionType> getSelectedExceptionTypes() {
		return selectedExceptionTypes;
	}

	public void setSelectedExceptionTypes(List<ActionExceptionType> selectedExceptionTypes) {
		this.selectedExceptionTypes = selectedExceptionTypes;
	}

	public List<Level> getLevels() {
		return levels;
	}

	public void setLevels(List<Level> levels) {
		this.levels = levels;
	}

	public List<Level> getSelectedLevels() {
		return selectedLevels;
	}

	public void setSelectedLevels(List<Level> selectedLevels) {
		this.selectedLevels = selectedLevels;
	}

	public List<Action> getTreeActions() {
		return treeActions;
	}

	public void setTreeActions(List<Action> treeActions) {
		this.treeActions = treeActions;
	}

}