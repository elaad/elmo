package nl.enexis.scip.mp.rs.model;

/*
 * #%L
 * SCiP-MP-Service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;


public class Measurement {

	String meter, timestamp; // meter id and time stamp in ISO8601 format
	List<MeasurementValue> values;
	
	public String getMeter() {
		return meter;
	}
	
	public void setMeter(String meter) {
		this.meter = meter;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public List<MeasurementValue> getValues() {
		return values;
	}

	public void setValues(List<MeasurementValue> values) {
		this.values = values;
	}

	
	@Override
	public String toString() {
		return "Measurement (meter=" + meter + ", timestamp=" + timestamp
				+ ", values=" + values + ")";
	}

}
