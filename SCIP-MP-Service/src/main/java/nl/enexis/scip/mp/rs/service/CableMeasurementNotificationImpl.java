package nl.enexis.scip.mp.rs.service;

/*
 * #%L
 * SCiP-MP-Service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.model.Transformer;
import nl.enexis.scip.model.enumeration.MeasuresFor;
import nl.enexis.scip.mp.rs.model.CableMeasurementNotification;
import nl.enexis.scip.mp.rs.model.Measurement;
import nl.enexis.scip.mp.rs.model.MeasurementValue;
import nl.enexis.scip.mp.rs.model.ServiceResponse;
import nl.enexis.scip.service.MeasurementStorageService;
import nl.enexis.scip.service.TopologyStorageService;

@Named
public class CableMeasurementNotificationImpl {
	
    @Inject
    @Named("topologyStorageService")
    TopologyStorageService topologyStorageService;
    
	@Inject
	@Named("measurementStorageService")
	MeasurementStorageService measurementStorageService;
    
	// inner utility class: rejection message
    public class Rejection {
    	
    	private final String pattern;
    	Rejection(String pattern) { this.pattern = pattern; }    	
    	public String format(Object[] args) { return MessageFormat.format(pattern, args); }
    }

    // Cable Measurement Rejections
    private final Rejection CMR01 = new Rejection("CMR01: {0} is missing, or has no value.");
    private final Rejection CMR02 = new Rejection("CMR02: {0} not found in current configuration.");
    private final Rejection CMR03 = new Rejection("CMR03: {0} is not associated with {1}.");
    private final Rejection CMR04 = new Rejection("CMR04: {0} for {1} should contain {2}.");
    private final Rejection CMR05 = new Rejection("CMR05: {0} is not a valid {1}.");
    private final Rejection CMR06 = new Rejection("CMR06: {0} is not active.");
	
    
	// method to validate the cable measurement request (no actual processing yet)
  	public String validate(CableMeasurementNotification notification) 
			throws ActionException {
		
		String mpId = notification.getMp();
		if (mpId == null || mpId.isEmpty()) {
			return CMR01.format(new Object[]{"mp"});
		}
		
		String dsoId = notification.getDso();
		if (dsoId == null || dsoId.isEmpty()) {
			return CMR01.format(new Object[]{"dso"});
		}
		
		String cableId = notification.getCable();
		if (cableId == null || cableId.isEmpty()) {
			return CMR01.format(new Object[]{"cable"});
		}
		
		// do we know this cable and is its transformer active?
		// note: we explicitly allow measurements for inactive cables!
		Cable cable = topologyStorageService.findCableByCableId(cableId);
		if (cable == null) {
			return CMR02.format(new Object[]{"cable " + cableId});
		}
		
		Transformer trafo = cable.getTransformer();
		if (! trafo.isActive()) {
			return CMR06.format(new Object[]{"transformer of " + cableId});
		}
		
		// is the MP the measurement provider, and is it active?
		if (! mpId.equals(trafo.getMeasurementProvider().getId())) {
			return CMR03.format(new Object[]{"cable " + cableId, "mp " + mpId});
		}
		
		if (! trafo.getMeasurementProvider().isActive()) {
			return CMR06.format(new Object[]{"measurement provider " + mpId});
		}
		
		// does the DSO "own" this cable? Not really important right now, but who knows...
		if (! dsoId.equals(cable.getDso().getEan13())) {
			return CMR03.format(new Object[]{"cable " + cableId, "dso " + dsoId});
		}
		
		// do we have measurements?
		List<Measurement> measurements = notification.getMeasurements();
		if (measurements == null || measurements.isEmpty()) {
			return CMR04.format(
					new Object[]{"notification", "cable " + cableId, "at least 1 measurement."});
		}
		
		long im = 0; // validate the measurements
		for (Measurement measurement : measurements) {
			
			++im;
			
			String meterId = measurement.getMeter();
			if (meterId == null || meterId.isEmpty()) {
				return CMR01.format(new Object[]{"measurement[" + im + "]/meter"});
			}
			
			String timestamp = measurement.getTimestamp();
			if (timestamp == null || timestamp.isEmpty()) {
				return CMR01.format(new Object[]{"measurement[" + im + "]/timestamp"});
			}
			
			try { javax.xml.bind.DatatypeConverter.parseDateTime(timestamp);
			} catch(IllegalArgumentException e) {
				return CMR05.format(
						new Object[]{"measurement[" + im + "]/timestamp " + timestamp, "ISO8601 date"});
			}
		
			// do we know this meter and is it active?
			Meter meter = topologyStorageService.findMeterByMeterId(meterId);
			if (meter == null) {
				return CMR02.format(new Object[]{"measurement[" + im + "]/meter " + meterId});
			}
			if (! meter.isActive()) {
				return CMR06.format(new Object[]{"measurement[" + im + "]/meter " + meterId});
			}
			
			// is this a meter of the specified cable?
			List<Meter> meters = cable.getTransformer().getMeters();
			if (! meters.contains(meter)) {
				return CMR03.format(new Object[]{"meter " + meterId, "cable " + cableId});
			}
			
			// do we have enough measurement values?
			List<MeasurementValue> values = measurement.getValues();
			if (values == null || values.size() != 3) {
				return CMR04.format(
						new Object[]{"measurement[" + im + "]", "meter " + meterId, "exactly 3 values"});
			}
			
			long iv = 0; // validate the values in a measurement
			int labelbitfield = 0;  // used to check if we have all labels
			for (MeasurementValue value : values) {
				
				++iv;
				
				Double val = value.getValue();
				// Probably never happens. JSON casts null to 0.0 (for numeric members anyway)
				if (val == null) {
					return CMR01.format(
							new Object[]{"measurement[" + im + "]/value[" + iv + "]/value"});
				}
				
				String label = value.getLabel();
				if (label == null || label.isEmpty()) {
					return CMR01.format(
							new Object[]{"measurement[" + im + "]/value[" + iv + "]/label"});
				}
				
				// register each label and reject unknown ones
				switch(label) {
					case "IGEM1": labelbitfield = labelbitfield | 1; break;
					case "IGEM2": labelbitfield = labelbitfield | 2; break;
					case "IGEM3": labelbitfield = labelbitfield | 4; break;
					default: return CMR05.format(
						new Object[]{"measurement[" + im + "]/value[" + iv + "]/label " + label, "category"});
				}
			}
			
			if (labelbitfield != 7) {
				return CMR04.format(
						new Object[]{"measurement[" + im + "]", "meter " + meterId, "IGEM1, IGEM2 and IGEM3"});
			}
		}
		
		return null; // no validation errors if we get here
	}
  	
  	
  	
	// method to process the cable measurement request (which should be validated already)
  	public void process(CableMeasurementNotification notification) 
			throws ActionException {
		
		Cable cable = topologyStorageService.findCableByCableId(notification.getCable());
		Date lastMeasurementDate = cable.getTransformer().getLastMeasurement();

		// this map will hold the measurements we get (key is the time in seconds since Epoch)
		SortedMap<Long, nl.enexis.scip.model.Measurement> 
			modelMap = new TreeMap<Long, nl.enexis.scip.model.Measurement>();

		// iterate over the measurements in the notification, adding to or updating in the map
		for (Measurement measurement : notification.getMeasurements()) {
			
			Calendar c = javax.xml.bind.DatatypeConverter.parseDateTime(measurement.getTimestamp());
			long timestamp = 1000 * (c.getTimeInMillis() / 1000);  // round to nearest second
			
			nl.enexis.scip.model.Measurement modelMeasurement = modelMap.get(timestamp);
			// if an existing measurement is not found, load or create it and add it to the map
			if (modelMeasurement == null) {
				Date date = new Date(timestamp);
				if (lastMeasurementDate != null && timestamp <= lastMeasurementDate.getTime()) {
					modelMeasurement = measurementStorageService.getMeasurementForCableAndDate(date, cable);
				}
				if (modelMeasurement == null) {
					modelMeasurement = new nl.enexis.scip.model.Measurement(cable, date);
				}
				modelMap.put(timestamp, modelMeasurement);
			}
			
			nl.enexis.scip.model.MeasurementValue modelValue = null;
			// try to find an existing measurement value for the current meter
			Meter meter = topologyStorageService.findMeterByMeterId(measurement.getMeter());
			for (nl.enexis.scip.model.MeasurementValue mv : modelMeasurement.getMeasurementValues()) {
				if (mv.getMeter().equals(meter)) {
					modelValue = mv;
				} break;
			}
			
			// if no existing found, add a new measurement value for this meter
			if (modelValue == null) {
				modelValue = new nl.enexis.scip.model.MeasurementValue();
				modelMeasurement.getMeasurementValues().add(modelValue);
				modelValue.setMeasurement(modelMeasurement);
				modelValue.setMeter(meter);
			}
			
			// add values (or override the existing ones when it was found earlier)
			for (MeasurementValue value : measurement.getValues()) {
				switch (value.getLabel()) {
					case "IGEM1": modelValue.setIGem1(new BigDecimal(value.getValue())); break;
					case "IGEM2": modelValue.setIGem2(new BigDecimal(value.getValue())); break;
					case "IGEM3": modelValue.setIGem3(new BigDecimal(value.getValue())); break;
				}
			}
			
			ActionLogger.debug("Added: {0} with values IGEM1={1} IGEM2={2} IGEM3={3} for meter {4}", new Object[] 
				{ modelMeasurement, modelValue.getIGem1(), modelValue.getIGem2(), modelValue.getIGem3(), meter }, null, false);
		}
		
		// finally, for each measurement, we need to set whether it is complete
		// first build a list of meters that we expect to receive for this cable
		List<Meter> expectedMeters = new ArrayList<Meter>();
		for (Meter m : cable.getTransformer().getMeters()) {
			if (m.getMeasures() == MeasuresFor.CARS || m.getMeasures() == MeasuresFor.TOTALS) {
				expectedMeters.add(m);
			}
		}
		
		// for EACH measurement in the map, build a list of received meters
		for (nl.enexis.scip.model.Measurement m : modelMap.values()) {
			
			List<Meter> receivedMeters = new ArrayList<Meter>();
			for (nl.enexis.scip.model.MeasurementValue v: m.getMeasurementValues()) {
				receivedMeters.add(v.getMeter());
			}
			
			// if all expected meters are received, the measurement is complete
			m.setComplete(receivedMeters.containsAll(expectedMeters)); 
		}

		// store the measurements and update the transformer (last measurement time)
		if (!modelMap.isEmpty()) {
			measurementStorageService.mergeMeasurements(modelMap);
			if (lastMeasurementDate == null || modelMap.lastKey() > lastMeasurementDate.getTime()) {
				topologyStorageService.updateTransformerLastMeasurement(
					cable.getTransformer().getTrafoId(), new Date(modelMap.lastKey())
				);
			}
		}
	}
	
  	
  	// main method
	public ServiceResponse cableMeasurementResponse(CableMeasurementNotification notification) 
			throws ActionException {
		
		String rejectionmessage = this.validate(notification); // validate the request before processing
		if (rejectionmessage != null) {
			return new ServiceResponse(false, rejectionmessage);
		}

		this.process(notification); // process validated notification

		return new ServiceResponse(true, "OK");
	}
}
