package nl.enexis.scip.mp.rs.model;

/*
 * #%L
 * SCiP-MP-Service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;


public class CableMeasurementNotification {
	
	String mp, dso, cable; // measurement provider id, DSO id (ean13), and cable id
	
	List<Measurement> measurements;
			
	public String getMp() {
		return mp;
	}

	public void setMp(String mp) {
		this.mp = mp;
	}

	public String getDso() {
		return dso;
	}
	
	public void setDso(String dso) {
		this.dso = dso;
	}

	public String getCable() {
		return cable;
	}

	public void setCable(String cable) {
		this.cable = cable;
	}

	public List<Measurement> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<Measurement> measurements) {
		this.measurements = measurements;
	}

	
	@Override
	public String toString() {
		return "CableMeasurementNotification (mp=" + mp + ", dso=" + dso
				+ ", cable=" + cable + ", measurements=" + measurements + ")";
	}

}
