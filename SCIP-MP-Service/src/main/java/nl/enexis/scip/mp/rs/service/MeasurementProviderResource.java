package nl.enexis.scip.mp.rs.service;

/*
 * #%L
 * SCiP-MP-Service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.mp.rs.model.CableMeasurementNotification;
import nl.enexis.scip.mp.rs.model.ServiceResponse;

@Path("REST")
public class MeasurementProviderResource {

	// for JAX-RS
	public MeasurementProviderResource() {
	} 

	// @GET()
	// @Path("txt/test")
	// public String TxtTest() {
	// return "The time is: " + (new Date());
	// }

	@GET()
	@Path("json/test")
	@Produces("application/json")
	public ServiceResponse JsonTest() {
		return new ServiceResponse(true, new Date().toString());
	}

	@Inject
	private CableMeasurementNotificationImpl cmnImpl;

	@POST()
	@Path("json/cablemeasurement")
	@Consumes("application/json")
	@ActionContext(event = ActionEvent.RECEIVE_MEASUREMENTS, doRethrow = true)
	public Response JsonCableMeasurement(CableMeasurementNotification notification) throws ActionException {

		ActionLogger.trace("Notification received: {0}", new Object[] { notification }, null, false);
		ServiceResponse response = cmnImpl.cableMeasurementResponse(notification);

		if (response.isAccepted()) {
			ActionLogger.info("Notification for cable {0} accepted.", new Object[] { notification.getCable() }, null, false);
		} else {
			ActionLogger.warn("Notification for cable {0} rejected: {1}", new Object[] { notification.getCable(), response.getMessage() },
					null, true);
		}

		return Response.status(200).entity(response).type("application/json").build();
	}

}
