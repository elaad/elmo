package nl.enexis.scip.mp.rs.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.mp.rs.model.CableMeasurementNotification;
import nl.enexis.scip.mp.rs.model.ServiceResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ActionLogger.class, Response.class })
public class MeasurementProviderResourceTest {

	@InjectMocks
	private MeasurementProviderResource measurementProviderResource;

	@Mock
	private CableMeasurementNotificationImpl cmnImpl;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ActionLogger.class);
		PowerMockito.mockStatic(Response.class);
	}

	// Verify that there is correct logged when responses are accepted
	@Test
	public void testJsonCableMeasurement_AcceptedResponse() throws Exception {
		CableMeasurementNotification notification = new CableMeasurementNotification();
		ServiceResponse serviceResponse = new ServiceResponse(true, "SERVICERESPONSEMESSAGE");
		// extra explicit setting of accepted
		serviceResponse.setAccepted(true);
		when(cmnImpl.cableMeasurementResponse(any(CableMeasurementNotification.class))).thenReturn(serviceResponse);

		ResponseBuilder responseBuilderMock = mock(ResponseBuilder.class);
		when(responseBuilderMock.entity(any(ServiceResponse.class))).thenReturn(responseBuilderMock);
		when(responseBuilderMock.type(anyString())).thenReturn(responseBuilderMock);
		when(responseBuilderMock.build()).thenReturn(null);

		when(Response.status(anyInt())).thenReturn(responseBuilderMock);

		measurementProviderResource.JsonCableMeasurement(notification);

		PowerMockito.verifyStatic(times(1));
		ActionLogger.trace(eq("Notification received: {0}"), any(Object[].class), any(Throwable.class), eq(false));

		PowerMockito.verifyStatic(times(1));
		ActionLogger.info(eq("Notification for cable {0} accepted."), any(Object[].class), any(Throwable.class), eq(false));
	}

	// Verify that there is correct logged when responses are not accepted
	@Test
	public void testJsonCableMeasurement_NotAcceptedResponse() throws Exception {
		CableMeasurementNotification notification = new CableMeasurementNotification();
		ServiceResponse serviceResponse = new ServiceResponse(false, "SERVICERESPONSEMESSAGE");
		// extra explicit setting of accepted
		serviceResponse.setAccepted(false);
		when(cmnImpl.cableMeasurementResponse(any(CableMeasurementNotification.class))).thenReturn(serviceResponse);

		ResponseBuilder responseBuilderMock = mock(ResponseBuilder.class);
		when(responseBuilderMock.entity(any(ServiceResponse.class))).thenReturn(responseBuilderMock);
		when(responseBuilderMock.type(anyString())).thenReturn(responseBuilderMock);
		when(responseBuilderMock.build()).thenReturn(null);

		when(Response.status(anyInt())).thenReturn(responseBuilderMock);

		measurementProviderResource.JsonCableMeasurement(notification);

		PowerMockito.verifyStatic(times(1));
		ActionLogger.trace(eq("Notification received: {0}"), any(Object[].class), any(Throwable.class), eq(false));

		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(eq("Notification for cable {0} rejected: {1}"), any(Object[].class), any(Throwable.class), eq(true));
	}
}
