
package nl.enexis.scip.admin.ws.generated;

/*
 * #%L
 * SCiP-admin-service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FixedForecast complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FixedForecast">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Active" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Active"/>
 *         &lt;element name="Random" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Random"/>
 *         &lt;element name="Maximum" type="{http://www.enexis.nl/SCiP/admin/2013/10/}DecimalValue"/>
 *         &lt;element name="Deviation" type="{http://www.enexis.nl/SCiP/admin/2013/10/}DecimalValue"/>
 *         &lt;element name="DeviationBlocks" type="{http://www.enexis.nl/SCiP/admin/2013/10/}DeviationBlocks"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FixedForecast", propOrder = {
    "active",
    "random",
    "maximum",
    "deviation",
    "deviationBlocks"
})
public class FixedForecast {

    @XmlElement(name = "Active")
    protected boolean active;
    @XmlElement(name = "Random")
    protected boolean random;
    @XmlElement(name = "Maximum", required = true)
    protected BigDecimal maximum;
    @XmlElement(name = "Deviation", required = true)
    protected BigDecimal deviation;
    @XmlElement(name = "DeviationBlocks", required = true)
    protected String deviationBlocks;

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the random property.
     * 
     */
    public boolean isRandom() {
        return random;
    }

    /**
     * Sets the value of the random property.
     * 
     */
    public void setRandom(boolean value) {
        this.random = value;
    }

    /**
     * Gets the value of the maximum property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaximum() {
        return maximum;
    }

    /**
     * Sets the value of the maximum property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaximum(BigDecimal value) {
        this.maximum = value;
    }

    /**
     * Gets the value of the deviation property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDeviation() {
        return deviation;
    }

    /**
     * Sets the value of the deviation property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDeviation(BigDecimal value) {
        this.deviation = value;
    }

    /**
     * Gets the value of the deviationBlocks property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviationBlocks() {
        return deviationBlocks;
    }

    /**
     * Sets the value of the deviationBlocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviationBlocks(String value) {
        this.deviationBlocks = value;
    }

}
