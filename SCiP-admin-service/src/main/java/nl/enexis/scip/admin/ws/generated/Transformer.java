
package nl.enexis.scip.admin.ws.generated;

/*
 * #%L
 * SCiP-admin-service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Transformer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Transformer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Active" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Active"/>
 *         &lt;element name="TransformerId" type="{http://www.enexis.nl/SCiP/admin/2013/10/}TransformerId"/>
 *         &lt;element name="Meter" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Meter" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Transformer", propOrder = {
    "active",
    "transformerId",
    "meter"
})
public class Transformer {

    @XmlElement(name = "Active")
    protected boolean active;
    @XmlElement(name = "TransformerId", required = true)
    protected String transformerId;
    @XmlElement(name = "Meter", required = true)
    protected List<Meter> meter;

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the transformerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransformerId() {
        return transformerId;
    }

    /**
     * Sets the value of the transformerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransformerId(String value) {
        this.transformerId = value;
    }

    /**
     * Gets the value of the meter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the meter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Meter }
     * 
     * 
     */
    public List<Meter> getMeter() {
        if (meter == null) {
            meter = new ArrayList<Meter>();
        }
        return this.meter;
    }

}
