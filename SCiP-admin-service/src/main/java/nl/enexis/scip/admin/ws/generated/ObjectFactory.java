
package nl.enexis.scip.admin.ws.generated;

/*
 * #%L
 * SCiP-admin-service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.enexis.scip.admin.ws.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddCableRequest_QNAME = new QName("http://www.enexis.nl/SCiP/admin/2013/10/", "AddCableRequest");
    private final static QName _AddCableResponse_QNAME = new QName("http://www.enexis.nl/SCiP/admin/2013/10/", "AddCableResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.enexis.scip.admin.ws.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddCableResponse }
     * 
     */
    public AddCableResponse createAddCableResponse() {
        return new AddCableResponse();
    }

    /**
     * Create an instance of {@link AddCableRequest }
     * 
     */
    public AddCableRequest createAddCableRequest() {
        return new AddCableRequest();
    }

    /**
     * Create an instance of {@link Meter }
     * 
     */
    public Meter createMeter() {
        return new Meter();
    }

    /**
     * Create an instance of {@link Connection }
     * 
     */
    public Connection createConnection() {
        return new Connection();
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link Cable }
     * 
     */
    public Cable createCable() {
        return new Cable();
    }

    /**
     * Create an instance of {@link FixedForecast }
     * 
     */
    public FixedForecast createFixedForecast() {
        return new FixedForecast();
    }

    /**
     * Create an instance of {@link Dso }
     * 
     */
    public Dso createDso() {
        return new Dso();
    }

    /**
     * Create an instance of {@link Csp }
     * 
     */
    public Csp createCsp() {
        return new Csp();
    }

    /**
     * Create an instance of {@link Transformer }
     * 
     */
    public Transformer createTransformer() {
        return new Transformer();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCableRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.enexis.nl/SCiP/admin/2013/10/", name = "AddCableRequest")
    public JAXBElement<AddCableRequest> createAddCableRequest(AddCableRequest value) {
        return new JAXBElement<AddCableRequest>(_AddCableRequest_QNAME, AddCableRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddCableResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.enexis.nl/SCiP/admin/2013/10/", name = "AddCableResponse")
    public JAXBElement<AddCableResponse> createAddCableResponse(AddCableResponse value) {
        return new JAXBElement<AddCableResponse>(_AddCableResponse_QNAME, AddCableResponse.class, null, value);
    }

}
