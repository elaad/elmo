
package nl.enexis.scip.admin.ws.generated;

/*
 * #%L
 * SCiP-admin-service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Cable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Active" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Active"/>
 *         &lt;element name="CableId" type="{http://www.enexis.nl/SCiP/admin/2013/10/}CableId"/>
 *         &lt;element name="MaxCapacity" type="{http://www.enexis.nl/SCiP/admin/2013/10/}DecimalValue"/>
 *         &lt;element name="MaxCapacityForCars" type="{http://www.enexis.nl/SCiP/admin/2013/10/}DecimalValue"/>
 *         &lt;element name="SafetyFactor" type="{http://www.enexis.nl/SCiP/admin/2013/10/}DecimalValue"/>
 *         &lt;element name="RemainingFactor" type="{http://www.enexis.nl/SCiP/admin/2013/10/}DecimalValue"/>
 *         &lt;element name="UsageMeasurementResponsible" type="{http://www.enexis.nl/SCiP/admin/2013/10/}UsageMeasurementResponsible"/>
 *         &lt;element name="Dso" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Dso"/>
 *         &lt;element name="Transformer" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Transformer"/>
 *         &lt;element name="Csp" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Csp" maxOccurs="unbounded"/>
 *         &lt;element name="FixedForecast" type="{http://www.enexis.nl/SCiP/admin/2013/10/}FixedForecast"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cable", propOrder = {
    "active",
    "cableId",
    "maxCapacity",
    "maxCapacityForCars",
    "safetyFactor",
    "remainingFactor",
    "usageMeasurementResponsible",
    "dso",
    "transformer",
    "csp",
    "fixedForecast"
})
public class Cable {

    @XmlElement(name = "Active")
    protected boolean active;
    @XmlElement(name = "CableId", required = true)
    protected String cableId;
    @XmlElement(name = "MaxCapacity", required = true)
    protected BigDecimal maxCapacity;
    @XmlElement(name = "MaxCapacityForCars", required = true)
    protected BigDecimal maxCapacityForCars;
    @XmlElement(name = "SafetyFactor", required = true)
    protected BigDecimal safetyFactor;
    @XmlElement(name = "RemainingFactor", required = true)
    protected BigDecimal remainingFactor;
    @XmlElement(name = "UsageMeasurementResponsible", required = true)
    protected UsageMeasurementResponsible usageMeasurementResponsible;
    @XmlElement(name = "Dso", required = true)
    protected Dso dso;
    @XmlElement(name = "Transformer", required = true)
    protected Transformer transformer;
    @XmlElement(name = "Csp", required = true)
    protected List<Csp> csp;
    @XmlElement(name = "FixedForecast", required = true)
    protected FixedForecast fixedForecast;

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the cableId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCableId() {
        return cableId;
    }

    /**
     * Sets the value of the cableId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCableId(String value) {
        this.cableId = value;
    }

    /**
     * Gets the value of the maxCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxCapacity() {
        return maxCapacity;
    }

    /**
     * Sets the value of the maxCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxCapacity(BigDecimal value) {
        this.maxCapacity = value;
    }

    /**
     * Gets the value of the maxCapacityForCars property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxCapacityForCars() {
        return maxCapacityForCars;
    }

    /**
     * Sets the value of the maxCapacityForCars property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxCapacityForCars(BigDecimal value) {
        this.maxCapacityForCars = value;
    }

    /**
     * Gets the value of the safetyFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSafetyFactor() {
        return safetyFactor;
    }

    /**
     * Sets the value of the safetyFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSafetyFactor(BigDecimal value) {
        this.safetyFactor = value;
    }

    /**
     * Gets the value of the remainingFactor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRemainingFactor() {
        return remainingFactor;
    }

    /**
     * Sets the value of the remainingFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRemainingFactor(BigDecimal value) {
        this.remainingFactor = value;
    }

    /**
     * Gets the value of the usageMeasurementResponsible property.
     * 
     * @return
     *     possible object is
     *     {@link UsageMeasurementResponsible }
     *     
     */
    public UsageMeasurementResponsible getUsageMeasurementResponsible() {
        return usageMeasurementResponsible;
    }

    /**
     * Sets the value of the usageMeasurementResponsible property.
     * 
     * @param value
     *     allowed object is
     *     {@link UsageMeasurementResponsible }
     *     
     */
    public void setUsageMeasurementResponsible(UsageMeasurementResponsible value) {
        this.usageMeasurementResponsible = value;
    }

    /**
     * Gets the value of the dso property.
     * 
     * @return
     *     possible object is
     *     {@link Dso }
     *     
     */
    public Dso getDso() {
        return dso;
    }

    /**
     * Sets the value of the dso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dso }
     *     
     */
    public void setDso(Dso value) {
        this.dso = value;
    }

    /**
     * Gets the value of the transformer property.
     * 
     * @return
     *     possible object is
     *     {@link Transformer }
     *     
     */
    public Transformer getTransformer() {
        return transformer;
    }

    /**
     * Sets the value of the transformer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transformer }
     *     
     */
    public void setTransformer(Transformer value) {
        this.transformer = value;
    }

    /**
     * Gets the value of the csp property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the csp property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCsp().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Csp }
     * 
     * 
     */
    public List<Csp> getCsp() {
        if (csp == null) {
            csp = new ArrayList<Csp>();
        }
        return this.csp;
    }

    /**
     * Gets the value of the fixedForecast property.
     * 
     * @return
     *     possible object is
     *     {@link FixedForecast }
     *     
     */
    public FixedForecast getFixedForecast() {
        return fixedForecast;
    }

    /**
     * Sets the value of the fixedForecast property.
     * 
     * @param value
     *     allowed object is
     *     {@link FixedForecast }
     *     
     */
    public void setFixedForecast(FixedForecast value) {
        this.fixedForecast = value;
    }

}
