
package nl.enexis.scip.admin.ws.generated;

/*
 * #%L
 * SCiP-admin-service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Csp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Csp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Active" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Active"/>
 *         &lt;element name="ActiveForCable" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Active"/>
 *         &lt;element name="ActiveForDso" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Active"/>
 *         &lt;element name="NumberOfForecastBlocksForCable" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Ean" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Ean13"/>
 *         &lt;element name="Name" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Name"/>
 *         &lt;element name="Endpoint" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Endpoint"/>
 *         &lt;element name="Connection" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Connection" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Csp", propOrder = {
    "active",
    "activeForCable",
    "activeForDso",
    "numberOfForecastBlocksForCable",
    "ean",
    "name",
    "endpoint",
    "connection"
})
public class Csp {

    @XmlElement(name = "Active")
    protected boolean active;
    @XmlElement(name = "ActiveForCable")
    protected boolean activeForCable;
    @XmlElement(name = "ActiveForDso")
    protected boolean activeForDso;
    @XmlElement(name = "NumberOfForecastBlocksForCable")
    protected int numberOfForecastBlocksForCable;
    @XmlElement(name = "Ean", required = true)
    protected String ean;
    @XmlElement(name = "Name", required = true)
    protected String name;
    @XmlElement(name = "Endpoint", required = true)
    protected String endpoint;
    @XmlElement(name = "Connection", required = true)
    protected List<Connection> connection;

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the activeForCable property.
     * 
     */
    public boolean isActiveForCable() {
        return activeForCable;
    }

    /**
     * Sets the value of the activeForCable property.
     * 
     */
    public void setActiveForCable(boolean value) {
        this.activeForCable = value;
    }

    /**
     * Gets the value of the activeForDso property.
     * 
     */
    public boolean isActiveForDso() {
        return activeForDso;
    }

    /**
     * Sets the value of the activeForDso property.
     * 
     */
    public void setActiveForDso(boolean value) {
        this.activeForDso = value;
    }

    /**
     * Gets the value of the numberOfForecastBlocksForCable property.
     * 
     */
    public int getNumberOfForecastBlocksForCable() {
        return numberOfForecastBlocksForCable;
    }

    /**
     * Sets the value of the numberOfForecastBlocksForCable property.
     * 
     */
    public void setNumberOfForecastBlocksForCable(int value) {
        this.numberOfForecastBlocksForCable = value;
    }

    /**
     * Gets the value of the ean property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan() {
        return ean;
    }

    /**
     * Sets the value of the ean property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan(String value) {
        this.ean = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the endpoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * Sets the value of the endpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndpoint(String value) {
        this.endpoint = value;
    }

    /**
     * Gets the value of the connection property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the connection property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConnection().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Connection }
     * 
     * 
     */
    public List<Connection> getConnection() {
        if (connection == null) {
            connection = new ArrayList<Connection>();
        }
        return this.connection;
    }

}
