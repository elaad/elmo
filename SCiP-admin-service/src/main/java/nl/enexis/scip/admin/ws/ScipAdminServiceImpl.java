package nl.enexis.scip.admin.ws;

/*
 * #%L
 * SCiP-admin-service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.jws.WebService;
import javax.xml.ws.BindingType;

import nl.enexis.scip.admin.ws.generated.AddCableRequest;
import nl.enexis.scip.admin.ws.generated.AddCableResponse;
import nl.enexis.scip.admin.ws.generated.ScipAdminService;

@WebService(serviceName = "ScipAdminService", endpointInterface = "nl.enexis.scip.admin.ws.generated.ScipAdminService", targetNamespace = "http://www.enexis.nl/SCiP/admin/2013/10/")
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
public class ScipAdminServiceImpl implements ScipAdminService {

//	@Inject
//	@Named("topologyService")
//	TopologyService topologyService;

	@Override
	public AddCableResponse addCable(AddCableRequest request) {
		AddCableResponse response = new AddCableResponse();
		return response;
//		Result result = new Result();
//		response.setResult(result);
//
//		try {
//
//			nl.enexis.scip.admin.ws.generated.Cable reqCable = request.getCable();
//			Dso dso = new Dso();
//			dso.setEan13(reqCable.getDso().getEan());
//			dso.setName(reqCable.getDso().getName());
//			dso.setActive(reqCable.getDso().isActive());
//
//			Transformer transformer = new Transformer();
//			transformer.setTrafoId(reqCable.getTransformer().getTransformerId());
//			transformer.setActive(reqCable.getTransformer().isActive());
//
//			Cable cable = new Cable();
//			cable.setCableId(reqCable.getCableId());
//			cable.setMaxCapacity(reqCable.getMaxCapacity());
//			cable.setMaxCarCapacity(reqCable.getMaxCapacityForCars());
//			cable.setSafetyFactor(reqCable.getSafetyFactor());
//			cable.setRemainingFactor(reqCable.getRemainingFactor());
//			cable.setUsageMeasurementResponsible(UsageMeasurementResponsible.valueOf(reqCable
//					.getUsageMeasurementResponsible().name()));
//			cable.setDso(dso);
//			cable.setTransformer(transformer);
//			cable.setActive(reqCable.isActive());
//			//test purposes
//			cable.setFixedForecast(reqCable.getFixedForecast().isActive());
//			cable.setFixedForecastRandom(reqCable.getFixedForecast().isRandom());
//			cable.setFixedForecastMax(reqCable.getFixedForecast().getMaximum());
//			cable.setFixedForecastDeviation(reqCable.getFixedForecast().getDeviation());
//			cable.setFixedForecastDeviationBlocks(reqCable.getFixedForecast().getDeviationBlocks());
//
//			transformer.getCables().add(cable);
//			dso.getCables().add(cable);
//
//			for (nl.enexis.scip.admin.ws.generated.Meter reqMeter : reqCable.getTransformer().getMeter()) {
//				Meter meter = new Meter();
//				meter.setCable(cable);
//				meter.setTransformer(transformer);
//				meter.setMeasures(MeasuresFor.valueOf(reqMeter.getMeasures().name()));
//				meter.setMeterId(reqMeter.getMeterId());
//				meter.setMeterName(reqMeter.getNameOrType());
//				meter.setActive(reqMeter.isActive());
//
//				cable.getMeters().add(meter);
//				transformer.getMeters().add(meter);
//			}
//
//			for (nl.enexis.scip.admin.ws.generated.Csp reqCsp : reqCable.getCsp()) {
//				Csp csp = new Csp();
//				csp.setEan13(reqCsp.getEan());
//				csp.setName(reqCsp.getName());
//				csp.setEndpoint(reqCsp.getEndpoint());
//				csp.setActive(reqCsp.isActive());
//
//				DsoCsp dsoCsp = new DsoCsp();
//				dsoCsp.setDso(dso);
//				dsoCsp.setCsp(csp);
//				dsoCsp.setActive(reqCsp.isActiveForDso());
//				csp.getDsoCsps().add(dsoCsp);
//				dso.getDsoCsps().add(dsoCsp);
//
//				CableCsp cableCsp = new CableCsp();
//				cableCsp.setCable(cable);
//				cableCsp.setCsp(csp);
//				cableCsp.setActive(reqCsp.isActiveForCable());
//				cableCsp.setNumberOfBlocks(reqCsp.getNumberOfForecastBlocksForCable());
//				cable.getCableCsps().add(cableCsp);
//				csp.getCableCsps().add(cableCsp);
//
//				ArrayList<CspConnection> cspConnections = new ArrayList<CspConnection>();
//				for (Connection reqConnection : reqCsp.getConnection()) {
//					CspConnection cspConnection = new CspConnection();
//					cspConnection.setEan18(reqConnection.getEan());
//					cspConnection.setCapacity(reqConnection.getCapacity());
//					cspConnection.setCableCsp(cableCsp);
//					cspConnection.setActive(reqConnection.isActive());
//					cspConnections.add(cspConnection);
//				}
//				cableCsp.setCspConnections(cspConnections);
//			}
//
//			topologyService.processCableTopology(cable);
//
//			result.setType(ResultType.SUCCESS);
//			result.setCode("");
//			result.setDescription("Message processed");
//			result.setMessage("Message processed");
//			return response;
//		} catch (SCiPException e) {
//			result.setType(ResultType.ERROR);
//			result.setCode(e.getError().getCode().name());
//			result.setMessage(e.getError().getMessage());
//			result.setDescription(e.getError().getDetail());
//			return response;
//		}
	}
}