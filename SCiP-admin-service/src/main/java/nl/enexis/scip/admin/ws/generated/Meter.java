
package nl.enexis.scip.admin.ws.generated;

/*
 * #%L
 * SCiP-admin-service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Meter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Meter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Active" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Active"/>
 *         &lt;element name="MeterId" type="{http://www.enexis.nl/SCiP/admin/2013/10/}MeterId"/>
 *         &lt;element name="NameOrType" type="{http://www.enexis.nl/SCiP/admin/2013/10/}MeterNameOrType"/>
 *         &lt;element name="Measures" type="{http://www.enexis.nl/SCiP/admin/2013/10/}Measures"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Meter", propOrder = {
    "active",
    "meterId",
    "nameOrType",
    "measures"
})
public class Meter {

    @XmlElement(name = "Active")
    protected boolean active;
    @XmlElement(name = "MeterId", required = true)
    protected String meterId;
    @XmlElement(name = "NameOrType", required = true)
    protected String nameOrType;
    @XmlElement(name = "Measures", required = true)
    protected Measures measures;

    /**
     * Gets the value of the active property.
     * 
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     */
    public void setActive(boolean value) {
        this.active = value;
    }

    /**
     * Gets the value of the meterId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeterId() {
        return meterId;
    }

    /**
     * Sets the value of the meterId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeterId(String value) {
        this.meterId = value;
    }

    /**
     * Gets the value of the nameOrType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameOrType() {
        return nameOrType;
    }

    /**
     * Sets the value of the nameOrType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameOrType(String value) {
        this.nameOrType = value;
    }

    /**
     * Gets the value of the measures property.
     * 
     * @return
     *     possible object is
     *     {@link Measures }
     *     
     */
    public Measures getMeasures() {
        return measures;
    }

    /**
     * Sets the value of the measures property.
     * 
     * @param value
     *     allowed object is
     *     {@link Measures }
     *     
     */
    public void setMeasures(Measures value) {
        this.measures = value;
    }

}
