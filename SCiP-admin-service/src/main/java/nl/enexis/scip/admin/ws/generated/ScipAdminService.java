package nl.enexis.scip.admin.ws.generated;

/*
 * #%L
 * SCiP-admin-service
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.6.6
 * 2014-03-17T14:01:55.052+01:00
 * Generated source version: 2.6.6
 * 
 */
@WebService(targetNamespace = "http://www.enexis.nl/SCiP/admin/2013/10/", name = "ScipAdminService")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface ScipAdminService {

    @WebResult(name = "AddCableResponse", targetNamespace = "http://www.enexis.nl/SCiP/admin/2013/10/", partName = "parameters")
    @WebMethod(operationName = "AddCable", action = "/AddCable")
    public AddCableResponse addCable(
        @WebParam(partName = "parameters", name = "AddCableRequest", targetNamespace = "http://www.enexis.nl/SCiP/admin/2013/10/")
        AddCableRequest parameters
    );
}
