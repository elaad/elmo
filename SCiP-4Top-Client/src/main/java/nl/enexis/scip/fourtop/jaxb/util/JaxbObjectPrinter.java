package nl.enexis.scip.fourtop.jaxb.util;

/*
 * #%L
 * SCiP-4Top
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import nl.enexis.scip.action.ActionLogger;

public class JaxbObjectPrinter {

	public static String toXmlString(Object object) {
		try {
			StringWriter writer = new StringWriter();
			JAXBContext ctx = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = ctx.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(object, writer);
			return writer.toString();
		} catch (JAXBException e) {
			ActionLogger.warn("Error printing JAXB object " + object.getClass().getName() + " to string!", null, e, false);
		}
		return "";
	}
}
