package nl.enexis.scip.fourtop.client;

/*
 * #%L
 * SCiP-4Top
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.fourtop.jaxb.generated.TrafoDetails;
import nl.enexis.scip.fourtop.jaxb.util.JaxbObjectPrinter;
import nl.enexis.scip.model.MeasurementProvider;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

public class FourTopClient {

	// inner class to add basic authentication header using a request filter
	class BasicAuthenticator implements ClientRequestFilter {

		private final String token;

		public BasicAuthenticator(String token) {
			this.token = token;
		}

		public void filter(ClientRequestContext requestContext) throws IOException {
			MultivaluedMap<String, Object> headers = requestContext.getHeaders();
			headers.add("Authorization", "Basic " + DatatypeConverter.printBase64Binary(token.getBytes("UTF-8")));
		}
	}

	static private boolean initialized = false;

	int connectionTimeout, readTimeout = 60;
	private String baseURL = null;
	private MeasurementProvider measurementProvider = null;

	public FourTopClient(MeasurementProvider measurementProvider, int connectionTimeout, int readTimeout)
			throws ActionException {
		FourTopClient.initialize();

		this.measurementProvider = measurementProvider;
		this.baseURL = measurementProvider.getEndpoint();
		this.connectionTimeout = connectionTimeout;
		this.readTimeout = readTimeout;

		ActionLogger.trace("Created MP client for endpoint baseURL {0}", new Object[] { this.baseURL }, null, false);
	}

	static private void initialize() throws ActionException {
		try {
			if (!initialized) {
				// this initialization only needs to be done once per VM
				RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
				initialized = true;
				ActionLogger.trace("Initialized RESTEasy provider factory for MP calls", null, null, false);
			}
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.UNEXPECTED,
					"Exception initializing MP proxy: " + e.getMessage(), e);
		}
	}

	/**
	 * Calls MP.
	 * 
	 * @param character
	 * @param trafoId
	 * @param startTime
	 * @return
	 * @throws FourTopException
	 */
	public TrafoDetails getCableMeasurements(String character, String trafoId, Date lastSuccessTime)
			throws ActionException {
		try {

			ResteasyClientBuilder resteasyClientBuilder;

			if (measurementProvider.getUsername() != null && measurementProvider.getUsername().length() > 0) {
				resteasyClientBuilder = new ResteasyClientBuilder().register(new BasicAuthenticator(
						measurementProvider.getUsername() + ":" + measurementProvider.getPassword()));
			} else {
				resteasyClientBuilder = new ResteasyClientBuilder();
			}

			ResteasyClient resteasyClient = resteasyClientBuilder
					.establishConnectionTimeout(this.connectionTimeout, TimeUnit.SECONDS)
					.socketTimeout(this.readTimeout, TimeUnit.SECONDS).build();

			ResteasyWebTarget target = resteasyClient.target(this.baseURL);

			FourTopProxy proxy = target.proxy(FourTopProxy.class);

			ActionLogger.trace("Created MP RESTEasy proxy for baseURL {0}", new Object[] { this.baseURL }, null, false);

			String param = "";
			if (character != null && !"".equals(character.trim()) && trafoId != null && !"".equals(trafoId.trim())) {
				param = character + "_" + trafoId;
			} else {
				throw new ActionException(ActionExceptionType.DATA,
						"No character and trafoId supplied for request parameter 'nxs' for call to MP");
			}

			if (lastSuccessTime != null) {
				param = param + "_" + (lastSuccessTime.getTime() / 1000 + 1);
			}

			ActionLogger.debug("Calling MP through endpoint {0}?nxs={1}", new Object[] { this.baseURL, param }, null,
					false);

			Response output;
			try {
				output = proxy.getCableMeasurements(param);
			} catch (Exception e) {
				ActionException ae = new ActionException(ActionExceptionType.CONNECTION,
						"Fetching measurements failed for mp: " + measurementProvider.getName(), e);
				ae.getTargets().add(measurementProvider);
				throw ae;
			}

			TrafoDetails trafoDetails = output.readEntity(TrafoDetails.class);

			ActionLogger.trace("Response from MP call:\n{0}",
					new Object[] { JaxbObjectPrinter.toXmlString(trafoDetails) }, null, false);

			return trafoDetails;
		} catch (Exception e) {
			if (e instanceof ActionException) {
				throw (ActionException) e;
			} else {
				throw new ActionException(ActionExceptionType.UNEXPECTED, "Exception calling MP: " + e.getMessage(), e);
			}
		}
	}
}
