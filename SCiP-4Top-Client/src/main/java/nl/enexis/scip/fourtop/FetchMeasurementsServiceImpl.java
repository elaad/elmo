package nl.enexis.scip.fourtop;

/*
 * #%L
 * SCiP-4Top
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.fourtop.client.FourTopClient;
import nl.enexis.scip.fourtop.jaxb.generated.TrafoDetails;
import nl.enexis.scip.fourtop.jaxb.generated.TrafoDetails.Trafo;
import nl.enexis.scip.fourtop.jaxb.generated.TrafoDetails.Trafo.Meter.Meetwaarde;
import nl.enexis.scip.fourtop.jaxb.generated.TrafoDetails.Trafo.Meter.Meetwaarde.Range;
import nl.enexis.scip.model.Measurement;
import nl.enexis.scip.model.MeasurementProvider;
import nl.enexis.scip.model.MeasurementValue;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.model.Transformer;
import nl.enexis.scip.service.FetchMeasurementsServiceLocal;
import nl.enexis.scip.service.GeneralService;
import nl.enexis.scip.service.MeasurementStorageService;
import nl.enexis.scip.service.TopologyStorageService;
import nl.enexis.scip.util.ForecastBlockUtil;

/**
 * Session Bean implementation class FourTopFetchMeasurementsBean
 */
@Named("fetchMeasurementsService")
@Stateless
public class FetchMeasurementsServiceImpl implements FetchMeasurementsServiceLocal {

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@EJB
	FetchMeasurementsServiceLocal self;

	@Inject
	@Named("measurementStorageService")
	MeasurementStorageService measurementStorageService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	public FetchMeasurementsServiceImpl() {
	}

	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	@ActionContext(event = ActionEvent.FETCH_MEASUREMENTS, doRethrow = true)
	public void fetchTrafoMeasurements(Transformer transformer) throws ActionException {
		self.dofetchTrafoMeasurements(transformer);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void dofetchTrafoMeasurements(Transformer transformer) throws ActionException {
		ActionLogger.info("Start processing {0}", new Object[] { transformer }, null, false);

		Integer maxHoursNoValues = null;
		try {
			if (!transformer.isActive()) {
				ActionLogger.warn("Not active {0}", new Object[] { transformer }, null, true);
				return;
			}

			transformer = topologyStorageService.findTransformerByTrafoId(transformer.getTrafoId());

			MeasurementProvider provider = transformer.getMeasurementProvider();

			if (!provider.isActive()) {
				ActionLogger.warn("Not active {0}", new Object[] { provider }, null, true);
				return;
			}

			int connectionTimeout = generalService.getConfigurationParameterAsInteger("GetMeasurementsConnectionTimeoutSec");
			int readTimeout = generalService.getConfigurationParameterAsInteger("GetMeasurementsReadTimeoutSec");

			FourTopClient client = new FourTopClient(transformer.getMeasurementProvider(), connectionTimeout, readTimeout);

			TrafoDetails trafoDetails = client.getCableMeasurements("S", transformer.getTrafoId(), transformer.getLastMeasurement());

			this.checkMeasurements(transformer, trafoDetails);

			SortedMap<Long, Measurement> measurements = new TreeMap<Long, Measurement>();

			for (Trafo trafo : trafoDetails.getTrafo()) {
				for (nl.enexis.scip.fourtop.jaxb.generated.TrafoDetails.Trafo.Meter meter : trafo.getMeter()) {
					this.prepareMeasurements(transformer, meter, measurements);

				}
			}

			// check if last measurement complete. if not then do not store
			while (!measurements.isEmpty()) {
				Long key = measurements.lastKey();
				Measurement last = measurements.get(key);
				if (!last.isComplete()) {
					ActionLogger.warn("Skip saving incomplete measurement {0} for {1}",
							new Object[] { last.getDate(), transformer.getCable() }, null, true);
					measurements.remove(key);
				} else {
					break;
				}
			}

			if (!measurements.isEmpty()) {
				measurementStorageService.storeMeasurements(measurements);
				topologyStorageService.updateTransformerLastMeasurement(transformer.getTrafoId(), new Date(measurements.lastKey()));
				transformer.setLastMeasurement(new Date(measurements.lastKey()));
			}

			maxHoursNoValues = generalService.getConfigurationParameterAsInteger("GetMeasurementsMaxHoursNoValues");

			if (new Date().getTime() - transformer.getLastMeasurement().getTime() > maxHoursNoValues.intValue() * 3600 * 1000) {
				throw new ActionException(ActionExceptionType.DATA, "No measurement values received for more then " + maxHoursNoValues
						+ " hours! Transformer: " + transformer.getTrafoId() + "; Last received: " + transformer.getLastMeasurement());
			}
		} catch (ActionException ae) {
			throw ae;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.UNEXPECTED, "Could not retrieve data for " + transformer, e);
		} finally {
			ActionLogger.info("End processing {0}", new Object[] { transformer }, null, false);
		}
	}

	private void checkMeasurements(Transformer transformer, TrafoDetails trafoDetails) throws ActionException {
		// check trafo
		Trafo msgTrafo = trafoDetails.getTrafo().get(0);
		if (msgTrafo == null || !msgTrafo.getTrafonummer().getValue().equals(transformer.getTrafoId())) {
			throw new ActionException(ActionExceptionType.MESSAGE, "Received transformer '"
					+ (msgTrafo == null ? "" : msgTrafo.getTrafonummer().getValue()) + "' but expected transformer '"
					+ transformer.getTrafoId() + "'");
		}

		// check if all active meters are sent
		for (Meter meter : transformer.getMeters()) {
			if (meter.isActive()) {
				boolean found = false;
				for (nl.enexis.scip.fourtop.jaxb.generated.TrafoDetails.Trafo.Meter msgMeter : msgTrafo.getMeter()) {
					if (msgMeter.getMeterId().equals(meter.getMeterId())) {
						found = true;
						break;
					}
				}
				if (!found) {
					throw new ActionException(ActionExceptionType.MESSAGE, "Missing values for active meter: " + meter.getMeterId());
				}
			}
		}

		// check if all sent meters are known
		for (nl.enexis.scip.fourtop.jaxb.generated.TrafoDetails.Trafo.Meter msgMeter : msgTrafo.getMeter()) {
			boolean found = false;
			for (Meter meter : transformer.getMeters()) {
				if (msgMeter.getMeterId().equals(meter.getMeterId())) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new ActionException(ActionExceptionType.MESSAGE, "Meter supplied by response fetch measurements not known! Meter: "
						+ msgMeter.getMeterId());
			}
		}

	}

	private void prepareMeasurements(Transformer transformer, TrafoDetails.Trafo.Meter meter, SortedMap<Long, Measurement> measurements)
			throws ActionException {

		Meter dbMeter = topologyStorageService.findMeterByMeterId(meter.getMeterId());

		ActionLogger.debug("Start processing {0} for {1}", new Object[] { dbMeter, transformer }, null, false);

		if (dbMeter == null) {
			throw new ActionException(ActionExceptionType.MESSAGE, "Received meter not known! Meter: " + meter.getMeterId());
		}

		if (!dbMeter.isActive()) {
			ActionLogger.warn("Not active {0}", new Object[] { dbMeter }, null, true);
			return;
		}

		boolean missingChecked = false;

		for (Meetwaarde meetwaarde : meter.getMeetwaarde()) {
			for (Range range : meetwaarde.getRange()) {
				Measurement measurement = measurements.get(range.getDate() * 1000);

				if (measurement == null) {
					measurement = new Measurement();
					measurement.setDate(new Date(range.getDate() * 1000));
					measurement.setCable(dbMeter.getTransformer().getCable());
					List<MeasurementValue> measurementValues = new ArrayList<MeasurementValue>();
					measurement.setMeasurementValues(measurementValues);
					measurements.put(range.getDate() * 1000, measurement);

					if (!missingChecked) {
						this.checkForMissingMeasurements(measurement, measurements);
						missingChecked = true;
					}
				}

				this.getMeasurementValue(transformer, dbMeter, meetwaarde, range, measurement);

			}
		}

		ActionLogger.debug("End processing {0} for {1}", new Object[] { dbMeter, transformer }, null, false);
	}

	protected MeasurementValue getMeasurementValue(Transformer transformer, Meter dbMeter, TrafoDetails.Trafo.Meter.Meetwaarde meetwaarde,
			TrafoDetails.Trafo.Meter.Meetwaarde.Range range, Measurement measurement) {
		MeasurementValue measurementValue = null;
		
		for (MeasurementValue mv : measurement.getMeasurementValues()) {
			if (mv.getMeter().equals(dbMeter)) {
				measurementValue = mv;
				break;
			}
		}

		if (measurementValue == null) {
			measurementValue = new MeasurementValue();
			measurementValue.setMeasurement(measurement);
			measurementValue.setMeter(dbMeter);
			measurement.getMeasurementValues().add(measurementValue);
		}

		if (transformer.getActiveMetersCount() == measurement.getMeasurementValues().size()) {
			measurement.setComplete(true);
		}

		this.getMeasurementValueByName(meetwaarde, range, measurementValue);
		
		return measurementValue;
	}

	protected void getMeasurementValueByName(TrafoDetails.Trafo.Meter.Meetwaarde meetwaarde,
			TrafoDetails.Trafo.Meter.Meetwaarde.Range range, MeasurementValue measurementValue) {
		switch (meetwaarde.getNaam()) {
		case "IGEM1":
			measurementValue.setIGem1(range.getValue());
			break;
		case "IGEM2":
			measurementValue.setIGem2(range.getValue());
			break;
		case "IGEM3":
			measurementValue.setIGem3(range.getValue());
			break;
		case "IMAX1":
			measurementValue.setIMax1(range.getValue());
			break;
		case "IMAX2":
			measurementValue.setIMax2(range.getValue());
			break;
		case "IMAX3":
			measurementValue.setIMax3(range.getValue());
			break;
		case "PMAX1":
			measurementValue.setPMax1(range.getValue());
			break;
		case "PMAX2":
			measurementValue.setPMax2(range.getValue());
			break;
		case "PMAX3":
			measurementValue.setPMax3(range.getValue());
			break;
		case "PMAX0":
			measurementValue.setPMax0(range.getValue());
			break;
		case "SMAX1":
			measurementValue.setSMax1(range.getValue());
			break;
		case "SMAX2":
			measurementValue.setSMax2(range.getValue());
			break;
		case "SMAX3":
			measurementValue.setSMax3(range.getValue());
			break;
		case "SMAX0":
			measurementValue.setSMax0(range.getValue());
			break;
		case "COS1":
			measurementValue.setCos1(range.getValue());
			break;
		case "COS2":
			measurementValue.setCos2(range.getValue());
			break;
		case "COS3":
			measurementValue.setCos3(range.getValue());
			break;
		case "EP1":
			measurementValue.setEp1(range.getValue());
			break;
		case "EP2":
			measurementValue.setEp2(range.getValue());
			break;
		case "EP3":
			measurementValue.setEp3(range.getValue());
			break;
		case "EP0":
			measurementValue.setEp0(range.getValue());
			break;
		case "UGEM1":
			measurementValue.setUGem1(range.getValue());
			break;
		case "UGEM2":
			measurementValue.setUGem2(range.getValue());
			break;
		case "UGEM3":
			measurementValue.setUGem3(range.getValue());
			break;
		default:
			break;
		}
	}

	private void checkForMissingMeasurements(Measurement measurement, SortedMap<Long, Measurement> measurements) throws ActionException {
		Measurement temp = measurement;
		boolean found = false;

		// TODO based on last gotten measurement instead of going back.

		int measurementsPerBlock = generalService.getConfigurationParameterAsInteger("MeasurementsPerBlock");
		int blockMinutes = generalService.getConfigurationParameterAsInteger("ForecastBlockMinutes");
		int measurementMinutes = blockMinutes / measurementsPerBlock;

		do {
			Date date = ForecastBlockUtil.addMinutesToDate(new Date(temp.getDate().getTime()), measurementMinutes * -1);

			if (!measurements.containsKey(date.getTime())) {

				List<Measurement> foundMeasurements = measurementStorageService.getMeasurementForCableBetweenDates(date, date,
						measurement.getCable());

				if (foundMeasurements.isEmpty()) {
					temp = new Measurement();
					temp.setDate(date);
					temp.setCable(measurement.getCable());
					List<MeasurementValue> measurementValues = new ArrayList<MeasurementValue>();
					temp.setMeasurementValues(measurementValues);
					measurements.put(date.getTime(), temp);
				} else {
					found = true;
				}
			} else {
				found = true;
			}
		} while (!found);
	}
}
