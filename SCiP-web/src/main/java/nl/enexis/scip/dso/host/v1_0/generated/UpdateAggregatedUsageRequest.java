
package nl.enexis.scip.dso.host.v1_0.generated;

/*
 * #%L
 * SCiP-web
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines the CSP request towards DSO for an update
 * 						of the CSPs aggregated usage
 * 					
 * 
 * <p>Java class for UpdateAggregatedUsageRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateAggregatedUsageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Usage" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{http://OSCP/DSO/2013/06/}EMSP" minOccurs="0"/>
 *                   &lt;element ref="{http://OSCP/DSO/2013/06/}Cable"/>
 *                   &lt;element ref="{http://OSCP/DSO/2013/06/}UsageBlock" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateAggregatedUsageRequest", propOrder = {
    "usage"
})
public class UpdateAggregatedUsageRequest {

    @XmlElement(name = "Usage", required = true)
    protected List<UpdateAggregatedUsageRequest.Usage> usage;

    /**
     * Gets the value of the usage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateAggregatedUsageRequest.Usage }
     * 
     * 
     */
    public List<UpdateAggregatedUsageRequest.Usage> getUsage() {
        if (usage == null) {
            usage = new ArrayList<UpdateAggregatedUsageRequest.Usage>();
        }
        return this.usage;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{http://OSCP/DSO/2013/06/}EMSP" minOccurs="0"/>
     *         &lt;element ref="{http://OSCP/DSO/2013/06/}Cable"/>
     *         &lt;element ref="{http://OSCP/DSO/2013/06/}UsageBlock" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "emsp",
        "cable",
        "usageBlock"
    })
    public static class Usage {

        @XmlElement(name = "EMSP")
        protected String emsp;
        @XmlElement(name = "Cable", required = true)
        protected Cable cable;
        @XmlElement(name = "UsageBlock", required = true)
        protected List<UsageBlock> usageBlock;

        /**
         * Gets the value of the emsp property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEMSP() {
            return emsp;
        }

        /**
         * Sets the value of the emsp property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEMSP(String value) {
            this.emsp = value;
        }

        /**
         * Gets the value of the cable property.
         * 
         * @return
         *     possible object is
         *     {@link Cable }
         *     
         */
        public Cable getCable() {
            return cable;
        }

        /**
         * Sets the value of the cable property.
         * 
         * @param value
         *     allowed object is
         *     {@link Cable }
         *     
         */
        public void setCable(Cable value) {
            this.cable = value;
        }

        /**
         * Gets the value of the usageBlock property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the usageBlock property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUsageBlock().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link UsageBlock }
         * 
         * 
         */
        public List<UsageBlock> getUsageBlock() {
            if (usageBlock == null) {
                usageBlock = new ArrayList<UsageBlock>();
            }
            return this.usageBlock;
        }

    }

}
