
package nl.enexis.scip.dso.host.v0_9.generated;

/*
 * #%L
 * SCiP-web
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DetailCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DetailCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MESSAGE_PROCESSED"/>
 *     &lt;enumeration value="CSP_UNKNOWN"/>
 *     &lt;enumeration value="DSO_UNKNOWN"/>
 *     &lt;enumeration value="CABLE_UNKNOWN"/>
 *     &lt;enumeration value="DSO_CSP_NOT_RELATED"/>
 *     &lt;enumeration value="DSO_CABLE_NOT_RELATED"/>
 *     &lt;enumeration value="CSP_CABLE_NOT_RELATED"/>
 *     &lt;enumeration value="UNEXPECTED_EXCEPTION"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "DetailCode")
@XmlEnum
public enum DetailCode {

    MESSAGE_PROCESSED,
    CSP_UNKNOWN,
    DSO_UNKNOWN,
    CABLE_UNKNOWN,
    DSO_CSP_NOT_RELATED,
    DSO_CABLE_NOT_RELATED,
    CSP_CABLE_NOT_RELATED,
    UNEXPECTED_EXCEPTION;

    public String value() {
        return this.name();
    }

    public static DetailCode fromValue(String v) {
        return valueOf(v);
    }

}
