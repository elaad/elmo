
package nl.enexis.scip.dso.host.v1_0.generated;

/*
 * #%L
 * SCiP-web
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnitOfMeasure.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UnitOfMeasure">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Wh"/>
 *     &lt;enumeration value="kWh"/>
 *     &lt;enumeration value="varh"/>
 *     &lt;enumeration value="kvarh"/>
 *     &lt;enumeration value="W"/>
 *     &lt;enumeration value="kW"/>
 *     &lt;enumeration value="var"/>
 *     &lt;enumeration value="kvar"/>
 *     &lt;enumeration value="A"/>
 *     &lt;enumeration value="V"/>
 *     &lt;enumeration value="ASU"/>
 *     &lt;enumeration value="dB"/>
 *     &lt;enumeration value="Deg"/>
 *     &lt;enumeration value="F"/>
 *     &lt;enumeration value="g"/>
 *     &lt;enumeration value="Hz"/>
 *     &lt;enumeration value="kPa"/>
 *     &lt;enumeration value="lx"/>
 *     &lt;enumeration value="ms2"/>
 *     &lt;enumeration value="N"/>
 *     &lt;enumeration value="ohm"/>
 *     &lt;enumeration value="pct"/>
 *     &lt;enumeration value="RH"/>
 *     &lt;enumeration value="RPM"/>
 *     &lt;enumeration value="s"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "UnitOfMeasure")
@XmlEnum
public enum UnitOfMeasure {

    @XmlEnumValue("Wh")
    WH("Wh"),
    @XmlEnumValue("kWh")
    K_WH("kWh"),
    @XmlEnumValue("varh")
    VARH("varh"),
    @XmlEnumValue("kvarh")
    KVARH("kvarh"),
    W("W"),
    @XmlEnumValue("kW")
    K_W("kW"),
    @XmlEnumValue("var")
    VAR("var"),
    @XmlEnumValue("kvar")
    KVAR("kvar"),
    A("A"),
    V("V"),
    ASU("ASU"),
    @XmlEnumValue("dB")
    D_B("dB"),
    @XmlEnumValue("Deg")
    DEG("Deg"),
    F("F"),
    @XmlEnumValue("g")
    G("g"),
    @XmlEnumValue("Hz")
    HZ("Hz"),
    @XmlEnumValue("kPa")
    K_PA("kPa"),
    @XmlEnumValue("lx")
    LX("lx"),
    @XmlEnumValue("ms2")
    MS_2("ms2"),
    N("N"),
    @XmlEnumValue("ohm")
    OHM("ohm"),
    @XmlEnumValue("pct")
    PCT("pct"),
    RH("RH"),
    RPM("RPM"),
    @XmlEnumValue("s")
    S("s");
    private final String value;

    UnitOfMeasure(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static UnitOfMeasure fromValue(String v) {
        for (UnitOfMeasure c: UnitOfMeasure.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
