package nl.enexis.scip.dso.host.v1_0;

/*
 * #%L
 * SCiP-web
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.Addressing;

import nl.enexis.scip.Behaviour;
import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.dso.host.v1_0.generated.Adjustment;
import nl.enexis.scip.dso.host.v1_0.generated.CapacityBlock;
import nl.enexis.scip.dso.host.v1_0.generated.DistributionSystemOperatorService;
import nl.enexis.scip.dso.host.v1_0.generated.GetCapacityForecastRequest;
import nl.enexis.scip.dso.host.v1_0.generated.GetCapacityForecastResponse;
import nl.enexis.scip.dso.host.v1_0.generated.LogLevelType;
import nl.enexis.scip.dso.host.v1_0.generated.Parties;
import nl.enexis.scip.dso.host.v1_0.generated.RequestAdjustedCapacityRequest;
import nl.enexis.scip.dso.host.v1_0.generated.RequestAdjustedCapacityResponse;
import nl.enexis.scip.dso.host.v1_0.generated.Result;
import nl.enexis.scip.dso.host.v1_0.generated.Result.Details;
import nl.enexis.scip.dso.host.v1_0.generated.Result.Details.Detail;
import nl.enexis.scip.dso.host.v1_0.generated.ResultType;
import nl.enexis.scip.dso.host.v1_0.generated.UnitOfMeasure;
import nl.enexis.scip.dso.host.v1_0.generated.UpdateAggregatedUsageRequest;
import nl.enexis.scip.dso.host.v1_0.generated.UpdateAggregatedUsageRequest.Usage;
import nl.enexis.scip.dso.host.v1_0.generated.UpdateAggregatedUsageResponse;
import nl.enexis.scip.dso.host.v1_0.generated.UsageBlock;
import nl.enexis.scip.dso.host.v1_0.generated.ValueType;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustment;
import nl.enexis.scip.model.CspAdjustmentMessage;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspGetForecastMessage;
import nl.enexis.scip.model.CspUsage;
import nl.enexis.scip.model.CspUsageMessage;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.service.AdjustmentService;
import nl.enexis.scip.service.CspService;
import nl.enexis.scip.service.UsageService;

import org.apache.cxf.feature.Features;

@WebService(serviceName = "DistributionSystemOperatorServiceV10", endpointInterface = "nl.enexis.scip.dso.host.v1_0.generated.DistributionSystemOperatorService", targetNamespace = "http://OSCP/DSO/2013/06/")
@Addressing
@HandlerChain(file = "/jaxws-handlers-1.0.xml")
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@Features(features = "org.apache.cxf.feature.LoggingFeature")
public class DistributionSystemOperatorServiceImpl implements DistributionSystemOperatorService {

	@Inject
	@Named("usageService")
	UsageService usageService;

	@Inject
	@Behaviour(BehaviourType.SC)
	CspService cspService;

	@Inject
	@Named("adjustmentService")
	AdjustmentService adjustmentService;

	@Override
	@ActionContext(event = ActionEvent.UPDATE_AGGREGATED_USAGE)
	public UpdateAggregatedUsageResponse updateAggregatedUsage(UpdateAggregatedUsageRequest parameters,
			javax.xml.ws.Holder<Parties> parties, javax.xml.ws.Holder<java.lang.Integer> priority,
			javax.xml.ws.Holder<java.util.Date> datetime) {

		Csp csp = new Csp();
		csp.setEan13(parties.value.getSenderID());

		Dso dso = new Dso();
		dso.setEan13(parties.value.getReceiverID());

		CspUsageMessage cspUsageMessage = this.setCspUsage(parameters, priority, datetime, csp, dso);

		try {
			usageService.receiveUsage(cspUsageMessage);
		} catch (ActionException e) {
			ActionLogger.error(e.getMessage(), new Object[] {}, e, true);
		}

		Result result = new Result();
		if (cspUsageMessage.isSuccessful()) {
			result.setType(ResultType.SUCCESS);
			result.setDetails(this.createDetails(true));
		} else {
			result.setType(ResultType.ERROR);
			result.setDetails(this.createDetails(false));
		}

		UpdateAggregatedUsageResponse response = new UpdateAggregatedUsageResponse();
		response.setResult(result);

		return response;

	}

	protected CspUsageMessage setCspUsage(UpdateAggregatedUsageRequest parameters, javax.xml.ws.Holder<java.lang.Integer> priority,
			javax.xml.ws.Holder<java.util.Date> datetime, Csp csp, Dso dso) {

		CspUsageMessage cspUsageMessage = this.getCspUsageMessage(priority, datetime, csp, dso);

		List<CspUsage> cspUsages = cspUsageMessage.getCspUsages();

		List<Usage> usageList = parameters.getUsage();
		for (Usage usage : usageList) {
			CspUsage cspUsage = new CspUsage();

			Cable cable = new Cable();
			cable.setCableId(usage.getCable().getCableId());

			CableCsp cableCsp = new CableCsp();
			cableCsp.setCable(cable);
			cableCsp.setCsp(csp);
			cspUsage.setCableCsp(cableCsp);

			List<CspUsageValue> cspUsageValues = this.setCspUsageValues(usage, cspUsage, cableCsp);

			cspUsage.setCspUsageValues(cspUsageValues);

			cspUsages.add(cspUsage);
		}
		return cspUsageMessage;
	}

	private CspUsageMessage getCspUsageMessage(javax.xml.ws.Holder<java.lang.Integer> priority,
			javax.xml.ws.Holder<java.util.Date> datetime, Csp csp, Dso dso) {
		CspUsageMessage cspUsageMessage = new CspUsageMessage();
		cspUsageMessage.setCsp(csp.getEan13());
		cspUsageMessage.setDso(dso.getEan13());
		cspUsageMessage.setPriority(priority.value.intValue());
		cspUsageMessage.setDateTime(datetime.value);
		cspUsageMessage.setEventId(nl.enexis.scip.action.ActionContext.getAction().getId());
		return cspUsageMessage;
	}

	protected List<CspUsageValue> setCspUsageValues(Usage usage, CspUsage cspUsage, CableCsp cableCsp) {
		List<CspUsageValue> cspUsageValues = new ArrayList<CspUsageValue>(0);

		List<UsageBlock> usageBlocks = usage.getUsageBlock();
		for (UsageBlock usageBlock : usageBlocks) {
			CspUsageValue cspUsageValue = new CspUsageValue();
			cspUsageValue.setCableCsp(cableCsp);
			cspUsageValue.setCspUsage(cspUsage);
			cspUsageValue.setStartedAt(usageBlock.getStartTime());
			cspUsageValue.setEndedAt(usageBlock.getEndTime());
			List<ValueType> usageValues = usageBlock.getUsage();
			BigDecimal energyConsumption = null;
			BigDecimal averageCurrent = null;
			for (ValueType usageValue : usageValues) {
				if (usageValue.getPhase() == null || usageValue.getPhase().intValue() == 1) {
					if (usageValue.getUnit().equals(UnitOfMeasure.WH)) {
						energyConsumption = usageValue.getValue();
					} else if (usageValue.getUnit().equals(UnitOfMeasure.A)) {
						averageCurrent = usageValue.getValue();
					}
				}
			}
			cspUsageValue.setAverageCurrent(averageCurrent != null ? averageCurrent : new BigDecimal("0.00"));
			cspUsageValue.setEnergyConsumption(energyConsumption != null ? energyConsumption : new BigDecimal("0.00"));
			cspUsageValues.add(cspUsageValue);
		}
		return cspUsageValues;
	}

	@Override
	@ActionContext(event = ActionEvent.GET_CAPACITY_FORECAST)
	public GetCapacityForecastResponse getCapacityForecast(GetCapacityForecastRequest parameters, javax.xml.ws.Holder<Parties> parties,
			javax.xml.ws.Holder<java.lang.Integer> priority, javax.xml.ws.Holder<java.util.Date> datetime) {

		Csp csp = new Csp();
		csp.setEan13(parties.value.getSenderID());

		Dso dso = new Dso();
		dso.setEan13(parties.value.getReceiverID());

		CspGetForecastMessage cspGetForecastMessage = this.getCspGetForcastMessage(priority, datetime, csp, dso);

		Set<String> forCables = this.getForCables(parameters);

		try {
			cspService.sendForecastRequestedByCsp(cspGetForecastMessage, forCables);
		} catch (ActionException e) {
			ActionLogger.error(e.getMessage(), new Object[] {}, e, true);
		}

		Result result = new Result();
		if (cspGetForecastMessage.isSuccessful()) {
			result.setType(ResultType.SUCCESS);
			result.setDetails(this.createDetails(true));
		} else {
			result.setType(ResultType.ERROR);
			result.setDetails(this.createDetails(false));
		}

		GetCapacityForecastResponse response = new GetCapacityForecastResponse();
		response.setResult(result);

		return response;

	}

	protected Set<String> getForCables(GetCapacityForecastRequest parameters) {
		Set<String> forCables = null;
		if (!parameters.getCable().isEmpty()) {
			forCables = new HashSet<String>();
			for (nl.enexis.scip.dso.host.v1_0.generated.Cable cable : parameters.getCable()) {
				forCables.add(cable.getCableId());
			}
		}
		return forCables;
	}

	protected CspGetForecastMessage getCspGetForcastMessage(javax.xml.ws.Holder<java.lang.Integer> priority,
			javax.xml.ws.Holder<java.util.Date> datetime, Csp csp, Dso dso) {
		CspGetForecastMessage cspGetForecastMessage = new CspGetForecastMessage();
		cspGetForecastMessage.setCsp(csp.getEan13());
		cspGetForecastMessage.setDso(dso.getEan13());
		cspGetForecastMessage.setPriority(priority.value.intValue());
		cspGetForecastMessage.setDateTime(datetime.value);
		cspGetForecastMessage.setEventId(nl.enexis.scip.action.ActionContext.getAction().getId());
		return cspGetForecastMessage;
	}

	@Override
	@ActionContext(event = ActionEvent.REQUEST_ADJUSTED_CAPACITY)
	public RequestAdjustedCapacityResponse requestAdjustedCapacity(RequestAdjustedCapacityRequest parameters,
			javax.xml.ws.Holder<Parties> parties, javax.xml.ws.Holder<java.lang.Integer> priority,
			javax.xml.ws.Holder<java.util.Date> datetime) {

		Csp csp = new Csp();
		csp.setEan13(parties.value.getSenderID());

		Dso dso = new Dso();
		dso.setEan13(parties.value.getReceiverID());

		CspAdjustmentMessage cspAdjustmentMessage = this.getCspAdjustmentMessage(priority, datetime, csp, dso);

		this.setCspAdjustmentMessage(parameters, csp, cspAdjustmentMessage);
		
		try {
			adjustmentService.receiveAdjustment(cspAdjustmentMessage);
			if (cspAdjustmentMessage.isSuccessful()) {
				adjustmentService.setAdjustmentSuccessful(cspAdjustmentMessage);
			}
		} catch (ActionException e) {
			ActionLogger.error(e.getMessage(), new Object[] {}, e, true);
		}
		
		RequestAdjustedCapacityResponse response = new RequestAdjustedCapacityResponse();
		Result result = new Result();
		
		this.processCspAdjustmentMessage(response, result, cspAdjustmentMessage);

		response.setResult(result);

		return response;

	}

	protected void setCspAdjustmentMessage(RequestAdjustedCapacityRequest parameters, Csp csp, CspAdjustmentMessage cspAdjustmentMessage) {
		List<CspAdjustment> cspAdjustments = cspAdjustmentMessage.getCspAdjustments();
		
		List<Adjustment> adjustmentList = parameters.getAdjustment();
		for (Adjustment adjustment : adjustmentList) {
			CspAdjustment cspAdjustment = new CspAdjustment();
		
			Cable cable = new Cable();
			cable.setCableId(adjustment.getCable().getCableId());
		
			CableCsp cableCsp = new CableCsp();
			cableCsp.setCable(cable);
			cableCsp.setCsp(csp);
			cspAdjustment.setCableCsp(cableCsp);
		
			List<CspAdjustmentValue> cspAdjustmentValues = this.setupCspAdjustmentValues(adjustment, cspAdjustment, cableCsp);
		
			cspAdjustment.setCspAdjustmentValues(cspAdjustmentValues);
		
			cspAdjustments.add(cspAdjustment);
		}
	}

	protected CspAdjustmentMessage getCspAdjustmentMessage(javax.xml.ws.Holder<java.lang.Integer> priority,
			javax.xml.ws.Holder<java.util.Date> datetime, Csp csp, Dso dso) {
		CspAdjustmentMessage cspAdjustmentMessage = new CspAdjustmentMessage();
		cspAdjustmentMessage.setCsp(csp.getEan13());
		cspAdjustmentMessage.setDso(dso.getEan13());
		cspAdjustmentMessage.setPriority(priority.value.intValue());
		cspAdjustmentMessage.setDateTime(datetime.value);
		cspAdjustmentMessage.setEventId(nl.enexis.scip.action.ActionContext.getAction().getId());
		return cspAdjustmentMessage;
	}

	protected void processCspAdjustmentMessage(RequestAdjustedCapacityResponse response, Result result,
			CspAdjustmentMessage cspAdjustmentMessage) {
		if (cspAdjustmentMessage.isSuccessful()) {
			result.setType(ResultType.SUCCESS);
			result.setDetails(this.createDetails(true));

			List<Adjustment> respAdjustments = response.getAdjustment();

			for (CspAdjustment cspAdjustment : cspAdjustmentMessage.getCspAdjustments()) {
				Adjustment respAdjustment = new Adjustment();
				nl.enexis.scip.dso.host.v1_0.generated.Cable respCable = new nl.enexis.scip.dso.host.v1_0.generated.Cable();
				respCable.setCableId(cspAdjustment.getCableCsp().getCable().getCableId());
				respAdjustment.setCable(respCable);

				List<CapacityBlock> respBlocks = respAdjustment.getCapacityBlock();

				for (CspAdjustmentValue cspAdjustmentValue : cspAdjustment.getCspAdjustmentValues()) {
					CapacityBlock respBlock = new CapacityBlock();
					respBlock.setStartTime(cspAdjustmentValue.getStartedAt());
					respBlock.setEndTime(cspAdjustmentValue.getEndedAt());
					ValueType value = new ValueType();
					value.setValue(cspAdjustmentValue.getAssigned());
					value.setUnit(UnitOfMeasure.A);
					respBlock.getCapacity().add(value);

					respBlocks.add(respBlock);
				}

				respAdjustments.add(respAdjustment);
			}
		} else {
			result.setType(ResultType.ERROR);
			result.setDetails(this.createDetails(false));
		}
	}

	protected List<CspAdjustmentValue> setupCspAdjustmentValues(Adjustment adjustment, CspAdjustment cspAdjustment, CableCsp cableCsp) {
		List<CspAdjustmentValue> cspAdjustmentValues = new ArrayList<CspAdjustmentValue>(0);

		List<CapacityBlock> adjustmentBlocks = adjustment.getCapacityBlock();
		for (CapacityBlock adjustmentBlock : adjustmentBlocks) {
			CspAdjustmentValue cspAdjustmentValue = new CspAdjustmentValue();
			cspAdjustmentValue.setCableCsp(cableCsp);
			cspAdjustmentValue.setCspAdjustment(cspAdjustment);
			cspAdjustmentValue.setStartedAt(adjustmentBlock.getStartTime());
			cspAdjustmentValue.setEndedAt(adjustmentBlock.getEndTime());
			for (ValueType adjustmentValue : adjustmentBlock.getCapacity()) {
				if ((adjustmentValue.getPhase() == null || adjustmentValue.getPhase().intValue() == 1)
						&& adjustmentValue.getUnit().equals(UnitOfMeasure.A)) {
					cspAdjustmentValue.setRequested(adjustmentValue.getValue());
					cspAdjustmentValue.setUnitOfMeasure(adjustmentValue.getUnit().toString());
				}
			}
			cspAdjustmentValues.add(cspAdjustmentValue);
		}
		return cspAdjustmentValues;
	}

	protected Details createDetails(boolean isSuccess) {
		Action action = nl.enexis.scip.action.ActionContext.getAction();

		Details details = new Details();

		if (isSuccess) {
			Detail detail = new Detail();
			detail.setCode("MESSAGE_PROCESSED");
			detail.setLevel(LogLevelType.INFO);
			detail.setMessage("Service call successful processed by DSO.");
			details.getDetail().add(detail);
		} else {
			Detail detail = new Detail();
			detail.setCode("EXCEPTION");
			detail.setLevel(LogLevelType.ERROR);
			detail.setMessage("Service call NOT successful processed by DSO.");
			details.getDetail().add(detail);
		}

		List<ActionMessage> msgs = action.getMessages();
		for (ActionMessage msg : msgs) {
			if (msg.getLevel().equals(Level.ERROR) || msg.getLevel().equals(Level.WARN)) {
				Detail detail = new Detail();
				detail.setCode(msg.getExceptionType().toString());
				if (msg.getLevel().equals(Level.ERROR)) {
					detail.setLevel(LogLevelType.ERROR);
				} else {
					detail.setLevel(LogLevelType.WARN);
				}
				detail.setMessage(msg.getMessage());
				detail.setDescription(msg.getRootCauseMessage());
				details.getDetail().add(detail);
			}
		}

		return details;
	}

}