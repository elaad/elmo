package nl.enexis.scip.dso.host.v0_9.handler;

/*
 * #%L
 * SCiP-web
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Iterator;

import javax.xml.soap.Node;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import nl.enexis.scip.action.ActionContext;

import org.jboss.ws.api.handler.GenericSOAPHandler;
import org.w3c.dom.NodeList;

public class WsActionContextHandler extends GenericSOAPHandler<SOAPMessageContext> {

	@Override
	public boolean handleInbound(SOAPMessageContext msgContext) {
		ActionContext.removeAction();

		try {
			SOAPMessage soapMsg = msgContext.getMessage();
			SOAPEnvelope soapEnv = soapMsg.getSOAPPart().getEnvelope();
			SOAPHeader soapHeader = soapEnv.getHeader();

			// if no header, return
			if (soapHeader == null) {
				return true;
			}

			@SuppressWarnings("rawtypes")
			Iterator it = soapHeader.extractAllHeaderElements();
			while (it.hasNext()) {
				Node header = (Node) it.next();
				if ("Identification".equals(header.getLocalName())) {
					NodeList nodes = header.getChildNodes();
					for (int i = 0; i < nodes.getLength(); i++) {
						if ("CSP".equals(nodes.item(i).getLocalName())) {
							ActionContext.setActor(nodes.item(i).getTextContent());
						} else if ("EventId".equals(nodes.item(i).getLocalName())) {
							ActionContext.setId(nodes.item(i).getTextContent());
						}
					}
				}
			}
		} catch (SOAPException e) {
		}

		return true;
	}

	@Override
	public boolean handleOutbound(SOAPMessageContext msgContext) {
		ActionContext.removeAction();
		return true;
	}
}