package nl.enexis.scip.dso.host.v0_9;

/*
 * #%L
 * SCiP-web
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebParam.Mode;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.Holder;

import nl.enexis.scip.Behaviour;
import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.dso.host.v0_9.generated.Adjustment;
import nl.enexis.scip.dso.host.v0_9.generated.CapacityBlock;
import nl.enexis.scip.dso.host.v0_9.generated.DetailCode;
import nl.enexis.scip.dso.host.v0_9.generated.DetailLevel;
import nl.enexis.scip.dso.host.v0_9.generated.DistributionSystemOperatorService;
import nl.enexis.scip.dso.host.v0_9.generated.GetCapacityForecastRequest;
import nl.enexis.scip.dso.host.v0_9.generated.GetCapacityForecastResponse;
import nl.enexis.scip.dso.host.v0_9.generated.Identification;
import nl.enexis.scip.dso.host.v0_9.generated.RequestAdjustedCapacityRequest;
import nl.enexis.scip.dso.host.v0_9.generated.RequestAdjustedCapacityResponse;
import nl.enexis.scip.dso.host.v0_9.generated.Result;
import nl.enexis.scip.dso.host.v0_9.generated.Result.Details;
import nl.enexis.scip.dso.host.v0_9.generated.Result.Details.Detail;
import nl.enexis.scip.dso.host.v0_9.generated.ResultType;
import nl.enexis.scip.dso.host.v0_9.generated.UnitOfMeasure;
import nl.enexis.scip.dso.host.v0_9.generated.UpdateAggregatedUsageRequest;
import nl.enexis.scip.dso.host.v0_9.generated.UpdateAggregatedUsageRequest.Usage;
import nl.enexis.scip.dso.host.v0_9.generated.UpdateAggregatedUsageResponse;
import nl.enexis.scip.dso.host.v0_9.generated.UsageBlock;
import nl.enexis.scip.dso.host.v0_9.generated.ValueType;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustment;
import nl.enexis.scip.model.CspAdjustmentMessage;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspGetForecastMessage;
import nl.enexis.scip.model.CspUsage;
import nl.enexis.scip.model.CspUsageMessage;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.service.AdjustmentService;
import nl.enexis.scip.service.CspService;
import nl.enexis.scip.service.UsageService;

import org.apache.cxf.feature.Features;

@Dependent
@WebService(serviceName = "DistributionSystemOperatorService", endpointInterface = "nl.enexis.scip.dso.host.v0_9.generated.DistributionSystemOperatorService", targetNamespace = "http://OSCP/DSO/2013/06/")
@HandlerChain(file = "/jaxws-handlers-0.9.xml")
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@Features(features = "org.apache.cxf.feature.LoggingFeature")
public class DistributionSystemOperatorServiceImpl implements DistributionSystemOperatorService {

	@Inject
	@Named("usageService")
	UsageService usageService;

	@Inject
	@Behaviour(BehaviourType.SC)
	CspService cspService;

	@Inject
	@Named("adjustmentService")
	AdjustmentService adjustmentService;

	@Override
	@WebResult(name = "UpdateAggregatedUsageResponse", targetNamespace = "http://OSCP/DSO/2013/06/", partName = "parameters")
	@WebMethod(operationName = "UpdateAggregatedUsage", action = "/UpdateAggregatedUsage")
	@ActionContext(event = ActionEvent.UPDATE_AGGREGATED_USAGE)
	public UpdateAggregatedUsageResponse updateAggregatedUsage(
			@WebParam(partName = "parameters", name = "UpdateAggregatedUsageRequest", targetNamespace = "http://OSCP/DSO/2013/06/") UpdateAggregatedUsageRequest parameters,
			@WebParam(partName = "identification", mode = Mode.INOUT, name = "Identification", targetNamespace = "http://OSCP/DSO/2013/06/", header = true) Holder<Identification> identification,
			@WebParam(partName = "priority", mode = Mode.INOUT, name = "Priority", targetNamespace = "http://OSCP/DSO/2013/06/", header = true) Holder<Integer> priority) {

		UpdateAggregatedUsageResponse response = new UpdateAggregatedUsageResponse();
		Result result = new Result();

		Csp csp = new Csp();
		csp.setEan13(identification.value.getCSP());

		Dso dso = new Dso();
		dso.setEan13(identification.value.getDSO());

		CspUsageMessage cspUsageMessage = new CspUsageMessage();
		cspUsageMessage.setCsp(csp.getEan13());
		cspUsageMessage.setDso(dso.getEan13());
		cspUsageMessage.setPriority(priority.value.intValue());
		cspUsageMessage.setDateTime(identification.value.getDateTime());
		cspUsageMessage.setEventId(identification.value.getEventId());

		List<CspUsage> cspUsages = cspUsageMessage.getCspUsages();

		List<Usage> usageList = parameters.getUsage();
		for (Usage usage : usageList) {
			CspUsage cspUsage = new CspUsage();

			Cable cable = new Cable();
			cable.setCableId(usage.getCable().getCableId());

			CableCsp cableCsp = new CableCsp();
			cableCsp.setCable(cable);
			cableCsp.setCsp(csp);
			cspUsage.setCableCsp(cableCsp);

			List<CspUsageValue> cspUsageValues = new ArrayList<CspUsageValue>(0);

			List<UsageBlock> usageBlocks = usage.getUsageBlock();
			for (UsageBlock usageBlock : usageBlocks) {
				CspUsageValue cspUsageValue = new CspUsageValue();
				cspUsageValue.setCableCsp(cableCsp);
				cspUsageValue.setCspUsage(cspUsage);
				cspUsageValue.setStartedAt(usageBlock.getStartTime());
				cspUsageValue.setEndedAt(usageBlock.getEndTime());
				List<ValueType> usageValues = usageBlock.getUsage();
				BigDecimal energyConsumption = null;
				BigDecimal averageCurrent = null;
				for (ValueType usageValue : usageValues) {
					if (usageValue.getUnitOfMeasure().equals(UnitOfMeasure.WH)) {
						energyConsumption = usageValue.getValue();
					} else if (usageValue.getUnitOfMeasure().equals(UnitOfMeasure.AMP)) {
						averageCurrent = usageValue.getValue();
					}
				}
				cspUsageValue.setAverageCurrent(averageCurrent != null ? averageCurrent : new BigDecimal("0.00"));
				cspUsageValue.setEnergyConsumption(energyConsumption != null ? energyConsumption : new BigDecimal(
						"0.00"));
				cspUsageValues.add(cspUsageValue);
			}

			cspUsage.setCspUsageValues(cspUsageValues);

			cspUsages.add(cspUsage);
		}

		try {
			usageService.receiveUsage(cspUsageMessage);
		} catch (ActionException e) {
			ActionLogger.error(e.getMessage(), new Object[] {}, e, true);
		}

		if (cspUsageMessage.isSuccessful()) {
			result.setType(ResultType.SUCCESS);
			result.setDetails(this.createDetails(true));
		} else {
			result.setType(ResultType.ERROR);
			result.setDetails(this.createDetails(false));
		}

		response.setResult(result);

		return response;
	}

	@Override
	@WebResult(name = "GetCapacityForecastResponse", targetNamespace = "http://OSCP/DSO/2013/06/", partName = "parameters")
	@WebMethod(operationName = "GetCapacityForecast", action = "/GetCapacityForecast")
	@ActionContext(event = ActionEvent.GET_CAPACITY_FORECAST)
	public GetCapacityForecastResponse getCapacityForecast(
			@WebParam(partName = "parameters", name = "GetCapacityForecastRequest", targetNamespace = "http://OSCP/DSO/2013/06/") GetCapacityForecastRequest parameters,
			@WebParam(partName = "identification", mode = Mode.INOUT, name = "Identification", targetNamespace = "http://OSCP/DSO/2013/06/", header = true) Holder<Identification> identification,
			@WebParam(partName = "priority", mode = Mode.INOUT, name = "Priority", targetNamespace = "http://OSCP/DSO/2013/06/", header = true) Holder<Integer> priority) {

		GetCapacityForecastResponse response = new GetCapacityForecastResponse();
		Result result = new Result();

		Csp csp = new Csp();
		csp.setEan13(identification.value.getCSP());

		Dso dso = new Dso();
		dso.setEan13(identification.value.getDSO());

		CspGetForecastMessage cspGetForecastMessage = new CspGetForecastMessage();
		cspGetForecastMessage.setCsp(csp.getEan13());
		cspGetForecastMessage.setDso(dso.getEan13());
		cspGetForecastMessage.setPriority(priority.value.intValue());
		cspGetForecastMessage.setDateTime(identification.value.getDateTime());
		cspGetForecastMessage.setEventId(identification.value.getEventId());

		Set<String> forCables = null;

		try {
			cspService.sendForecastRequestedByCsp(cspGetForecastMessage, forCables);
		} catch (ActionException e) {
			ActionLogger.error(e.getMessage(), new Object[] {}, e, true);
		}

		if (cspGetForecastMessage.isSuccessful()) {
			result.setType(ResultType.SUCCESS);
			result.setDetails(this.createDetails(true));
		} else {
			result.setType(ResultType.ERROR);
			result.setDetails(this.createDetails(false));
		}

		response.setResult(result);

		return response;
	}

	@Override
	@WebResult(name = "RequestAdjustedCapacityResponse", targetNamespace = "http://OSCP/DSO/2013/06/", partName = "parameters")
	@WebMethod(operationName = "RequestAdjustedCapacity", action = "/RequestAdjustedCapacity")
	@ActionContext(event = ActionEvent.REQUEST_ADJUSTED_CAPACITY)
	public RequestAdjustedCapacityResponse requestAdjustedCapacity(
			@WebParam(partName = "parameters", name = "RequestAdjustedCapacityRequest", targetNamespace = "http://OSCP/DSO/2013/06/") RequestAdjustedCapacityRequest parameters,
			@WebParam(partName = "identification", mode = Mode.INOUT, name = "Identification", targetNamespace = "http://OSCP/DSO/2013/06/", header = true) Holder<Identification> identification,
			@WebParam(partName = "priority", mode = Mode.INOUT, name = "Priority", targetNamespace = "http://OSCP/DSO/2013/06/", header = true) Holder<Integer> priority) {

		RequestAdjustedCapacityResponse response = new RequestAdjustedCapacityResponse();
		Result result = new Result();

		Csp csp = new Csp();
		csp.setEan13(identification.value.getCSP());

		Dso dso = new Dso();
		dso.setEan13(identification.value.getDSO());

		CspAdjustmentMessage cspAdjustmentMessage = new CspAdjustmentMessage();
		cspAdjustmentMessage.setCsp(csp.getEan13());
		cspAdjustmentMessage.setDso(dso.getEan13());
		cspAdjustmentMessage.setPriority(priority.value.intValue());
		cspAdjustmentMessage.setDateTime(new Date());
		cspAdjustmentMessage.setEventId(identification.value.getEventId());

		List<CspAdjustment> cspAdjustments = cspAdjustmentMessage.getCspAdjustments();

		List<Adjustment> adjustmentList = parameters.getAdjustment();
		for (Adjustment adjustment : adjustmentList) {
			CspAdjustment cspAdjustment = new CspAdjustment();

			Cable cable = new Cable();
			cable.setCableId(adjustment.getCable().getCableId());

			CableCsp cableCsp = new CableCsp();
			cableCsp.setCable(cable);
			cableCsp.setCsp(csp);
			cspAdjustment.setCableCsp(cableCsp);

			List<CspAdjustmentValue> cspAdjustmentValues = new ArrayList<CspAdjustmentValue>(0);

			List<CapacityBlock> adjustmentBlocks = adjustment.getCapacityBlock();
			for (CapacityBlock adjustmentBlock : adjustmentBlocks) {
				CspAdjustmentValue cspAdjustmentValue = new CspAdjustmentValue();
				cspAdjustmentValue.setCableCsp(cableCsp);
				cspAdjustmentValue.setCspAdjustment(cspAdjustment);
				cspAdjustmentValue.setStartedAt(adjustmentBlock.getStartTime());
				cspAdjustmentValue.setEndedAt(adjustmentBlock.getEndTime());
				cspAdjustmentValue.setRequested(adjustmentBlock.getCapacity().getValue());
				cspAdjustmentValue.setUnitOfMeasure(adjustmentBlock.getCapacity().getUnitOfMeasure().value());
				cspAdjustmentValues.add(cspAdjustmentValue);
			}

			cspAdjustment.setCspAdjustmentValues(cspAdjustmentValues);

			cspAdjustments.add(cspAdjustment);
		}

		try {
			adjustmentService.receiveAdjustment(cspAdjustmentMessage);
			if (cspAdjustmentMessage.isSuccessful()) {
				adjustmentService.setAdjustmentSuccessful(cspAdjustmentMessage);
			}
		} catch (ActionException e) {
			ActionLogger.error(e.getMessage(), new Object[] {}, e, true);
		}

		if (cspAdjustmentMessage.isSuccessful()) {
			result.setType(ResultType.SUCCESS);
			result.setDetails(this.createDetails(true));

			List<Adjustment> respAdjustments = response.getAdjustment();

			for (CspAdjustment cspAdjustment : cspAdjustmentMessage.getCspAdjustments()) {
				Adjustment respAdjustment = new Adjustment();
				nl.enexis.scip.dso.host.v0_9.generated.Cable respCable = new nl.enexis.scip.dso.host.v0_9.generated.Cable();
				respCable.setCableId(cspAdjustment.getCableCsp().getCable().getCableId());
				respAdjustment.setCable(respCable);

				List<CapacityBlock> respBlocks = respAdjustment.getCapacityBlock();

				for (CspAdjustmentValue cspAdjustmentValue : cspAdjustment.getCspAdjustmentValues()) {
					CapacityBlock respBlock = new CapacityBlock();
					respBlock.setStartTime(cspAdjustmentValue.getStartedAt());
					respBlock.setEndTime(cspAdjustmentValue.getEndedAt());
					ValueType value = new ValueType();
					value.setValue(cspAdjustmentValue.getAssigned());
					value.setUnitOfMeasure(UnitOfMeasure.AMP);
					respBlock.setCapacity(value);

					respBlocks.add(respBlock);
				}

				respAdjustments.add(respAdjustment);
			}
		} else {
			result.setType(ResultType.ERROR);
			result.setDetails(this.createDetails(false));
		}

		response.setResult(result);

		return response;
	}

	private Details createDetails(boolean isSuccess) {
		Action action = nl.enexis.scip.action.ActionContext.getAction();

		Details details = new Details();

		if (isSuccess) {
			Detail detail = new Detail();
			detail.setCode(DetailCode.MESSAGE_PROCESSED);
			detail.setLevel(DetailLevel.INFO);
			detail.setMessage("Service call successful processed by DSO.");
			details.getDetail().add(detail);
		} else {
			Detail detail = new Detail();
			detail.setCode(DetailCode.UNEXPECTED_EXCEPTION);
			detail.setLevel(DetailLevel.ERROR);
			detail.setMessage("Service call NOT successful processed by DSO.");
			details.getDetail().add(detail);
		}

		List<ActionMessage> msgs = action.getMessages();
		for (ActionMessage msg : msgs) {
			if (msg.getLevel().equals(Level.ERROR) || msg.getLevel().equals(Level.WARN)) {
				Detail detail = new Detail();
				detail.setCode(DetailCode.UNEXPECTED_EXCEPTION);
				if (msg.getLevel().equals(Level.ERROR)) {
					detail.setLevel(DetailLevel.ERROR);
				} else {
					detail.setLevel(DetailLevel.WARN);
				}
				detail.setMessage(msg.getMessage());
				detail.setDescription(msg.getRootCauseMessage());
				details.getDetail().add(detail);
			}
		}

		return details;
	}

}