
package nl.enexis.scip.dso.host.v0_9.generated;

/*
 * #%L
 * SCiP-web
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.enexis.scip.dso.host.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetCapacityForecastResponse_QNAME = new QName("http://OSCP/DSO/2013/06/", "GetCapacityForecastResponse");
    private final static QName _RequestAdjustedCapacityRequest_QNAME = new QName("http://OSCP/DSO/2013/06/", "RequestAdjustedCapacityRequest");
    private final static QName _RequestAdjustedCapacityResponse_QNAME = new QName("http://OSCP/DSO/2013/06/", "RequestAdjustedCapacityResponse");
    private final static QName _GetCapacityForecastRequest_QNAME = new QName("http://OSCP/DSO/2013/06/", "GetCapacityForecastRequest");
    private final static QName _Identification_QNAME = new QName("http://OSCP/DSO/2013/06/", "Identification");
    private final static QName _UpdateAggregatedUsageResponse_QNAME = new QName("http://OSCP/DSO/2013/06/", "UpdateAggregatedUsageResponse");
    private final static QName _Priority_QNAME = new QName("http://OSCP/DSO/2013/06/", "Priority");
    private final static QName _UpdateAggregatedUsageRequest_QNAME = new QName("http://OSCP/DSO/2013/06/", "UpdateAggregatedUsageRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.enexis.scip.dso.host.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Result }
     * 
     */
    public Result createResult() {
        return new Result();
    }

    /**
     * Create an instance of {@link Result.Details }
     * 
     */
    public Result.Details createResultDetails() {
        return new Result.Details();
    }

    /**
     * Create an instance of {@link UpdateAggregatedUsageRequest }
     * 
     */
    public UpdateAggregatedUsageRequest createUpdateAggregatedUsageRequest() {
        return new UpdateAggregatedUsageRequest();
    }

    /**
     * Create an instance of {@link UsageBlock }
     * 
     */
    public UsageBlock createUsageBlock() {
        return new UsageBlock();
    }

    /**
     * Create an instance of {@link ValueType }
     * 
     */
    public ValueType createValueType() {
        return new ValueType();
    }

    /**
     * Create an instance of {@link GetCapacityForecastResponse }
     * 
     */
    public GetCapacityForecastResponse createGetCapacityForecastResponse() {
        return new GetCapacityForecastResponse();
    }

    /**
     * Create an instance of {@link Cable }
     * 
     */
    public Cable createCable() {
        return new Cable();
    }

    /**
     * Create an instance of {@link RequestAdjustedCapacityRequest }
     * 
     */
    public RequestAdjustedCapacityRequest createRequestAdjustedCapacityRequest() {
        return new RequestAdjustedCapacityRequest();
    }

    /**
     * Create an instance of {@link RequestAdjustedCapacityResponse }
     * 
     */
    public RequestAdjustedCapacityResponse createRequestAdjustedCapacityResponse() {
        return new RequestAdjustedCapacityResponse();
    }

    /**
     * Create an instance of {@link CapacityBlock }
     * 
     */
    public CapacityBlock createCapacityBlock() {
        return new CapacityBlock();
    }

    /**
     * Create an instance of {@link Identification }
     * 
     */
    public Identification createIdentification() {
        return new Identification();
    }

    /**
     * Create an instance of {@link GetCapacityForecastRequest }
     * 
     */
    public GetCapacityForecastRequest createGetCapacityForecastRequest() {
        return new GetCapacityForecastRequest();
    }

    /**
     * Create an instance of {@link UpdateAggregatedUsageResponse }
     * 
     */
    public UpdateAggregatedUsageResponse createUpdateAggregatedUsageResponse() {
        return new UpdateAggregatedUsageResponse();
    }

    /**
     * Create an instance of {@link Adjustment }
     * 
     */
    public Adjustment createAdjustment() {
        return new Adjustment();
    }

    /**
     * Create an instance of {@link Result.Details.Detail }
     * 
     */
    public Result.Details.Detail createResultDetailsDetail() {
        return new Result.Details.Detail();
    }

    /**
     * Create an instance of {@link UpdateAggregatedUsageRequest.Usage }
     * 
     */
    public UpdateAggregatedUsageRequest.Usage createUpdateAggregatedUsageRequestUsage() {
        return new UpdateAggregatedUsageRequest.Usage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCapacityForecastResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/DSO/2013/06/", name = "GetCapacityForecastResponse")
    public JAXBElement<GetCapacityForecastResponse> createGetCapacityForecastResponse(GetCapacityForecastResponse value) {
        return new JAXBElement<GetCapacityForecastResponse>(_GetCapacityForecastResponse_QNAME, GetCapacityForecastResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestAdjustedCapacityRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/DSO/2013/06/", name = "RequestAdjustedCapacityRequest")
    public JAXBElement<RequestAdjustedCapacityRequest> createRequestAdjustedCapacityRequest(RequestAdjustedCapacityRequest value) {
        return new JAXBElement<RequestAdjustedCapacityRequest>(_RequestAdjustedCapacityRequest_QNAME, RequestAdjustedCapacityRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestAdjustedCapacityResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/DSO/2013/06/", name = "RequestAdjustedCapacityResponse")
    public JAXBElement<RequestAdjustedCapacityResponse> createRequestAdjustedCapacityResponse(RequestAdjustedCapacityResponse value) {
        return new JAXBElement<RequestAdjustedCapacityResponse>(_RequestAdjustedCapacityResponse_QNAME, RequestAdjustedCapacityResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCapacityForecastRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/DSO/2013/06/", name = "GetCapacityForecastRequest")
    public JAXBElement<GetCapacityForecastRequest> createGetCapacityForecastRequest(GetCapacityForecastRequest value) {
        return new JAXBElement<GetCapacityForecastRequest>(_GetCapacityForecastRequest_QNAME, GetCapacityForecastRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Identification }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/DSO/2013/06/", name = "Identification")
    public JAXBElement<Identification> createIdentification(Identification value) {
        return new JAXBElement<Identification>(_Identification_QNAME, Identification.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateAggregatedUsageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/DSO/2013/06/", name = "UpdateAggregatedUsageResponse")
    public JAXBElement<UpdateAggregatedUsageResponse> createUpdateAggregatedUsageResponse(UpdateAggregatedUsageResponse value) {
        return new JAXBElement<UpdateAggregatedUsageResponse>(_UpdateAggregatedUsageResponse_QNAME, UpdateAggregatedUsageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/DSO/2013/06/", name = "Priority")
    public JAXBElement<Integer> createPriority(Integer value) {
        return new JAXBElement<Integer>(_Priority_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateAggregatedUsageRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://OSCP/DSO/2013/06/", name = "UpdateAggregatedUsageRequest")
    public JAXBElement<UpdateAggregatedUsageRequest> createUpdateAggregatedUsageRequest(UpdateAggregatedUsageRequest value) {
        return new JAXBElement<UpdateAggregatedUsageRequest>(_UpdateAggregatedUsageRequest_QNAME, UpdateAggregatedUsageRequest.class, null, value);
    }

}
