
package nl.enexis.scip.dso.host.v0_9.generated;

/*
 * #%L
 * SCiP-web
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Adjustment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Adjustment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://OSCP/DSO/2013/06/}Cable"/>
 *         &lt;element ref="{http://OSCP/DSO/2013/06/}CapacityBlock" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Adjustment", propOrder = {
    "cable",
    "capacityBlock"
})
public class Adjustment {

    @XmlElement(name = "Cable", required = true)
    protected Cable cable;
    @XmlElement(name = "CapacityBlock", required = true)
    protected List<CapacityBlock> capacityBlock;

    /**
     * Gets the value of the cable property.
     * 
     * @return
     *     possible object is
     *     {@link Cable }
     *     
     */
    public Cable getCable() {
        return cable;
    }

    /**
     * Sets the value of the cable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cable }
     *     
     */
    public void setCable(Cable value) {
        this.cable = value;
    }

    /**
     * Gets the value of the capacityBlock property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the capacityBlock property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCapacityBlock().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CapacityBlock }
     * 
     * 
     */
    public List<CapacityBlock> getCapacityBlock() {
        if (capacityBlock == null) {
            capacityBlock = new ArrayList<CapacityBlock>();
        }
        return this.capacityBlock;
    }

}
