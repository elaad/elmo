package nl.enexis.scip.dso.host.v1_0;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import nl.enexis.scip.UnitTestHelper;
import nl.enexis.scip.action.ActionContext;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.dso.host.v1_0.generated.Adjustment;
import nl.enexis.scip.dso.host.v1_0.generated.Cable;
import nl.enexis.scip.dso.host.v1_0.generated.CapacityBlock;
import nl.enexis.scip.dso.host.v1_0.generated.GetCapacityForecastRequest;
import nl.enexis.scip.dso.host.v1_0.generated.LogLevelType;
import nl.enexis.scip.dso.host.v1_0.generated.Parties;
import nl.enexis.scip.dso.host.v1_0.generated.RequestAdjustedCapacityRequest;
import nl.enexis.scip.dso.host.v1_0.generated.RequestAdjustedCapacityResponse;
import nl.enexis.scip.dso.host.v1_0.generated.Result;
import nl.enexis.scip.dso.host.v1_0.generated.Result.Details;
import nl.enexis.scip.dso.host.v1_0.generated.Result.Details.Detail;
import nl.enexis.scip.dso.host.v1_0.generated.ResultType;
import nl.enexis.scip.dso.host.v1_0.generated.UnitOfMeasure;
import nl.enexis.scip.dso.host.v1_0.generated.UpdateAggregatedUsageRequest;
import nl.enexis.scip.dso.host.v1_0.generated.UpdateAggregatedUsageRequest.Usage;
import nl.enexis.scip.dso.host.v1_0.generated.UpdateAggregatedUsageResponse;
import nl.enexis.scip.dso.host.v1_0.generated.UsageBlock;
import nl.enexis.scip.dso.host.v1_0.generated.ValueType;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustment;
import nl.enexis.scip.model.CspAdjustmentMessage;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspUsage;
import nl.enexis.scip.model.CspUsageMessage;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.service.AdjustmentService;
import nl.enexis.scip.service.CspService;
import nl.enexis.scip.service.UsageService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ActionLogger.class, ActionContext.class })
public class DistributionSystemOperatorServiceImplTest {

	@InjectMocks
	private DistributionSystemOperatorServiceImpl dsosi;

	@Mock
	private UsageService usageService;

	@Mock
	private CspService cspService;

	@Mock
	private AdjustmentService adjustmentService;

	private javax.xml.ws.Holder<Parties> parties;
	private javax.xml.ws.Holder<java.lang.Integer> priority;
	private javax.xml.ws.Holder<java.util.Date> datetime;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		parties = null;
		priority = null;
		datetime = null;
	}

	private ValueType makeValueType(double value, UnitOfMeasure unitOfMeasure) {
		ValueType valueType = new ValueType();
		valueType.setPhase(1);
		valueType.setUnit(unitOfMeasure);
		valueType.setValue(new BigDecimal(value));
		return valueType;
	}

	private UsageBlock makeUsageBlock() {
		UsageBlock usageBlock = new UsageBlock();
		usageBlock.setStartTime(UnitTestHelper.getDate23Dec2015at11h00m00s());
		usageBlock.setEndTime(UnitTestHelper.getDate23Dec2015at11h00m27s());
		return usageBlock;
	}

	private Csp makeCsp(String ean13) {
		Csp csp = new Csp();
		csp.setEan13(ean13);
		return csp;
	}

	private Dso makeDso(String ean13) {
		Dso dso = new Dso();
		dso.setEan13(ean13);
		return dso;
	}

	private Usage makeUsage() {
		Cable cable = new Cable();
		cable.setCableId("CABLEID");
		Usage usage = new Usage();
		usage.setCable(cable);
		return usage;
	}

	private Adjustment makeAdjustment() {
		Cable cable = new Cable();
		cable.setCableId("CABLEID");
		Adjustment adjustment = new Adjustment();
		adjustment.setCable(cable);
		return adjustment;
	}

	private CspAdjustment makeCspAdjustment() {
		nl.enexis.scip.model.Cable cable = new nl.enexis.scip.model.Cable();
		cable.setCableId("CABLEID");
		CableCsp cableCsp = new CableCsp();
		cableCsp.setCable(cable);
		CspAdjustment cspAdjustment = new CspAdjustment();
		cspAdjustment.setCableCsp(cableCsp);
		return cspAdjustment;
	}

	private javax.xml.ws.Holder<Parties> makeParties() {
		javax.xml.ws.Holder<Parties> parties = new javax.xml.ws.Holder<Parties>();
		parties.value = new Parties();
		parties.value.setReceiverID("RECEIVERID");
		parties.value.setSenderID("SENDERID");
		return parties;
	}

	private CspAdjustmentValue makeCspAdjustmentValue(double value) {
		CspAdjustmentValue cspAdjustmentValue = new CspAdjustmentValue();
		cspAdjustmentValue.setStartedAt(UnitTestHelper.getDate23Dec2015at11h00m00s());
		cspAdjustmentValue.setEndedAt(UnitTestHelper.getDate23Dec2015at11h00m27s());
		cspAdjustmentValue.setAssigned(new BigDecimal(value));
		return cspAdjustmentValue;
	}

	private void prepare() {
		parties = this.makeParties();
		priority = null;
		datetime = null;
	}

	// Verify that update is correctly processed
	@Test
	public void testUpdateAggregatedUsage_Successful() throws Exception {
		this.prepare();
		UpdateAggregatedUsageRequest parameters = null;
		CspUsageMessage cspUsageMessage = new CspUsageMessage();
		cspUsageMessage.setSuccessful(true);

		DistributionSystemOperatorServiceImpl dsosiSpy = spy(dsosi);
		doReturn(cspUsageMessage).when(dsosiSpy).setCspUsage(any(), any(), any(), any(), any());
		Details detailsTrue = new Details();
		Details detailsFalse = new Details();
		doReturn(detailsTrue).when(dsosiSpy).createDetails(eq(true));
		doReturn(detailsFalse).when(dsosiSpy).createDetails(eq(false));

		UpdateAggregatedUsageResponse result = dsosiSpy.updateAggregatedUsage(parameters, parties, priority, datetime);

		verify(usageService, times(1)).receiveUsage(cspUsageMessage);
		assertEquals(ResultType.SUCCESS, result.getResult().getType());
		assertEquals(detailsTrue, result.getResult().getDetails());
	}

	// Verify that not successful update is correctly processed
	@Test
	public void testUpdateAggregatedUsage_NotSuccessful() throws Exception {
		this.prepare();
		UpdateAggregatedUsageRequest parameters = null;
		CspUsageMessage cspUsageMessage = new CspUsageMessage();
		cspUsageMessage.setSuccessful(false);

		PowerMockito.mockStatic(ActionLogger.class);
		doThrow(new ActionException()).when(usageService).receiveUsage(any());

		DistributionSystemOperatorServiceImpl dsosiSpy = spy(dsosi);
		doReturn(cspUsageMessage).when(dsosiSpy).setCspUsage(any(), any(), any(), any(), any());
		Details detailsTrue = new Details();
		Details detailsFalse = new Details();
		doReturn(detailsTrue).when(dsosiSpy).createDetails(eq(true));
		doReturn(detailsFalse).when(dsosiSpy).createDetails(eq(false));

		// execute
		UpdateAggregatedUsageResponse result = dsosiSpy.updateAggregatedUsage(parameters, parties, priority, datetime);

		// verify
		verify(usageService, times(1)).receiveUsage(cspUsageMessage);
		PowerMockito.verifyStatic(times(1));

		ActionLogger.error(eq(null), any(Object[].class), any(Throwable.class), eq(true));
		assertEquals(ResultType.ERROR, result.getResult().getType());
		assertEquals(detailsFalse, result.getResult().getDetails());
	}

	// Assert that the cable, csp, dso, priority and datetime values are set accordingly
	@Test
	public void testSetCspUsage() {
		UpdateAggregatedUsageRequest parameters = new UpdateAggregatedUsageRequest();
		Usage usage = this.makeUsage();
		parameters.getUsage().add(usage);
		javax.xml.ws.Holder<java.lang.Integer> priority = new javax.xml.ws.Holder<java.lang.Integer>();
		priority.value = new Integer(5);
		javax.xml.ws.Holder<java.util.Date> datetime = new javax.xml.ws.Holder<java.util.Date>();
		datetime.value = UnitTestHelper.getDate23Dec2015at11h33m27s();
		Csp csp = this.makeCsp("CSPEAN13");
		Dso dso = this.makeDso("DSOEAN13");

		dsosi = new DistributionSystemOperatorServiceImpl() {
			@Override
			protected List<CspUsageValue> setCspUsageValues(Usage usage, CspUsage cspUsage, CableCsp cableCsp) {
				return null;
			}
		};

		CspUsageMessage result = dsosi.setCspUsage(parameters, priority, datetime, csp, dso);

		assertEquals("CSPEAN13", result.getCsp());
		assertEquals("DSOEAN13", result.getDso());
		assertEquals(5, result.getPriority());
		assertEquals(UnitTestHelper.getDate23Dec2015at11h33m27s(), result.getDateTime());
		assertEquals("EventId should be set", true, result.getEventId() != null);
		assertEquals("CABLEID", result.getCspUsages().get(0).getCableCsp().getCable().getCableId());
		assertEquals(csp, result.getCspUsages().get(0).getCableCsp().getCsp());
	}

	// Assert that the (last) correct valuetype is processed in the cspusagevalues
	@Test
	public void testSetCspUsageValues() {
		// prepare
		Usage usage = new Usage();
		UsageBlock usageBlock1 = this.makeUsageBlock();
		ValueType valueType = this.makeValueType(666, UnitOfMeasure.WH);
		usageBlock1.getUsage().add(valueType);
		ValueType valueType2 = this.makeValueType(1337, UnitOfMeasure.A);
		usageBlock1.getUsage().add(valueType2);
		usage.getUsageBlock().add(usageBlock1);

		UsageBlock usageBlock2 = this.makeUsageBlock();
		ValueType valueType3 = this.makeValueType(1447, UnitOfMeasure.A);
		usageBlock2.getUsage().add(valueType3);
		ValueType valueType4 = this.makeValueType(0.666, UnitOfMeasure.F);
		usageBlock2.getUsage().add(valueType4);
		usage.getUsageBlock().add(usageBlock2);

		CspUsage cspUsage = new CspUsage();
		CableCsp cableCsp = new CableCsp();

		// execute
		List<CspUsageValue> result = dsosi.setCspUsageValues(usage, cspUsage, cableCsp);

		// verify
		assertEquals(new BigDecimal(666).doubleValue(), result.get(0).getEnergyConsumption().doubleValue(), 0.001);
		assertEquals(new BigDecimal(1337).doubleValue(), result.get(0).getAverageCurrent().doubleValue(), 0.001);

		assertEquals(new BigDecimal(0).doubleValue(), result.get(1).getEnergyConsumption().doubleValue(), 0.001);
		assertEquals(new BigDecimal(1447).doubleValue(), result.get(1).getAverageCurrent().doubleValue(), 0.001);

		assertEquals(cspUsage, result.get(0).getCspUsage());
		assertEquals(cableCsp, result.get(0).getCableCsp());

		assertEquals(cspUsage, result.get(1).getCspUsage());
		assertEquals(cableCsp, result.get(1).getCableCsp());
	}

	// Verify that alle cables added to the parameters are returned correctly
	@Test
	public void testGetForCables() {
		GetCapacityForecastRequest parameters = new GetCapacityForecastRequest();
		Cable cable1 = new Cable();
		cable1.setCableId("CABLEIDFIRST");
		parameters.getCable().add(cable1);
		Cable cable2 = new Cable();
		cable2.setCableId("CABLEIDSECOND");
		parameters.getCable().add(cable2);

		Set<String> result = dsosi.getForCables(parameters);

		Object[] resultString = result.toArray();
		assertEquals("CABLEIDFIRST", resultString[0]);
		assertEquals("CABLEIDSECOND", resultString[1]);
	}

	// Assert that the result type is error in case of no success
	@Test
	public void testProcessCspAdjustmentMessage_NotSuccessful() {
		RequestAdjustedCapacityResponse response = new RequestAdjustedCapacityResponse();
		Result result = new Result();
		CspAdjustmentMessage cspAdjustmentMessage = new CspAdjustmentMessage();
		cspAdjustmentMessage.setSuccessful(false);

		dsosi = new DistributionSystemOperatorServiceImpl() {
			@Override
			protected Details createDetails(boolean isSuccess) {
				Detail detail = new Detail();
				detail.setCode("CODENOTSUCCES");
				Details details = new Details();
				details.getDetail().add(detail);
				return details;
			}
		};

		dsosi.processCspAdjustmentMessage(response, result, cspAdjustmentMessage);

		assertEquals(ResultType.ERROR, result.getType());
		assertEquals("CODENOTSUCCES", result.getDetails().getDetail().get(0).getCode());
	}

	// Assure that the CspAdjustmentMessage is correctly set
	@Test
	public void testProcessCspAdjustmentMessage_SuccesfulCspAdjustmentMessage() {
		RequestAdjustedCapacityResponse response = new RequestAdjustedCapacityResponse();
		Result result = new Result();
		CspAdjustmentMessage cspAdjustmentMessage = new CspAdjustmentMessage();
		cspAdjustmentMessage.setSuccessful(true);

		CspAdjustment cspAdjustment = this.makeCspAdjustment();
		List<CspAdjustment> cspAdjustmentList = new ArrayList<CspAdjustment>();
		cspAdjustmentList.add(cspAdjustment);
		cspAdjustmentMessage.setCspAdjustments(cspAdjustmentList);

		CspAdjustmentValue cspAdjustmentValue = this.makeCspAdjustmentValue(1499.88);
		List<CspAdjustmentValue> cspAdjustmentValueList = new ArrayList<CspAdjustmentValue>();
		cspAdjustmentValueList.add(cspAdjustmentValue);
		cspAdjustment.setCspAdjustmentValues(cspAdjustmentValueList);

		dsosi = new DistributionSystemOperatorServiceImpl() {
			@Override
			protected Details createDetails(boolean isSuccess) {
				Detail detail = new Detail();
				detail.setCode("CODESUCCES");
				Details details = new Details();
				details.getDetail().add(detail);
				return details;
			}
		};

		dsosi.processCspAdjustmentMessage(response, result, cspAdjustmentMessage);

		Adjustment resultAdjustment = response.getAdjustment().get(0);

		assertEquals("CABLEID", resultAdjustment.getCable().getCableId());

		CapacityBlock resultCapacityBlock = resultAdjustment.getCapacityBlock().get(0);
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m00s(), resultCapacityBlock.getStartTime());
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27s(), resultCapacityBlock.getEndTime());

		ValueType resultValueType = resultCapacityBlock.getCapacity().get(0);
		assertEquals(new BigDecimal(1499.88).doubleValue(), resultValueType.getValue().doubleValue(), 0.001);
		assertEquals(UnitOfMeasure.A, resultValueType.getUnit());

		assertEquals(ResultType.SUCCESS, result.getType());
		assertEquals("CODESUCCES", result.getDetails().getDetail().get(0).getCode());
	}

	// Assure that CspAdjustmentMessage is correctly set
	@Test
	public void testSetCspAdjustmentMessage() throws Exception {
		Adjustment adjustment = this.makeAdjustment();

		RequestAdjustedCapacityRequest parameters = new RequestAdjustedCapacityRequest();
		parameters.getAdjustment().add(adjustment);
		Csp csp = this.makeCsp("CSPEAN13");
		CspAdjustmentMessage cspAdjustmentMessage = new CspAdjustmentMessage();

		dsosi = new DistributionSystemOperatorServiceImpl() {
			@Override
			protected List<CspAdjustmentValue> setupCspAdjustmentValues(Adjustment adjustment, CspAdjustment cspAdjustment,
					CableCsp cableCsp) {
				List<CspAdjustmentValue> list = new ArrayList<CspAdjustmentValue>();
				for (int i = 0; i < 4; i++) {
					list.add(new CspAdjustmentValue());
				}
				return list;
			}
		};

		dsosi.setCspAdjustmentMessage(parameters, csp, cspAdjustmentMessage);

		CspAdjustment resultCspAdjustment = cspAdjustmentMessage.getCspAdjustments().get(0);

		nl.enexis.scip.model.Cable cableModel = new nl.enexis.scip.model.Cable();
		cableModel.setCableId(adjustment.getCable().getCableId());
		assertEquals(cableModel, resultCspAdjustment.getCableCsp().getCable());
		assertEquals(csp, resultCspAdjustment.getCableCsp().getCsp());
		assertEquals(4, resultCspAdjustment.getCspAdjustmentValues().size());
	}

	// Assert that the cable csp, csp adjustment and requested values are set accordingly
	@Test
	public void testSetupCspAdjustmentValues() {
		CapacityBlock capacityBlock = new CapacityBlock();
		capacityBlock.setStartTime(UnitTestHelper.getDate23Dec2015at11h00m00s());
		capacityBlock.setEndTime(UnitTestHelper.getDate23Dec2015at11h00m27s());
		Adjustment adjustment = new Adjustment();
		adjustment.getCapacityBlock().add(capacityBlock);

		ValueType valueType = this.makeValueType(52.65499999999, UnitOfMeasure.A);
		valueType.setPhase(1);
		adjustment.getCapacityBlock().get(0).getCapacity().add(valueType);

		CspAdjustment cspAdjustment = new CspAdjustment();
		CableCsp cableCsp = new CableCsp();

		List<CspAdjustmentValue> result = dsosi.setupCspAdjustmentValues(adjustment, cspAdjustment, cableCsp);

		assertEquals(cableCsp, result.get(0).getCableCsp());
		assertEquals(cspAdjustment, result.get(0).getCspAdjustment());
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m00s(), result.get(0).getStartedAt());
		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m27s(), result.get(0).getEndedAt());
		assertEquals(new BigDecimal(52.655).doubleValue(), result.get(0).getRequested().doubleValue(), 0.001);
		assertEquals(UnitOfMeasure.A.toString(), result.get(0).getUnitOfMeasure());
	}

	// Assure that created details contain the correct data in case of an successful creation of details
	@Test
	public void testCreateDetails_Successful() {
		Details result = dsosi.createDetails(true);

		assertEquals("MESSAGE_PROCESSED", result.getDetail().get(0).getCode());
		assertEquals(LogLevelType.INFO, result.getDetail().get(0).getLevel());
		assertEquals("Service call successful processed by DSO.", result.getDetail().get(0).getMessage());

		assertEquals(1, result.getDetail().size());
	}

	// Assure that created details contain the correct data in case of an exception of level error
	@Test
	public void testCreateDetails_NotSuccessful() {
		PowerMockito.mockStatic(ActionContext.class);
		mock(ActionContext.class);
		Action action = spy(Action.class);
		ActionMessage actionMessage = new ActionMessage();
		actionMessage.setLevel(Level.ERROR);
		actionMessage.setRootCauseMessage("ROOTCAUSE");
		actionMessage.setMessage("MESSAGE");
		actionMessage.setExceptionType(ActionExceptionType.CONFIGURATION);
		List<ActionMessage> actionMessageList = new ArrayList<ActionMessage>();
		actionMessageList.add(actionMessage);
		action.setMessages(actionMessageList);
		PowerMockito.when(ActionContext.getAction()).thenReturn(action);

		Details result = dsosi.createDetails(false);

		Detail resultDetail0 = result.getDetail().get(0);
		Detail resultDetail1 = result.getDetail().get(1);

		assertEquals("EXCEPTION", resultDetail0.getCode());
		assertEquals(LogLevelType.ERROR, resultDetail0.getLevel());
		assertEquals("Service call NOT successful processed by DSO.", resultDetail0.getMessage());

		assertEquals(ActionExceptionType.CONFIGURATION.toString(), resultDetail1.getCode());
		assertEquals(LogLevelType.ERROR, resultDetail1.getLevel());
		assertEquals("MESSAGE", resultDetail1.getMessage());
		assertEquals("ROOTCAUSE", resultDetail1.getDescription());
	}
}
