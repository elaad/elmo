package nl.enexis.su.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.Behaviour;
import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.dao.CspUsageValueDao;
import nl.enexis.scip.dao.LocationMeasurementValueDao;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.CableLocation;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.Location;
import nl.enexis.scip.model.LocationMeasurement;
import nl.enexis.scip.model.LocationMeasurementValue;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.model.enumeration.MeasurementType;
import nl.enexis.scip.service.CspService;
import nl.enexis.scip.service.ForecastStorageService;
import nl.enexis.scip.service.GeneralService;
import nl.enexis.scip.service.TopologyService;
import nl.enexis.scip.service.TopologyStorageService;
import nl.enexis.scip.util.ForecastBlockUtil;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

@Behaviour(BehaviourType.SU)
@Stateless
public class ForecastServiceSuImpl implements ForecastServiceSuLocal {

	@EJB
	ForecastServiceSuLocal self;

	@Inject
	LocationMeasurementValueDao locationMeasurementValueDao;

	@Inject
	CspUsageValueDao cspUsageValueDao;

	@Inject
	@Named("forecastStorageService")
	ForecastStorageService forecastStorageService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Inject
	@Named("topologyService")
	TopologyService topologyService;

	@Inject
	@Behaviour(BehaviourType.SU)
	CspService cspService;

	@Override
	@Asynchronous
	public void doForecastAsync(String cableId) {
		try {
			self.doForecast(cableId);
		} catch (ActionException e) {
			// Do nothing
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ActionContext(event = ActionEvent.CALCULATE_FORECAST, doRethrow = true)
	public void doForecast(String cableId) throws ActionException {
		Cable cable = topologyStorageService.findCableByCableId(cableId);

		nl.enexis.scip.action.ActionContext.addActionTarget(cable);

		if (cable == null) {
			throw new ActionException(ActionExceptionType.DATA, "Cable not found! Cable: " + cableId);
		} else if (!cable.isActive()) {
			ActionLogger.warn("No forecast for Cable " + cableId + ". Cable is not active.", new Object[] {}, null,
					true);
			return;
		} else if (!cable.getDso().isActive()) {
			ActionLogger.warn("No forecast for Cable " + cableId + ". Dso " + cable.getDso().getEan13()
					+ " is not active.", new Object[] {}, null, true);
			return;
		}

		CableForecast cableForecast = new CableForecast();
		Date calculationDate = new Date();
		cableForecast.setId(nl.enexis.scip.action.ActionContext.getId());
		cableForecast.setDatetime(calculationDate);

		forecastStorageService.storeCableForecast(cableForecast);

		int blockMinutes = Integer
				.parseInt(generalService.getConfigurationParameter("ForecastBlockMinutes").getValue());

		List<CableCsp> cableCsps = cable.getCableCsps();
		int numberOfCsps = cableCsps.size();

		if (numberOfCsps == 0) {
			throw new ActionException(ActionExceptionType.DATA, "No Csps for Cable! Cable: " + cableId);
		}

		List<CableLocation> cableLocations = cable.getCableLocations();
		int numberOfLocations = cableLocations.size();

		if (numberOfLocations != 1) {
			throw new ActionException(ActionExceptionType.DATA, "No Location for Cable! Cable: " + cableId);
		}
		Location location = cableLocations.get(0).getLocation();

		// check for active csp with active connections
		boolean hasActiveCsp = topologyService.hasCableActiveCsp(cableId, false);
		if (!hasActiveCsp) {
			throw new ActionException(ActionExceptionType.DATA, "No active Csps for Cable! Cable: " + cableId);
		}

		cableForecast.setBlockMinutes(blockMinutes);
		cableForecast.setCable(cable);
		cableForecast.setFixed(false);
		BigDecimal remainingCapacityFactor = cable.getRemainingFactor();
		cableForecast.setRemainingCapacityFactor(remainingCapacityFactor);

		BigDecimal forecastSafetyFactor = cable.getSafetyFactor();
		cableForecast.setForecastSafetyFactor(forecastSafetyFactor);

		forecastStorageService.updateCableForecast(cableForecast);

		this.calculateForecast(cableForecast, location);

		forecastStorageService.addOutputsForCableForecast(cableForecast);

		cableForecast.setSuccessful(true);
		forecastStorageService.updateCableForecast(cableForecast);

		cspService.doCspForecastAsync(cableForecast.getId());
	}

	private void calculateForecast(CableForecast cableForecast, Location location) throws ActionException {
		int blockMinutes = cableForecast.getBlockMinutes();

		int numberOfInputs = generalService.getConfigurationParameterAsInteger("SuForecastInputBlocks");
		int daysOffset = generalService.getConfigurationParameterAsInteger("SuForecastInputDaysOffset");

		Cable cable = cableForecast.getCable();

		CableCsp cableCsp = cable.getCableCsps().get(0);

		Map<String, Object> timeBlock = ForecastBlockUtil.getCurrentBlockValues(blockMinutes);

		// Correct date with offset and set to midnight before
		Date queriesToDate = ForecastBlockUtil.getDate(ForecastBlockUtil.addDaysToDate(
				ForecastBlockUtil.getStartDateOfBlock(timeBlock, blockMinutes), -1 * daysOffset));

		Map<Long, CspUsageValue> usages = this.getCspUsageValues(cableCsp, numberOfInputs, queriesToDate);
		Map<Long, CspUsageValue> usagesMinusOneWeek = this.determineUsagesMinusOneWeek(usages);

		LocationMeasurement locationMeasurement = this.getLocationMeasurement(location, MeasurementType.IRR, false);
		Map<Long, LocationMeasurementValue> measuredIrradiations = this.getMeasuredIrradiations(locationMeasurement,
				numberOfInputs, queriesToDate, true);
		this.matchIrradiationWithUsage(usagesMinusOneWeek, measuredIrradiations);

		int inputsSize = usagesMinusOneWeek.size();

		OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
		double[] y = new double[inputsSize];
		double[][] x = new double[inputsSize][26];

		int i = 0;
		for (Long blockStart : usagesMinusOneWeek.keySet()) {

			y[i] = usages.get(blockStart).getEnergyConsumption().doubleValue();

			int k = 0;

			// set dummy for hour first 24 columns of x are for hour dummy
			// variable
			for (int j = 0; j < 24; j++) {
				x[i][k] = usages.get(blockStart).getHour() == j ? 1 : 0;
				k++;
			}

			x[i][k] = usagesMinusOneWeek.get(blockStart).getEnergyConsumption().doubleValue();

			k++;

			x[i][k] = measuredIrradiations.get(blockStart).getValue().doubleValue();

			i++;
		}

		regression.newSampleData(y, x);

		double[] beta = regression.estimateRegressionParameters();

		ActionLogger.trace("Forecast trainingset y:\n" + ArrayUtils.toString(y), new Object[] {}, null, false);
		ActionLogger.trace("Forecast trainingset x:\n" + ArrayUtils.toString(x), new Object[] {}, null, false);
		ActionLogger.trace("Forecast trainingset beta:\n" + ArrayUtils.toString(beta), new Object[] {}, null, false);

		int numberOfOutputs = Integer.parseInt(generalService.getConfigurationParameter("SuForecastOutputBlocks")
				.getValue());
		int minNumberOfOutputs = Integer.parseInt(generalService.getConfigurationParameter("SuForecastMinimalOutputBlocks")
				.getValue());

		int blocksPerHour = ForecastBlockUtil.getBlocksPerHour(blockMinutes);

		timeBlock = ForecastBlockUtil.getCurrentBlockValues(blockMinutes);

		locationMeasurement = this.getLocationMeasurement(location, MeasurementType.IRR, true);
		Map<Long, LocationMeasurementValue> forecastedIrradiations = this.getMeasuredIrradiations(locationMeasurement,
				numberOfOutputs, ForecastBlockUtil.getStartDateOfBlock(timeBlock, blockMinutes), false);

		CableForecastOutput lastOutput = null;
		for (int j = 0; j < numberOfOutputs; j++) {
			CableForecastOutput output = new CableForecastOutput();
			output.setCableForecast(cableForecast);
			output.setDay((Date) timeBlock.get(ForecastBlockUtil.DAY));
			output.setDayOfWeek(ForecastBlockUtil.getDayOfWeek(output.getDay()));
			output.setHour((Integer) timeBlock.get(ForecastBlockUtil.HOUR));
			output.setBlock((Integer) timeBlock.get(ForecastBlockUtil.BLOCK));

			Date blockStart = ForecastBlockUtil.getStartDateOfBlock(timeBlock, blockMinutes);
			Date weekBefore = ForecastBlockUtil.addDaysToDate(blockStart, -7);
			CspUsageValue usageWeekBefore = usages.get(new Long(weekBefore.getTime()));

			LocationMeasurementValue irradiation = forecastedIrradiations.get(new Long(blockStart.getTime()));

			if (usageWeekBefore == null || irradiation == null) {
				timeBlock = ForecastBlockUtil.getNextBlockValues(output.getDay(), output.getHour(), output.getBlock(),
						blocksPerHour);
				continue;
			}

			int hourParam = ((Integer) timeBlock.get(ForecastBlockUtil.HOUR)).intValue();

			output.setAvailableCapacity(BigDecimal.valueOf(beta[0])
					// hour
					.add(BigDecimal.valueOf(beta[hourParam + 1]))
					// usage -7
					.add(BigDecimal.valueOf(beta[25]).multiply(usageWeekBefore.getEnergyConsumption()))
					// irradiation
					.add(BigDecimal.valueOf(beta[26]).multiply(irradiation.getValue()))
					.setScale(2, RoundingMode.HALF_UP));

			cableForecast.getCableForecastOutputs().add(output);
			lastOutput = output;

			timeBlock = ForecastBlockUtil.getNextBlockValues(output.getDay(), output.getHour(), output.getBlock(),
					blocksPerHour);
		}
		
		if (cableForecast.getCableForecastOutputs().isEmpty()) {
			throw new ActionException(ActionExceptionType.DATA,
					"Forecast is empty! No forecast blocks could be calculated!");
		} else if (cableForecast.getCableForecastOutputs().size() < minNumberOfOutputs) {
			ActionLogger
					.error("Number of forecast blocks is less then {0}. Possible problems with forecast input data! Start of last block is {1}",
							new Object[] {
									minNumberOfOutputs,
									ForecastBlockUtil.getStartDateOfBlock(lastOutput.getDay(), lastOutput.getHour(),
											lastOutput.getBlock(), blockMinutes) }, null, true);
		}
	}

	private LocationMeasurement getLocationMeasurement(Location location, MeasurementType irr, boolean isForecast)
			throws ActionException {
		for (LocationMeasurement locationMeasurement : location.getLocationMeasurements()) {
			if (locationMeasurement.isForecast() == isForecast && locationMeasurement.getMeasurementType().equals(irr)) {
				return locationMeasurement;
			}
		}

		throw new ActionException(ActionExceptionType.DATA, "No active LocationMeasurement! Location: "
				+ location.getLocationId() + "; Measurement type: " + irr.name() + "; Is forecast: " + isForecast);
	}

	private Map<Long, LocationMeasurementValue> getMeasuredIrradiations(LocationMeasurement locationMeasurement,
			int numberOfInputs, Date startDateOfBlock, boolean beforeDate) {
		List<LocationMeasurementValue> irradiations = locationMeasurementValueDao.getIrradiations(locationMeasurement,
				numberOfInputs, startDateOfBlock, beforeDate);
		Map<Long, LocationMeasurementValue> irradiationsMap = new HashMap<Long, LocationMeasurementValue>();
		for (LocationMeasurementValue irradiation : irradiations) {
			irradiationsMap.put(irradiation.getKey().getDatetime().getTime(), irradiation);
		}
		return irradiationsMap;
	}

	private Map<Long, CspUsageValue> getCspUsageValues(CableCsp cableCsp, int numberOfInputs, Date toDate) {
		List<CspUsageValue> usages = cspUsageValueDao.getUsages(cableCsp, numberOfInputs, toDate);
		Map<Long, CspUsageValue> usagesMap = new HashMap<Long, CspUsageValue>();
		for (CspUsageValue cspUsageValue : usages) {
			usagesMap.put(cspUsageValue.getStartedAt().getTime(), cspUsageValue);
		}
		return usagesMap;
	}

	private Map<Long, CspUsageValue> determineUsagesMinusOneWeek(Map<Long, CspUsageValue> usages) {
		Map<Long, CspUsageValue> usagesMinusOneWeek = new HashMap<Long, CspUsageValue>();

		for (Long key : usages.keySet()) {
			Date minusOneWeek = ForecastBlockUtil.addDaysToDate(new Date(key), -7);
			Long minusOneWeekKey = new Long(minusOneWeek.getTime());
			if (usages.containsKey(minusOneWeekKey)) {
				usagesMinusOneWeek.put(key, usages.get(minusOneWeekKey));
			}
		}

		return usagesMinusOneWeek;
	}

	private void matchIrradiationWithUsage(Map<Long, CspUsageValue> usages,
			Map<Long, LocationMeasurementValue> irradiations) {

		Map<Long, LocationMeasurementValue> matchedIrradiations = new HashMap<Long, LocationMeasurementValue>();
		List<Long> usageKeysToRemove = new ArrayList<Long>();

		for (Long key : usages.keySet()) {
			if (irradiations.containsKey(key)) {
				matchedIrradiations.put(key, irradiations.get(key));
			} else {
				usageKeysToRemove.add(key);
			}
		}

		for (Long key : usageKeysToRemove) {
			usages.remove(key);
		}

		irradiations.clear();
		irradiations.putAll(matchedIrradiations);

	}

}