package nl.enexis.su.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.Behaviour;
import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspGetForecastMessage;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.service.CspMessagingService;
import nl.enexis.scip.service.ForecastStorageService;
import nl.enexis.scip.service.GeneralService;
import nl.enexis.scip.service.TopologyService;
import nl.enexis.scip.service.TopologyStorageService;
import nl.enexis.scip.util.ForecastBlockUtil;

@Behaviour(BehaviourType.SU)
@Stateless
public class CspServiceSuImpl implements CspServiceSuLocal {

	@EJB
	CspServiceSuLocal self;

	@Inject
	@Named("forecastStorageService")
	ForecastStorageService forecastStorageService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Inject
	@Named("topologyService")
	TopologyService topologyService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Inject
	@Named("cspMessagingService")
	CspMessagingService cspMessagingService;

	@Override
	@Asynchronous
	public void doCspForecastAsync(String cableForecastId) {
		try {
			self.doCspForecast(cableForecastId);
		} catch (ActionException e) {
			// Do nothing;
		}
	}

	@Override
	// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ActionContext(event = ActionEvent.CSP_FORECAST_DIVISION, doRethrow = true)
	public void doCspForecast(String cableForecastId) throws ActionException {
		CableForecast cableForecast = forecastStorageService.findCableForecastById(cableForecastId);
		Cable cable = topologyStorageService.findCableByCableId(cableForecast.getCable().getCableId());

		nl.enexis.scip.action.ActionContext.addActionTarget(cable);

		List<CableCsp> cableCsps = cable.getCableCsps();
		int numberOfCsps = cableCsps.size();

		if (numberOfCsps == 0) {
			throw new ActionException(ActionExceptionType.DATA, "No Csps for Cable! Cable: " + cable.getCableId());
		}

		// check for active csp with active connections
		boolean hasActiveCsp = topologyService.hasCableActiveCsp(cable.getCableId(), false);
		if (!hasActiveCsp) {
			throw new ActionException(ActionExceptionType.DATA, "No active Csps for Cable! Cable: "
					+ cable.getCableId());
		}

		List<CspForecast> cspForecasts = new ArrayList<CspForecast>();

		Date calculationDate = new Date();
		for (CableCsp cableCsp : cableCsps) {
			if (cableCsp.isActive()) {
				CspForecast cspForecast = new CspForecast();
				cspForecast.setCableForecast(cableForecast);
				cspForecast.setCableCsp(cableCsp);
				cspForecast.setNumberOfBlocks(cableCsp.getNumberOfBlocks());
				cspForecast.setCalculatedAt(calculationDate);
				List<CspForecastValue> cspForecastValues = new ArrayList<CspForecastValue>();
				cspForecast.setCspForecastValues(cspForecastValues);
				cspForecasts.add(cspForecast);
			}
		}

		List<CableForecastOutput> cableForecastOutputs = cableForecast.getCableForecastOutputs();

		for (CableForecastOutput cableForecastOutput : cableForecastOutputs) {
			for (CspForecast cspForecast : cspForecasts) {
				CspForecastValue cspForecastValue = new CspForecastValue();

				cspForecastValue.setCspForecast(cspForecast);
				cspForecastValue.setCableForecastOutput(cableForecastOutput);

				cspForecastValue.setAssigned(cableForecastOutput.getAvailableCapacity());
				cspForecastValue.setRemaining(new BigDecimal("0.00"));

				cspForecast.getCspForecastValues().add(cspForecastValue);
			}

		}

		forecastStorageService.storeCspForecasts(cspForecasts);
	}

	@Override
	public void sendForecastRequestedByCsp(CspGetForecastMessage cspGetForecastMessage, Set<String> forCables)
			throws ActionException {
		throw new ActionException(ActionExceptionType.NOT_SUPPORTED, "Implemented in sames service for SC!");
	}

	@Override
	@Asynchronous
	public void sendForecastForCspAsync(Long cspForecastId, int priority) {
		try {
			self.sendForecastForCsp(cspForecastId, priority);
		} catch (ActionException e) {
			// Do nothing
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ActionContext(event = ActionEvent.SEND_CAPACITY_FORECAST, doRethrow = true)
	public void sendForecastForCsp(Long cspForecastId, int priority) throws ActionException {
		CspForecast cspForecast = forecastStorageService.getCspForcast(cspForecastId);
		this.doSendForecastForCsp(cspForecast, priority);
	}

	@Override
	public void sendForecastForCable(String cableId, int defaultPriority) throws ActionException {
		Cable cable = topologyStorageService.findCableByCableId(cableId);

		List<CspForecast> cfs = forecastStorageService.getLastCspForecastsForCable(cable);

		for (CspForecast cspForecast : cfs) {
			self.sendForecastForCspAsync(cspForecast.getId(), defaultPriority);
		}
	}

	private void doSendForecastForCsp(CspForecast cspForecast, int priority) throws ActionException {
		CableCsp cableCsp = cspForecast.getCableCsp();

		CspForecastMessage cspForecastMessage = new CspForecastMessage();
		cspForecastMessage.setCsp(cableCsp.getCsp().getEan13());
		cspForecastMessage.setDso(cableCsp.getCable().getDso().getEan13());
		cspForecastMessage.setPriority(priority);
		cspForecastMessage.setDateTime(new Date());
		cspForecastMessage.setEventId(nl.enexis.scip.action.ActionContext.getId());

		forecastStorageService.storeCspForecastMessage(cspForecastMessage);

		if (cspForecast != null) {
			CableForecastOutput lastCableForecastOutput = cspForecast.getCspForecastValues()
					.get(cspForecast.getCspForecastValues().size() - 1).getCableForecastOutput();
			// TODO: This generates error
			Date endDateCspForecast = ForecastBlockUtil.getEndDateOfBlock(lastCableForecastOutput.getDay(),
					lastCableForecastOutput.getHour(), lastCableForecastOutput.getBlock(), lastCableForecastOutput
							.getCableForecast().getBlockMinutes());

			if (endDateCspForecast.after(new Date())) {
				cspForecastMessage.getCspForecasts().add(cspForecast);
			} else {
				cspForecast = null;
			}
		}

		if (cspForecast != null) {
			forecastStorageService.updateCspForecastMessageWithCspForecasts(cspForecastMessage);

			cspMessagingService.pushCspForecast(cspForecastMessage, cableCsp.getCsp());

			forecastStorageService.setCspForecastMessageSuccessful(cspForecastMessage.getId());
		} else {
			throw new ActionException(ActionExceptionType.DATA, "No forecast available for Csp! Csp: "
					+ cableCsp.getCsp().getEan13());
		}
	}
}