package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionTargetInOut;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.action.model.TimeBlock;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspUsage;
import nl.enexis.scip.model.CspUsageMessage;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named("usageService")
@Stateless
public class UsageServiceImpl implements UsageServiceLocal {

	@EJB
	UsageServiceLocal self;

	@Inject
	@Named("forecastStorageService")
	ForecastStorageService forecastStorageService;

	@Inject
	@Named("cspStorageService")
	CspStorageService cspStorageService;

	@Inject
	@Named("topologyService")
	TopologyService topologyService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Override
	public void receiveUsage(CspUsageMessage cspUsageMessage) throws ActionException {
		Date currentDate = new Date();

		try {
			this.store(cspUsageMessage);

			String cspEan = cspUsageMessage.getCsp();
			String dsoEan = cspUsageMessage.getDso();

			topologyService.checkRelationCspAndDso(cspEan, dsoEan);

			this.updateCspUsages(cspUsageMessage, cspEan, dsoEan, currentDate);

			this.updateDeviationsFor(cspUsageMessage);
		} catch (ActionException actionException) {
			throw actionException;
		} catch (Exception e) {
			throw new ActionException(e.getMessage(), e);
		}
	}

	private void store(CspUsageMessage cspUsageMessage) throws ActionException {
		if (cspStorageService.findCspUsageMessageByEventId(cspUsageMessage.getEventId()) != null) {
			throw new ActionException(ActionExceptionType.MESSAGE, "Event not unique identified with id " + cspUsageMessage.getEventId());
		}

		cspStorageService.storeCspUsageMessage(cspUsageMessage);
	}

	private void updateCspUsages(CspUsageMessage cspUsageMessage, String cspEan, String dsoEan, Date currentDate) throws ActionException {
		int blockMinutes = Integer.parseInt(generalService.getConfigurationParameter("ForecastBlockMinutes").getValue());
		TimeBlock timeBlockOfStart;
		TimeBlock timeBlockOfEnd;
		List<CspUsage> cspUsages = cspUsageMessage.getCspUsages();
		for (CspUsage cspUsage : cspUsages) {
			String cableId = cspUsage.getCableCsp().getCable().getCableId();

			topologyService.checkCableRelatedToCspAndDso(cableId, cspEan, dsoEan);

			for (CspUsageValue cspUsageValue : cspUsage.getCspUsageValues()) {
				if (cspUsageValue.getStartedAt().after(currentDate) || cspUsageValue.getEndedAt().after(currentDate)) {
					throw new ActionException(ActionExceptionType.DATA, "Usage reported for future. Reported start: "
							+ cspUsageValue.getStartedAt() + "; Reported end: " + cspUsageValue.getEndedAt() + "; Now: " + currentDate);
				}

				timeBlockOfStart = TimeBlock.createForm(cspUsageValue.getStartedAt(), blockMinutes);
				timeBlockOfEnd = TimeBlock.createForm(cspUsageValue.getEndedAt(), blockMinutes);
				if (!timeBlockOfStart.equals(timeBlockOfEnd)) {
					throw new ActionException(ActionExceptionType.DATA, "Usage reported over block boundaries. Reported start: "
							+ cspUsageValue.getStartedAt() + "; Reported end: " + cspUsageValue.getEndedAt() + "; Block size (minutes): "
							+ blockMinutes);
				}

				cspUsageValue.setDay(timeBlockOfStart.getDay());
				cspUsageValue.setDayOfWeek(ForecastBlockUtil.getDayOfWeek(cspUsageValue.getDay()));
				cspUsageValue.setHour(timeBlockOfStart.getHour());
				cspUsageValue.setBlock(timeBlockOfStart.getBlockOfHour());
			}
		}
	}

	private void updateDeviationsFor(CspUsageMessage cspUsageMessage) throws ActionException {
		cspStorageService.storeCspUsages(cspUsageMessage);
		cspStorageService.setCspUsageMessageSuccessful(cspUsageMessage.getId());
		cspUsageMessage.setSuccessful(true);

		// start deviation calculation for this usage message async
		self.determineCspDeviations(cspUsageMessage.getEventId());
	}

	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ActionContext(event = ActionEvent.CSP_DEVIATION_DETERMINATION, doRethrow = false)
	public void determineCspDeviations(String cspUsageMessageEventId) throws ActionException {
		CspUsageMessage cspUsageMessage = cspStorageService.findCspUsageMessageByEventId(cspUsageMessageEventId);

		Csp csp = topologyStorageService.findCspByEan13(cspUsageMessage.getCsp());
		nl.enexis.scip.action.ActionContext.addActionTarget(csp, ActionTargetInOut.IN);

		List<CspUsage> cspUsages = cspUsageMessage.getCspUsages();

		for (CspUsage cspUsage : cspUsages) {
			Cable cable = cspUsage.getCableCsp().getCable();

			List<CspUsageValue> cspUsageValues = cspUsage.getCspUsageValues();

			for (CspUsageValue cspUsageValue : cspUsageValues) {
				CspForecastValue cfv = forecastStorageService.getLastCspForecastValueForCspUsageValue(cspUsageValue);

				BigDecimal deviation = new BigDecimal("0.00");

				if (BehaviourType.SC.equals(cable.getBehaviour())) {
					if (cfv != null) {
						deviation = cspUsageValue.getAverageCurrent().subtract(cfv.getAssigned());
					} else {
						deviation = cspUsageValue.getAverageCurrent();
					}
				} else if (BehaviourType.SU.equals(cable.getBehaviour())) {
					if (cfv != null) {
						deviation = cspUsageValue.getEnergyConsumption().subtract(cfv.getAssigned());
					} else {
						deviation = cspUsageValue.getEnergyConsumption();
					}
				}

				cspUsageValue.setDeviation(deviation);
				cspStorageService.updateCspUsageValue(cspUsageValue);
			}
		}
	}
}