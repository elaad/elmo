package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.dao.CableForecastDao;
import nl.enexis.scip.dao.CableForecastDeviationDayDao;
import nl.enexis.scip.dao.CableForecastInputDao;
import nl.enexis.scip.dao.CableForecastOutputDao;
import nl.enexis.scip.dao.CspForecastDao;
import nl.enexis.scip.dao.CspForecastMessageDao;
import nl.enexis.scip.dao.CspForecastValueDao;
import nl.enexis.scip.dao.CspGetForecastMessageDao;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastDeviationDay;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspGetForecastMessage;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.enumeration.DeviationDayType;

@Named("forecastStorageService")
@Stateless
public class ForecastStorageServiceImpl implements ForecastStorageServiceLocal {

	@Inject
	CspForecastDao cspForecastDao;

	@Inject
	CspForecastMessageDao cspForecastMessageDao;

	@Inject
	CspGetForecastMessageDao cspGetForecastMessageDao;

	@Inject
	CspForecastValueDao cspForecastValueDao;

	@Inject
	CableForecastDao cableForecastDao;

	@Inject
	CableForecastInputDao cableForecastInputDao;

	@Inject
	CableForecastOutputDao cableForecastOutputDao;

	@Inject
	CableForecastDeviationDayDao cableForecastDeviationDayDao;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Override
	public List<CableForecastInput> getCableForecastInputs(CableForecast cf) throws ActionException {
		try {
			return cableForecastInputDao.getInputsForCableForecast(cf);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding inputs for CableForecast!  CableForecast: " + cf.getId(), e);
		}
	}

	@Override
	public CableForecast getFirstCableForecastOfDay(Cable cable, Date day) throws ActionException {
		try {
			return cableForecastDao.getFirstCableForecastOfDay(cable, day);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding first CableForecast entity for day! Cable: " + cable.getCableId() + "; Day: " + day,
					e);
		}
	}

	@Override
	public CableForecast findCableForecastById(String cableForecastId) throws ActionException {
		try {
			return cableForecastDao.find(cableForecastId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CableForecast entity! Id: " + cableForecastId, e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeCableForecast(CableForecast cableForecast) throws ActionException {
		try {
			cableForecastDao.create(cableForecast);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error storing new CableForecast entity! Id: "
					+ cableForecast.getId() + "; CalculationDate: " + cableForecast.getDatetime(), e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateCableForecast(CableForecast cableForecast) throws ActionException {
		try {
			cableForecastDao.update(cableForecast);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating CableForecast entity! Id: "
					+ cableForecast.getId() + "; CalculationDate: " + cableForecast.getDatetime(), e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void addOutputsForCableForecast(CableForecast cableForecast) throws ActionException {
		try {
			CableForecast cableForecastEntity = cableForecastDao.find(cableForecast.getId());

			List<CableForecastOutput> outputs = cableForecast.getCableForecastOutputs();

			for (CableForecastOutput output : outputs) {
				CableForecastOutput outputEntity = new CableForecastOutput();
				outputEntity.setCableForecast(cableForecastEntity);
				outputEntity.setDay(output.getDay());
				outputEntity.setDayOfWeek(output.getDayOfWeek());
				outputEntity.setHour(output.getHour());
				outputEntity.setBlock(output.getBlock());
				outputEntity.setAvailableCapacity(output.getAvailableCapacity());
				outputEntity.setUsageExclCars1(output.getUsageExclCars1());
				outputEntity.setUsageExclCars2(output.getUsageExclCars2());
				outputEntity.setUsageExclCars3(output.getUsageExclCars3());
				outputEntity = cableForecastOutputDao.create(outputEntity);
				cableForecastEntity.getCableForecastOutputs().add(outputEntity);
			}
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error adding CableForecastOutput entities to CableForecast entity! Id: " + cableForecast.getId()
							+ "; CalculationDate: " + cableForecast.getDatetime(),
					e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeCspForecasts(List<CspForecast> cspForecasts) throws ActionException {
		try {
			for (CspForecast cspForecast : cspForecasts) {
				CspForecast cspForecastEntity = new CspForecast();

				CableCsp cableCspEntity = topologyStorageService.findCableCspByCableAndCsp(
						cspForecast.getCableCsp().getCsp(), cspForecast.getCableCsp().getCable());
				cspForecastEntity.setCableCsp(cableCspEntity);
				cspForecastEntity.setCableForecast(cspForecast.getCableForecast());
				cspForecastEntity.setCalculatedAt(cspForecast.getCalculatedAt());
				cspForecastEntity.setNumberOfBlocks(cspForecast.getNumberOfBlocks());
				cspForecastEntity.setChange(cspForecast.getChange());
				cspForecastEntity.setChangeValue(cspForecast.getChangeValue());

				cspForecastEntity = cspForecastDao.create(cspForecastEntity);

				List<CspForecastValue> cspForecastValues = cspForecast.getCspForecastValues();
				for (CspForecastValue cspForecastValue : cspForecastValues) {
					CspForecastValue cspForecastValueEntity = new CspForecastValue();

					cspForecastValueEntity.setCspForecast(cspForecastEntity);
					cspForecastValueEntity.setAssigned(cspForecastValue.getAssigned());
					cspForecastValueEntity.setRemaining(cspForecastValue.getRemaining());

					CableForecastOutput cableForecastOutputEntity = cableForecastOutputDao
							.find(cspForecastValue.getCableForecastOutput().getId());
					cspForecastValueEntity.setCableForecastOutput(cableForecastOutputEntity);

					cspForecastValueEntity.setCspAdjustmentValue(cspForecastValue.getCspAdjustmentValue());

					cspForecastEntity.getCspForecastValues().add(cspForecastValueEntity);

					CspForecastValue cfv = cspForecastValueDao.create(cspForecastValueEntity);

					if (cspForecastValue.isChangeCheckBlock()) {
						cspForecastEntity.setChangeBlock(cfv);
						cspForecastDao.update(cspForecastEntity);
					}
				}

			}
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error storing new CspForecast(Value) entity!", e);
		}

	}

	@Override
	public List<CspForecast> getCspForcastForCableCsp(CableCsp cableCsp) throws ActionException {
		try {
			return cspForecastDao.getCspForcastForCableCsp(cableCsp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error fetching CspForecast entitities for CableCsp " + cableCsp.getId() + "!", e);
		}
	}

	@Override
	public CspForecast getLastCspForcastForCableCsp(CableCsp cableCsp) throws ActionException {
		try {
			return cspForecastDao.getLastCspForcastForCableCsp(cableCsp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error fetching last not send CspForecast entity for CableCsp " + cableCsp.getId() + "!", e);
		}
	}

	@Override
	public List<CspForecast> getCspForcastByCspForecastMessageId(Long cspForecastMessageId) throws ActionException {
		try {
			return cspForecastDao.getByCspForecastMessageId(cspForecastMessageId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error fetching CspForecast entitities for CspForecastMessage " + cspForecastMessageId + "!", e);
		}
	}

	@Override
	public List<CspForecastValue> getCspForecastValuesByCspForecastId(Long cspForecastId, boolean fetchCableForecast,
			boolean fetchCspAdjustmentValue) throws ActionException {
		try {
			List<CspForecastValue> cfvvalues = cspForecastValueDao.getByCspForecastId(cspForecastId);
			if (fetchCableForecast || fetchCspAdjustmentValue) {
				CspAdjustmentValue cav;
				for (CspForecastValue cfv : cfvvalues) {
					if (fetchCableForecast)
						cfv.getCableForecastOutput().getCableForecast().getBlockMinutes();
					if (fetchCspAdjustmentValue) {
						cav = cfv.getCspAdjustmentValue();
						if (cav != null)
							cav.getAssigned();
					}
					// needed because these relations are LAZY
				}
			}
			return cfvvalues;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error fetching CspForecastValue entities for CspForecast " + cspForecastId + "!", e);
		}
	}

	@Override
	public List<CspForecast> getCspForcastByDate(Date dateFrom, Date dateTo, boolean getCableCsp)
			throws ActionException {
		try {
			List<CspForecast> forecasts = cspForecastDao.getByDate(dateFrom, dateTo);
			if (getCableCsp) {
				for (CspForecast cf : forecasts) {
					cf.getCableCsp().getCable().getCableId(); // need this
																// because
																// CableCsp is
																// not EAGER
				}
			}
			return forecasts;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error fetching CspForecast entities between " + dateFrom + " and " + dateTo + "!", e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeCspForecastMessage(CspForecastMessage cspForecastMessage) throws ActionException {
		try {
			cspForecastMessageDao.create(cspForecastMessage);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error storing new CspForecastMessage entity! Csp: " + cspForecastMessage.getCsp() + "; Dso: "
							+ cspForecastMessage.getDso() + "; EventId: " + cspForecastMessage.getEventId()
							+ "; Priority: " + cspForecastMessage.getPriority(),
					e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateCspForecastMessageWithCspForecasts(CspForecastMessage cspForecastMessage) throws ActionException {
		try {
			CspForecastMessage cspForecastMessageEntity = cspForecastMessageDao.find(cspForecastMessage.getId());

			List<CspForecast> cspForecasts = cspForecastMessage.getCspForecasts();
			for (CspForecast cspForecast : cspForecasts) {
				CspForecast cspForecastEntity = cspForecastDao.find(cspForecast.getId());
				cspForecastMessageEntity.getCspForecasts().add(cspForecastEntity);
			}

			cspForecastMessageEntity = cspForecastMessageDao.update(cspForecastMessageEntity);

		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error storing new CspForecastMessage entity! Csp: " + cspForecastMessage.getCsp() + "; Dso: "
							+ cspForecastMessage.getDso() + "; EventId: " + cspForecastMessage.getEventId()
							+ "; Priority: " + cspForecastMessage.getPriority(),
					e);
		}
	}

	@Override
	public void updateCspForecastValue(CspForecastValue cfv) throws ActionException {
		try {
			cspForecastValueDao.update(cfv);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error updating new CspForecastValue entity! Id: " + cfv.getId(), e);
		}
	}

	@Override
	public List<CspForecastMessage> getMessagesForCspForecast(CspForecast cspForecast) throws ActionException {
		try {
			return cspForecastMessageDao.getMessagesForCspForecast(cspForecast);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CspForecastMessage entity for CspForecast! Id: " + cspForecast.getId(), e);
		}
	}

	@Override
	public List<CspForecastMessage> getCspForecastMessagesByDateCspAndCable(String csp, Date dateFrom, Date dateTo,
			Cable cable) throws ActionException {
		try {
			return cspForecastMessageDao.getMessagesByDateCspAndCable(csp, dateFrom, dateTo, cable);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CspForecastMessage entity for csp '" + csp + "'", e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeCspGetForecastMessage(CspGetForecastMessage cspGetForecastMessage) throws ActionException {
		try {
			cspGetForecastMessageDao.create(cspGetForecastMessage);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error storing new CspGetForecastMessage entity!",
					e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void setCspGetForecastMessageSuccessful(Long id) throws ActionException {
		try {
			CspGetForecastMessage entity = cspGetForecastMessageDao.find(id);
			entity.setSuccessful(true);
			cspGetForecastMessageDao.update(entity);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error setting new CspGetForecastMessage entity to successful! Id: " + id, e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void setCspForecastMessageSuccessful(Long id) throws ActionException {
		try {
			CspForecastMessage entity = cspForecastMessageDao.find(id);
			entity.setSuccessful(true);
			cspForecastMessageDao.update(entity);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error setting new CspForecastMessage entity to successful!", e);
		}
	}

	@Override
	public List<CableForecast> searchCableForecasts(Cable cable, Date dateFrom, Date dateTo) throws ActionException {
		try {
			List<CableForecast> cfs = cableForecastDao.search(cable, dateFrom, dateTo);
			return cfs;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error searching Error entities!", e);
		}
	}

	@Override
	public CspGetForecastMessage findCspGetForecastMessageByEventId(String eventId) throws ActionException {
		try {
			return cspGetForecastMessageDao.findByEventId(eventId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CspGetForecastMessage entity by eventId! eventId: " + eventId, e);
		}
	}

	@Override
	public CableForecastOutput getLastCableForecastOutput(CableForecast cableForecast, Date day, int hour, int block)
			throws ActionException {
		try {
			// cable = cableDao.find(cable.getCableId());
			return cableForecastOutputDao.getLastCableForecastOutput(cableForecast, day, hour, block);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding last CableForecastOutput entity for forecast block of cable! Cable: "
							+ cableForecast.getCable().getCableId() + " - day: " + day + " - hour: " + hour
							+ " - block: " + block,
					e);
		}
	}

	@Override
	public CspForecastValue getLastCspForecastValueForCableForecastOutput(CableForecastOutput cableForecastOutput,
			Csp csp) throws ActionException {
		try {
			return cspForecastValueDao.getLastCspForecastValueForCableForecastOutput(cableForecastOutput, csp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding last CspForecastValue entity for CableForecastOutput entity! CableForecastOutput id: "
							+ cableForecastOutput.getId() + "; Csp: " + csp.getEan13(),
					e);
		}
	}

	@Override
	public CableForecast getLastCableForecast(Cable cable) throws ActionException {
		try {
			return cableForecastDao.getLastCableForecast(cable);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding last CableForecast entity for cable! Cable: " + cable.getCableId(), e);
		}
	}

	@Override
	public CableForecastInput findCableForecastInputForBlock(Cable cable, Date blockDay, Integer blockHour,
			Integer blockNumber) throws ActionException {
		try {
			return cableForecastInputDao.findCableForecastInputForBlock(cable, blockDay, blockHour, blockNumber);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CableForecastInput entity for cable and block! Cable: " + cable.getCableId(), e);
		}
	}

	@Override
	public List<CableForecastInput> getForForecastInput(Cable cable, int totalNumber) throws ActionException {
		try {
			return cableForecastInputDao.getForForecastInput(cable, totalNumber);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CableForecastInput entities for forecast of cable! Cable: " + cable.getCableId(), e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void createCableForecastInput(CableForecastInput input) throws ActionException {
		try {
			cableForecastInputDao.create(input);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error creating CableForecastInput entity! Cable: " + input.getCable().getCableId(), e);
		}
	}

	@Override
	public CableForecastInput getLastCableForecastInput(Cable cable, Date fromDate) throws ActionException {
		try {
			return cableForecastInputDao.findLastCableForecastInputAfterDate(cable, fromDate);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding CableForecastInput entity! " + cable,
					e);
		}
	}

	@Override
	public CableForecastDeviationDay getLastCableForecastDeviationDay(Cable cable) throws ActionException {
		try {
			return cableForecastDeviationDayDao.getLastForCable(cable);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error getting deviation days info for cable: " + cable.getCableId(), e);
		}
	}

	@Override
	public List<CableForecastDeviationDay> getNotDeviatedCableForecastDeviationDays(Cable cable, int maxResults)
			throws ActionException {
		try {
			return cableForecastDeviationDayDao.getLastNotDeviatedDays(cable, maxResults);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error getting last " + maxResults
					+ " not deviated deviation days info for cable: " + cable.getCableId(), e);
		}
	}

	@Override
	public void storeCableForecastDeviationDay(CableForecastDeviationDay cfdd) throws ActionException {
		try {
			cableForecastDeviationDayDao.create(cfdd);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error storing deviation day " + cfdd.getDay() + " for cable: " + cfdd.getCable().getCableId(), e);
		}
	}

	@Override
	public boolean isCableForecastDeviationDay(Cable cable, Date day) throws ActionException {
		try {
			CableForecastDeviationDay cfdd = cableForecastDeviationDayDao.getForCableAndDay(cable, day);
			if (cfdd == null || cfdd.getType().equals(DeviationDayType.NO_DEVIATION)) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error getting deviation day for " + day + " and cable: " + cable.getCableId(), e);
		}
	}

	@Override
	public List<CspForecast> getLastCspForecastsForCable(Cable cable) throws ActionException {
		try {
			return cspForecastDao.getLastCspForecastsForCable(cable);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error getting last csp forecasts for cable: " + cable.getCableId(), e);
		}
	}

	@Override
	public CspForecast getCspForcast(Long id) throws ActionException {
		try {
			return cspForecastDao.find(id);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error getting csp forecast: " + id, e);
		}
	}

	@Override
	public CspForecastValue getLastCspForecastValueForCspUsageValue(CspUsageValue cspUsageValue)
			throws ActionException {
		return cspForecastValueDao.getLastCspForecastValueForCspUsageValue(cspUsageValue);
	}

}
