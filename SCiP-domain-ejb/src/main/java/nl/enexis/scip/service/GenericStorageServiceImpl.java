package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.dao.AdminUserDao;
import nl.enexis.scip.dao.AdminUserNotificationDao;
import nl.enexis.scip.dao.AuditLogItemDao;
import nl.enexis.scip.dao.ConfigurationParameterDao;
import nl.enexis.scip.model.AdminUser;
import nl.enexis.scip.model.AdminUserNotification;
import nl.enexis.scip.model.AuditLogItem;
import nl.enexis.scip.model.ConfigurationParameter;
import nl.enexis.scip.model.enumeration.NotificationInterval;

@Named("genericStorageService")
@Stateless
public class GenericStorageServiceImpl implements GenericStorageService {

	@Inject
	AdminUserDao adminUserDao;
	
	@Inject
	AuditLogItemDao auditLogItemDao;

	@Inject
	AdminUserNotificationDao adminUserNotificationDao;

	@Inject
	ConfigurationParameterDao configurationParameterDao;

	
	@Override
	public ConfigurationParameter findConfigurationParameter(String name) throws ActionException {
		try {
			return configurationParameterDao.find(name);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding ConfigurationParameter entity! Name: " + name, e);
		}
	}

	@Override
	public List<AdminUser> getAllAdminUsers() throws ActionException {
		try {
			List<AdminUser> adminUsers = adminUserDao.getAll();

			return adminUsers;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all AdminUser entities!", e);
		}
	}


	@Override
	public List<ConfigurationParameter> getAllConfigurationParameters() throws ActionException {
		try {
			List<ConfigurationParameter> configs = configurationParameterDao.getAll();
			return configs;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error fetching all ConfigurationParameter entities!", e);
		}
	}

	@Override
	public ConfigurationParameter updateConfigurationParameter(ConfigurationParameter config) throws ActionException {
		try {
			return configurationParameterDao.update(config);
		} catch (OptimisticLockException e) {
			throw new ActionException(ActionExceptionType.STORAGE, "ConfigurationParameter: " + config.getName()
					+ " Not updated, was changed by another user", e);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error updating ConfigurationParameter entity! Name: " + config.getName(), e);
		}
	}

	@Override
	public AdminUser findAdminUserByUserId(String userId) throws ActionException {
		try {
			return adminUserDao.find(userId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding AdminUser entity! userId: " + userId,
					e);
		}
	}

	@Override
	public AdminUser updateAdminUser(AdminUser user) throws ActionException {
		try {
			return adminUserDao.update(user);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating AdminUser entity! user: "
					+ user.getUserId(), e);
		}
	}

	@Override
	public AdminUserNotification createAdminUserNotification(AdminUserNotification notification) throws ActionException {
		try {
			AdminUser user = this.findAdminUserByUserId(notification.getUser().getUserId());
			notification.setUser(user);
			return adminUserNotificationDao.create(notification);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error create AdminUserNotification entity! notification: " + notification, e);
		}
	}

	@Override
	public AdminUserNotification updateAdminUserNotification(AdminUserNotification notification) throws ActionException {
		try {
			return adminUserNotificationDao.update(notification);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error create AdminUserNotification entity! notification: " + notification, e);
		}
	}

	@Override
	public void deleteAdminUserNotification(AdminUserNotification notification) throws ActionException {
		try {
			notification = adminUserNotificationDao.merge(notification);
			adminUserNotificationDao.delete(notification);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error delete AdminUserNotification entity! notification: " + notification, e);
		}
	}

	@Override
	public List<AdminUserNotification> findActiveDirectAdminUserNotifications(ActionEvent type) throws ActionException {
		try {
			return adminUserNotificationDao.findActiveDirectAdminUserNotifications(type);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding direct active AdminUserNotification entities! event: " + type.toString(), e);
		}
	}

	@Override
	public void deleteAdminUser(AdminUser user) throws ActionException {
		try {
			user = adminUserDao.find(user.getUserId());
			adminUserDao.delete(user);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error removing AdminUser entities! event: "
					+ user.toString(), e);
		}
	}

	@Override
	public List<AdminUserNotification> findActiveIntervalAdminUserNotifications(NotificationInterval interval)
			throws ActionException {
		try {
			return adminUserNotificationDao.findActiveIntervalAdminUserNotifications(interval);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding interval active AdminUserNotification entities! interval: " + interval.toString(), e);
		}
	}
	
	
	@Override
	public List<AuditLogItem> getAllAuditLogItems() throws ActionException {
		try {
			return auditLogItemDao.getAll();
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching AuditLogItems entities!", e);
		}
	}
	
	
	@Override
	public List<AuditLogItem> getAuditLogItemsBetweenDates(Date startDate, Date endDate) throws ActionException {
		try {
			return auditLogItemDao.getBetweenDates(startDate, endDate);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching AuditLogItems entities!", e);
		}
	}
	
	
	@Override
	public List<String> getDistinctAuditLogEntities() throws ActionException {
		try {
			return auditLogItemDao.getDistinctEntities();
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching distinct AuditLog entities!", e);
		}
	}

}
