package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.dao.MeasurementDao;
import nl.enexis.scip.dao.MeasurementValueDao;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Measurement;
import nl.enexis.scip.model.MeasurementValue;

@Named("measurementStorageService")
@Stateless
public class MeasurementStorageServiceImpl implements MeasurementStorageService {

    @Inject
    MeasurementValueDao measurementValueDao;
    
    @Inject
    MeasurementDao measurementDao;

    @Override
    public void storeMeasurements(Map<Long, Measurement> measurements) throws ActionException {
        try {
            for (Long measurementTimestamp : measurements.keySet()) {
                Measurement measurement = measurements.get(measurementTimestamp);
                measurement = measurementDao.create(measurement);
                
                for (MeasurementValue measurementValue : measurement.getMeasurementValues()) {
                    measurementValueDao.create(measurementValue);
                }
            }
        } catch (Exception e) {
            throw new ActionException(ActionExceptionType.STORAGE, "Error storing Measurement(value)s entities!", e);
        }
    }
    
    @Override
    public void mergeMeasurements(Map<Long, Measurement> measurements) throws ActionException {
        try {
            for (Long measurementTimestamp : measurements.keySet()) {
                Measurement measurement = measurements.get(measurementTimestamp);
                Measurement mergedmeasurement = measurementDao.merge(measurement);
                
                //mergedmeasurement.setMeasurementValues(measurement.getMeasurementValues());
                for (MeasurementValue mv : measurement.getMeasurementValues()) {
                    mv.setMeasurement(mergedmeasurement); 
                    measurementValueDao.merge(mv);
                }
            }
        } catch (Exception e) {
            throw new ActionException(ActionExceptionType.STORAGE, "Error merging Measurement(value)s entities!", e);
        }
    }

    @Override
    public Measurement getMeasurementForCableAndDate(Date date, Cable cable)
            throws ActionException {
        try {
            return measurementDao.getMeasurementForCableAndDate(date, cable);
        } catch (Exception e) {
            throw new ActionException(ActionExceptionType.STORAGE, "Error measurements 4Top cable!", e);
        }
    }
    
    @Override
    public List<Measurement> getMeasurementForCableBetweenDates(Date startDate, Date endDate, Cable cable)
            throws ActionException {
        try {
            List<Measurement> measurements = measurementDao.getMeasurementForCableBetweenDates(startDate, endDate, cable);
            return measurements;
        } catch (Exception e) {
            throw new ActionException(ActionExceptionType.STORAGE, "Error measurements 4Top cable!", e);
        }
    }
    
    @Override
    public List<Measurement> getMeasurementForProviderIdBetweenDates(Date startDate, Date endDate, String providerId)
            throws ActionException {
        try {
            List<Measurement> measurements = measurementDao.getMeasurementForProviderIdBetweenDates(startDate, endDate, providerId);
            return measurements;
        } catch (Exception e) {
            throw new ActionException(ActionExceptionType.STORAGE, "Error measurements 4Top cable!", e);
        }
    }

    @Override
    public List<Measurement> getMeasurementBetweenDates(Date startDate, Date endDate)
            throws ActionException {
        try {
            List<Measurement> measurements = measurementDao.getMeasurementBetweenDates(startDate, endDate);
            return measurements;
        } catch (Exception e) {
            throw new ActionException(ActionExceptionType.STORAGE, "Error measurements 4Top cable!", e);
        }
    }

    @Override
    public Measurement getClosestCompleteMeasurement(Measurement measurement,
            boolean doForward) throws ActionException {
        try {
            return measurementDao.findClosestCompleteMeasurement(measurement,doForward);
        } catch (Exception e) {
            throw new ActionException(ActionExceptionType.STORAGE,
                    "Error finding closest complete Measurement entity! " + measurement, e);
        }
    }

}
