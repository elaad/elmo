package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.OptimisticLockException;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.dao.CableCspDao;
import nl.enexis.scip.dao.CableDao;
import nl.enexis.scip.dao.CspConnectionDao;
import nl.enexis.scip.dao.CspDao;
import nl.enexis.scip.dao.DsoCspDao;
import nl.enexis.scip.dao.DsoDao;
import nl.enexis.scip.dao.MeasurementProviderDao;
import nl.enexis.scip.dao.MeterDao;
import nl.enexis.scip.dao.TransformerDao;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspConnection;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.DsoCsp;
import nl.enexis.scip.model.MeasurementProvider;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.model.Transformer;
import nl.enexis.scip.model.enumeration.BehaviourType;

@Named("topologyStorageService")
@Stateless
public class TopologyStorageServiceImpl implements TopologyStorageServiceLocal {

	@Inject
	CableCspDao cableCspDao;

	@Inject
	MeterDao meterDao;

	@Inject
	CableDao cableDao;

	@Inject
	CspDao cspDao;

	@Inject
	MeasurementProviderDao mpDao;

	@Inject
	DsoCspDao dsoCspDao;

	@Inject
	DsoDao dsoDao;

	@Inject
	CspConnectionDao cspConnectionDao;

	@Inject
	TransformerDao transformerDao;

	@Override
	public Csp findCspByEan13(String cspEan) throws ActionException {
		try {
			return cspDao.find(cspEan);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding Csp entity! Csp: " + cspEan, e);
		}
	}

	@Override
	public Dso findDsoByEan13(String dsoEan) throws ActionException {
		try {
			return dsoDao.find(dsoEan);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding Dso entity! Dso: " + dsoEan, e);
		}
	}

	@Override
	public CableCsp findCableCspByCableAndCsp(Csp csp, Cable cable) throws ActionException {
		try {
			csp = cspDao.find(csp.getEan13());

			cable = cableDao.find(cable.getCableId());

			return cableCspDao.getCableCspByCableAndCsp(cable, csp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding CableCsp entity! Csp: "
					+ csp.getEan13() + " - Cable: " + cable.getCableId(), e);
		}
	}

	@Override
	public List<Cable> getAllCables() throws ActionException {
		return this.getAllCables(false);
	}

	@Override
	public List<Cable> getAllActiveCables(boolean isActive) throws ActionException {
		try {
			List<Cable> cables = cableDao.getAllActive(isActive);

			return cables;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all Cable entities!", e);
		}
	}

	@Override
	public List<Cable> getAllCables(boolean showDeleted) throws ActionException {
		try {
			List<Cable> cables = cableDao.getAll(showDeleted);

			return cables;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all Cable entities!", e);
		}
	}

	@Override
	public List<Dso> getAllDsos() throws ActionException {
		return this.getAllDsos(false);
	}

	@Override
	public List<Dso> getAllDsos(boolean showDeleted) throws ActionException {
		try {
			List<Dso> dsos = dsoDao.getAll(showDeleted);

			return dsos;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all Dso entities!", e);
		}
	}

	@Override
	public List<Csp> getAllCsps() throws ActionException {
		return this.getAllCsps(false);
	}

	@Override
	public List<Csp> getAllCsps(boolean showDeleted) throws ActionException {
		try {
			List<Csp> csps = cspDao.getAll(showDeleted);
			return csps;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all CSP entities!", e);
		}
	}

	@Override
	public List<MeasurementProvider> getAllMPs() throws ActionException {
		return this.getAllMPs(false);
	}

	@Override
	public List<MeasurementProvider> getAllMPs(boolean showDeleted) throws ActionException {
		try {
			List<MeasurementProvider> mps = mpDao.getAll(showDeleted);
			return mps;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all MP entities!", e);
		}
	}

	@Override
	public List<DsoCsp> getAllDsoCsps() throws ActionException {
		return this.getAllDsoCsps(false);
	}

	@Override
	public List<DsoCsp> getAllDsoCsps(boolean showDeleted) throws ActionException {
		try {
			List<DsoCsp> dsoCsps = dsoCspDao.getAll(showDeleted);
			return dsoCsps;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all DSO CSPs entities!", e);
		}
	}

	@Override
	public List<CableCsp> getAllCableCsps() throws ActionException {
		return this.getAllCableCsps(false);
	}

	@Override
	public List<CableCsp> getAllCableCsps(boolean showDeleted) throws ActionException {
		try {
			List<CableCsp> cableCsps = cableCspDao.getAll(showDeleted);
			return cableCsps;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all Cable CSPs entities!", e);
		}
	}

	@Override
	public List<CspConnection> getAllCspConnections() throws ActionException {
		return this.getAllCspConnections(false);
	}

	@Override
	public List<CspConnection> getAllCspConnections(boolean showDeleted) throws ActionException {
		try {
			List<CspConnection> cspConnections = cspConnectionDao.getAll(showDeleted);
			return cspConnections;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all CSP Connections entities!", e);
		}
	}

	@Override
	public Cable findCableByCableId(String cableId) throws ActionException {
		try {
			return cableDao.find(cableId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding Cable entity! Cable: " + cableId, e);
		}
	}

	@Override
	public List<Cable> getCablesByCspEan13(String ean13) throws ActionException {
		try {
			return cableDao.getByCspEan13(ean13);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error getting cables for Csp: " + ean13, e);
		}
	}

	@Override
	public List<Csp> getCspsByCableId(String cableId) throws ActionException {
		try {
			return cspDao.getByCableId(cableId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error getting CSPs for cable: " + cableId, e);
		}
	}

	@Override
	public Cable getCompleteCableTopology(String cableId) throws ActionException {
		Cable cable = this.findCableByCableId(cableId);
		for (CableCsp cableCsp : cable.getCableCsps()) {
			cableCsp.getCspConnections();
			for (DsoCsp dsoCsp : cableCsp.getCsp().getDsoCsps()) {
				dsoCsp.getCsp();
			}
		}
		return cable;
	}

	@Override
	public Csp getCompleteCspTopology(String cspEan) throws ActionException {
		Csp csp = this.findCspByEan13(cspEan);
		for (CableCsp cableCsp : csp.getCableCsps()) {
			cableCsp.getCspConnections();
		}
		return csp;
	}

	@Override
	public List<Transformer> getAllTransformers() throws ActionException {
		return this.getAllTransformers(false);
	}

	@Override
	public List<Transformer> getAllTransformers(boolean showDeleted) throws ActionException {
		try {
			return transformerDao.getAll(showDeleted);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all Transformer entities!", e);
		}
	}

	@Override
	public List<Meter> getAllMeters() throws ActionException {
		return this.getAllMeters(false);
	}

	@Override
	public List<Meter> getAllMeters(boolean showDeleted) throws ActionException {
		try {
			return meterDao.getAll(showDeleted);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error fetching all Meter entities!", e);
		}
	}

	@Override
	public Meter findMeterByMeterId(String meterId) throws ActionException {
		try {
			Meter meter = meterDao.find(meterId);
			return meter;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding Meter entity! Id: " + meterId, e);
		}
	}

	@Override
	public Dso createDso(Dso dso) throws ActionException {
		try {
			return dsoDao.create(dso);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating Dso entity! Ean13: "
					+ dso.getEan13(), e);
		}
	}

	@Override
	public Transformer findDsoByTrafoId(String trafoId) throws ActionException {
		try {
			return transformerDao.find(trafoId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding Transformer entity! Id: " + trafoId,
					e);
		}
	}

	@Override
	public Transformer createTransformer(Transformer transformer) throws ActionException {
		try {
			return transformerDao.create(transformer);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating Transformer entity! Id: "
					+ transformer.getTrafoId(), e);
		}
	}

	@Override
	public Cable createCable(Cable cable) throws ActionException {
		try {
			cable.setCspLastCapacityChange(new Date());
			Dso dso = dsoDao.find(cable.getDso().getEan13());
			cable.setDso(dso);

			Transformer transformer = transformerDao.find(cable.getTransformer().getTrafoId());
			cable.setTransformer(transformer);

			return cableDao.create(cable);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating Cable entity! Id: "
					+ cable.getCableId(), e);
		}
	}

	@Override
	public Meter createMeter(Meter meter) throws ActionException {
		try {
			Transformer transformer = transformerDao.find(meter.getTransformer().getTrafoId());
			meter.setTransformer(transformer);

			return meterDao.create(meter);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating Meter entity! Id: "
					+ meter.getMeterId(), e);
		}
	}

	@Override
	public Csp createCsp(Csp csp) throws ActionException {
		try {
			return cspDao.create(csp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating Csp entity! Ean13: "
					+ csp.getEan13(), e);
		}
	}

	@Override
	public CableCsp createCableCsp(CableCsp cableCsp) throws ActionException {
		try {
			Cable cable = cableDao.find(cableCsp.getCable().getCableId());
			cableCsp.setCable(cable);

			Csp csp = cspDao.find(cableCsp.getCsp().getEan13());
			cableCsp.setCsp(csp);

			return cableCspDao.create(cableCsp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating CableCsp entity! Cable: "
					+ cableCsp.getCable().getCableId() + " Csp: " + cableCsp.getCsp().getEan13(), e);
		}
	}

	@Override
	public CspConnection findCspConnectionByEan18(String ean18) throws ActionException {
		try {
			return cspConnectionDao.find(ean18);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding CspConnection entity! Ean18: "
					+ ean18, e);
		}
	}

	@Override
	public CspConnection createCspConnection(CspConnection cspConnection) throws ActionException {
		try {
			return cspConnectionDao.create(cspConnection);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating CspConnection entity! Ean18: "
					+ cspConnection.getEan18(), e);
		}
	}

	@Override
	public Dso updateDso(Dso dso) throws ActionException {
		try {
			return dsoDao.update(dso);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating Dso entity! Ean13: "
					+ dso.getEan13(), e);
		}
	}

	@Override
	public MeasurementProvider updateMP(MeasurementProvider mp) throws ActionException {
		try {
			return mpDao.update(mp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating MP entity! ID: " + mp.getId(), e);
		}
	}

	@Override
	public Cable updateCable(Cable cable) throws ActionException {
		try {
			return cableDao.update(cable);
		} catch (OptimisticLockException e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Cable: " + cable.getCableId()
					+ " Not updated, was changed by another user", e);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating Cable entity! Id: "
					+ cable.getCableId(), e);
		}
	}

	@Override
	public Meter updateMeter(Meter meter) throws ActionException {
		try {
			return meterDao.update(meter);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating Meter entity! Id: "
					+ meter.getMeterId(), e);
		}
	}

	@Override
	public Csp updateCsp(Csp csp) throws ActionException {
		try {
			return cspDao.update(csp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating Csp entity! Ean13: "
					+ csp.getEan13(), e);
		}
	}

	@Override
	public CspConnection updateCspConnection(CspConnection cspConnection) throws ActionException {
		try {
			return cspConnectionDao.update(cspConnection);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating CspConnection entity! Ean18: "
					+ cspConnection.getEan18(), e);
		}
	}

	@Override
	public CableCsp updateCableCsp(CableCsp cableCsp) throws ActionException {
		try {
			return cableCspDao.update(cableCsp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating CableCsp entity! Cable: "
					+ cableCsp.getCable().getCableId() + "; Csp: " + cableCsp.getCsp().getEan13(), e);
		}
	}

	@Override
	public DsoCsp findDsoCspByDsoAndCsp(Dso dso, Csp csp) throws ActionException {
		try {
			csp = cspDao.find(csp.getEan13());

			dso = dsoDao.find(dso.getEan13());

			return dsoCspDao.getDsoCspByDsoAndCsp(dso, csp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding DsoCsp entity! Csp: "
					+ csp.getEan13() + " - Dso: " + dso.getEan13(), e);
		}
	}

	@Override
	public DsoCsp updateDsoCsp(DsoCsp dsoCsp) throws ActionException {
		try {
			return dsoCspDao.update(dsoCsp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating DsoCsp entity! Dso: "
					+ dsoCsp.getDso().getEan13() + " - Csp: " + dsoCsp.getCsp().getEan13(), e);
		}
	}

	@Override
	public DsoCsp createDsoCsp(DsoCsp dsoCsp) throws ActionException {
		try {
			return dsoCspDao.create(dsoCsp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating DsoCsp entity! Dso: "
					+ dsoCsp.getDso().getEan13() + " - Csp: " + dsoCsp.getCsp().getEan13(), e);
		}
	}

	@Override
	public Transformer updateTransformer(Transformer transformer) throws ActionException {
		try {
			return transformerDao.update(transformer);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating Transformer entity! Id: "
					+ transformer.getTrafoId(), e);
		}
	}

	@Override
	public Transformer findTransformerByTrafoId(String trafoId) throws ActionException {
		try {
			Transformer transformer = transformerDao.find(trafoId);
			return transformer;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding Transformer entity! Id: " + trafoId,
					e);
		}
	}

	@Override
	public MeasurementProvider findMeasurementProviderById(String mpId) throws ActionException {
		try {
			MeasurementProvider measurementProvider = mpDao.find(mpId);
			return measurementProvider;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding MeasurementProvider entity! Id: "
					+ mpId, e);
		}
	}

	@Override
	public void updateTransformerLastMeasurement(String transformerId, Date lastMeasurement) throws ActionException {
		try {
			Transformer transformer = transformerDao.find(transformerId);
			transformer.setLastMeasurement(lastMeasurement);
			transformerDao.update(transformer);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error updating date lastMeasurement Transformer entity ! Id: " + transformerId, e);
		}
	}

	@Override
	public MeasurementProvider createMeasurementProvider(MeasurementProvider mp) throws ActionException {
		try {
			return mpDao.create(mp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error creating MeasurementProvider entity! ID: "
					+ mp.getId(), e);
		}
	}

	@Override
	public CableCsp findCableCspById(long id) throws ActionException {
		try {
			CableCsp cableCsp = cableCspDao.find(id);
			return cableCsp;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding Cable Csp entity! Id: " + id, e);
		}
	}

	@Override
	public void deleteCable(Cable cable) throws ActionException {
		try {
			cableDao.delete(cable);
		} catch (Exception e) {
			if (cable != null) {
				throw new ActionException(ActionExceptionType.STORAGE, "Error deleting Cable entity! ID: "
						+ cable.getCableId(), e);
			} else {
				throw new ActionException(ActionExceptionType.STORAGE, "Error deleting Cable entity! ID: Null pointer",
						e);
			}
		}
	}

	@Override
	public void deleteCsp(Csp csp) throws ActionException {
		try {
			cspDao.delete(csp);
		} catch (Exception e) {
			if (csp != null) {
				throw new ActionException(ActionExceptionType.STORAGE, "Error deleting CPS entity! ID: "
						+ csp.getEan13(), e);
			} else {
				throw new ActionException(ActionExceptionType.STORAGE, "Error deleting CPS entity! ID: Null pointer", e);
			}
		}
	}

	@Override
	public void deleteDso(Dso dso) throws ActionException {
		try {
			dsoDao.delete(dso);
		} catch (Exception e) {
			if (dso != null) {
				throw new ActionException(ActionExceptionType.STORAGE, "Error deleting DSO entity! ID: "
						+ dso.getEan13(), e);
			} else {
				throw new ActionException(ActionExceptionType.STORAGE, "Error deleting DSO entity! Null pointer", e);
			}
		}
	}

	@Override
	public void deleteMeasurementProvider(MeasurementProvider mp) throws ActionException {
		try {
			mpDao.delete(mp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error deleting Measurement Provider entity! ID: "
					+ mp.getId(), e);
		}
	}

	@Override
	public void deleteMeter(Meter meter) throws ActionException {
		try {
			meterDao.delete(meter);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error deleting Meter entity! ID: "
					+ meter.getMeterId(), e);
		}
	}

	@Override
	public void deleteTransformer(Transformer transformer) throws ActionException {
		try {
			transformerDao.delete(transformer);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error deleting Transformer entity! ID: "
					+ transformer.getTrafoId(), e);
		}
	}

	@Override
	public void deleteDsoCsp(DsoCsp dsoCsp) throws ActionException {
		try {
			dsoCspDao.delete(dsoCsp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error deleting DsoCsp entity! ID: "
					+ dsoCsp.getId(), e);
		}
	}

	@Override
	public void deleteCableCsp(CableCsp cableCsp) throws ActionException {
		try {
			cableCspDao.delete(cableCsp);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error deleting CableCsp entity! ID: "
					+ cableCsp.getId(), e);
		}
	}

	@Override
	public void deleteCspConnection(CspConnection cspConnection) throws ActionException {
		try {
			cspConnectionDao.delete(cspConnection);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error deleting CspConnection entity! ID: "
					+ cspConnection.getEan18(), e);
		}
	}

	@Override
	public List<CableCsp> getAllCompleteActiveCableCsps() throws ActionException {
		try {
			List<CableCsp> ccs1 = cableCspDao.getAllCompleteActiveCableCsps(BehaviourType.SC);
			List<CableCsp> ccs2 = cableCspDao.getAllCompleteActiveCableCsps(BehaviourType.SU);
			ccs1.addAll(ccs2);
			return ccs1;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error getting active Csps for complete active cables!", e);
		}
	}
}
