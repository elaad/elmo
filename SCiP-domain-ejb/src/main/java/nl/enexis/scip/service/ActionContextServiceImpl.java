package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.dao.ActionDao;
import nl.enexis.scip.dao.ActionMessageDao;
import nl.enexis.scip.dao.ActionMessageTargetDao;
import nl.enexis.scip.dao.ActionTargetDao;
import nl.enexis.scip.interceptor.annotation.Log;
import nl.enexis.scip.interceptor.annotation.enumeration.LogLevel;

@Named("actionContextService")
@Stateless
@Local
public class ActionContextServiceImpl implements ActionContextService {

	@Inject
	ActionDao actionDao;

	@Inject
	ActionTargetDao actionTargetDao;

	@Inject
	ActionMessageDao actionMessageDao;

	@Inject
	ActionMessageTargetDao actionMessageTargetDao;

	@Override
	@Log(logLevel = LogLevel.TRACE, logParams = false, logStackTrace = true)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveOrUpdateActionContext() {
		Action action = nl.enexis.scip.action.ActionContext.getAction();
		actionDao.merge(action);
	}

	@Override
	@Log(logLevel = LogLevel.TRACE, logParams = false, logStackTrace = true)
	public List<String> getLoggedActionTargets() {
		List<String> actionTargets = actionTargetDao.getLoggedActionTargets();
		List<String> actionMessageTargets = actionMessageTargetDao.getLoggedActionTargets();
		Set<String> targetSet = new HashSet<String>();
		targetSet.addAll(actionTargets);
		targetSet.addAll(actionMessageTargets);
		List<String> targets = new ArrayList<String>();
		targets.addAll(targetSet);
		Collections.sort(targets);
		return targets;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<String> getLoggedActors() {
		List<String> actors = actionDao.getLoggedActors();
		return actors;
	}

	@Override
	public List<ActionMessage> searchActionMessages(String actionId, Date fromDate, Date toDate, String targetType,
			String targetId, List<ActionEvent> selectedEvents, List<Level> selectedLevels,
			List<ActionExceptionType> selectedExceptionTypes, String actor) {
		return actionMessageDao.searchActionMessages(actionId, fromDate, toDate, targetType, targetId, selectedEvents,
				selectedLevels, selectedExceptionTypes, actor);
	}

	@Override
	public List<Action> getNotificationActions(ActionEvent event, Date fromDate, Date toDate, List<Level> levels) {
		return actionDao.getNotificationActions(event, fromDate, toDate, levels);
	}

}