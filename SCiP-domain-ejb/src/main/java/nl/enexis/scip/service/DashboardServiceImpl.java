package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.dao.ActionDao;
import nl.enexis.scip.dao.ActionMessageDao;
import nl.enexis.scip.dao.CableForecastInputDao;
import nl.enexis.scip.dao.CspForecastMessageDao;
import nl.enexis.scip.dao.CspForecastValueDao;
import nl.enexis.scip.dao.CspLocationMeasurementDao;
import nl.enexis.scip.dao.CspUsageValueDao;
import nl.enexis.scip.dao.LocationMeasurementValueDao;
import nl.enexis.scip.dao.MeasurementDao;
import nl.enexis.scip.dao.NoneEntityDao;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CspLocationMeasurement;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.DsoCsp;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named("dashboardService")
@Stateless
public class DashboardServiceImpl implements DashboardServiceLocal {

	@Inject
	LocationMeasurementValueDao locationMeasurementValueDao;

	@Inject
	CspForecastMessageDao cspForecastMessageDao;

	@Inject
	CspLocationMeasurementDao cspLocationMeasurementDao;

	@Inject
	CspUsageValueDao cspUsageValueDao;

	@Inject
	CspForecastValueDao cspForecastValueDao;

	@Inject
	CableForecastInputDao cableForecastInputDao;

	@Inject
	ActionDao actionDao;

	@Inject
	ActionMessageDao actionMessageDao;

	@Inject
	MeasurementDao measurementDao;

	@Inject
	NoneEntityDao noneEntityDao;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Date[]> getLastSuForecastInputDates() {
		Map<String, HashMap<Boolean, Date>> irrDatesTotal = locationMeasurementValueDao.getLastIrrValueDatesForSu();
		Map<String, Date> usageDates = cspUsageValueDao.getLastUsageValueDatesForSu();

		Map<String, Date[]> dates = new HashMap<String, Date[]>();

		for (String key : usageDates.keySet()) {
			Date usageDate = usageDates.get(key);
			Date[] cableDates = new Date[3];
			cableDates[2] = usageDate;
			dates.put(key, cableDates);
		}

		for (String key : irrDatesTotal.keySet()) {
			Map<Boolean, Date> irrDates = irrDatesTotal.get(key);

			Date[] cableDates;
			if (dates.containsKey(key)) {
				cableDates = dates.get(key);
			} else {
				cableDates = new Date[3];
				dates.put(key, cableDates);
			}

			for (Boolean isForecast : irrDates.keySet()) {
				Date irrDate = irrDates.get(isForecast);
				if (isForecast) {
					cableDates[0] = irrDate;
				} else {
					cableDates[1] = irrDate;
				}
			}
		}

		return dates;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<Cable, Date> getLastScForecastInputDates() {
		return cableForecastInputDao.getLastInputForCables();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Action getLastHeartbeatEvent(DsoCsp dsoCsp) {
		return actionDao.getLastHeartbeatAction(dsoCsp);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<ActionEvent, Long> getActionEventErrorCounts(Integer lastNumberOfDays) {
		Date toDate = new Date();
		Date fromDate = ForecastBlockUtil.addDaysToDate(toDate, -1 * lastNumberOfDays);
		return actionDao.getPeriodErrorCountsPerActionEvent(fromDate, toDate);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Date> getLastSuccessSendLocationMeasurmentForCsps() {
		Map<String, CspLocationMeasurement> mapClms = new HashMap<String, CspLocationMeasurement>();
		List<CspLocationMeasurement> clms = cspLocationMeasurementDao.getAll();
		for (CspLocationMeasurement clm : clms) {
			mapClms.put(clm.getActionTargetId(), clm);
		}

		Map<String, Date> results = actionDao.getLastSuccessSendLocationMeasurmentForCsps();
		for (String key : results.keySet()) {
			CspLocationMeasurement clm = mapClms.get(key);
			if (!clm.isDoSent()) {
				results.remove(key);
			}
		}

		return results;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<CableCsp, Date> getLastSuccessForecastMessageForCableCsps() {
		return cspForecastMessageDao.getLastSuccessForecastMessageForCableCsps();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<Cable, Long> getIncompleteMeasurmentCounts(Integer lastNumberOfDays) {
		Date toDate = new Date();
		Date fromDate = ForecastBlockUtil.addDaysToDate(toDate, -1 * lastNumberOfDays);
		return measurementDao.getPeriodIncompleteMeasurmentPerCable(fromDate, toDate);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<CableCsp, Long> getCspUsageDeviationCounts(Integer lastNumberOfDays, Long deviationMargin) {
		Map<CableCsp, Long> counts = new HashMap<CableCsp, Long>();

		Date toDate = new Date();
		Date fromDate = ForecastBlockUtil.addDaysToDate(toDate, -1 * lastNumberOfDays);

		List<CspUsageValue> usages = cspUsageValueDao.getCspUsagesByDate(fromDate, toDate);

		for (CspUsageValue cuv : usages) {
			BigDecimal deviation = cuv.getDeviation();

			// No deviatation if it is zero then equal usage as assigned
			if (deviation.compareTo(BigDecimal.ZERO) == 0) {
				continue;
			}

			BehaviourType behaviour = cuv.getCableCsp().getCable().getBehaviour();

			boolean isDeviated = false;
			if (BehaviourType.SC.equals(behaviour)) {
				BigDecimal assigned = cuv.getAverageCurrent().subtract(cuv.getDeviation());
				if (assigned.compareTo(BigDecimal.ZERO) == 0) {
					if (deviation.compareTo(BigDecimal.ZERO) > 0) {
						isDeviated = true;
					}
				} else if (assigned.compareTo(BigDecimal.ZERO) > 0 && deviation.compareTo(BigDecimal.ZERO) > 0) {
					BigDecimal deviationPerc = deviation.multiply(BigDecimal.valueOf(100)).divide(assigned, 2,
							RoundingMode.HALF_UP);
					if (deviationPerc.compareTo(new BigDecimal(deviationMargin)) >= 0) {
						isDeviated = true;
					}
				}
			} else if (BehaviourType.SU.equals(behaviour)) {
				BigDecimal assigned = cuv.getEnergyConsumption().subtract(cuv.getDeviation());
				if (assigned.compareTo(BigDecimal.ZERO) == 0) {
					if (deviation.compareTo(BigDecimal.ZERO) != 0) {
						isDeviated = true;
					}
				} else {
					BigDecimal deviationPerc = deviation.abs().multiply(BigDecimal.valueOf(100))
							.divide(assigned.abs(), 1, RoundingMode.HALF_UP);
					if (deviationPerc.compareTo(new BigDecimal(deviationMargin)) >= 0) {
						isDeviated = true;
					}
				}

			}

			if (isDeviated) {
				CableCsp cableCsp = cuv.getCableCsp();
				if (counts.containsKey(cableCsp)) {
					Long count = counts.get(cableCsp);
					counts.put(cableCsp, ++count);
				} else {
					counts.put(cableCsp, 1L);
				}
			}
		}

		return counts;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Long> getConnectivityErrorCounts(Integer lastNumberOfDays) {
		Date toDate = new Date();
		Date fromDate = ForecastBlockUtil.addDaysToDate(toDate, -1 * lastNumberOfDays);
		return actionMessageDao.getConnectivityErrorCountsPerParty(fromDate, toDate);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Double> getActionEventTurnaroundTimes(Integer lastNumberOfDays) {
		Date toDate = new Date();
		Date fromDate = ForecastBlockUtil.addDaysToDate(toDate, -1 * lastNumberOfDays);
		return actionDao.getActionEventTurnaroundTimes(fromDate, toDate);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Double> getTimerEventTurnaroundTimes(Integer lastNumberOfDays) {
		Date toDate = new Date();
		Date fromDate = ForecastBlockUtil.addDaysToDate(toDate, -1 * lastNumberOfDays);
		return actionDao.getTimerEventTurnaroundTimes(fromDate, toDate);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Date getLastDbCleanupDate() {
		return noneEntityDao.getLastDbCleanupDate();
	}
}