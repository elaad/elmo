package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.Behaviour;
import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspConnection;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecast.AssignmentChange;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspGetForecastMessage;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.util.ForecastBlockUtil;

@Behaviour(BehaviourType.SC)
@Stateless
public class CspServiceImpl implements CspServiceLocal {

	@EJB
	CspServiceLocal self;

	@Inject
	@Named("cspStorageService")
	CspStorageService cspStorageService;

	@Inject
	@Named("forecastStorageService")
	ForecastStorageService forecastStorageService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Inject
	@Named("cspMessagingService")
	CspMessagingService cspMessagingService;

	@Inject
	@Named("topologyService")
	TopologyService topologyService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Override
	@Asynchronous
	public void doCspForecastAsync(String cableForecastId) {
		try {
			self.doCspForecast(cableForecastId);
		} catch (ActionException e) {
			// Do nothing;
		}
	}

	@Override
	// @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ActionContext(event = ActionEvent.CSP_FORECAST_DIVISION, doRethrow = true)
	public void doCspForecast(String cableForecastId) throws ActionException {

		CableForecast cableForecast = forecastStorageService.findCableForecastById(cableForecastId);
		Cable cable = topologyStorageService.findCableByCableId(cableForecast.getCable().getCableId());

		nl.enexis.scip.action.ActionContext.addActionTarget(cable);

		List<CableCsp> cableCsps = cable.getCableCsps();
		int numberOfCsps = cableCsps.size();

		if (numberOfCsps == 0) {
			throw new ActionException(ActionExceptionType.DATA, "No Csps for Cable! Cable: " + cable.getCableId());
		}

		// check for active csp with active connections
		boolean hasActiveCsp = topologyService.hasCableActiveCsp(cable.getCableId(), true);
		if (!hasActiveCsp) {
			throw new ActionException(ActionExceptionType.DATA, "No active Csps for Cable! Cable: " + cable.getCableId());
		}

		int maxUsageWeeks = new Integer(generalService.getConfigurationParameter("CspForecastDivisionCriticalChangeWeeks").getValue())
				.intValue();

		// get last critical change in topology regarding csp forecast
		Date lastCriticalChange = topologyService.lastCableCspTopologyChange(cable.getCableId());

		// get number of weeks for which usage info should be available after
		// last critical change based on start first forecast block
		int availableUsageWeeks = this.getAvailableUsageWeeks(cableForecast, lastCriticalChange, maxUsageWeeks);
		BigDecimal availableUsageWeeksFactor = new BigDecimal(availableUsageWeeks)
				.divide(new BigDecimal(maxUsageWeeks), new MathContext(3)).setScale(3, RoundingMode.HALF_UP);

		Map<CableCsp, BigDecimal> cspConnectionFactors = this.getCspConnectionFactors(cable);

		List<CspForecast> lastCspForecasts = forecastStorageService.getLastCspForecastsForCable(cable);
		List<CspForecast> cspForecasts = this.setupCspForecastBasedOnCspConnectionFactors(cableForecast, cableCsps, cspConnectionFactors);

		List<CableForecastOutput> cableForecastOutputs = cableForecast.getCableForecastOutputs();
		for (CableForecastOutput cableForecastOutput : cableForecastOutputs) {

			// map sorted on receive date of possible adjustment message. if no
			// adjustment then key of map (timestamp) will be
			SortedMap<Long, CspForecastValue> cspForecastValuesMap = new TreeMap<Long, CspForecastValue>();

			BigDecimal remainingFromForecast = cableForecast.getRemainingCapacityFactor()
					.multiply(cableForecastOutput.getAvailableCapacity()).setScale(2, RoundingMode.HALF_UP);

			List<CspForecastValue> cspForecastValuesList = this.getCspForecastsAndSetAssignedForecastValue(availableUsageWeeks,
					availableUsageWeeksFactor, cspConnectionFactors, cspForecasts, cableForecastOutput, cspForecastValuesMap);

			BigDecimal actualRemaining = remainingFromForecast;

			actualRemaining = this.processNettoReturns(cspForecastValuesMap, actualRemaining);

			actualRemaining = this.processNettoExtra(cspForecastValuesMap, actualRemaining);

			// set remaining values
			for (CspForecastValue cspForecastValue : cspForecastValuesList) {
				cspForecastValue.setRemaining(actualRemaining);
			}
		}

		// Determine CSP forecast changed
		BigDecimal noCspChangeFactor = new BigDecimal(generalService.getConfigurationParameterAsString("NoCspChangeFactor"));
		for (CspForecast cspForecast : cspForecasts) {
			this.setCspForecastChange(cspForecast, lastCspForecasts, noCspChangeFactor);
		}

		forecastStorageService.storeCspForecasts(cspForecasts);
	}

	protected List<CspForecastValue> getCspForecastsAndSetAssignedForecastValue(int availableUsageWeeks, BigDecimal availableUsageWeeksFactor,
			Map<CableCsp, BigDecimal> cspConnectionFactors, List<CspForecast> cspForecasts, CableForecastOutput cableForecastOutput,
			SortedMap<Long, CspForecastValue> cspForecastValuesMap) throws ActionException {
		List<CspForecastValue> cspForecastValuesList = new ArrayList<CspForecastValue>();

		Map<CableCsp, BigDecimal> cspUsageFactors = null;

		if (availableUsageWeeksFactor.compareTo(new BigDecimal("0")) > 0) {
			// determine cspUsageFactors
			cspUsageFactors = this.getCspUsageFactors(cableForecastOutput, availableUsageWeeks);
		}

		BigDecimal availableCapacity = cableForecastOutput.getAvailableCapacity().multiply(
				new BigDecimal("1.00").subtract(cableForecastOutput.getCableForecast().getRemainingCapacityFactor()));

		Date day = cableForecastOutput.getDay();
		int hour = cableForecastOutput.getHour();
		int block = cableForecastOutput.getBlock();

		int counter = 0;
		for (CspForecast cspForecast : cspForecasts) {
			CspForecastValue cspForecastValue = new CspForecastValue();

			cspForecastValuesList.add(cspForecastValue);

			cspForecastValue.setCspForecast(cspForecast);
			cspForecastValue.setCableForecastOutput(cableForecastOutput);

			// TODO: Check on number of CSPs on cable. If one then no
			// connection factor determination
			BigDecimal assigned;
			if (cspUsageFactors == null) {
				BigDecimal cspFactor = cspConnectionFactors.get(cspForecast.getCableCsp());
				assigned = availableCapacity.multiply(cspFactor);
			} else {
				BigDecimal cspFactorConnection = cspConnectionFactors.get(cspForecast.getCableCsp()).multiply(
						new BigDecimal("1.00").subtract(availableUsageWeeksFactor));
				BigDecimal cspFactorUsage = cspUsageFactors.get(cspForecast.getCableCsp()).multiply(availableUsageWeeksFactor);
				assigned = availableCapacity.multiply(cspFactorConnection.add(cspFactorUsage));
			}
			cspForecastValue.setAssigned(assigned);

			cspForecast.getCspForecastValues().add(cspForecastValue);

			// get last reservation or return
			CspAdjustmentValue lastCspAdjustmentsValue = cspStorageService.getLastCspAdjustmentsValueForBlock(cspForecast.getCableCsp(),
					day, hour, block);
			cspForecastValue.setCspAdjustmentValue(lastCspAdjustmentsValue);

			if (lastCspAdjustmentsValue == null) {		
				counter++;
				cspForecastValuesMap.put(new Long(counter), cspForecastValue);
			} else {
				long dateTime = lastCspAdjustmentsValue.getCspAdjustment().getCspAdjustmentMessage().getDateTime().getTime();
				cspForecastValuesMap.put(new Long(dateTime), cspForecastValue);
			}
		}
		return cspForecastValuesList;
	}

	protected List<CspForecast> setupCspForecastBasedOnCspConnectionFactors(CableForecast cableForecast, List<CableCsp> cableCsps,
			Map<CableCsp, BigDecimal> cspConnectionFactors) {
		List<CspForecast> cspForecasts = new ArrayList<CspForecast>();

		Date calculationDate = new Date();
		for (CableCsp cableCsp : cableCsps) {
			if (cspConnectionFactors.containsKey(cableCsp)) {
				CspForecast cspForecastNew = new CspForecast();
				cspForecastNew.setCableForecast(cableForecast);
				cspForecastNew.setCableCsp(cableCsp);
				cspForecastNew.setNumberOfBlocks(cableCsp.getNumberOfBlocks());
				cspForecastNew.setCalculatedAt(calculationDate);
				List<CspForecastValue> cspForecastValues = new ArrayList<CspForecastValue>();
				cspForecastNew.setCspForecastValues(cspForecastValues);
				cspForecasts.add(cspForecastNew);
			}
		}
		return cspForecasts;
	}

	protected BigDecimal processNettoReturns(SortedMap<Long, CspForecastValue> cspForecastValuesMap, BigDecimal actualRemaining) {
		Iterator<Long> keyIter = cspForecastValuesMap.keySet().iterator();
		while (keyIter.hasNext()) {
			Long key = keyIter.next();
			CspForecastValue cspForecastValue = cspForecastValuesMap.get(key);

			CspAdjustmentValue cspAdjustmentValue = cspForecastValue.getCspAdjustmentValue();
			if (cspAdjustmentValue != null) {
				// && !cspAdjustmentValue.isExtraCapacity()) {
				BigDecimal needed = cspAdjustmentValue.getAssigned().add(cspAdjustmentValue.getCspForecastValue().getAssigned());
				if (needed.compareTo(cspForecastValue.getAssigned()) < 0) {
					BigDecimal back = cspForecastValue.getAssigned().subtract(needed);
					actualRemaining = actualRemaining.add(back);
					cspForecastValue.setAssigned(needed);
					keyIter.remove();
				}
			} else {
				keyIter.remove();
			}
		}
		return actualRemaining;
	}

	protected BigDecimal processNettoExtra(SortedMap<Long, CspForecastValue> cspForecastValuesMap, BigDecimal actualRemaining) {
		Iterator<Long> keyIter;
		keyIter = cspForecastValuesMap.keySet().iterator();
		while (keyIter.hasNext()) {
			Long key = keyIter.next();
			CspForecastValue cspForecastValue = cspForecastValuesMap.get(key);

			CspAdjustmentValue cspAdjustmentValue = cspForecastValue.getCspAdjustmentValue();
			if (cspAdjustmentValue != null) {
				// && cspAdjustmentValue.isExtraCapacity()) {
				BigDecimal needed = cspAdjustmentValue.getAssigned().add(cspAdjustmentValue.getCspForecastValue().getAssigned());
				if (needed.compareTo(cspForecastValue.getAssigned()) > 0) {
					BigDecimal extra = needed.subtract(cspForecastValue.getAssigned());
					if (extra.compareTo(actualRemaining) < 0) {
						cspForecastValue.setAssigned(needed);
						actualRemaining = actualRemaining.subtract(extra);
					} else {
						cspForecastValue.setAssigned(cspForecastValue.getAssigned().add(actualRemaining));
						actualRemaining = new BigDecimal("0.00");
					}
				}
			}
		}
		return actualRemaining;
	}

	protected void setCspForecastChange(CspForecast newCspForecast, List<CspForecast> lastCspForecasts, BigDecimal noCspChangeFactor) {
		int blockMinutes = newCspForecast.getCableForecast().getBlockMinutes();

		Map<String, Object> blockValues = ForecastBlockUtil.getBlockValues(new Date(), blockMinutes);
		// Check based on next block after current block
		blockValues = ForecastBlockUtil.addBlocksToBlockValues(blockValues, blockMinutes, 1);
		CspForecastValue newBlock = this.getCspForecastBlock(newCspForecast, blockValues, blockMinutes);
		newBlock.setChangeCheckBlock(true);

		for (CspForecast lastCspForecast : lastCspForecasts) {
			if (newCspForecast.getCableCsp().equals(lastCspForecast.getCableCsp())) {
				CspForecastValue formerBlock = this.getCspForecastBlock(lastCspForecast, blockValues, blockMinutes);

				if (formerBlock == null) {
					break;
				} else {
					BigDecimal noChangeInterval = newCspForecast.getCableForecast().getCable().getMaxCarCapacity()
							.multiply(noCspChangeFactor);
					BigDecimal change = newBlock.getAssigned().subtract(formerBlock.getAssigned());

					newCspForecast.setChangeValue(change);

					if (noChangeInterval.compareTo(change.abs()) < 0) {
						if (change.compareTo(new BigDecimal(0)) > 0) {
							newCspForecast.setChange(AssignmentChange.UP);
						} else {
							newCspForecast.setChange(AssignmentChange.DOWN);
						}
					} else {
						newCspForecast.setChange(AssignmentChange.EQUAL);
					}
				}
			}
		}

		if (newCspForecast.getChange() == null) {
			newCspForecast.setChange(AssignmentChange.UP);
			newCspForecast.setChangeValue(newBlock.getAssigned());
		}
	}

	protected CspForecastValue getCspForecastBlock(CspForecast cspForecast, Map<String, Object> blockValues, int blockMinutes) {
		List<CspForecastValue> values = cspForecast.getCspForecastValues();

		for (CspForecastValue value : values) {
			CableForecastOutput cfo = value.getCableForecastOutput();
			Date date1 = ForecastBlockUtil.getStartDateOfBlock(blockValues, blockMinutes);
			Date date2 = ForecastBlockUtil.getStartDateOfBlock(cfo.getDay(), cfo.getHour(), cfo.getBlock(), blockMinutes);
			if (date1.equals(date2)) {
				return value;
			}
		}

		return null;
	}

	protected Map<CableCsp, BigDecimal> getCspUsageFactors(CableForecastOutput cableForecastOutput, int availableUsageWeeks)
			throws ActionException {

		Map<CableCsp, BigDecimal> cspUsageFactors = new HashMap<CableCsp, BigDecimal>();
		BigDecimal totalUsage = new BigDecimal("0.00");

		Cable cable = cableForecastOutput.getCableForecast().getCable();

		Date fromDate = ForecastBlockUtil.addDaysToDate(cableForecastOutput.getDay(), -1 * 7 * availableUsageWeeks);

		List<CspUsageValue> usages = this.getCspUsageValues(cable, cableForecastOutput.getDayOfWeek(), cableForecastOutput.getHour(),
				cableForecastOutput.getBlock(), fromDate);

		for (CspUsageValue usage : usages) {
			CableCsp cableCsp = usage.getCableCsp();
			Csp csp = cableCsp.getCsp();
			if (topologyService.isCspActiveForCable(cable.getCableId(), csp.getEan13())) {
				totalUsage = totalUsage.add(usage.getEnergyConsumption());
				if (cspUsageFactors.containsKey(cableCsp)) {
					cspUsageFactors.put(cableCsp, cspUsageFactors.get(cableCsp).add(usage.getEnergyConsumption()));
				} else {
					cspUsageFactors.put(cableCsp, usage.getEnergyConsumption());
				}
			}
		}

		if (totalUsage.compareTo(new BigDecimal("0")) > 0) {
			for (CableCsp cableCsp : cspUsageFactors.keySet()) {
				BigDecimal cspUsageFactor = cspUsageFactors.get(cableCsp).divide(totalUsage, new MathContext(3))
						.setScale(3, RoundingMode.HALF_UP);
				cspUsageFactors.put(cableCsp, cspUsageFactor);
			}
			return cspUsageFactors;
		} else {
			return null;
		}
	}

	protected List<CspUsageValue> getCspUsageValues(Cable cable, int dayOfWeek, int hour, int block, Date fromDate) throws ActionException {
		List<CspUsageValue> cspUsageValues = cspStorageService.getCspUsageValuesForCableForecastBlock(cable, dayOfWeek, hour, block,
				fromDate);

		int numberOfExtraUsageBlocks = new Integer(generalService.getConfigurationParameter("CspForecastDivisionExtraUsageBlocks")
				.getValue()).intValue();
		int blockMinutes = new Integer(generalService.getConfigurationParameter("ForecastBlockMinutes").getValue()).intValue();
		int blocksPerHour = ForecastBlockUtil.getBlocksPerHour(blockMinutes);

		if (numberOfExtraUsageBlocks > 0) {
			// Get extra usage blocks in 'future'
			int extraBlock = block;
			int extraBlockHour = hour;
			int extraBlockDayOfWeek = dayOfWeek;
			Date extraBlockFromDate = new Date(fromDate.getTime());

			for (int i = 1; i <= numberOfExtraUsageBlocks; i++) {
				extraBlock = extraBlock + 1 > blocksPerHour ? 1 : extraBlock + 1;
				extraBlockHour = extraBlock == 1 ? extraBlockHour + 1 > 23 ? 0 : extraBlockHour + 1 : extraBlockHour;
				extraBlockDayOfWeek = extraBlock == 1 && extraBlockHour == 0 ? extraBlockDayOfWeek + 1 > 7 ? 1 : extraBlockDayOfWeek + 1
						: extraBlockDayOfWeek;
				extraBlockFromDate = extraBlock == 1 && extraBlockHour == 0 ? ForecastBlockUtil.addDaysToDate(extraBlockFromDate, 1)
						: extraBlockFromDate;
				cspUsageValues.addAll(cspStorageService.getCspUsageValuesForCableForecastBlock(cable, extraBlockDayOfWeek, extraBlockHour,
						extraBlock, extraBlockFromDate));
			}

			// Get extra usage blocks in 'past'
			extraBlock = block;
			extraBlockHour = hour;
			extraBlockDayOfWeek = dayOfWeek;
			extraBlockFromDate = new Date(fromDate.getTime());

			for (int i = 1; i <= numberOfExtraUsageBlocks; i++) {
				extraBlock = extraBlock - 1 == 0 ? blocksPerHour : extraBlock - 1;
				extraBlockHour = extraBlock == blocksPerHour ? extraBlockHour - 1 < 0 ? 23 : extraBlockHour - 1 : extraBlockHour;
				extraBlockDayOfWeek = extraBlock == blocksPerHour && extraBlockHour == 23 ? extraBlockDayOfWeek - 1 == 0 ? 7
						: extraBlockDayOfWeek - 1 : extraBlockDayOfWeek;
				extraBlockFromDate = extraBlock == blocksPerHour && extraBlockHour == 23 ? ForecastBlockUtil.addDaysToDate(
						extraBlockFromDate, -1) : extraBlockFromDate;
				cspUsageValues.addAll(cspStorageService.getCspUsageValuesForCableForecastBlock(cable, extraBlockDayOfWeek, extraBlockHour,
						extraBlock, extraBlockFromDate));
			}
		}

		return cspUsageValues;
	}

	protected int getAvailableUsageWeeks(CableForecast cableForecast, Date lastCriticalChange, int maxUsageWeeks) {
		int availableUsageWeeks = 0;

		CableForecastOutput firstForecastBlock = cableForecast.getCableForecastOutputs().get(0);
		Date startFirstForecastBlock = ForecastBlockUtil.getStartDateOfBlock(firstForecastBlock.getDay(), firstForecastBlock.getHour(),
				firstForecastBlock.getBlock(), cableForecast.getBlockMinutes());

		for (int i = 1; i <= maxUsageWeeks; i++) {
			Date checkDate = ForecastBlockUtil.addDaysToDate(startFirstForecastBlock, -1 * 7 * i);

			if (checkDate.after(lastCriticalChange)) {
				availableUsageWeeks = i;
			} else {
				break;
			}
		}

		return availableUsageWeeks;
	}

	protected Map<CableCsp, BigDecimal> getCspConnectionFactors(Cable cable) throws ActionException {
		Map<CableCsp, BigDecimal> cspConnectionFactors = new HashMap<CableCsp, BigDecimal>();

		BigDecimal cableConnectionTotal = new BigDecimal("0.00");
		for (CableCsp cableCsp : cable.getCableCsps()) {
			Csp csp = cableCsp.getCsp();
			if (topologyService.isCspActiveForCable(cable.getCableId(), csp.getEan13())) {
				BigDecimal cspConnectionTotal = new BigDecimal("0.00");
				for (CspConnection cspConnection : cableCsp.getCspConnections()) {
					if (cspConnection.isActive()) {
						cspConnectionTotal = cspConnectionTotal.add(cspConnection.getCapacity());
					}
				}
				cspConnectionFactors.put(cableCsp, cspConnectionTotal);
				cableConnectionTotal = cableConnectionTotal.add(cspConnectionTotal);
			}
		}

		for (CableCsp cableCsp : cspConnectionFactors.keySet()) {
			BigDecimal cspConnectionFactor = cspConnectionFactors.get(cableCsp).divide(cableConnectionTotal, new MathContext(3))
					.setScale(3, RoundingMode.HALF_UP);
			cspConnectionFactors.put(cableCsp, cspConnectionFactor);
		}

		return cspConnectionFactors;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendForecastRequestedByCsp(CspGetForecastMessage cspGetForecastMessage, Set<String> forCables) throws ActionException {
		try {
			if (forecastStorageService.findCspGetForecastMessageByEventId(cspGetForecastMessage.getEventId()) != null) {
				throw new ActionException(ActionExceptionType.MESSAGE, "Event not unique identified with id "
						+ cspGetForecastMessage.getEventId());
			}

			forecastStorageService.storeCspGetForecastMessage(cspGetForecastMessage);

			String cspEan = cspGetForecastMessage.getCsp();
			String dsoEan = cspGetForecastMessage.getDso();

			topologyService.checkRelationCspAndDso(cspEan, dsoEan);

			boolean didRequestSpecificCables = true;

			Set<String> intendedForecastCables = forCables;
			if (forCables == null || forCables.isEmpty()) {
				didRequestSpecificCables = false;
				intendedForecastCables = new HashSet<String>();
				Csp csp = topologyStorageService.findCspByEan13(cspEan);
				List<CableCsp> cableCsps = csp.getCableCsps();
				for (CableCsp cableCsp : cableCsps) {
					if (cableCsp.isActive() && cableCsp.getCable().getDso().getEan13().equals(dsoEan)) {
						intendedForecastCables.add(cableCsp.getCable().getCableId());
					}
				}
			}

			this.sendForecastCables(intendedForecastCables, cspGetForecastMessage, didRequestSpecificCables);

			forecastStorageService.setCspGetForecastMessageSuccessful(cspGetForecastMessage.getId());
			cspGetForecastMessage.setSuccessful(true);
		} catch (ActionException ae) {
			throw ae;
		} catch (Exception e) {
			ActionException ae = new ActionException(e.getMessage(), e);
			throw ae;
		}
	}

	private void sendForecastCables(Set<String> intendedForecastCables, CspGetForecastMessage cspGetForecastMessage,
			boolean didRequestSpecificCables) {
		String cspEan = cspGetForecastMessage.getCsp();
		for (String cableId : intendedForecastCables) {
			if (!(cableId == null || cableId.trim().isEmpty())) {
				try {
					Cable cable = topologyStorageService.findCableByCableId(cableId);

					if (cable == null) {
						throw new ActionException(ActionExceptionType.DATA, "Forecast requested for unknown cable csp relation; cable: "
								+ cableId + "; csp: " + cspEan);
					}

					List<CspForecast> cspForecasts = forecastStorageService.getLastCspForecastsForCable(cable);

					if (topologyService.isCspActiveForCable(cableId, cspEan) && cable.isActive()) {
						Csp csp = topologyStorageService.findCspByEan13(cspEan);
						CspForecast cspForecast = null;

						for (CspForecast cf : cspForecasts) {
							if (cf.getCableCsp().getCsp().equals(csp)) {
								cspForecast = cf;
								break;
							}
						}

						if (cspForecast != null) {
							Date endDateCspForecast = this.getEndDateCspForecast(cspForecast);
							if (endDateCspForecast.before(new Date())) {
								cspForecast = null;
								throw new ActionException(ActionExceptionType.DATA, "No forecast available for cable csp relation; cable: "
										+ cableId + "; csp: " + cspEan);
							} else {
								self.sendForecastForCspAsync(cspForecast.getId(), cspGetForecastMessage.getPriority());
							}
						}
					} else {
						if (didRequestSpecificCables) {
							throw new ActionException(ActionExceptionType.DATA,
									"Forecast requested for inactive cable csp relation; cable: " + cableId + "; csp: " + cspEan);
						}
					}
				} catch (ActionException ae) {
					ActionLogger.warn(ae.getMessage(), new Object[] {}, null, true);
				} catch (Exception e) {
					ActionException ae = new ActionException(e.getMessage(), e);
					ActionLogger.warn("Exception sending forecast for a cable", new Object[] {}, ae, true);
				}
			}
		}
	}

	@Override
	@Asynchronous
	@ActionContext(event = ActionEvent.SEND_CAPACITY_FORECAST, doRethrow = false)
	public void sendForecastForCable(String cableId, int priority) throws ActionException {
		Cable cable = topologyStorageService.findCableByCableId(cableId);

		List<CspForecast> cfs = forecastStorageService.getLastCspForecastsForCable(cable);

		int msgNumber = 0;

		List<CspForecast> errorDownCfs = new ArrayList<CspForecast>();

		for (CspForecast cf : cfs) {
			if (AssignmentChange.DOWN.equals(cf.getChange()) || AssignmentChange.EQUAL.equals(cf.getChange())) {
				msgNumber++;
				try {
					this.doSendForecastForCsp(cf, priority, msgNumber);
				} catch (ActionException e) {
					ActionLogger.error("Error sending forecast to csp! Due to: " + e.getMessage(), new Object[] { cf }, e, true);
					if (AssignmentChange.DOWN.equals(cf.getChange())) {
						errorDownCfs.add(cf);
					}
				}
			}
		}

		if (!errorDownCfs.isEmpty()) {
			BigDecimal totalErrorDown = new BigDecimal("0.00");

			for (CspForecast cf : errorDownCfs) {
				totalErrorDown = totalErrorDown.add(cf.getChangeValue().abs());
			}

			BigDecimal totalUp = new BigDecimal("0.00");
			for (CspForecast cf : cfs) {
				if (AssignmentChange.UP.equals(cf.getChange())) {
					totalUp = totalUp.add(cf.getChangeBlock().getAssigned());
				}
			}

			for (CspForecast cf : cfs) {
				if (AssignmentChange.UP.equals(cf.getChange())) {
					cf.getChangeBlock().setOriginalAssigned(cf.getChangeBlock().getAssigned());
					BigDecimal correction = totalErrorDown.multiply(cf.getChangeBlock().getAssigned())
							.divide(totalUp, RoundingMode.HALF_UP);
					cf.getChangeBlock().setAssigned(
							cf.getChangeBlock().getAssigned().subtract(correction).setScale(2, RoundingMode.HALF_UP));
					forecastStorageService.updateCspForecastValue(cf.getChangeBlock());
				}
			}

		}

		for (CspForecast cf : cfs) {
			if (AssignmentChange.UP.equals(cf.getChange())) {
				msgNumber++;
				try {
					this.doSendForecastForCsp(cf, priority, msgNumber);
				} catch (Exception e) {
					ActionLogger.error("Error sending forecast to csp! Due to: " + e.getMessage(), new Object[] { cf }, e, true);
				}
			}
		}
	}

	@Override
	@Asynchronous
	public void sendForecastForCspAsync(Long cspForecastId, int priority) {
		try {
			self.sendForecastForCsp(cspForecastId, priority);
		} catch (ActionException e) {
			// Do nothing
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ActionContext(event = ActionEvent.SEND_CAPACITY_FORECAST, doRethrow = true)
	public void sendForecastForCsp(Long cspForecastId, int priority) throws ActionException {
		CspForecast cspForecast = forecastStorageService.getCspForcast(cspForecastId);
		this.doSendForecastForCsp(cspForecast, priority, null);
	}

	private void doSendForecastForCsp(CspForecast cspForecast, int priority, Integer msgNumber) throws ActionException {
		CspForecast intendedCspForecast = forecastStorageService.getCspForcast(cspForecast.getId());

		if (intendedCspForecast != null) {
			CableCsp cableCsp = intendedCspForecast.getCableCsp();

			CspForecastMessage cspForecastMessage = new CspForecastMessage();
			cspForecastMessage.setCsp(cableCsp.getCsp().getEan13());
			cspForecastMessage.setDso(cableCsp.getCable().getDso().getEan13());
			cspForecastMessage.setPriority(priority);
			cspForecastMessage.setDateTime(new Date());
			cspForecastMessage.setEventId(nl.enexis.scip.action.ActionContext.getId() + (msgNumber == null ? "" : ":" + msgNumber));
			forecastStorageService.storeCspForecastMessage(cspForecastMessage);

			Date endDateCspForecast = this.getEndDateCspForecast(intendedCspForecast);

			if (endDateCspForecast.after(new Date())) {
				cspForecastMessage.getCspForecasts().add(intendedCspForecast);

				forecastStorageService.updateCspForecastMessageWithCspForecasts(cspForecastMessage);

				cspMessagingService.pushCspForecast(cspForecastMessage, cableCsp.getCsp());

				forecastStorageService.setCspForecastMessageSuccessful(cspForecastMessage.getId()); // auto-generated
																									// Id
			} else {
				throw new ActionException(ActionExceptionType.DATA, "No forecast available for Csp! Csp: " + cableCsp.getCsp().getEan13());
			}
		} else {
			throw new ActionException(ActionExceptionType.DATA, "No forecast available for Csp!");
		}
	}

	private Date getEndDateCspForecast(CspForecast cspForecast) {
		CableForecastOutput lastCableForecastOutput = cspForecast.getCspForecastValues().get(cspForecast.getCspForecastValues().size() - 1)
				.getCableForecastOutput();
		Date endDateCspForecast = ForecastBlockUtil.getEndDateOfBlock(lastCableForecastOutput.getDay(), lastCableForecastOutput.getHour(),
				lastCableForecastOutput.getBlock(), lastCableForecastOutput.getCableForecast().getBlockMinutes());
		return endDateCspForecast;
	}
}