package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.Behaviour;
import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.model.enumeration.UsageMeasurementResponsible;
import nl.enexis.scip.util.ForecastBlockUtil;

import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

@Behaviour(BehaviourType.SC)
@Stateless
public class ForecastServiceImpl implements ForecastServiceLocal {

	@EJB
	ForecastServiceLocal self;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Inject
	@Named("forecastStorageService")
	ForecastStorageService forecastStorageService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Inject
	@Named("topologyService")
	TopologyService topologyService;

	@Inject
	@Behaviour(BehaviourType.SC)
	CspService cspService;

	@Override
	@Asynchronous
	public void doForecastAsync(String cableId) {
		try {
			self.doForecast(cableId);
		} catch (ActionException e) {
			// Do nothing
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ActionContext(event = ActionEvent.CALCULATE_FORECAST, doRethrow = true)
	public void doForecast(String cableId) throws ActionException {

		Cable cable = topologyStorageService.findCableByCableId(cableId);

		nl.enexis.scip.action.ActionContext.addActionTarget(cable);

		if (cable == null) {
			throw new ActionException(ActionExceptionType.DATA, "Cable not found! Cable: " + cableId);
		} else if (!cable.isActive()) {
			ActionLogger.warn("No forecast for Cable " + cableId + ". Cable is not active.", new Object[] {}, null,
					true);
			return;
		} else if (!cable.getDso().isActive()) {
			ActionLogger.warn("No forecast for Cable " + cableId + ". Dso " + cable.getDso().getEan13()
					+ " is not active.", new Object[] {}, null, true);
			return;
		}

		List<CableCsp> cableCsps = cable.getCableCsps();
		int numberOfCsps = cableCsps.size();

		if (numberOfCsps == 0) {
			ActionLogger.warn("No forecast for Cable " + cableId + ". No csps related", new Object[] {}, null, true);
			return;
		} else {

		}

		// check for active csp with active connections
		boolean hasActiveCsp = topologyService.hasCableActiveCsp(cableId, true);
		if (!hasActiveCsp) {
			ActionLogger.warn("No forecast for Cable " + cableId + ". No active csps related", new Object[] {}, null,
					true);
			return;
		}

		boolean doFixedForecast = cable.isFixedForecast();
		int blockMinutes = Integer
				.parseInt(generalService.getConfigurationParameter("ForecastBlockMinutes").getValue());
		CableForecast cableForecast = new CableForecast();
		Date calculationDate = new Date();
		cableForecast.setId(nl.enexis.scip.action.ActionContext.getId());
		cableForecast.setDatetime(calculationDate);
		cableForecast.setBlockMinutes(blockMinutes);
		cableForecast.setCable(cable);
		cableForecast.setFixed(doFixedForecast);
		BigDecimal remainingCapacityFactor = cable.getRemainingFactor();
		cableForecast.setRemainingCapacityFactor(remainingCapacityFactor);
		BigDecimal forecastSafetyFactor = cable.getSafetyFactor();
		cableForecast.setForecastSafetyFactor(forecastSafetyFactor);

		forecastStorageService.storeCableForecast(cableForecast);

		boolean success = true;
		if (!doFixedForecast) {
			success = this.calculateDynamicForecast(cableForecast);
		} else {
			this.calculateFixedForecast(cableForecast);
		}

		forecastStorageService.addOutputsForCableForecast(cableForecast);

		cableForecast.setSuccessful(success);
		forecastStorageService.updateCableForecast(cableForecast);

		if (success) {
			cspService.doCspForecastAsync(cableForecast.getId());
		}

	}

	private void getForecastInputs(CableForecast cableForecast) throws ActionException {
		int numberOfInputs = Integer.parseInt(generalService.getConfigurationParameter("ForecastInputBlocks")
				.getValue());

		List<CableForecastInput> inputs = forecastStorageService.getForForecastInput(cableForecast.getCable(),
				numberOfInputs);

		cableForecast.setCableForecastInputs(inputs);
	}

	private boolean calculateDynamicForecast(CableForecast cableForecast) throws ActionException {

		Cable cable = cableForecast.getCable();

		boolean isDsoUsageMeasurementResponsible = cable.getUsageMeasurementResponsible() == UsageMeasurementResponsible.DSO;

		if (!isDsoUsageMeasurementResponsible) {
			throw new ActionException(ActionExceptionType.NOT_SUPPORTED,
					"Forecast based on CSP or MIXED usage information not yet implemented!");
		}

		CableForecastOutput lastOutput = null;
		int blockMinutes = cableForecast.getBlockMinutes().intValue();

		this.getForecastInputs(cableForecast);
		List<CableForecastInput> inputs = cableForecast.getCableForecastInputs();
		int numberOfInputs = inputs.size();
		
		int minNumberOfInputs = Integer.parseInt(generalService.getConfigurationParameter("ForecastMinimalInputBlocks")
				.getValue());

		if (numberOfInputs >=  minNumberOfInputs) {

			double[] y1 = new double[numberOfInputs];
			double[][] x1 = new double[numberOfInputs][26];
			double[] y2 = new double[numberOfInputs];
			double[][] x2 = new double[numberOfInputs][26];
			double[] y3 = new double[numberOfInputs];
			double[][] x3 = new double[numberOfInputs][26];

			for (int i = 0; i < numberOfInputs; i++) {
				CableForecastInput input = inputs.get(i);
				y1[i] = input.getAmpNow1().doubleValue();
				y2[i] = input.getAmpNow2().doubleValue();
				y3[i] = input.getAmpNow3().doubleValue();

				// For intercept with y-axis no input needed
				// x1[i][0] = 1;
				// x2[i][0] = 1;
				// x3[i][0] = 1;

				int x = 0;

				// set dummy for hour first 24 columns of x are for hour dummy
				// variable
				for (int j = 0; j < 24; j++) {
					x1[i][x] = input.getHour() == j ? 1 : 0;
					x2[i][x] = input.getHour() == j ? 1 : 0;
					x3[i][x] = input.getHour() == j ? 1 : 0;
					x++;
				}

				x1[i][x] = input.getAmpMinusOneDay1().doubleValue();
				x2[i][x] = input.getAmpMinusOneDay2().doubleValue();
				x3[i][x] = input.getAmpMinusOneDay3().doubleValue();

				x++;

				x1[i][x] = input.getAmpMinusOneWeek1().doubleValue();
				x2[i][x] = input.getAmpMinusOneWeek2().doubleValue();
				x3[i][x] = input.getAmpMinusOneWeek3().doubleValue();
			}

			OLSMultipleLinearRegression regression1 = new OLSMultipleLinearRegression();
			OLSMultipleLinearRegression regression2 = new OLSMultipleLinearRegression();
			OLSMultipleLinearRegression regression3 = new OLSMultipleLinearRegression();

			regression1.newSampleData(y1, x1);
			regression2.newSampleData(y2, x2);
			regression3.newSampleData(y3, x3);

			double[] beta1 = regression1.estimateRegressionParameters();
			double[] beta2 = regression2.estimateRegressionParameters();
			double[] beta3 = regression3.estimateRegressionParameters();

			ActionLogger.trace("Forecast trainingset y1:\n" + Arrays.toString(y1), new Object[] {}, null, false);
			ActionLogger.trace("Forecast trainingset x1:\n" + Arrays.toString(x1), new Object[] {}, null, false);
			ActionLogger.trace("Forecast trainingset beta1:\n" + Arrays.toString(beta1), new Object[] {}, null,
					false);
			ActionLogger.trace("Forecast trainingset y2:\n" + Arrays.toString(y2), new Object[] {}, null, false);
			ActionLogger.trace("Forecast trainingset x2:\n" + Arrays.toString(x2), new Object[] {}, null, false);
			ActionLogger.trace("Forecast trainingset beta2:\n" + Arrays.toString(beta2), new Object[] {}, null,
					false);
			ActionLogger.trace("Forecast trainingset y3:\n" + Arrays.toString(y3), new Object[] {}, null, false);
			ActionLogger.trace("Forecast trainingset x3:\n" + Arrays.toString(x3), new Object[] {}, null, false);
			ActionLogger.trace("Forecast trainingset beta3:\n" + Arrays.toString(beta3), new Object[] {}, null,
					false);

			int blocksPerHour = ForecastBlockUtil.getBlocksPerHour(blockMinutes);

			Map<String, Object> blockValues = ForecastBlockUtil.getBlockValues(cableForecast.getDatetime(),
					blockMinutes);

			int numberOfOutputs = Integer.parseInt(generalService.getConfigurationParameter("ForecastOutputBlocks")
					.getValue());

			for (int i = 0; i < numberOfOutputs; i++) {
				CableForecastOutput output = new CableForecastOutput();
				output.setCableForecast(cableForecast);
				output.setDay((Date) blockValues.get(ForecastBlockUtil.DAY));
				output.setDayOfWeek(ForecastBlockUtil.getDayOfWeek(output.getDay()));
				output.setHour((Integer) blockValues.get(ForecastBlockUtil.HOUR));
				output.setBlock((Integer) blockValues.get(ForecastBlockUtil.BLOCK));

				// Get forecast block day before and use y and x minus 6 days
				Date dayBefore = ForecastBlockUtil.addDaysToDate((Date) blockValues.get(ForecastBlockUtil.DAY), -1);
				CableForecastInput input = forecastStorageService.findCableForecastInputForBlock(cable, dayBefore,
						(Integer) blockValues.get(ForecastBlockUtil.HOUR),
						(Integer) blockValues.get(ForecastBlockUtil.BLOCK));

				if (input == null) {
					blockValues = ForecastBlockUtil.getNextBlockValues(output.getDay(), output.getHour(),
							output.getBlock(), blocksPerHour);
					continue;
				}

				int hourParam = ((Integer) blockValues.get(ForecastBlockUtil.HOUR)).intValue();

				output.setUsageExclCars1(BigDecimal.valueOf(beta1[0]).add(BigDecimal.valueOf(beta1[hourParam + 1]))
						.add(BigDecimal.valueOf(beta1[25]).multiply(input.getAmpNow1()))
						.add(BigDecimal.valueOf(beta1[26]).multiply(input.getAmpMinusSixDay1()))
						.setScale(2, RoundingMode.HALF_UP));

				output.setUsageExclCars2(BigDecimal.valueOf(beta2[0]).add(BigDecimal.valueOf(beta2[hourParam + 1]))
						.add(BigDecimal.valueOf(beta2[25]).multiply(input.getAmpNow2()))
						.add(BigDecimal.valueOf(beta2[26]).multiply(input.getAmpMinusSixDay2()))
						.setScale(2, RoundingMode.HALF_UP));

				output.setUsageExclCars3(BigDecimal.valueOf(beta3[0]).add(BigDecimal.valueOf(beta3[hourParam + 1]))
						.add(BigDecimal.valueOf(beta3[25]).multiply(input.getAmpNow3()))
						.add(BigDecimal.valueOf(beta3[26]).multiply(input.getAmpMinusSixDay3()))
						.setScale(2, RoundingMode.HALF_UP));

				BigDecimal maxOfPhases = output.getUsageExclCars1().max(output.getUsageExclCars2())
						.max(output.getUsageExclCars3());

				if (maxOfPhases.compareTo(new BigDecimal("0")) < 0) {
					// TODO: Something more used by cars then in total
					ActionLogger.trace(
							"# " + i + "; More used by cars then used in total for cable " + cable.getCableId()
									+ "; Day: " + output.getDay() + "; Hour: " + output.getHour() + "; Block: "
									+ output.getBlock(), new Object[] {}, null, false);

					maxOfPhases = new BigDecimal("0.00");
				}

				BigDecimal availableForCars = cable.getMaxCapacity().subtract(maxOfPhases);

				availableForCars = cable.getMaxCarCapacity().min(availableForCars);

				if (availableForCars.compareTo(cable.getMaxCapacity()) == 0
						|| availableForCars.compareTo(cable.getMaxCarCapacity()) == 0) {
					// TODO: Something more used by cars then in total
					ActionLogger.trace(
							"# " + i + "; Available for cars equals max capacity for cable or max available for cars "
									+ cable.getCableId() + "; Day: " + output.getDay() + "; Hour: " + output.getHour()
									+ "; Block: " + output.getBlock(), new Object[] {}, null, false);
				}

				BigDecimal safetyCorrectedAvailableForCars = availableForCars.multiply(
						new BigDecimal("1.00").subtract(cableForecast.getForecastSafetyFactor())).setScale(2,
						RoundingMode.HALF_UP);
				output.setAvailableCapacity(safetyCorrectedAvailableForCars.compareTo(new BigDecimal("0")) < 0 ? new BigDecimal(
						"0.00") : safetyCorrectedAvailableForCars);

				cableForecast.getCableForecastOutputs().add(output);
				lastOutput = output;

				blockValues = ForecastBlockUtil.getNextBlockValues(output.getDay(), output.getHour(),
						output.getBlock(), blocksPerHour);
			}

		}

		int minNumberOfOutputs = Integer.parseInt(generalService.getConfigurationParameter(
				"ForecastMinimalOutputBlocks").getValue());

		if (cableForecast.getCableForecastOutputs().isEmpty()) {
			cable.setFixedForecast(true);
			cable.setUserId(nl.enexis.scip.action.ActionContext.getActor());  // for audit log item
			cable.setComments("Not enough forecast input data; set to FIXED forecast.");
			topologyStorageService.updateCable(cable);

			if (numberOfInputs < minNumberOfInputs) {
				ActionLogger.error("Forecast is empty! Not enough forecast input blocks available! Number of inputs "
						+ numberOfInputs + " (needed: " + minNumberOfInputs + ")! Switching to FIXED forecast!",
						new Object[] {},
						new ActionException(ActionExceptionType.DATA, "Not enough forecast input data"), true);
			} else {
				ActionLogger
						.error("Forecast is empty! No forecast output blocks could be calculated! Switching to FIXED forecast!",
								new Object[] {}, new ActionException(ActionExceptionType.DATA,
										"No forecast output block calculated"), true);
			}

			// start dynamic calculation async
			self.doForecastAsync(cable.getCableId());

			return false;
		} else if (cableForecast.getCableForecastOutputs().size() < minNumberOfOutputs) {
			ActionLogger
					.error("Number of forecast blocks is less then {0}. Possible problems with forecast input data! Start of last block is {1}",
							new Object[] {
									minNumberOfOutputs,
									ForecastBlockUtil.getStartDateOfBlock(lastOutput.getDay(), lastOutput.getHour(),
											lastOutput.getBlock(), blockMinutes) }, null, true);
		}

		return true;
	}

	private void calculateFixedForecast(CableForecast cableForecast) throws ActionException {
		Cable cable = cableForecast.getCable();

		BigDecimal fixedAvailableCapacityMaxValue;
		BigDecimal fixedAvailableCapacityMaxDeviation;
		boolean doRandomDeviation;
		Set<String> fixedAvailableCapacityDeviationBlocks;

		fixedAvailableCapacityMaxValue = cable.getFixedForecastMax();
		if (fixedAvailableCapacityMaxValue == null) {
			throw new ActionException(ActionExceptionType.DATA, "FIXED_FORECAST_MAX not available for cable "
					+ cable.getCableId());
		}

		fixedAvailableCapacityMaxDeviation = cable.getFixedForecastDeviation();
		if (fixedAvailableCapacityMaxDeviation == null) {
			throw new ActionException(ActionExceptionType.DATA, "FIXED_FORECAST_DEVIATION not available for cable "
					+ cable.getCableId());
		}

		fixedAvailableCapacityDeviationBlocks = new HashSet<String>();
		if (cable.getFixedForecastDeviationBlocks() == null) {
			throw new ActionException(ActionExceptionType.DATA,
					"FIXED_FORECAST_DEVIATION_BLOCKS not available for cable " + cable.getCableId());
		}
		String[] deviationBlocks = cable.getFixedForecastDeviationBlocks().split(",");
		for (String devBlock : deviationBlocks) {
			fixedAvailableCapacityDeviationBlocks.add(devBlock);
		}

		doRandomDeviation = cable.isFixedForecastRandom();

		int numberOfOutputs = Integer.parseInt(generalService.getConfigurationParameter("ForecastOutputBlocks")
				.getValue());
		int blockMinutes = cableForecast.getBlockMinutes().intValue();
		int blocksPerHour = ForecastBlockUtil.getBlocksPerHour(blockMinutes);

		Map<String, Object> blockValues = ForecastBlockUtil.getBlockValues(cableForecast.getDatetime(), blockMinutes);

		for (int i = 0; i < numberOfOutputs; i++) {
			CableForecastOutput output = new CableForecastOutput();
			output.setCableForecast(cableForecast);
			output.setDay((Date) blockValues.get(ForecastBlockUtil.DAY));
			output.setDayOfWeek(ForecastBlockUtil.getDayOfWeek(output.getDay()));
			output.setHour((Integer) blockValues.get(ForecastBlockUtil.HOUR));
			output.setBlock((Integer) blockValues.get(ForecastBlockUtil.BLOCK));
			BigDecimal availableForCars = new BigDecimal("0.00");
			if (fixedAvailableCapacityDeviationBlocks.contains(new String("" + output.getBlock()))) {
				if (doRandomDeviation) {
					BigDecimal randomFactor = new BigDecimal("1.00").subtract(new BigDecimal(Math.random()));
					BigDecimal randomDeviation = fixedAvailableCapacityMaxDeviation.multiply(randomFactor).setScale(2,
							RoundingMode.HALF_UP);
					availableForCars = fixedAvailableCapacityMaxValue.subtract(randomDeviation);
					// output.setAvailableCapacity(fixedAvailableCapacityMaxValue.subtract(randomDeviation));
				} else {
					availableForCars = fixedAvailableCapacityMaxValue.subtract(fixedAvailableCapacityMaxDeviation);
					// output.setAvailableCapacity(fixedAvailableCapacityMaxValue.subtract(fixedAvailableCapacityMaxDeviation));
				}
			} else {
				availableForCars = fixedAvailableCapacityMaxValue;
				// output.setAvailableCapacity(fixedAvailableCapacityMaxValue);
			}

			BigDecimal safetyCorrectedAvailableForCars = availableForCars.multiply(
					new BigDecimal("1.00").subtract(cableForecast.getForecastSafetyFactor())).setScale(2,
					RoundingMode.HALF_UP);
			output.setAvailableCapacity(safetyCorrectedAvailableForCars);
			output.setUsageExclCars1(new BigDecimal("0.00"));
			output.setUsageExclCars2(new BigDecimal("0.00"));
			output.setUsageExclCars3(new BigDecimal("0.00"));

			cableForecast.getCableForecastOutputs().add(output);

			blockValues = ForecastBlockUtil.getNextBlockValues(output.getDay(), output.getHour(), output.getBlock(),
					blocksPerHour);
		}

	}
}