package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.Behaviour;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.model.TimeBlock;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustment;
import nl.enexis.scip.model.CspAdjustmentMessage;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.enumeration.BehaviourType;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named("adjustmentService")
@Stateless
public class AdjustmentServiceImpl implements AdjustmentServiceLocal {

	@Inject
	@Named("cspStorageService")
	CspStorageService cspStorageService;

	@Inject
	@Named("forecastStorageService")
	ForecastStorageService forecastStorageService;

	@Inject
	@Named("topologyService")
	TopologyService topologyService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Inject
	@Behaviour(BehaviourType.SC)
	CspService cspService;

	@Override
	public void receiveAdjustment(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException {
		Date currentDate = new Date();

		try {
			if (cspStorageService.findCspAdjustmentMessageByEventId(cspAdjustmentMessage.getEventId()) != null) {
				throw new ActionException(ActionExceptionType.MESSAGE, "Event not unique identified with id "
						+ cspAdjustmentMessage.getEventId());
			}

			cspAdjustmentMessage = cspStorageService.storeCspAdjustmentMessage(cspAdjustmentMessage);

			List<CspAdjustment> cspAdjustments = cspAdjustmentMessage.getCspAdjustments();

			String cspEan = cspAdjustmentMessage.getCsp();
			String dsoEan = cspAdjustmentMessage.getDso();

			topologyService.checkRelationCspAndDso(cspEan, dsoEan);

			int blockMinutes = Integer.parseInt(generalService.getConfigurationParameter("ForecastBlockMinutes")
					.getValue());

			TimeBlock timeBlockOfStart;
			TimeBlock timeBlockOfEnd;
			
			for (CspAdjustment cspAdjustment : cspAdjustments) {
				String cableId = cspAdjustment.getCableCsp().getCable().getCableId();

				cspAdjustment.setCspAdjustmentMessage(cspAdjustmentMessage);

				topologyService.checkCableRelatedToCspAndDso(cableId, cspEan, dsoEan);

				List<CspAdjustmentValue> cspAdjustmentValuesToDelete = new ArrayList<CspAdjustmentValue>(0);

				for (CspAdjustmentValue cspAdjustmentValue : cspAdjustment.getCspAdjustmentValues()) {
					if (cspAdjustmentValue.getEndedAt().before(currentDate)) {
						/*
						 * TODO: Skipped exception if block is before current
						 * date. Blocks are to be ignored. Needs solution. For
						 * now log message.
						 */
						ActionLogger.warn("Adjustment block ignored. Requested for past. Block start: "
								+ cspAdjustmentValue.getStartedAt() + "; Block end: " + cspAdjustmentValue.getEndedAt()
								+ "; Now: " + currentDate + ";", new Object[] {}, null, true);

						cspAdjustmentValuesToDelete.add(cspAdjustmentValue);

					}

//					Date startDay = ForecastBlockUtil.getDate(cspAdjustmentValue.getStartedAt());
//					Date endDay = ForecastBlockUtil.getDate(cspAdjustmentValue.getEndedAt());
//					int startHour = ForecastBlockUtil.getHour(cspAdjustmentValue.getStartedAt());
//					int endHour = ForecastBlockUtil.getHour(cspAdjustmentValue.getEndedAt());
//					int startBlock = ForecastBlockUtil.getBlock(cspAdjustmentValue.getStartedAt(), blockMinutes);
//					int endBlock = ForecastBlockUtil.getBlock(cspAdjustmentValue.getEndedAt(), blockMinutes);
//
//					if (startHour != endHour || startBlock != endBlock || !startDay.equals(endDay)) {

					timeBlockOfStart = TimeBlock.createForm(cspAdjustmentValue.getStartedAt(), blockMinutes);
					timeBlockOfEnd = TimeBlock.createForm(cspAdjustmentValue.getEndedAt(), blockMinutes);
					if (!timeBlockOfStart.equals(timeBlockOfEnd)) {
						/*
						 * TODO: Skipped exception if block is exceeded. Blocks
						 * are to be ignored. Needs solution. For now log
						 * message.
						 */
						ActionLogger.warn(
								"Adjustment block ignored. Requested block exceeds over block boundaries. Block start: "
										+ cspAdjustmentValue.getStartedAt() + "; Block end: "
										+ cspAdjustmentValue.getEndedAt() + "; Block size (minutes): " + blockMinutes,
								new Object[] {}, null, true);

						cspAdjustmentValuesToDelete.add(cspAdjustmentValue);

					}

					String unitOfMeasure = generalService.getConfigurationParameter("CapacityUnitOfMeasure").getValue();

					if (!unitOfMeasure.contains(cspAdjustmentValue.getUnitOfMeasure())) {
						throw new ActionException(ActionExceptionType.MESSAGE,
								"Adjustment requested with incorrect unit of measure. Expected: " + unitOfMeasure
										+ "; Received: " + cspAdjustmentValue.getUnitOfMeasure());
					}

					cspAdjustmentValue.setDay(timeBlockOfStart.getDay());
					cspAdjustmentValue.setDayOfWeek(ForecastBlockUtil.getDayOfWeek(cspAdjustmentValue.getDay()));
					cspAdjustmentValue.setHour(timeBlockOfStart.getHour());
					cspAdjustmentValue.setBlock(timeBlockOfStart.getBlockOfHour());

//					cspAdjustmentValue.setDay(startDay);
//					cspAdjustmentValue.setDayOfWeek(ForecastBlockUtil.getDayOfWeek(cspAdjustmentValue.getDay()));
//					cspAdjustmentValue.setHour(startHour);
//					cspAdjustmentValue.setBlock(startBlock);
				}

				for (CspAdjustmentValue cspAdjustmentValueToDelete : cspAdjustmentValuesToDelete) {
					cspAdjustment.getCspAdjustmentValues().remove(cspAdjustmentValueToDelete);
				}

			}

			this.checkAllAdjustmentRequestsAreUnique(cspAdjustmentMessage);

			this.determineAssignments(cspAdjustmentMessage);

			cspStorageService.storeCspAdjustments(cspAdjustmentMessage);

			this.redoCspForecasts(cspAdjustmentMessage);

			cspAdjustmentMessage.setSuccessful(true);
		} catch (Exception e) {
			throw new ActionException(e.getMessage(), e);
		}
	}

	private void redoCspForecasts(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException {
		List<CspAdjustment> cspAdjustments = cspAdjustmentMessage.getCspAdjustments();

		Set<Cable> cables = new HashSet<Cable>();

		for (CspAdjustment cspAdjustment : cspAdjustments) {
			cables.add(cspAdjustment.getCableCsp().getCable());
		}

		for (Cable cable : cables) {
			CableForecast cableForecast = forecastStorageService.getLastCableForecast(cable);
			cspService.doCspForecastAsync(cableForecast.getId());
		}

	}

	private void checkAllAdjustmentRequestsAreUnique(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

		List<CspAdjustment> adjustments = cspAdjustmentMessage.getCspAdjustments();

		Set<String> ids = new HashSet<String>();

		for (CspAdjustment cspAdjustment : adjustments) {
			List<CspAdjustmentValue> adjustmentValues = cspAdjustment.getCspAdjustmentValues();
			for (CspAdjustmentValue cspAdjustmentValue : adjustmentValues) {
				String id = "" + cspAdjustmentValue.getCableCsp().getCable().getCableId() + "-"
						+ df.format(cspAdjustmentValue.getDay()) + "-" + cspAdjustmentValue.getHour() + "-"
						+ cspAdjustmentValue.getBlock();
				if (!ids.add(id)) {
					throw new ActionException(ActionExceptionType.MESSAGE,
							"Two (or more) adjustments requested for same forecast block of cable in the one message. Block identification: "
									+ id);
				}
			}
		}
	}

	private void determineAssignments(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException {
		List<CspAdjustment> cspAdjustments = cspAdjustmentMessage.getCspAdjustments();

		Csp csp = topologyStorageService.findCspByEan13(cspAdjustmentMessage.getCsp());

		for (CspAdjustment cspAdjustment : cspAdjustments) {
			for (CspAdjustmentValue cspAdjustmentValue : cspAdjustment.getCspAdjustmentValues()) {
				Cable cable = cspAdjustment.getCableCsp().getCable();
				Date day = cspAdjustmentValue.getDay();
				int hour = cspAdjustmentValue.getHour();
				int block = cspAdjustmentValue.getBlock();

				CableForecast cableForecast = forecastStorageService.getLastCableForecast(cable);

				CableForecastOutput cableForecastOutput = forecastStorageService.getLastCableForecastOutput(
						cableForecast, day, hour, block);

				if (cableForecastOutput == null) {
					throw new ActionException(ActionExceptionType.DATA,
							"No forecast block found as base for adjustment request calculations! Cable: "
									+ cable.getCableId() + " - day: " + day + " - hour: " + hour + " - block: " + block);
				}

				CspForecastValue lastCspForecastValue = forecastStorageService
						.getLastCspForecastValueForCableForecastOutput(cableForecastOutput, csp);

				if (lastCspForecastValue == null) {
					throw new ActionException(ActionExceptionType.DATA,
							"No forecast block found as base for adjustment request calculations! Cable: "
									+ cable.getCableId() + " - day: " + day + " - hour: " + hour + " - block: " + block);
				}

				cspAdjustmentValue.setCspForecastValue(lastCspForecastValue);

				// request extra or give back?
				if (cspAdjustmentValue.getRequested().compareTo(new BigDecimal("0.00")) >= 0) {
					cspAdjustmentValue.setExtraCapacity(true);

					BigDecimal capacityStillAvailable = lastCspForecastValue.getRemaining();

					// if still available >= cspAdjustmentValue.getRequested()
					if (capacityStillAvailable.compareTo(cspAdjustmentValue.getRequested()) >= 0) {
						// then assigned = requested
						cspAdjustmentValue.setAssigned(cspAdjustmentValue.getRequested());
					} else {
						// else assign what was still available
						cspAdjustmentValue.setAssigned(capacityStillAvailable);
					}
				} else {
					// give back ==> max to give back is csp received last
					// capacity
					// get last communicated available capacity
					cspAdjustmentValue.setExtraCapacity(false);

					if (lastCspForecastValue.getAssigned().add(cspAdjustmentValue.getRequested())
							.compareTo(new BigDecimal("0.00")) >= 0) {
						// then assigned = requested
						cspAdjustmentValue.setAssigned(cspAdjustmentValue.getRequested());
					} else {
						// else assign last communicated as give back meaning
						// that minimal
						// required capacity falls back to zero
						cspAdjustmentValue.setAssigned(lastCspForecastValue.getAssigned().negate());
					}
				}
			}
		}
	}

	@Override
	public void setAdjustmentSuccessful(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException {
		try {
			cspStorageService.setCspAdjustmentMessageSuccessful(cspAdjustmentMessage);
		} catch (Exception e) {
			throw new ActionException(e.getMessage(), e);
		}
	}
}