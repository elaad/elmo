package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.dao.CspLocationMeasurementDao;
import nl.enexis.scip.dao.LocationMeasurementDao;
import nl.enexis.scip.dao.LocationMeasurementValueDao;
import nl.enexis.scip.model.CspLocationMeasurement;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.LocationMeasurement;
import nl.enexis.scip.model.LocationMeasurementValue;
import nl.enexis.scip.model.LocationMeasurementValuePK;
import nl.enexis.scip.model.MeasurementProvider;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.apache.commons.net.ftp.FTPReply;

@Named("locationMeasurementsService")
@Stateless
public class LocationMeasurementsServiceImpl implements LocationMeasurementsServiceLocal {

	@EJB
	LocationMeasurementsServiceLocal self;

	@Inject
	@Named("cspMessagingService")
	CspMessagingService cspMessagingService;

	@Inject
	LocationMeasurementDao locationMeasurementDao;

	@Inject
	CspLocationMeasurementDao cspLocationMeasurementDao;

	@Inject
	LocationMeasurementValueDao locationMeasurementValueDao;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	@ActionContext(event = ActionEvent.FETCH_LOCATION_MEASUREMENTS, doRethrow = true)
	public void fetchMeasurementValues(LocationMeasurement locationMeasurement) throws ActionException {
		FTPClient ftp = new FTPClient();
		FTPClientConfig config = new FTPClientConfig();
		ftp.configure(config);

		String lastFileName = null;
		String baseDir = null;
		// String lastFileStoragePath = null;

		try {
			baseDir = generalService.getConfigurationParameter("BaseDir").getValue();

			this.connectToFtp(ftp);

			// ftp.enterLocalActiveMode();
			// For azure must use passive mode
			ftp.enterLocalPassiveMode();
			Filter filter = new Filter(locationMeasurement);
			FTPFile[] files = ftp.listFiles("", filter);
			this.isSuccessReply(ftp);
			SortedSet<FTPFile> filesSorted = this.getSortedList(files);

			for (FTPFile ftpFile : filesSorted) {
				if (ftpFile.isFile()) {
					lastFileName = ftpFile.getName();

					ActionLogger.debug("FTP: FILE: " + lastFileName + "; Datetime: " + ftpFile.getTimestamp().getTime().toString(),
							new Object[] {}, null, false);

					InputStream is = ftp.retrieveFileStream(lastFileName);

					OutputStream out = new FileOutputStream(baseDir + lastFileName);
					IOUtils.copy(is, out);
					is.close();
					out.close();

					this.processLocationMeasurement(locationMeasurement, ftp, baseDir, lastFileName);

					lastFileName = null;
				}
			}

			ftp.logout();
			this.isSuccessReply(ftp);
		} catch (ActionException ae) {
			this.newFilePath(lastFileName, baseDir);
			throw ae;
		} catch (Exception e) {
			ActionException newAe = new ActionException(ActionExceptionType.FTP, "FILE '" + lastFileName + "' not processed.", e);
			this.newFilePath(lastFileName, baseDir);
			throw newAe;
		} finally {
			this.disconnectFtp(ftp);
		}
	}

	protected void connectToFtp(FTPClient ftp) throws ActionException, IOException {
		this.prepareConnectToFtp(ftp);

		ftp.login(generalService.getConfigurationParameter("FtpUser").getValue(), generalService.getConfigurationParameter("FtpPassword")
				.getValue());
		this.isSuccessReply(ftp);

		String pathName = generalService.getConfigurationParameter("FtpBasePath").getValue();

		ftp.changeWorkingDirectory(pathName);
		this.isSuccessReply(ftp);

		ftp.setFileType(FTP.ASCII_FILE_TYPE);
		this.isSuccessReply(ftp);
	}

	protected void disconnectFtp(FTPClient ftp) {
		if (ftp.isConnected()) {
			try {
				ftp.disconnect();
			} catch (IOException ioe) {
				ActionLogger.warn(ioe.getMessage(), new Object[] {}, null, false);
			}
		}
	}

	protected void newFilePath(String lastFileName, String baseDir) {
		String lastFileStoragePath = baseDir + lastFileName;
		if (baseDir != null && lastFileName != null) {
			try {
				File file = new File(lastFileStoragePath);
				file.renameTo(new File(generalService.getConfigurationParameter("BaseDir").getValue()
						+ generalService.getConfigurationParameter("ErrorDir").getValue() + lastFileName));
			} catch (Exception e) {
				ActionLogger.warn(e.getMessage(), new Object[] {}, null, false);
			}
		}
	}

	protected void processLocationMeasurement(LocationMeasurement locationMeasurement, FTPClient ftp, String baseDir, String lastFileName)
			throws ActionException, IOException {
		boolean isSuccess = true;
		try {
			self.processLocationMeasurementFile(locationMeasurement, new FileInputStream(baseDir + lastFileName));
		} catch (ActionException ae){
			isSuccess = false;
			throw ae;
		} catch (Exception e) {
			isSuccess = false;	
			throw new ActionException(ActionExceptionType.FTP, "FILE '" + lastFileName + "' not processed.", e);
		} finally {
			ftp.completePendingCommand();
			this.isSuccessReply(ftp);

			File file = new File(baseDir + lastFileName);
			file.renameTo(new File(baseDir + generalService.getConfigurationParameter(isSuccess ? "ProcessedDir" : "ErrorDir").getValue()
					+ lastFileName));

			ftp.deleteFile(lastFileName);
			this.isSuccessReply(ftp);
		}
	}

	protected void prepareConnectToFtp(FTPClient ftp) throws ActionException {
		try {
			ftp.connect(generalService.getConfigurationParameter("FtpServer").getValue());
			this.isSuccessReply(ftp);
		} catch (ActionException ae) {
			ActionException aeMessage = new ActionException(ActionExceptionType.CONNECTION, "Failed to connect FTP server Meteo Consult",
					ae);
			MeasurementProvider mp = new MeasurementProvider();
			mp.setId("Meteo");
			aeMessage.getTargets().add(mp);
			throw aeMessage;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.CONFIGURATION, "FtpServer configuration parameter not processed correctly", e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<LocationMeasurement> getAllLocationMeasurements() {
		return locationMeasurementDao.getAll();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<CspLocationMeasurement> getAllCspLocationMeasurements() {
		return cspLocationMeasurementDao.getAll();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void processLocationMeasurementFile(LocationMeasurement locationMeasurement, InputStream is) throws ActionException {

		try {
			String dateFormat = generalService.getConfigurationParameter("FetchLocationMeasurementDateFormat").getValue();
			String separator = generalService.getConfigurationParameter("FetchLocationMeasurementSeparator").getValue();
			int dateField = Integer.parseInt(generalService.getConfigurationParameter("FetchLocationMeasurementDateField").getValue());
			int valueField = Integer.parseInt(generalService.getConfigurationParameter("FetchLocationMeasurementValueField").getValue());

			BufferedReader input = new BufferedReader(new InputStreamReader(is));

			boolean doRead = true;
			// Read first header line
			String line = input.readLine();

			List<LocationMeasurementValue> lmvs = new ArrayList<LocationMeasurementValue>();

			boolean isFirst = true;

			do {
				line = input.readLine();
				doRead = line == null ? false : true;

				if (doRead && line.trim().length() != 0) {
					ActionLogger.trace("INPUT LINE: " + line, new Object[] {}, null, false);

					String[] fields = line.split(separator);

					LocationMeasurementValuePK lmvPK = new LocationMeasurementValuePK();
					Date valueDate = new SimpleDateFormat(dateFormat).parse(fields[dateField]);
					lmvPK.setDatetime(valueDate);
					lmvPK.setLocationMeasurementId(locationMeasurement.getId());

					LocationMeasurementValue lmv = new LocationMeasurementValue();
					lmv.setKey(lmvPK);
					lmv.setValue(new BigDecimal(fields[valueField]));
					lmv.setLocationMeasurement(locationMeasurement);

					if (isFirst) {
						List<CspLocationMeasurement> cspLocationMeasurements = locationMeasurement.getCspLocationMeasurements();
						for (CspLocationMeasurement cspLocationMeasurement : cspLocationMeasurements) {
							cspLocationMeasurement.setSendFrom(valueDate);
							cspLocationMeasurementDao.update(cspLocationMeasurement);
						}
						isFirst = false;
					}

					lmv = locationMeasurementValueDao.merge(lmv);
					lmvs.add(lmv);
				}
			} while (doRead);

			// self.sendMeasurementValuesToCsp(lmvs);
		} catch (ActionException ae) {
			throw ae;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.FTP, e.getMessage(), e);
		}
	}

	@Override
	@Asynchronous
	@ActionContext(event = ActionEvent.SEND_LOCATION_MEASUREMENTS, doRethrow = true)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void sendMeasurementValuesToCsp(CspLocationMeasurement clm) throws ActionException {
		int blockMinutes = generalService.getConfigurationParameterAsInteger("ForecastBlockMinutes");
		Dso dso = clm.getLocationMeasurement().getLocation().getCableLocations().get(0).getCable().getDso();

		List<LocationMeasurementValue> lmvs = locationMeasurementValueDao.getLocationMeasurementValuesAfterDate(
				clm.getLocationMeasurement(), clm.getSendFrom());

		cspMessagingService.pushWeatherUpdate(blockMinutes, clm.getLocationMeasurement(), lmvs, clm.getCsp(), dso);

		clm.setSendFrom(null);
		cspLocationMeasurementDao.update(clm);
	}

	protected void isSuccessReply(FTPClient ftp) throws ActionException {
		int reply = ftp.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			try {
				ftp.disconnect();
			} catch (IOException e) {
				// throw new ActionException(ActionExceptionType.FTP,
				// "FTP disconnect error!", e);
			}
			throw new ActionException(ActionExceptionType.FTP, "FTP reply: " + ftp.getReplyString());
		} else {
			ActionLogger.trace("FTP reply: " + ftp.getReplyString(), new Object[] {}, null, false);
		}
	}

	protected SortedSet<FTPFile> getSortedList(FTPFile[] files) {
		// create a TreeSet which will sort each element as it is added.
		SortedSet<FTPFile> sorted = new TreeSet<FTPFile>(new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				FTPFile f1 = (FTPFile) o1;
				FTPFile f2 = (FTPFile) o2;
				return f1.getTimestamp().getTime().compareTo(f2.getTimestamp().getTime());
			}

		});

		for (FTPFile file : files) {
			sorted.add(file);
		}

		return sorted;
	}

	public class Filter implements FTPFileFilter {
		private LocationMeasurement locationMeasurement;

		public Filter(LocationMeasurement locationMeasurement) {
			this.locationMeasurement = locationMeasurement;
		}

		@Override
		public boolean accept(FTPFile ftpFile) {
			String filterStr = "" + locationMeasurement.getLocation().getLocationId() + "_"
					+ (locationMeasurement.isForecast() ? "F" : "O");
			return ftpFile.isFile() && ftpFile.getName().startsWith(filterStr);
		}
	}
}
