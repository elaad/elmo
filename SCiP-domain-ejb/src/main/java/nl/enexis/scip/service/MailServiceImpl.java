package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.action.model.ActionMessageTarget;
import nl.enexis.scip.action.model.ActionTarget;
import nl.enexis.scip.model.AdminUserNotification;
import nl.enexis.scip.model.enumeration.NotificationInterval;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named("mailService")
@Stateless
public class MailServiceImpl implements MailService {

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Inject
	@Named("genericStorageService")
	GenericStorageService genericStorageService;

	@Inject
	@Named("actionContextService")
	ActionContextService actionContextService;

	@EJB
	MailService self;

	//
	@Override
	public void sendMail(String subject, String text, String tos) throws ActionException {
		try {

			final String env = generalService.getConfigurationParameterAsString("Environment");
			final String replyTos = generalService.getConfigurationParameterAsString("MailReplyTo");
			final String username = generalService.getConfigurationParameterAsString("MailUser");
			final String password = generalService.getConfigurationParameterAsString("MailPwd");
			final String smtpHost = generalService.getConfigurationParameterAsString("MailHost");
			final String smtpPort = generalService.getConfigurationParameterAsString("MailPort");
			final boolean doAuthenticate = generalService.getConfigurationParameterAsBoolean("MailDoAuthentication");
			final boolean useTLS = generalService.getConfigurationParameterAsBoolean("MailUseTLS");
			final boolean doDebug = generalService.getConfigurationParameterAsBoolean("MailDoDebug");
			final String node = System.getProperty("jboss.node.name", "");

			subject = "SmartCharging (" + env + "): " + ("".equals(node) ? "" : node + ":") + subject;

			Properties props = new Properties();
			if (doAuthenticate) {
				props.put("mail.smtp.auth", "true");
			}
			if (useTLS) {
				props.put("mail.smtp.starttls.enable", "true");
			}
			props.put("mail.smtp.host", smtpHost);
			props.put("mail.smtp.port", smtpPort);

			Session session = Session.getInstance(props);
			// if (doAuthenticate) {
			// session = Session.getInstance(props, new
			// javax.mail.Authenticator() {
			// protected PasswordAuthentication getPasswordAuthentication() {
			// return new PasswordAuthentication(username, password);
			// }
			// });
			// } else {
			// session = Session.getInstance(props);
			// }

			if (doDebug) {
				session.setDebug(true);
			}

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));

			Address[] repTosTmp = InternetAddress.parse(replyTos);
			Address[] repTos = new Address[repTosTmp.length + 1];
			for (int i = 0; i < repTos.length; i++) {
				if (i < repTosTmp.length) {
					repTos[i] = repTosTmp[i];
				} else {
					repTos[i] = new InternetAddress(username);
				}
			}
			message.setReplyTo(repTos);

			Address[] toAdresses = InternetAddress.parse(tos);
			if (toAdresses.length == 0) {
				message.setRecipients(Message.RecipientType.TO, repTos);
			} else {
				message.setRecipients(Message.RecipientType.TO, toAdresses);
			}

			message.setSubject(subject);
			text = "<html><body>" + text + "</body></html>";
			message.setContent(text, "text/html; charset=utf-8");

			Transport transport = session.getTransport("smtp");
			if (doAuthenticate) {
				transport.connect(username, password);
			} else {
				transport.connect();
			}

			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (MessagingException e) {
			throw new ActionException(ActionExceptionType.MAIL, e);
		}
	}

	@Override
	@Asynchronous
	@ActionContext(event = ActionEvent.NOTIFICATION, doRethrow = false, doDirectNotification = false)
	public void sendDirectActionNotification(Action action) throws ActionException {
		try {
			List<AdminUserNotification> notifications = genericStorageService
					.findActiveDirectAdminUserNotifications(action.getType());

			if (notifications.isEmpty()) {
				return;
			} else {

				// create to list
				Set<String> toSet = new HashSet<String>();

				for (AdminUserNotification notification : notifications) {
					if (notification.getUser().isActive() && notification.getUser().isReceiveEmail()) {
						if (notification.getUser().getEmail() != null
								&& !notification.getUser().getEmail().trim().isEmpty()) {
							if (Level.INFO.equals(notification.getLevel())) {
								toSet.add(notification.getUser().getEmail());
							} else if (Level.WARN.equals(notification.getLevel()) && action.containsWarnings(true)) {
								toSet.add(notification.getUser().getEmail());
							} else if (Level.ERROR.equals(notification.getLevel()) && action.containsErrors()) {
								toSet.add(notification.getUser().getEmail());
							}
						}
					}
				}

				String tos = this.createTosString(toSet);

				if (tos == null) {
					// no one to receive the message
					return;
				}

				// create message
				String subject = (action.containsErrors() ? "Error" : action.containsWarnings(true) ? "Warning"
						: "Notification") + " " + action.getType();
				String text = "<b>" + action.getType() + "</b><br/>" + "<b>Id:</b> " + action.getId() + "<br/>"
						+ "<b>Actor:</b> " + action.getActor() + "<br/>" + "<b>Start:</b> " + action.getStart()
						+ "<br/>" + "<b>End:</b> " + action.getEnd() + "<br/>";

				if (!action.getTargets().isEmpty()) {
					text = text + "<b>Targets:</b><ol>";
					for (ActionTarget target : action.getTargets()) {
						text = text + "<li>" + target.getDirection() + " : " + target.getType() + " : "
								+ target.getValue() + "</li>";
					}
					text = text + "</ol>";
				}

				if (!action.getMessages().isEmpty()) {
					text = text + "<b>Messages:</b><ol>";
					for (ActionMessage msg : action.getMessages()) {
						text = text + "<li>" + (msg.getLevel().equals(Level.ERROR) ? "<font style='color:red;'>" : "")
								+ "<b>" + msg.getLevel() + "</b> : " + msg.getDate() + "<br/>" + "Message: "
								+ msg.getMessage() + (msg.getLevel().equals(Level.ERROR) ? "</font>" : "") + "<br/>";
						if (!msg.getExceptionType().equals(ActionExceptionType.NO_EXCEPTION)) {
							text = text + "<b>Exception type:</b> " + msg.getExceptionType() + "<br/>"
									+ "<b>Cause:</b> " + msg.getCause() + "<br/>" + "<b>Cause message:</b> "
									+ msg.getCauseMessage() + "<br/>" + "<b>Root cause:</b> " + msg.getRootCause()
									+ "<br/>" + "<b>Root cause message:</b> " + msg.getRootCauseMessage();
						}
						if (!msg.getMessageTargets().isEmpty()) {
							text = text + "<b>Message targets:</b><ol>";
							for (ActionMessageTarget target : msg.getMessageTargets()) {
								text = text + "<li>" + target.getType() + " : " + target.getValue() + "</li>";
							}
							text = text + "</ol>";
						}
						text = text + "</li>";
					}
					text = text + "</ol>" + "<br/>";
				}

				self.sendMail(subject, text, tos);
			}
		} catch (ActionException e) {
			ActionLogger.error("Error mailing action notification!", new Object[] {}, e, false);
		}
	}

	@Override
	@Asynchronous
	@ActionContext(event = ActionEvent.NOTIFICATION, doRethrow = false, doDirectNotification = false)
	public void sendDailyActionNotification() throws ActionException {
		this.createAndSendIntervalNotification(NotificationInterval.DAILY);
	}

	@Override
	@Asynchronous
	@ActionContext(event = ActionEvent.NOTIFICATION, doRethrow = false, doDirectNotification = false)
	public void sendWeeklyActionNotification() throws ActionException {
		this.createAndSendIntervalNotification(NotificationInterval.WEEKLY);
	}

	private void createAndSendIntervalNotification(NotificationInterval interval) throws ActionException {
		Map<ActionEvent, Map<Level, Set<String>>> events = this.getNotificationSettings(interval);

		Date toDate = this.determineMailToDate();
		Date fromDate = this.determineMailFromDate(toDate, interval);

		for (ActionEvent event : events.keySet()) {
			Map<Level, Set<String>> levels = events.get(event);
			for (Level level : levels.keySet()) {
				String tos = this.createTosString(levels.get(level));

				if (tos == null) {
					continue;
				}

				List<Level> mailLevels = this.determinMailLevels(level);
				List<Action> actions = actionContextService.getNotificationActions(event, fromDate, toDate, mailLevels);

				Map<Level, Integer> counts = this.determineLevelCounts(level, actions);
				if (counts.keySet().isEmpty()) {
					continue;
				}

				// create message
				String subject = interval.name() + " overview for " + event.name() + " and level " + level.name();
				String text = "<b>" + event.name() + "</b><br/>" + "<b>From:</b> " + fromDate.toString() + "<br/>"
						+ "<b>To:</b> " + toDate.toString() + "<br/>" + "<b>Counts:</b><ol>";

				for (Level lvl : counts.keySet()) {
					text = text + "<li><b>" + lvl.name() + "</b> : " + counts.get(lvl).intValue() + "</li>";
				}
				text = text + "</ol>";

				self.sendMail(subject, text, tos);
			}
		}

	}

	private Map<Level, Integer> determineLevelCounts(Level level, List<Action> actions) {
		Map<Level, Integer> counts = new HashMap<Level, Integer>();

		if (Level.INFO.equals(level)) {
			int infoCount = this.getLevelCount(Level.INFO, actions);
			if (infoCount > 0) {
				counts.put(Level.INFO, infoCount);
			}
		}

		if (Level.WARN.equals(level) || Level.INFO.equals(level)) {
			int warnCount = this.getLevelCount(Level.WARN, actions);
			if (warnCount > 0) {
				counts.put(Level.WARN, warnCount);
			}
		}

		int errorCount = this.getLevelCount(Level.ERROR, actions);
		if (errorCount > 0) {
			counts.put(Level.ERROR, errorCount);
		}

		return counts;
	}

	private Integer getLevelCount(Level level, List<Action> actions) {
		if (Level.INFO.equals(level)) {
			return actions.size();
		} else {
			int count = 0;
			for (Action action : actions) {
				if (Level.WARN.equals(level)) {
					if (action.containsWarnings(false)) {
						count++;
					}
				} else if (Level.ERROR.equals(level)) {
					if (action.containsErrors()) {
						count++;
					}
				}
			}
			return count;
		}
	}

	private String createTosString(Set<String> toSet) {
		String tos = "";

		if (!toSet.isEmpty()) {
			for (String to : toSet) {
				tos = to + (tos.length() > 0 ? "," : "") + tos;
			}
		} else {
			return null;
		}

		return tos;
	}

	private List<Level> determinMailLevels(Level level) {
		List<Level> levels = new ArrayList<Level>();

		levels.add(Level.ERROR);

		if (Level.WARN.equals(level) || Level.INFO.equals(level)) {
			levels.add(Level.WARN);
		}

		if (Level.INFO.equals(level)) {
			levels.add(Level.INFO);
		}

		return levels;
	}

	private Date determineMailToDate() {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(ForecastBlockUtil.getDate(new Date()));

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(new Date());
		cal1.set(Calendar.HOUR_OF_DAY, cal2.get(Calendar.HOUR_OF_DAY));
		return new Date(cal1.getTime().getTime());
	}

	private Date determineMailFromDate(Date toDate, NotificationInterval interval) {
		return ForecastBlockUtil.addDaysToDate(toDate, NotificationInterval.DAILY.equals(interval) ? -1 : -7);
	}

	private Map<ActionEvent, Map<Level, Set<String>>> getNotificationSettings(NotificationInterval interval)
			throws ActionException {
		List<AdminUserNotification> notifications = genericStorageService
				.findActiveIntervalAdminUserNotifications(interval);

		Map<ActionEvent, Map<Level, Set<String>>> events = new HashMap<ActionEvent, Map<Level, Set<String>>>();

		for (AdminUserNotification notification : notifications) {
			if (notification.getUser().isActive() && notification.getUser().isReceiveEmail()) {
				if (notification.getUser().getEmail() != null && !notification.getUser().getEmail().trim().isEmpty()) {
					Map<Level, Set<String>> levels;
					ActionEvent event = notification.getEvent();
					if (events.containsKey(event)) {
						levels = events.get(event);
					} else {
						levels = new HashMap<Level, Set<String>>();
						events.put(event, levels);
					}

					Set<String> mails;
					Level level = notification.getLevel();
					if (levels.containsKey(level)) {
						mails = levels.get(level);
					} else {
						mails = new HashSet<String>();
						levels.put(level, mails);
					}

					mails.add(notification.getUser().getEmail());
				}
			}
		}

		return events;
	}

}