package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.model.ConfigurationParameter;

@Named("generalService")
@Stateless
public class GeneralServiceImpl implements GeneralServiceLocal {

	@Inject
	@Named("genericStorageService")
	GenericStorageService genericStorageService;

	@Override
	public ConfigurationParameter getConfigurationParameter(String name) throws ActionException {

		ConfigurationParameter param = genericStorageService.findConfigurationParameter(name);

		if (param == null) {
			throw new ActionException(ActionExceptionType.CONFIGURATION, "No ConfigurationParameter found with name "
					+ name);
		} else {
			return param;
		}

	}

	@Override
	public Integer getConfigurationParameterAsInteger(String name) throws ActionException {
		String value = "";
		try {
			value = this.getConfigurationParameter(name).getValue();
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			throw new ActionException(ActionExceptionType.CONFIGURATION, "Incorrect format for numeric integer value '"
					+ value + "' of '" + name + "'", e);
		} catch (ActionException e) {
			throw new ActionException(ActionExceptionType.CONFIGURATION, e.getMessage(), e);
		}
	}

	@Override
	public String getConfigurationParameterAsString(String name) throws ActionException {
		try {
			return this.getConfigurationParameter(name).getValue();
		} catch (ActionException e) {
			throw new ActionException(ActionExceptionType.CONFIGURATION, e.getMessage(), e);
		}
	}

	@Override
	public Double getConfigurationParameterAsDouble(String name) throws ActionException {
		String value = "";
		try {
			value = this.getConfigurationParameter(name).getValue();
			return Double.parseDouble(value);
		} catch (NumberFormatException e) {
			throw new ActionException(ActionExceptionType.CONFIGURATION, "Incorrect format for numeric double value '"
					+ value + "' of '" + name + "'", e);
		} catch (ActionException e) {
			throw new ActionException(ActionExceptionType.CONFIGURATION, e.getMessage(), e);
		}
	}

	@Override
	public Boolean getConfigurationParameterAsBoolean(String name) throws ActionException {
		String value = "";
		try {
			value = this.getConfigurationParameter(name).getValue();
			return Boolean.parseBoolean(value);
		} catch (ActionException e) {
			throw new ActionException(ActionExceptionType.CONFIGURATION, e.getMessage(), e);
		}
	}

}