package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionTargetInOut;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastDeviationDay;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.enumeration.DeviationDayType;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named("deviationDayDeterminationService")
@Stateless
public class DeviationDayDeterminationServiceImpl implements DeviationDayDeterminationServiceLocal {

	@EJB
	DeviationDayDeterminationServiceLocal self;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Inject
	@Named("forecastStorageService")
	ForecastStorageService forecastStorageService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Override
	@Asynchronous
	public void determineDeviationDayAsync(String cableId) {
		try {
			self.determineDeviationDay(cableId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@ActionContext(event = ActionEvent.DEVIATION_DAY_DETERMINATION, doRethrow = true)
	public void determineDeviationDay(String cableId) throws ActionException {

		Cable cable = topologyStorageService.findCableByCableId(cableId);

		nl.enexis.scip.action.ActionContext.addActionTarget(cable, ActionTargetInOut.IN);

		int maxDaysToCalculate = generalService
				.getConfigurationParameterAsInteger("NumberOfDeviationDaysForCalculations").intValue();
		double maxDeviationFactor = generalService.getConfigurationParameterAsDouble("MaximumDeviationDayFactor")
				.doubleValue();

		Date now = ForecastBlockUtil.getDate(new Date());
		Date last = ForecastBlockUtil.addDaysToDate(now, -1 * maxDaysToCalculate);

		CableForecastDeviationDay lastDevDay = forecastStorageService.getLastCableForecastDeviationDay(cable);

		if (lastDevDay != null && lastDevDay.getDay().after(last)) {
			last = lastDevDay.getDay();
		}

		// sorted treemap days with deviation calculation
		Map<Date, BigDecimal> calculatedDeviations = new TreeMap<Date, BigDecimal>();
		Map<Date, DeviationDayType> otherDeviations = new TreeMap<Date, DeviationDayType>();

		this.updateDeviationsUntilNow(cable, now, last, calculatedDeviations, otherDeviations);

		Set<Date> dates = new TreeSet<Date>();
		dates.addAll(calculatedDeviations.keySet());
		dates.addAll(otherDeviations.keySet());

		this.storeCableForecastDeviationDays(cable, maxDaysToCalculate, maxDeviationFactor, calculatedDeviations,
				otherDeviations, dates);
	}

	protected void updateDeviationsUntilNow(Cable cable, Date now, Date last,
			Map<Date, BigDecimal> calculatedDeviations, Map<Date, DeviationDayType> otherDeviations)
					throws ActionException {
		while (!last.equals(now)) {
			last = ForecastBlockUtil.addDaysToDate(last, 1);

			// get first success forecast of day
			CableForecast cf = this.getFirstCableForecastOfDay(cable, last);
			if (cf == null) {
				otherDeviations.put(last, DeviationDayType.NO_FORECAST);
				continue;
			} else if (cf.isFixed()) {
				otherDeviations.put(last, DeviationDayType.FIXED_FORECAST);
				continue;
			} else if (cf.getCableForecastOutputs().isEmpty()) {
				otherDeviations.put(last, DeviationDayType.NO_FORECAST);
				continue;
			}
			// get forecast inputs of day
			List<CableForecastInput> cfis = this.getCableForecastInputs(cf);
			Map<Long, CableForecastInput> cfisMap = this.mapCableForecastInputs(cfis);
			Map<Long, CableForecastOutput> cfosMap = this.mapCableForecastOutputs(cf);

			this.setDeviations(last, calculatedDeviations, otherDeviations, cfosMap, cfisMap);
		}
	}

	protected Map<Long, CableForecastInput> mapCableForecastInputs(List<CableForecastInput> cfis)
			throws ActionException {
		Map<Long, CableForecastInput> cfisMap = new HashMap<Long, CableForecastInput>();
		for (CableForecastInput cfi : cfis) {
			cfisMap.put(cfi.getBlockStart().getTime(), cfi);
		}
		return cfisMap;
	}

	protected Map<Long, CableForecastOutput> mapCableForecastOutputs(CableForecast cf) {
		List<CableForecastOutput> cfos = cf.getCableForecastOutputs();
		Map<Long, CableForecastOutput> cfosMap = new HashMap<Long, CableForecastOutput>();
		for (CableForecastOutput cfo : cfos) {
			Date blkStart = ForecastBlockUtil.getStartDateOfBlock(cfo.getDay(), cfo.getHour(), cfo.getBlock(),
					cf.getBlockMinutes());
			cfosMap.put(blkStart.getTime(), cfo);
		}
		return cfosMap;
	}

	protected void setDeviations(Date last, Map<Date, BigDecimal> calculatedDeviations,
			Map<Date, DeviationDayType> otherDeviations, Map<Long, CableForecastOutput> cfosMap,
			Map<Long, CableForecastInput> cfisMap) {
		int i = 0;
		BigDecimal deviation = new BigDecimal("0.00");
		Set<Long> keys = cfosMap.keySet();
		for (Long key : keys) {
			if (cfosMap.containsKey(key) && cfisMap.containsKey(key)) {
				i++;

				CableForecastOutput cfo = cfosMap.get(key);
				CableForecastInput cfi = cfisMap.get(key);

				// three phase differences
				BigDecimal ph1Diff = cfo.getUsageExclCars1().subtract(cfi.getAmpNow1()).abs();
				BigDecimal ph2Diff = cfo.getUsageExclCars2().subtract(cfi.getAmpNow2()).abs();
				BigDecimal ph3Diff = cfo.getUsageExclCars3().subtract(cfi.getAmpNow3()).abs();

				BigDecimal phAverageDiff = ph1Diff.add(ph2Diff).add(ph3Diff).divide(new BigDecimal("3"),
						RoundingMode.HALF_UP);
				deviation = deviation.add(phAverageDiff);
			}
		}

		if (i != 0) {
			deviation = deviation.divide(new BigDecimal(i), RoundingMode.HALF_UP);
			calculatedDeviations.put(last, deviation);
		} else {
			otherDeviations.put(last, DeviationDayType.NO_MEASUREMENTS);
		}
	}

	protected void storeCableForecastDeviationDays(Cable cable, int maxDaysToCalculate, double maxDeviationFactor,
			Map<Date, BigDecimal> calculatedDeviations, Map<Date, DeviationDayType> otherDeviations, Set<Date> dates)
					throws ActionException {
		for (Date day : dates) {
			boolean isCalculated = calculatedDeviations.containsKey(day);

			CableForecastDeviationDay cfdd = new CableForecastDeviationDay();
			cfdd.setCable(cable);
			cfdd.setDay(day);
			cfdd.setDeviation(isCalculated ? calculatedDeviations.get(day) : new BigDecimal("0.00"));

			BigDecimal average = this.getAverageDeviation(cable, maxDaysToCalculate);
			if (average == null) {
				cfdd.setAverageDeviation(cfdd.getDeviation());
			} else {
				cfdd.setAverageDeviation(average);
			}

			if (isCalculated) {
				if (cfdd.getDeviation().divide(cfdd.getAverageDeviation(), RoundingMode.HALF_UP)
						.compareTo(new BigDecimal(maxDeviationFactor)) < 0) {
					cfdd.setType(DeviationDayType.NO_DEVIATION);
				} else {
					cfdd.setType(DeviationDayType.DEVIATION);
				}
			} else {
				cfdd.setType(otherDeviations.get(day));
			}

			forecastStorageService.storeCableForecastDeviationDay(cfdd);
		}
	}

	private CableForecast getFirstCableForecastOfDay(Cable cable, Date day) throws ActionException {
		try {
			return forecastStorageService.getFirstCableForecastOfDay(cable, day);
		} catch (ActionException ae) {
			throw ae;
		} catch (Exception e) {
			e = new ActionException(e.getMessage(), e);
			throw (ActionException) e;
		}
	}

	private List<CableForecastInput> getCableForecastInputs(CableForecast cf) throws ActionException {
		try {
			return forecastStorageService.getCableForecastInputs(cf);
		} catch (ActionException ae) {
			throw ae;
		} catch (Exception e) {
			e = new ActionException(e.getMessage(), e);
			throw (ActionException) e;
		}
	}

	private BigDecimal getAverageDeviation(Cable cable, int numberOfDays) throws ActionException {
		List<CableForecastDeviationDay> cfdds = forecastStorageService.getNotDeviatedCableForecastDeviationDays(cable,
				numberOfDays);

		if (cfdds.isEmpty()) {
			return null;
		}

		BigDecimal average = new BigDecimal("0.00");

		for (CableForecastDeviationDay cfdd : cfdds) {
			average = average.add(cfdd.getDeviation());
		}
		average = average.divide(new BigDecimal(cfdds.size()), RoundingMode.HALF_UP);
		
		if (average.equals(new BigDecimal("0.00"))) {
			throw new ActionException(ActionExceptionType.DATA, "Encountered (very) unusual event that the (average) deviation of last 30 days without deviation is ZERO!");
		}

		return average;
	}

}