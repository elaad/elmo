package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspConnection;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.model.DsoCsp;

@Named("topologyService")
@Stateless
public class TopologyServiceImpl implements TopologyServiceLocal {

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	/**
	 * Find last date the topology on cable with provided id is changed
	 * regarding capacity Csp(Connection)s
	 * 
	 * @param cableId
	 * @return
	 * @throws ActionException
	 */
	@Override
	public Date lastCableCspTopologyChange(String cableId) throws ActionException {
		Cable cable = topologyStorageService.findCableByCableId(cableId);
		return cable.getCspLastCapacityChange();
	}

	@Override
	public boolean isCspActiveForCable(String cableId, String cspEan)
			throws ActionException {
		boolean isActive = false;

		Cable cable = topologyStorageService.findCableByCableId(cableId);

		Csp csp = topologyStorageService.findCspByEan13(cspEan);

		CableCsp cableCsp = topologyStorageService.findCableCspByCableAndCsp(
				csp, cable);

		if (cableCsp.isActive() && csp.isActive()) {
			for (DsoCsp dsoCsp : csp.getDsoCsps()) {
				if (dsoCsp.isActive() && dsoCsp.getDso().equals(cable.getDso())
						&& dsoCsp.getDso().isActive()) {
					for (CspConnection cspConnection : cableCsp
							.getCspConnections()) {
						if (cspConnection.isActive()) {
							isActive = true;
							break;
						}
					}
				}
			}
		}

		return isActive;
	}

	@Override
	public boolean hasCableActiveCsp(String cableId,
			boolean doIncludeConnections) throws ActionException {
		Cable cable = topologyStorageService.findCableByCableId(cableId);

		boolean hasActiveCsp = false;
		// TODO Generates error
		for (CableCsp cableCsp : cable.getCableCsps()) {
			Csp csp = cableCsp.getCsp();
			if (cableCsp.isActive() && csp.isActive()) {
				for (DsoCsp dsoCsp : csp.getDsoCsps()) {
					if (dsoCsp.isActive()
							&& dsoCsp.getDso().equals(cable.getDso())) {
						if (doIncludeConnections) {
							for (CspConnection cspConnection : cableCsp
									.getCspConnections()) {
								if (cspConnection.isActive()) {
									hasActiveCsp = true;
									break;
								}
							}
						} else {
							hasActiveCsp = true;
						}
						if (hasActiveCsp) {
							break;
						}
					}
				}
				if (hasActiveCsp) {
					break;
				}
			}
		}
		return hasActiveCsp;
	}

	@Override
	public void checkRelationCspAndDso(String cspEan, String dsoEan)
			throws ActionException {

		Csp csp = topologyStorageService.findCspByEan13(cspEan);

		if (csp == null) {
			throw new ActionException(ActionExceptionType.DATA,
					"Csp not found! Csp: " + cspEan);
		} else {
			if (!csp.isActive()) {
				throw new ActionException(ActionExceptionType.DATA,
						"Csp found but not active! Csp: " + cspEan);
			}
		}

		Dso dso = topologyStorageService.findDsoByEan13(dsoEan);

		if (dso == null) {
			throw new ActionException(ActionExceptionType.DATA,
					"Dso not found! Dso: " + dsoEan);
		} else {
			if (!dso.isActive()) {
				throw new ActionException(ActionExceptionType.DATA,
						"Dso found but not active! Dso: " + dsoEan);
			}
		}

		boolean dsoFoundForCsp = false;
		for (DsoCsp dsoCsp : csp.getDsoCsps()) {
			if (dsoCsp.getDso().getEan13().equals(dso.getEan13())) {
				dsoFoundForCsp = true;
				if (!dsoCsp.isActive()) {
					throw new ActionException(
							ActionExceptionType.DATA,
							"Dso-Csp relation found but not active! Dso: "
									+ dsoEan + " - Csp: " + cspEan);
				}
				break;
			}
		}

		if (!dsoFoundForCsp) {
			throw new ActionException(ActionExceptionType.DATA,
					"Provided Csp en Dso are not related! Dso: " + dsoEan
							+ "; Csp: " + cspEan);
		}

	}

	@Override
	public void checkCableRelatedToCspAndDso(String cableId, String cspEan,
			String dsoEan) throws ActionException {

		Cable cable = topologyStorageService.findCableByCableId(cableId);

		if (cable == null) {
			throw new ActionException(ActionExceptionType.DATA,
					"Cable not found! Cable: " + cableId);
		} else {
			if (!cable.isActive()) {
				throw new ActionException(ActionExceptionType.DATA,
						"Cable found but not active! Cable: " + cableId);
			}
		}

		if (!cable.getDso().getEan13().equals(dsoEan)) {
			throw new ActionException(ActionExceptionType.DATA,
					"Provided Cable en Dso are not related! Cable: " + cableId
							+ "; Dso: " + dsoEan);
		}

		if (!cable.getDso().isActive()) {
			throw new ActionException(ActionExceptionType.DATA,
					"Provided Cable en Dso are related but Dso not active! Cable: "
							+ cableId + "; Dso: " + dsoEan);
		}

		boolean cspFoundForCable = false;
		for (CableCsp cableCsp : cable.getCableCsps()) {
			if (cableCsp.getCsp().getEan13().equals(cspEan)) {
				cspFoundForCable = true;
				if (!cableCsp.getCsp().isActive()) {
					throw new ActionException(ActionExceptionType.DATA,
							"Provided Cable en Csp are related but Csp not active! Cable: "
									+ cableId + "; Csp: " + cspEan);
				} else if (!cableCsp.isActive()) {
					throw new ActionException(
							ActionExceptionType.DATA,
							"Cable-Csp relation found but not active! Cable: "
									+ cableId + "; Csp: " + cspEan);
				}
				break;
			}
		}

		if (!cspFoundForCable) {
			throw new ActionException(ActionExceptionType.DATA,
					"Provided Cable en Csp are not related! Cable: " + cableId
							+ "; Csp: " + cspEan);
		}
	}

//	@Override
//	public void processCableTopology(Cable cableToProcess) throws ActionException {
//		Error error = new Error();
//		error.setEvent(SCiPEvent.TOPOLOGY);
//		error.setEventId("Cable-" + cableToProcess.getCableId());
//
//		try {
//			Transformer transformer = this.addOrUpdateTransformer(cableToProcess.getTransformer());
//
//			Dso dso = this.addOrUpdateDso(cableToProcess.getDso());
//
//			Cable cable = this.addOrUpdateCable(cableToProcess, transformer, dso);
//
//			for (Meter meter : cableToProcess.getMeters()) {
//				this.addOrUpdateMeter(meter, transformer, cable);
//			}
//
//			for (CableCsp cableCspToProcess : cableToProcess.getCableCsps()) {
//				Csp cspToProcess = cableCspToProcess.getCsp();
//
//				Csp csp = this.addOrUpdateCsp(cspToProcess);
//
//				CableCsp cableCsp = this.addOrUpdateCableCsp(cableCspToProcess, cable, csp);
//
//				for (CspConnection cspConnection : cableCspToProcess.getCspConnections()) {
//					this.addOrUpdateCspConnection(cspConnection, cableCsp);
//				}
//
//				for (DsoCsp dsoCspToProcess : cspToProcess.getDsoCsps()) {
//					this.addOrUpdateDsoCsp(dsoCspToProcess, dso, csp);
//				}
//			}
//		} catch (ActionException e) {
//			e.setError(error);
//			catchEvent.fire(new ExceptionToCatchEvent(e));
//			throw e;
//		} catch (Exception e) {
//			ActionException ex = new ActionException(e.getMessage(), e);
//			ex.setError(error);
//			catchEvent.fire(new ExceptionToCatchEvent(ex));
//			throw ex;
//		}
//	}
//
//	private Dso addOrUpdateDso(Dso dso) throws ActionException {
//		Dso dsoDb = topologyStorageService.findDsoByEan13(dso.getEan13());
//
//		if (dsoDb != null) {
//			if (dsoDb.isActive() != dso.isActive()) {
//				dsoDb.setChangedDate(new Date());
//			}
//			dsoDb.setName(dso.getName());
//			dsoDb.setActive(dso.isActive());
//			return topologyStorageService.updateDso(dsoDb);
//		} else {
//			Dso newDso = new Dso();
//			newDso.setEan13(dso.getEan13());
//			newDso.setName(dso.getName());
//			newDso.setActive(dso.isActive());
//			Date currentDate = new Date();
//			newDso.setChangedDate(currentDate);
//			return topologyStorageService.createDso(newDso);
//		}
//	}
//
//	private Transformer addOrUpdateTransformer(Transformer transformer) throws ActionException {
//		Transformer transformerDb = topologyStorageService.findDsoByTrafoId(transformer.getTrafoId());
//
//		if (transformerDb != null) {
//			if (transformerDb.isActive() != transformer.isActive()) {
//				transformerDb.setChangedDate(new Date());
//			}
//			transformerDb.setActive(transformer.isActive());
//			if (!transformerDb.isActive()) {
//				transformerDb.setLastMeasurement(null);
//			}
//			return topologyStorageService.updateTransformer(transformerDb);
//		} else {
//			Transformer newTransformer = new Transformer();
//			newTransformer.setTrafoId(transformer.getTrafoId());
//			newTransformer.setActive(transformer.isActive());
//			Date currentDate = new Date();
//			newTransformer.setChangedDate(currentDate);
//			return topologyStorageService.createTransformer(newTransformer);
//		}
//	}
//
//	private Cable addOrUpdateCable(Cable cable, Transformer transformer, Dso dso) throws ActionException {
//		Cable cableDb = topologyStorageService.findCableByCableId(cable.getCableId());
//
//		if (cableDb != null) {
//			if (cableDb.isActive() != cable.isActive() || cableDb.getMaxCapacity().compareTo(cable.getMaxCapacity()) != 0
//					|| cableDb.getMaxCarCapacity().compareTo(cable.getMaxCarCapacity()) != 0
////					|| cableDb.getSafetyFactor().compareTo(cable.getSafetyFactor()) != 0
////					|| cableDb.getRemainingFactor().compareTo(cable.getRemainingFactor()) != 0
//					|| !cableDb.getUsageMeasurementResponsible().equals(cable.getUsageMeasurementResponsible())
//					|| !cableDb.getDso().equals(dso) || !cableDb.getTransformer().equals(transformer)) {
//				cableDb.setChangedDate(new Date());
//			}
//			cableDb.setDso(dso);
//			cableDb.setTransformer(transformer);
//			cableDb.setMaxCapacity(cable.getMaxCapacity());
//			cableDb.setMaxCarCapacity(cable.getMaxCarCapacity());
//			cableDb.setSafetyFactor(cable.getSafetyFactor());
//			cableDb.setRemainingFactor(cable.getRemainingFactor());
//			cableDb.setUsageMeasurementResponsible(cable.getUsageMeasurementResponsible());
//			cableDb.setActive(cable.isActive());
//
//			// test
//			cableDb.setFixedForecast(cable.isFixedForecast());
//			cableDb.setFixedForecastRandom(cable.isFixedForecastRandom());
//			cableDb.setFixedForecastMax(cable.getFixedForecastMax());
//			cableDb.setFixedForecastDeviation(cable.getFixedForecastDeviation());
//			cableDb.setFixedForecastDeviationBlocks(cable.getFixedForecastDeviationBlocks());
//
//			return topologyStorageService.updateCable(cableDb);
//		} else {
//			Cable newCable = new Cable();
//			newCable.setDso(dso);
//			newCable.setTransformer(transformer);
//			newCable.setCableId(cable.getCableId());
//			newCable.setMaxCapacity(cable.getMaxCapacity());
//			newCable.setMaxCarCapacity(cable.getMaxCarCapacity());
//			newCable.setSafetyFactor(cable.getSafetyFactor());
//			newCable.setRemainingFactor(cable.getRemainingFactor());
//			newCable.setUsageMeasurementResponsible(cable.getUsageMeasurementResponsible());
//			newCable.setActive(cable.isActive());
//			Date currentDate = new Date();
//			newCable.setChangedDate(currentDate);
//
//			// test
//			newCable.setFixedForecast(cable.isFixedForecast());
//			newCable.setFixedForecastRandom(cable.isFixedForecastRandom());
//			newCable.setFixedForecastMax(cable.getFixedForecastMax());
//			newCable.setFixedForecastDeviation(cable.getFixedForecastDeviation());
//			newCable.setFixedForecastDeviationBlocks(cable.getFixedForecastDeviationBlocks());
//
//			return topologyStorageService.createCable(newCable);
//		}
//	}
//
//	private Meter addOrUpdateMeter(Meter meter, Transformer transformer, Cable cable) throws ActionException {
//		Meter meterDb = topologyStorageService.findMeterByMeterId(meter.getMeterId());
//
//		if (meterDb != null) {
//			if (meterDb.isActive() != meter.isActive() || meterDb.getMeasures().equals(meter.getMeasures())
//					|| !meterDb.getTransformer().equals(transformer) || !meterDb.getCable().equals(cable)) {
//				meterDb.setChangedDate(new Date());
//			}
//			meterDb.setCable(cable);
//			meterDb.setTransformer(transformer);
//			meterDb.setMeasures(meter.getMeasures());
//			meterDb.setMeterName(meter.getMeterName());
//			meterDb.setActive(meter.isActive());
//			return topologyStorageService.updateMeter(meterDb);
//		} else {
//			Meter newMeter = new Meter();
//			newMeter.setCable(cable);
//			newMeter.setTransformer(transformer);
//			newMeter.setMeasures(meter.getMeasures());
//			newMeter.setMeterId(meter.getMeterId());
//			newMeter.setMeterName(meter.getMeterName());
//			newMeter.setActive(meter.isActive());
//			Date currentDate = new Date();
//			newMeter.setChangedDate(currentDate);
//			return topologyStorageService.createMeter(newMeter);
//		}
//	}
//
//	private Csp addOrUpdateCsp(Csp csp) throws ActionException {
//		Csp cspDb = topologyStorageService.findCspByEan13(csp.getEan13());
//
//		if (cspDb != null) {
//			if (cspDb.isActive() != csp.isActive()) {
//				cspDb.setChangedDate(new Date());
//			}
//			cspDb.setName(csp.getName());
//			cspDb.setEndpoint(csp.getEndpoint());
//			cspDb.setActive(csp.isActive());
//			return topologyStorageService.updateCsp(cspDb);
//		} else {
//			Csp newCsp = new Csp();
//			newCsp.setEan13(csp.getEan13());
//			newCsp.setName(csp.getName());
//			newCsp.setEndpoint(csp.getEndpoint());
//			newCsp.setActive(csp.isActive());
//			Date currentDate = new Date();
//			newCsp.setChangedDate(currentDate);
//			return topologyStorageService.createCsp(newCsp);
//		}
//	}
//
//	private CspConnection addOrUpdateCspConnection(CspConnection cspConnection, CableCsp cableCsp) throws ActionException {
//		CspConnection cspConnectionDb = topologyStorageService.findCspConnectionByEan18(cspConnection.getEan18());
//
//		if (cspConnectionDb != null) {
//			if (cspConnectionDb.isActive() != cspConnection.isActive()
//					|| cspConnectionDb.getCapacity().compareTo(cspConnection.getCapacity()) != 0
//					|| !cspConnectionDb.getCableCsp().equals(cableCsp)) {
//				cspConnectionDb.setChangedDate(new Date());
//			}
//			cspConnectionDb.setCapacity(cspConnection.getCapacity());
//			cspConnectionDb.setCableCsp(cableCsp);
//			cspConnectionDb.setActive(cspConnection.isActive());
//			return topologyStorageService.updateCspConnection(cspConnectionDb);
//		} else {
//			CspConnection newCspConnection = new CspConnection();
//			newCspConnection.setEan18(cspConnection.getEan18());
//			newCspConnection.setCapacity(cspConnection.getCapacity());
//			newCspConnection.setCableCsp(cableCsp);
//			newCspConnection.setActive(cspConnection.isActive());
//			Date currentDate = new Date();
//			newCspConnection.setAddedDate(currentDate);
//			newCspConnection.setChangedDate(currentDate);
//			return topologyStorageService.createCspConnection(newCspConnection);
//		}
//	}
//
//	private CableCsp addOrUpdateCableCsp(CableCsp cableCsp, Cable cable, Csp csp) throws ActionException {
//		CableCsp cableCspDb = topologyStorageService.findCableCspByCableAndCsp(cableCsp.getCsp(), cableCsp.getCable());
//
//		if (cableCspDb != null) {
//			if (cableCspDb.isActive() != cableCsp.isActive()) {
//				cableCspDb.setChangedDate(new Date());
//			}
//			cableCspDb.setNumberOfBlocks(cableCsp.getNumberOfBlocks());
//			cableCspDb.setActive(cableCsp.isActive());
//			return topologyStorageService.updateCableCsp(cableCspDb);
//		} else {
//			CableCsp newCableCsp = new CableCsp();
//			newCableCsp.setCable(cable);
//			newCableCsp.setCsp(csp);
//			newCableCsp.setNumberOfBlocks(cableCsp.getNumberOfBlocks());
//			newCableCsp.setActive(cableCsp.isActive());
//			Date currentDate = new Date();
//			newCableCsp.setAddedDate(currentDate);
//			newCableCsp.setChangedDate(currentDate);
//			return topologyStorageService.createCableCsp(newCableCsp);
//		}
//	}
//
//	private DsoCsp addOrUpdateDsoCsp(DsoCsp dsoCsp, Dso dso, Csp csp) throws ActionException {
//		DsoCsp dsoCspDb = topologyStorageService.findDsoCspByDsoAndCsp(dso, csp);
//
//		if (dsoCspDb != null) {
//			if (dsoCspDb.isActive() != dsoCsp.isActive()) {
//				dsoCspDb.setChangedDate(new Date());
//			}
//			dsoCspDb.setActive(dsoCsp.isActive());
//			return topologyStorageService.updateDsoCsp(dsoCspDb);
//		} else {
//			DsoCsp newDsoCsp = new DsoCsp();
//			newDsoCsp.setDso(dso);
//			newDsoCsp.setCsp(csp);
//			newDsoCsp.setActive(dsoCsp.isActive());
//			Date currentDate = new Date();
//			newDsoCsp.setAddedDate(currentDate);
//			newDsoCsp.setChangedDate(currentDate);
//			return topologyStorageService.createDsoCsp(newDsoCsp);
//		}
//	}

}