package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.Measurement;
import nl.enexis.scip.model.MeasurementValue;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.model.enumeration.MeasuresFor;
import nl.enexis.scip.model.enumeration.UsageMeasurementResponsible;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named("prepareForecastInputService")
@Stateless
public class PrepareForecastInputServiceImpl implements PrepareForecastInputServiceLocal {

	@Inject
	@Named("forecastStorageService")
	ForecastStorageService forecastStorageService;

	@Inject
	@Named("measurementStorageService")
	MeasurementStorageService measurementStorageService;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Override
	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@ActionContext(event = ActionEvent.PREPARE_FORECAST_INPUT, doRethrow = false)
	public void prepareForecastInputs(Cable inputCable) throws ActionException {
		String cableId = inputCable.getCableId();

		try {
			Cable cable = topologyStorageService.findCableByCableId(inputCable.getCableId());

			if (cable == null) {
				throw new ActionException(ActionExceptionType.DATA, "Cable not found! Cable: " + cableId);
			}

			if (!cable.isActive()) {
				ActionLogger.warn("Trying to prepare forecast input for inactive cable {0}", new Object[] { cableId }, null, true);
				return;
			}

			boolean includeCarUsage = cable.getUsageMeasurementResponsible() == UsageMeasurementResponsible.DSO;

			int numberOfInputs = Integer.parseInt(generalService.getConfigurationParameter("ForecastInputBlocks").getValue());
			int measurementsPerBlock = Integer.parseInt(generalService.getConfigurationParameter("MeasurementsPerBlock").getValue());
			int blockMinutes = Integer.parseInt(generalService.getConfigurationParameter("ForecastBlockMinutes").getValue());

			Map<String, Object> currentBlockValues = ForecastBlockUtil.getCurrentBlockValues(blockMinutes);
			Date currentBlockDate = ForecastBlockUtil.getStartDateOfBlock(currentBlockValues, blockMinutes);
			Map<String, Object> blockValues = ForecastBlockUtil.addBlocksToBlockValues(currentBlockValues, blockMinutes, numberOfInputs
					* -1);
			blockValues = ForecastBlockUtil.getPrevBlockValues(blockValues, blockMinutes);
			Date fromDate = ForecastBlockUtil.getStartDateOfBlock(blockValues, blockMinutes);

			CableForecastInput input = forecastStorageService.getLastCableForecastInput(cable, fromDate);

			if (input != null) {
				blockValues = ForecastBlockUtil.getBlockValues(input.getBlockStart(), blockMinutes);
				blockValues = ForecastBlockUtil.getNextBlockValues(blockValues, blockMinutes);
			}

			while (ForecastBlockUtil.getStartDateOfBlock(blockValues, blockMinutes).compareTo(currentBlockDate) < 0) {
				Date blockDay = (Date) blockValues.get(ForecastBlockUtil.DAY);
				Integer blockHour = (Integer) blockValues.get(ForecastBlockUtil.HOUR);
				Integer blockNumber = (Integer) blockValues.get(ForecastBlockUtil.BLOCK);

				input = new CableForecastInput();
				input.setCable(cable);
				input.setDay(blockDay);
				input.setHour(blockHour);
				input.setBlock(blockNumber);

				// Today Y
				Date startDate = ForecastBlockUtil.getStartDateOfBlock(blockDay, blockHour, blockNumber, blockMinutes);
				Date endDate = ForecastBlockUtil.getEndDateOfBlock(blockDay, blockHour, blockNumber, blockMinutes);

				input.setBlockStart(startDate);
				input.setBlockEnd(endDate);

				List<Measurement> measurementsAmpNow = this.getMeasurementForCableBetweenDates(input, startDate, endDate, cable,
						measurementsPerBlock, false);

				if (measurementsAmpNow.size() < measurementsPerBlock) {
					blockValues = ForecastBlockUtil.getNextBlockValues(blockValues, blockMinutes);
					continue;
				}

				// yesterday X-1
				startDate = ForecastBlockUtil.addDaysToDate(startDate, -1);
				endDate = ForecastBlockUtil.addDaysToDate(endDate, -1);
				List<Measurement> measurementsAmpMinusOneDay = this.getMeasurementForCableBetweenDates(input, startDate, endDate, cable,
						measurementsPerBlock, true);

				if (measurementsAmpMinusOneDay.size() < measurementsPerBlock) {
					blockValues = ForecastBlockUtil.getNextBlockValues(blockValues, blockMinutes);
					continue;
				}

				startDate = ForecastBlockUtil.addDaysToDate(startDate, -5);
				endDate = ForecastBlockUtil.addDaysToDate(endDate, -5);
				List<Measurement> measurementsAmpMinusSixDays = this.getMeasurementForCableBetweenDates(input, startDate, endDate, cable,
						measurementsPerBlock, true);

				if (measurementsAmpMinusSixDays.size() < measurementsPerBlock) {
					blockValues = ForecastBlockUtil.getNextBlockValues(blockValues, blockMinutes);
					continue;
				}

				startDate = ForecastBlockUtil.addDaysToDate(startDate, -1);
				endDate = ForecastBlockUtil.addDaysToDate(endDate, -1);
				List<Measurement> measurementsAmpMinusOneWeek = this.getMeasurementForCableBetweenDates(input, startDate, endDate, cable,
						measurementsPerBlock, true);

				if (measurementsAmpMinusOneWeek.size() < measurementsPerBlock) {
					blockValues = ForecastBlockUtil.getNextBlockValues(blockValues, blockMinutes);
					continue;
				}

				BigDecimal[] ampArray = this.calculateMeasurementAmps(input, measurementsAmpNow, includeCarUsage);
				BigDecimal dividor = new BigDecimal(String.valueOf(measurementsAmpNow.size()));
				input.setAmpNow1(ampArray[0].divide(dividor, RoundingMode.HALF_UP));
				input.setAmpNow2(ampArray[1].divide(dividor, RoundingMode.HALF_UP));
				input.setAmpNow3(ampArray[2].divide(dividor, RoundingMode.HALF_UP));

				ampArray = this.calculateMeasurementAmps(input, measurementsAmpMinusOneDay, includeCarUsage);
				BigDecimal dividor2 = new BigDecimal(String.valueOf(measurementsAmpMinusOneDay.size()));
				input.setAmpMinusOneDay1(ampArray[0].divide(dividor2, RoundingMode.HALF_UP));
				input.setAmpMinusOneDay2(ampArray[1].divide(dividor2, RoundingMode.HALF_UP));
				input.setAmpMinusOneDay3(ampArray[2].divide(dividor2, RoundingMode.HALF_UP));

				ampArray = this.calculateMeasurementAmps(input, measurementsAmpMinusSixDays, includeCarUsage);
				BigDecimal dividor4 = new BigDecimal(String.valueOf(measurementsAmpMinusSixDays.size()));
				input.setAmpMinusSixDay1(ampArray[0].divide(dividor4, RoundingMode.HALF_UP));
				input.setAmpMinusSixDay2(ampArray[1].divide(dividor4, RoundingMode.HALF_UP));
				input.setAmpMinusSixDay3(ampArray[2].divide(dividor4, RoundingMode.HALF_UP));

				ampArray = this.calculateMeasurementAmps(input, measurementsAmpMinusOneWeek, includeCarUsage);
				BigDecimal dividor3 = new BigDecimal(String.valueOf(measurementsAmpMinusOneWeek.size()));
				input.setAmpMinusOneWeek1(ampArray[0].divide(dividor3, RoundingMode.HALF_UP));
				input.setAmpMinusOneWeek2(ampArray[1].divide(dividor3, RoundingMode.HALF_UP));
				input.setAmpMinusOneWeek3(ampArray[2].divide(dividor3, RoundingMode.HALF_UP));

				forecastStorageService.createCableForecastInput(input);

				blockValues = ForecastBlockUtil.getNextBlockValues(blockValues, blockMinutes);
			}
		} catch (ActionException ae) {
			throw ae;
		} catch (Exception e) {
			throw new ActionException(e.getMessage(), e);
		}

	}

	// use delegate instead?
	protected BigDecimal[] calculateMeasurementAmps(CableForecastInput input, List<Measurement> measurementsAmp, boolean includeCarUsage) {
		BigDecimal[] ampArray = { new BigDecimal(0), new BigDecimal(0), new BigDecimal(0) };
		for (Measurement measurement : measurementsAmp) {
			List<MeasurementValue> measurementValues = measurement.getMeasurementValues();
			for (MeasurementValue measurementValue : measurementValues) {
				if (measurementValue.getMeter().getMeasures() == MeasuresFor.TOTALS) {
					ampArray[0] = ampArray[0].add(measurementValue.getIGem1());
					ampArray[1] = ampArray[1].add(measurementValue.getIGem2());
					ampArray[2] = ampArray[2].add(measurementValue.getIGem3());
				} else if (includeCarUsage && measurementValue.getMeter().getMeasures() == MeasuresFor.CARS) {
					ampArray[0] = ampArray[0].subtract(measurementValue.getIGem1());
					ampArray[1] = ampArray[1].subtract(measurementValue.getIGem2());
					ampArray[2] = ampArray[2].subtract(measurementValue.getIGem3());
				}
			}
		}
		return ampArray;
	}

	protected List<Measurement> getMeasurementForCableBetweenDates(CableForecastInput input, Date startDate, Date endDate, Cable cable,
			int measurementsPerBlock, boolean doCheckDeviatedDay) throws ActionException {

		Date start = new Date(startDate.getTime());
		Date end = new Date(endDate.getTime());

		int weeksBackward = 0;
		if (doCheckDeviatedDay) {
			Date day = ForecastBlockUtil.getDate(start);

			while (forecastStorageService.isCableForecastDeviationDay(cable, day)) {
				weeksBackward++;
				day = ForecastBlockUtil.addDaysToDate(start, -7 * weeksBackward);
			}
			start = ForecastBlockUtil.addDaysToDate(start, -7 * weeksBackward);
			end = ForecastBlockUtil.addDaysToDate(end, -7 * weeksBackward);
		}

		List<Measurement> measurements = measurementStorageService.getMeasurementForCableBetweenDates(start, end, cable);

		if (measurements.size() < measurementsPerBlock) {
			if (measurements.isEmpty()) {
				ActionLogger.debug("No measurements found for {0} forecast input. Search starting at {1} and used {2} weeks in past",
						new Object[] { cable.getCableId(), startDate.toString(), weeksBackward }, null, false);
			}

			// only the case for last blocks if measurements are not received yet
			return measurements;
		}

		List<Measurement> returnMeasurements = new ArrayList<Measurement>();

		for (Measurement measurement : measurements) {
			if (measurement.isComplete()) {
				returnMeasurements.add(measurement);
			} else {
				returnMeasurements.add(this.replaceIncompleteMeasurement(measurement));
				input.setCorrected(true); // only uses the last measurement
			}
		}

		return returnMeasurements;
	}

	protected Measurement replaceIncompleteMeasurement(Measurement measurement) throws ActionException {
		Measurement previous = measurementStorageService.getClosestCompleteMeasurement(measurement, false);
		Measurement next = measurementStorageService.getClosestCompleteMeasurement(measurement, true);

		if (previous == null && next == null) {
			throw new ActionException(ActionExceptionType.DATA,
					"No next and previous complete measurement to replace incomplete measurement for " + measurement);
		}

		List<Measurement> measurements = new ArrayList<Measurement>();

		if (previous != null) {
			measurements.add(previous);
		}
		if (next != null) {
			measurements.add(next);
		}

		BigDecimal totals1 = new BigDecimal("0.00");
		BigDecimal totals2 = new BigDecimal("0.00");
		BigDecimal totals3 = new BigDecimal("0.00");
		BigDecimal cars1 = new BigDecimal("0.00");
		BigDecimal cars2 = new BigDecimal("0.00");
		BigDecimal cars3 = new BigDecimal("0.00");
		Meter totals = null;
		Meter cars = null;

		for (Measurement otherMeasurement : measurements) {
			List<MeasurementValue> measurementValues = otherMeasurement.getMeasurementValues();
			for (MeasurementValue measurementValue : measurementValues) {
				if (measurementValue.getMeter().getMeasures() == MeasuresFor.TOTALS) {
					totals1 = totals1.add(measurementValue.getIGem1());
					totals2 = totals2.add(measurementValue.getIGem2());
					totals3 = totals3.add(measurementValue.getIGem3());
					totals = measurementValue.getMeter(); // TODO: can be more efficient/neater
				} else if (measurementValue.getMeter().getMeasures() == MeasuresFor.CARS) {
					cars1 = cars1.add(measurementValue.getIGem1());
					cars2 = cars2.add(measurementValue.getIGem2());
					cars3 = cars3.add(measurementValue.getIGem3());
					cars = measurementValue.getMeter(); // TODO: can be more efficient/neater
				}
			}
		}

		BigDecimal dividor = new BigDecimal(String.valueOf(measurements.size()));

		Measurement replace = new Measurement();

		MeasurementValue mv = new MeasurementValue();
		mv.setIGem1(totals1.divide(dividor, RoundingMode.HALF_UP));
		mv.setIGem2(totals2.divide(dividor, RoundingMode.HALF_UP));
		mv.setIGem3(totals3.divide(dividor, RoundingMode.HALF_UP));
		mv.setMeter(totals);
		replace.getMeasurementValues().add(mv);

		mv = new MeasurementValue();
		mv.setIGem1(cars1.divide(dividor, RoundingMode.HALF_UP));
		mv.setIGem2(cars2.divide(dividor, RoundingMode.HALF_UP));
		mv.setIGem3(cars3.divide(dividor, RoundingMode.HALF_UP));
		mv.setMeter(cars);
		replace.getMeasurementValues().add(mv);

		return replace;
	}
}