package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import nl.enexis.scip.action.ActionContext;
import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.cluster.command.ResetTimersCommand;
import nl.enexis.scip.timer.TimerInterface;

import org.wildfly.clustering.dispatcher.CommandDispatcher;
import org.wildfly.clustering.dispatcher.CommandDispatcherFactory;
import org.wildfly.clustering.dispatcher.CommandResponse;
import org.wildfly.clustering.group.Node;

@Named("timersService")
@Stateless
public class TimersServiceImpl implements TimersServiceRemote, TimersServiceLocal {

	private boolean isClustered = new Boolean(System.getProperty("scip.cluster.enabled", "false"));

	@Resource
	TimerService timerService;

	@Resource(lookup = "java:jboss/clustering/dispatcher/web")
	CommandDispatcherFactory dispatcherFactory = null;

	@Override
	public void resetTimers() throws ActionException {
		if (this.isClustered) {
//			CommandDispatcherFactory dispatcherFactory;
//			try {
//				Properties p = new Properties();
//				p.put(Context.URL_PKG_PREFIXES, "org.jboss.as.naming.interfaces:org.jboss.ejb.client.naming");
//				dispatcherFactory = (CommandDispatcherFactory) new InitialContext(p)
//						.lookup("java:jboss/clustering/dispatcher/web");
//			} catch (NamingException e) {
//				throw new ActionException(ActionExceptionType.TIMER, "Exception resetting timers on cluster nodes!", e);
//			}

			CommandDispatcher<String> dispatcher = dispatcherFactory.createCommandDispatcher(ActionContext.getId(), "");
			Map<Node, CommandResponse<Boolean>> responses;
			try {
				responses = dispatcher.executeOnCluster(new ResetTimersCommand());
			} catch (Exception e) {
				throw new ActionException(ActionExceptionType.TIMER, "Exception resetting timers!", e);
			}

			for (Node node : responses.keySet()) {
				try {
					if (!responses.get(node).get()) {
						throw new ActionException(ActionExceptionType.TIMER,
								"No all timers correctly reset on cluster node " + node.getName() + "!");
					}
				} catch (ExecutionException e) {
					throw new ActionException(ActionExceptionType.TIMER, "Exception resetting timers on cluster node "
							+ node.getName() + "!", e);
				}
			}
		} else {
			this.resetLocalTimers();
		}
	}

	@Override
	public void resetLocalTimers() throws ActionException {
		boolean success = true;

		Collection<Timer> timers = timerService.getAllTimers();

		for (Timer timer : timers) {
			try {
				if (ActionEvent.SEND_CAPACITY_FORECAST.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext().lookup("java:global/SCiP/SCiP-domain-ejb/CspMessageTimer!nl.enexis.scip.timer.TimerInterface"))
							.reset();
				} else if (ActionEvent.FETCH_LOCATION_MEASUREMENTS.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext()
							.lookup("java:global/SCiP/SCiP-domain-ejb/FetchLocationMeasurementTimer!nl.enexis.scip.timer.TimerInterface")).reset();
				} else if (ActionEvent.DEVIATION_DAY_DETERMINATION.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext()
							.lookup("java:global/SCiP/SCiP-domain-ejb/DeviationDayDeterminationTimer!nl.enexis.scip.timer.TimerInterface")).reset();
				} else if (ActionEvent.PREPARE_FORECAST_INPUT.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext()
							.lookup("java:global/SCiP/SCiP-domain-ejb/PrepareForecastInputTimer!nl.enexis.scip.timer.TimerInterface")).reset();
				} else if (ActionEvent.SEND_LOCATION_MEASUREMENTS.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext()
							.lookup("java:global/SCiP/SCiP-domain-ejb/SendLocationMeasurementTimer!nl.enexis.scip.timer.TimerInterface")).reset();
				} else if (ActionEvent.CALCULATE_FORECAST.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext().lookup("java:global/SCiP/SCiP-domain-ejb/ForecastTimer!nl.enexis.scip.timer.TimerInterface"))
							.reset();
				} else if (ActionEvent.FETCH_MEASUREMENTS.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext()
							.lookup("java:global/SCiP/SCiP-domain-ejb/FetchMeasurementsTimer!nl.enexis.scip.timer.TimerInterface")).reset();
				} else if (ActionEvent.HEARTBEAT.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext().lookup("java:global/SCiP/SCiP-domain-ejb/HeartbeatTimer!nl.enexis.scip.timer.TimerInterface"))
							.reset();
				} else if (ActionEvent.NOTIFICATION.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext().lookup("java:global/SCiP/SCiP-domain-ejb/NotificationsTimer!nl.enexis.scip.timer.TimerInterface"))
							.reset();
				} else if (ActionEvent.DASHBOARD.toString().equals(timer.getInfo())) {
					((TimerInterface) new InitialContext().lookup("java:global/SCiP/SCiP-domain-ejb/DashboardScheduler!nl.enexis.scip.timer.TimerInterface"))
							.reset();
				}
			} catch (NamingException e) {
				success = false;
				ActionLogger.error("Exception reset of timer {0}!", new Object[] { timer.getInfo() }, e, true);
			}
		}

		if (!success) {
			throw new ActionException(ActionExceptionType.TIMER, "Exception(s) occured during reset of timer(s)!");
		}
	}
}