package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.dao.CspAdjustmentDao;
import nl.enexis.scip.dao.CspAdjustmentMessageDao;
import nl.enexis.scip.dao.CspAdjustmentValueDao;
import nl.enexis.scip.dao.CspUsageDao;
import nl.enexis.scip.dao.CspUsageMessageDao;
import nl.enexis.scip.dao.CspUsageValueDao;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustment;
import nl.enexis.scip.model.CspAdjustmentMessage;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspUsage;
import nl.enexis.scip.model.CspUsageMessage;
import nl.enexis.scip.model.CspUsageValue;

@Named("cspStorageService")
@Stateless
public class CspStorageServiceImpl implements CspStorageService {

	@Inject
	CspUsageDao cspUsageDao;

	@Inject
	CspUsageValueDao cspUsageValueDao;

	@Inject
	CspUsageMessageDao cspUsageMessageDao;

	@Inject
	CspAdjustmentMessageDao cspAdjustmentMessageDao;

	@Inject
	CspAdjustmentDao cspAdjustmentDao;

	@Inject
	CspAdjustmentValueDao cspAdjustmentValueDao;

	@Inject
	@Named("topologyStorageService")
	TopologyStorageService topologyStorageService;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeCspUsages(CspUsageMessage cspUsageMessage) throws ActionException {
		try {
			CspUsageMessage cspUsageMessageEntity = cspUsageMessageDao.find(cspUsageMessage.getId());

			List<CspUsage> cspUsages = cspUsageMessage.getCspUsages();

			for (CspUsage cspUsage : cspUsages) {
				CspUsage cspUsageEntity = new CspUsage();

				CableCsp cableCspEntity = topologyStorageService.findCableCspByCableAndCsp(cspUsage.getCableCsp()
						.getCsp(), cspUsage.getCableCsp().getCable());
				cspUsageEntity.setCableCsp(cableCspEntity);
				cspUsageEntity.setCspUsageMessage(cspUsageMessageEntity);

				cspUsageEntity = cspUsageDao.create(cspUsageEntity);

				List<CspUsageValue> cspUsageValues = cspUsage.getCspUsageValues();
				for (CspUsageValue cspUsageValue : cspUsageValues) {
					CspUsageValue cspUsageValueEntity = new CspUsageValue();

					cspUsageValueEntity.setCspUsage(cspUsageEntity);
					cspUsageValueEntity.setCableCsp(cableCspEntity);

					cspUsageValueEntity.setStartedAt(cspUsageValue.getStartedAt());
					cspUsageValueEntity.setEndedAt(cspUsageValue.getEndedAt());
					cspUsageValueEntity.setDay(cspUsageValue.getDay());
					cspUsageValueEntity.setDayOfWeek(cspUsageValue.getDayOfWeek());
					cspUsageValueEntity.setHour(cspUsageValue.getHour());
					cspUsageValueEntity.setBlock(cspUsageValue.getBlock());
					cspUsageValueEntity.setEnergyConsumption(cspUsageValue.getEnergyConsumption());
					cspUsageValueEntity.setAverageCurrent(cspUsageValue.getAverageCurrent());

					cspUsageValueDao.setFormerCspUsageValuesAsNotLast(cspUsageValueEntity);

					cspUsageValueDao.create(cspUsageValueEntity);
				}

			}
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error storing new CspUsage(Value) entity!", e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeCspAdjustments(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException {
		try {
			// CspAdjustmentMessage cspAdjustmentMessageEntity =
			// cspAdjustmentMessageDao.find(cspAdjustmentMessage.getId());

			List<CspAdjustment> cspAdjustments = cspAdjustmentMessage.getCspAdjustments();

			for (CspAdjustment cspAdjustment : cspAdjustments) {
				CspAdjustment cspAdjustmentEntity = new CspAdjustment();

				CableCsp cableCspEntity = topologyStorageService.findCableCspByCableAndCsp(cspAdjustment.getCableCsp()
						.getCsp(), cspAdjustment.getCableCsp().getCable());
				cspAdjustmentEntity.setCableCsp(cableCspEntity);
				cspAdjustmentEntity.setCspAdjustmentMessage(cspAdjustmentMessage);

				cspAdjustmentEntity = cspAdjustmentDao.create(cspAdjustmentEntity);

				List<CspAdjustmentValue> cspAdjustmentValues = cspAdjustment.getCspAdjustmentValues();
				for (CspAdjustmentValue cspAdjustmentValue : cspAdjustmentValues) {
					CspAdjustmentValue cspAdjustmentValueEntity = new CspAdjustmentValue();

					cspAdjustmentValueEntity.setCspAdjustment(cspAdjustmentEntity);
					cspAdjustmentValueEntity.setCableCsp(cableCspEntity);
					cspAdjustmentValueEntity.setCspForecastValue(cspAdjustmentValue.getCspForecastValue());

					cspAdjustmentValueEntity.setStartedAt(cspAdjustmentValue.getStartedAt());
					cspAdjustmentValueEntity.setEndedAt(cspAdjustmentValue.getEndedAt());
					cspAdjustmentValueEntity.setDay(cspAdjustmentValue.getDay());
					cspAdjustmentValueEntity.setDayOfWeek(cspAdjustmentValue.getDayOfWeek());
					cspAdjustmentValueEntity.setHour(cspAdjustmentValue.getHour());
					cspAdjustmentValueEntity.setBlock(cspAdjustmentValue.getBlock());
					cspAdjustmentValueEntity.setRequested(cspAdjustmentValue.getRequested());
					cspAdjustmentValueEntity.setAssigned(cspAdjustmentValue.getAssigned());
					cspAdjustmentValueEntity.setExtraCapacity(cspAdjustmentValue.isExtraCapacity());

					cspAdjustmentValueDao.create(cspAdjustmentValueEntity);
				}

			}
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error storing new CspAdjustment(Value) entity!", e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void storeCspUsageMessage(CspUsageMessage cspUsageMessage) throws ActionException {
		try {
			cspUsageMessageDao.create(cspUsageMessage);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error storing new CspUsageMessage entity!", e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CspAdjustmentMessage storeCspAdjustmentMessage(CspAdjustmentMessage cspAdjustmentMessage)
			throws ActionException {
		try {
			cspAdjustmentMessage = cspAdjustmentMessageDao.create(cspAdjustmentMessage);
			cspAdjustmentMessageDao.flush();
			return cspAdjustmentMessage;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error storing new CspAdjustmentMessage entity!", e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void setCspUsageMessageSuccessful(Long id) throws ActionException {
		try {
			CspUsageMessage cspUsageMessage = cspUsageMessageDao.find(id);
			cspUsageMessage.setSuccessful(true);
			cspUsageMessageDao.update(cspUsageMessage);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error setting  new CspUsageMessage entity to successful! Id: " + id, e);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void setCspAdjustmentMessageSuccessful(CspAdjustmentMessage cspAdjustmentMessage) throws ActionException {
		try {
			cspAdjustmentMessage = cspAdjustmentMessageDao.find(cspAdjustmentMessage.getId());

			cspAdjustmentMessage.setSuccessful(true);

			cspAdjustmentMessageDao.update(cspAdjustmentMessage);
		} catch (Exception e) {
			throw new ActionException(
					ActionExceptionType.STORAGE,
					"Error setting  new CspAdjustmentMessage entity to successful! Id: " + cspAdjustmentMessage.getId(),
					e);
		}
	}

	@Override
	public CspUsageMessage findCspUsageMessageByEventId(String eventId) throws ActionException {
		try {
			return cspUsageMessageDao.findByEventId(eventId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CspUsageMessage entity by eventId! eventId: " + eventId, e);
		}
	}

	@Override
	public CspAdjustmentMessage findCspAdjustmentMessageByEventId(String eventId) throws ActionException {
		try {
			return cspAdjustmentMessageDao.findByEventId(eventId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CspAdjustmentMessage entity by eventId! eventId: " + eventId, e);
		}
	}

	@Override
	public List<CspAdjustmentMessage> getCspAdjustmentMessageByCableIdBetweenDates(String cableId, Date fromDate,
			Date toDate) throws ActionException {
		try {
			return cspAdjustmentMessageDao.getByCableIdBetweenDates(cableId, fromDate, toDate);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding CspAdjustmentMessages by cableId: "
					+ cableId, e);
		}
	}

	@Override
	public List<CspAdjustment> getCspAdjustmentByCspAdjustmentMessageId(long msgId) throws ActionException {
		try {
			return cspAdjustmentDao.getByCspAdjustmentMessageId(msgId);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CspAdjustment by CspAdjustmentMessage Id: " + msgId, e);
		}
	}

	@Override
	public List<CspUsageValue> getCspUsageValuesForCableForecastBlock(Cable cable, int dayOfWeek, int hour, int block,
			Date fromDate) throws ActionException {
		try {
			cable = topologyStorageService.findCableByCableId(cable.getCableId());

			return cspUsageValueDao.getCspUsageValuesForCableForecastBlock(cable, dayOfWeek, hour, block, fromDate);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding CspUsageValue entities for forecast block of cable! Cable: " + cable.getCableId()
							+ " - DayOfWeek: " + dayOfWeek + " - hour: " + hour + " - block: " + block
							+ " - fromDate: " + fromDate, e);
		}
	}

	@Override
	public List<CspUsageValue> getCspUsageValuesByDate(Date fromDate, Date toDate) throws ActionException {
		try {
			List<CspUsageValue> values = cspUsageValueDao.getCspUsagesByDate(fromDate, toDate);
			// needed because cablecsp is lazy
			for (CspUsageValue val : values) {
				val.getCableCsp().getCable().getBehaviour();
			}
			return values;
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error finding CspUsageValue entities "
					+ " between " + fromDate + " and " + toDate, e);
		}
	}

	@Override
	public CspAdjustmentValue getLastCspAdjustmentsValueForBlock(CableCsp cableCsp, Date day, int hour, int block)
			throws ActionException {
		try {
			Cable cable = topologyStorageService.findCableByCableId(cableCsp.getCable().getCableId());
			Csp csp = topologyStorageService.findCspByEan13(cableCsp.getCsp().getEan13());

			return cspAdjustmentValueDao.getLastCspAdjustmentsValueForBlock(cable, csp, day, hour, block);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE,
					"Error finding last CspAdjustmentsValue entity for forecast block of cable! Cable: "
							+ cableCsp.getCable().getCableId() + " - csp: " + cableCsp.getCsp().getEan13() + " - day: "
							+ day + " - hour: " + hour + " - block: " + block, e);
		}
	}

	@Override
	public List<CspAdjustmentValue> getCspAdjustmentValueByCspAdjustmentId(long adId) throws ActionException {
		return cspAdjustmentValueDao.getByCspAdjustmentId(adId);
	}

	@Override
	public void updateCspUsageValue(CspUsageValue cspUsageValue) throws ActionException {
		try {
			cspUsageValueDao.update(cspUsageValue);
		} catch (Exception e) {
			throw new ActionException(ActionExceptionType.STORAGE, "Error updating CspUsageValue entity: "
					+ cspUsageValue.getId(), e);
		}
	}

}
