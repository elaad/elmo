package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.persistence.NoResultException;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecastDeviationDay;
import nl.enexis.scip.model.enumeration.DeviationDayType;

@Named
public class CableForecastDeviationDayDao extends GenericDao<CableForecastDeviationDay> {

	/**
	 * Default constructor.
	 */
	public CableForecastDeviationDayDao() {
		this.type = CableForecastDeviationDay.class;
	}

	public CableForecastDeviationDay getLastForCable(Cable cable) {
		try {
			return (CableForecastDeviationDay) em.createNamedQuery("CableForecastDeviationDay.getLastForCable")
					.setParameter("cable", cable).setMaxResults(1).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public CableForecastDeviationDay getForCableAndDay(Cable cable, Date day) {
		try {
			return (CableForecastDeviationDay) em.createNamedQuery("CableForecastDeviationDay.getForCableAndDay")
					.setParameter("cable", cable).setParameter("day", day).setMaxResults(1).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CableForecastDeviationDay> getLastNotDeviatedDays(Cable cable, int maxResults) {
		return em.createNamedQuery("CableForecastDeviationDay.getLastNotDeviatedDays").setParameter("cable", cable)
				.setParameter("deviationType", DeviationDayType.NO_DEVIATION).setMaxResults(maxResults).getResultList();
	}

}
