package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import javax.inject.Named;

import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastOutput;

@Named
public class CableForecastOutputDao extends GenericDao<CableForecastOutput> {

	/**
	 * Default constructor.
	 */
	public CableForecastOutputDao() {
		this.type = CableForecastOutput.class;
	}

	public CableForecastOutput getLastCableForecastOutput(CableForecast cableForecast, Date day, int hour, int block) {
		@SuppressWarnings("rawtypes")
		List cfos = em.createNamedQuery("CableForecastOutput.getLastCableForecastOutput")
				.setParameter("cableForecast", cableForecast).setParameter("day", day).setParameter("hour", hour)
				.setParameter("block", block).getResultList();

		if (cfos.isEmpty()) {
			return null;
		} else {
			return (CableForecastOutput) cfos.get(0);
		}
	}

}
