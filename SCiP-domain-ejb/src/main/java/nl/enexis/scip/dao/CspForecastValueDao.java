package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;

import javax.inject.Named;

import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspUsageValue;

@Named
public class CspForecastValueDao extends GenericDao<CspForecastValue> {

	/**
	 * Default constructor.
	 */
	public CspForecastValueDao() {
		this.type = CspForecastValue.class;
	}

	public CspForecastValue getLastCspForecastValueForCableForecastOutput(CableForecastOutput cableForecastOutput,
			Csp csp) {
		@SuppressWarnings("rawtypes")
		List cfvs = em.createNamedQuery("CspForecastValue.getLastCspForecastValueForCableForecastOutput")
				.setParameter("cableForecastOutputId", cableForecastOutput.getId())
				.setParameter("cspEan", csp.getEan13()).getResultList();

		if (cfvs.isEmpty()) {
			return null;
		} else {
			return (CspForecastValue) cfvs.get(0);
		}
	}

	@SuppressWarnings("unchecked")
	public List<CspForecastValue> getByCspForecastId(Long cspForecastId) {

		return em.createNamedQuery("CspForecastValue.getByCspForecastId")
				.setParameter("fcastId", cspForecastId).getResultList();
	}

	public CspForecastValue getLastCspForecastValueForCspUsageValue(CspUsageValue cspUsageValue) {
		try {
			return em.createNamedQuery("CspForecastValue.getLastCspForecastValuesForCspUsageValue", type)
					.setParameter("day", cspUsageValue.getDay()).setParameter("hour", cspUsageValue.getHour())
					.setParameter("block", cspUsageValue.getBlock())
					.setParameter("cableCsp", cspUsageValue.getCableCsp()).setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
