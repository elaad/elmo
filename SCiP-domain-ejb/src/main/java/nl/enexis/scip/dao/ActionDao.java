package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.model.Action;
import nl.enexis.scip.action.model.ActionMessage.Level;
import nl.enexis.scip.action.model.ActionTarget;
import nl.enexis.scip.action.model.Action_;
import nl.enexis.scip.model.DsoCsp;

@Named
public class ActionDao extends GenericDao<Action> {

	public ActionDao() {
		this.type = Action.class;
	}

	public List<String> getLoggedActors() {
		return em.createNamedQuery("Action.getDistinctActors", String.class).getResultList();
	}

	public List<Action> getNotificationActions(ActionEvent event, Date fromDate, Date toDate, List<Level> levels) {
		return em.createNamedQuery("Action.getNotificationEvents", type).setParameter("event", event)
				.setParameter("from", fromDate).setParameter("to", toDate).setParameter("levels", levels)
				.getResultList();
	}

	public Map<ActionEvent, Long> getPeriodErrorCountsPerActionEvent(Date fromDate, Date toDate) {
		@SuppressWarnings("rawtypes")
		List result = em.createNamedQuery("Action.getPeriodErrorCountsPerEvent").setParameter("success", false)
				.setParameter("from", fromDate).setParameter("to", toDate).getResultList();

		Map<ActionEvent, Long> counts = new HashMap<ActionEvent, Long>();

		for (Object object : result) {
			if (object instanceof Object[]) {
				Object[] arr = (Object[]) object;
				counts.put((ActionEvent) arr[0], (Long) arr[1]);
			}
		}

		return counts;
	}

	public Action getLastHeartbeatAction(DsoCsp dsoCsp) {
		Action action;
		try {
			action = em.createNamedQuery("Action.getLastHeartbeatActionDsoCsp", type)
					.setParameter("dsoCspTargetId", dsoCsp.getActionTargetId()).setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			action = null;
		}

		return action;

	}

	public Map<String, Date> getLastSuccessSendLocationMeasurmentForCsps() {
		@SuppressWarnings("rawtypes")
		List queryResult = em.createNamedQuery("Action.getLastSuccessSendLocationMeasurmentForCsps").getResultList();

		Map<String, Date> results = new HashMap<String, Date>();

		for (Object object : queryResult) {
			if (object instanceof Object[]) {
				Object[] arr = (Object[]) object;
				results.put((String) arr[0], (Date) arr[1]);
			}
		}

		return results;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, Double> getActionEventTurnaroundTimes(Date fromDate, Date toDate) {
		CriteriaBuilder builder = em.getCriteriaBuilder();

		CriteriaQuery cq = builder.createQuery();
		Root<Action> root = cq.from(Action.class);

		javax.persistence.criteria.Expression<java.sql.Time> timeDiff = builder.function("TIMEDIFF",
				java.sql.Time.class, root.<Date> get("end"), root.<Date> get("start"));
		javax.persistence.criteria.Expression<Integer> timeToSec = builder.function("TIME_TO_SEC", Integer.class,
				timeDiff);
		javax.persistence.criteria.Expression<Double> avg = builder.function("AVG", Double.class, timeToSec);

		cq.multiselect(root.<ActionEvent> get("type"), avg)
				.where(builder.isNotNull(root.<Date> get("end")),
						builder.notEqual(root.<ActionEvent> get("type"), ActionEvent.TIMER),
						builder.between(root.<Date> get("start"), fromDate, toDate))
				.groupBy(root.<ActionEvent> get("type"));

		List results = em.createQuery(cq).getResultList();

		Map<String, Double> times = new HashMap<String, Double>();
		for (Object object : results) {
			Object[] result = (Object[]) object;
			times.put(((ActionEvent) result[0]).name(), (Double) result[1]);
		}

		return times;
		
//		SELECT EVENT, AVG(TIMESTAMPDIFF(SECOND,START,END)) AS TIME
//		FROM SC_ACTION 
//	    WHERE 
//	    END IS NOT NULL
//	    AND START BETWEEN '2015-04-13 00:00:00' AND '2015-04-20 00:00:00' 
//	    AND EVENT != 'TIMER'
//	    GROUP BY EVENT
//	    ORDER BY TIME DESC;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map<String, Double> getTimerEventTurnaroundTimes(Date fromDate, Date toDate) {

		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery cq = cb.createQuery();

		Root<Action> action = cq.from(Action.class);
		Join<Action, ActionTarget> target = action.join(Action_.targets);

		javax.persistence.criteria.Expression<java.sql.Time> timeDiff = cb.function("TIMEDIFF", java.sql.Time.class,
				action.<Date> get("end"), action.<Date> get("start"));
		javax.persistence.criteria.Expression<Integer> timeToSec = cb.function("TIME_TO_SEC", Integer.class, timeDiff);
		javax.persistence.criteria.Expression<Double> avg = cb.function("AVG", Double.class, timeToSec);

		cq.multiselect(target.<String> get("value"), avg)
				.where(cb.equal(action.<ActionEvent> get("type"), ActionEvent.TIMER),
						cb.isNotNull(action.<Date> get("end")),
						cb.between(action.<Date> get("start"), fromDate, toDate),
						cb.equal(target.<String> get("type"), "nl.enexis.scip.action.ActionEvent"))
				.groupBy(target.<String> get("value"));

		List results = em.createQuery(cq).getResultList();

		Map<String, Double> times = new HashMap<String, Double>();
		for (Object object : results) {
			Object[] result = (Object[]) object;
			times.put((String) result[0], (Double) result[1]);
		}

		return times;
		
//		SELECT b.VALUE, AVG(TIMESTAMPDIFF(SECOND,a.START,a.END)) AS TIME
//		FROM SC_ACTION a
//	    JOIN SC_ACTION_TARGET b ON a.ID = b.ACTION
//	    WHERE 
//	    a.EVENT = 'TIMER'
//		AND a.END IS NOT NULL
//	    AND a.START BETWEEN '2015-01-01 00:00:00' AND '2015-04-21 00:00:00' 
//	    AND b.TYPE = 'nl.enexis.scip.action.ActionEvent'
//	    GROUP BY b.VALUE
//	    ORDER BY TIME DESC;
	}
}
