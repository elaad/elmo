package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Named
public class NoneEntityDao {

	@PersistenceContext(name = "SCiP-jpa")
	protected EntityManager em;

	public NoneEntityDao() {
	}

	public Date getLastDbCleanupDate() {
		String queryString = "SELECT MAX(RUNDATE) FROM SC_CLEANUP_LOG";

		final Query query = this.em.createNativeQuery(queryString);

		Object result = query.getSingleResult();

		return (Date) result;
	}
}
