package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import javax.inject.Named;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustmentValue;

@Named
public class CspAdjustmentValueDao extends GenericDao<CspAdjustmentValue> {

	/**
	 * Default constructor.
	 */
	public CspAdjustmentValueDao() {
		this.type = CspAdjustmentValue.class;
	}

	public CspAdjustmentValue getLastCspAdjustmentsValueForBlock(Cable cable, Csp csp, Date day, int hour, int block) {
		@SuppressWarnings("rawtypes")
		List results = em.createNamedQuery("CspAdjustmentValue.getLastCspAdjustmentsValueForBlock")
				.setParameter("cable", cable).setParameter("csp", csp).setParameter("day", day).setParameter("hour", hour)
				.setParameter("block", block).setMaxResults(1).getResultList();

		if (results.isEmpty()) {
			return null;
		} else {
			return (CspAdjustmentValue) results.get(0);
		}
	}
	
	public List<CspAdjustmentValue> getByCspAdjustmentId(long adId) {
		List<CspAdjustmentValue> values = 
			em.createNamedQuery("CspAdjustmentValue.getByCspAdjustmentId").setParameter("adId", adId).getResultList();
		for (CspAdjustmentValue val : values) {
			val.getCspForecastValue().getCableForecastOutput().getCableForecast().getBlockMinutes();  
			// needed because CspForecastValue is LAZY
		}
		return values;
	}

}
