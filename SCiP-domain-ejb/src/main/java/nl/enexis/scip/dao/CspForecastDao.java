package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CspForecast;

@Named
public class CspForecastDao extends GenericDao<CspForecast> {

	/**
	 * Default constructor.
	 */
	public CspForecastDao() {
		this.type = CspForecast.class;
	}

	@SuppressWarnings("rawtypes")
	public CspForecast getLastCspForcastForCableCsp(CableCsp cableCsp) {
		List cspForecastEntities = em.createNamedQuery("CspForecast.getCspForcastForCableCsp")
				.setParameter("cableCsp", cableCsp).setFirstResult(0).setMaxResults(0).getResultList();

		if (cspForecastEntities.isEmpty()) {
			return null;
		} else {
			return (CspForecast) cspForecastEntities.get(0);
		}
	}

	@SuppressWarnings({ "unchecked" })
	public List<CspForecast> getCspForcastForCableCsp(CableCsp cableCsp) {

		return em.createNamedQuery("CspForecast.getCspForcastForCableCsp")
				.setParameter("cableCsp", cableCsp).setFirstResult(0).setMaxResults(0).getResultList();
	}

	@SuppressWarnings({ "unchecked" })
	public List<CspForecast> getByCspForecastMessageId(Long cspForecastMessageId) {

		return em.createNamedQuery("CspForecast.getByCspForecastMessageId")
				.setParameter("msgId", cspForecastMessageId).setFirstResult(0).setMaxResults(0).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CspForecast> getByDate(Date dateFrom, Date dateTo) {

		return em.createNamedQuery("CspForecast.getByDate").setParameter("dateFrom", dateFrom)
				.setParameter("dateTo", dateTo).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CspForecast> getLastCspForecastsForCable(Cable cable) {
		// Get last cable forecast id with csp forecasts
		String queryString = new String(
				"SELECT DISTINCT(a.CABLE_FORECAST) FROM SC_CSP_FORECAST a JOIN SC_CABLE_FORECAST b ON a.CABLE_FORECAST = b.ID WHERE b.cable = '"
						+ cable.getCableId() + "' ORDER BY a.CALCULATED_AT DESC LIMIT 0,1");
		Query query = this.em.createNativeQuery(queryString);

		String cableForecastId;

		try {
			cableForecastId = (String) query.getSingleResult();
		} catch (NoResultException e) {
			return new ArrayList<CspForecast>();
		}

		return em.createNamedQuery("CspForecast.getByCableForecast")
				.setParameter("cableForecastId", cableForecastId).getResultList();
	}
}
