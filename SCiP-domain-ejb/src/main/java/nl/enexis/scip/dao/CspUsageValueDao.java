package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.Query;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CspUsageValue;

@Named
public class CspUsageValueDao extends GenericDao<CspUsageValue> {

	/**
	 * Default constructor.
	 */
	public CspUsageValueDao() {
		this.type = CspUsageValue.class;
	}

	@SuppressWarnings("unchecked")
	public List<CspUsageValue> getCspUsageValuesForCableForecastBlock(Cable cable, int dayOfWeek, int hour, int block,
			Date fromDate) {
		return em.createNamedQuery("CspUsageValue.getCspUsagesForCableForecastBlock").setParameter("cable", cable)
				.setParameter("dayOfWeek", dayOfWeek).setParameter("hour", hour).setParameter("block", block)
				.setParameter("fromDate", fromDate).setParameter("last", true).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CspUsageValue> getUsages(CableCsp cableCsp, int numberOfInputs, Date startDateOfBlock) {
		return em.createNamedQuery("CspUsageValue.getCspUsagesForCableCspBeforeDate")
				.setParameter("cableCsp", cableCsp).setParameter("toDate", startDateOfBlock).setParameter("last", true)
				.setMaxResults(numberOfInputs).getResultList();
	}

	public void setFormerCspUsageValuesAsNotLast(CspUsageValue cuv) {
		Query query = em.createQuery("UPDATE CspUsageValue c SET c.last = :last "
				+ "WHERE c.cableCsp = :cableCsp AND c.day = :day " + "AND c.hour = :hour AND c.block = :block");
		query.setParameter("last", false).setParameter("cableCsp", cuv.getCableCsp()).setParameter("day", cuv.getDay())
				.setParameter("hour", cuv.getHour()).setParameter("block", cuv.getBlock()).executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<CspUsageValue> getCspUsagesByDate(Date fromDate, Date toDate) {
		return em.createNamedQuery("CspUsageValue.getCspUsagesByDate").setParameter("fromDate", fromDate)
				.setParameter("toDate", toDate).getResultList();
	}

	public Map<String, Date> getLastUsageValueDatesForSu() {
		@SuppressWarnings("rawtypes")
		List result = em.createNamedQuery("CspUsageValue.getLastUsageValuesForSu").getResultList();

		Map<String, Date> values = new HashMap<String, Date>();

		for (Object object : result) {
			if (object instanceof Object[]) {
				Object[] arr = (Object[]) object;
				values.put((String) arr[0], (Date) arr[1]);
			}
		}

		return values;
	}
}
