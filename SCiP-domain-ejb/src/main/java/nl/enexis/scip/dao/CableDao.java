package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;

import javax.inject.Named;

import nl.enexis.scip.model.Cable;

@Named
public class CableDao extends ActiveTopologyDao<Cable> {

	/**
	 * Default constructor.
	 */
	public CableDao() {
		this.type = Cable.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<Cable> getByCspEan13(String ean13) {
		return em.createNamedQuery("Cable.getByCspEan13").setParameter("ean13", ean13).getResultList();
	}

}
