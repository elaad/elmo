package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecastMessage;

@Named
public class CspForecastMessageDao extends GenericDao<CspForecastMessage> {

	/**
	 * Default constructor.
	 */
	public CspForecastMessageDao() {
		this.type = CspForecastMessage.class;
	}

	@SuppressWarnings("unchecked")
	public List<CspForecastMessage> getMessagesForCspForecast(CspForecast cspForecast) {
		return em.createNamedQuery("CspForecastMessage.findByCspForecast").setParameter("cspForecast", cspForecast)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<CspForecastMessage> getMessagesByDateCspAndCable(String csp, Date dateFrom, Date dateTo, Cable cable) {
		return em.createNamedQuery("CspForecastMessage.findByCspDateAndCable").setParameter("csp", csp)
				.setParameter("dateFrom", dateFrom).setParameter("dateTo", dateTo).setParameter("cable", cable)
				.getResultList();
	}

	public Map<CableCsp, Date> getLastSuccessForecastMessageForCableCsps() {
		@SuppressWarnings("rawtypes")
		List queryResult = em.createNamedQuery("CspForecastMessage.getLastSuccessForecastMessageForCableCsps")
				.getResultList();

		Map<CableCsp, Date> results = new HashMap<CableCsp, Date>();

		for (Object object : queryResult) {
			if (object instanceof Object[]) {
				Object[] arr = (Object[]) object;
				results.put((CableCsp) arr[0], (Date) arr[1]);
			}
		}

		return results;
	}

}
