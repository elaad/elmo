package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.NoResultException;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named
public class CableForecastInputDao extends GenericDao<CableForecastInput> {

	/**
	 * Default constructor.
	 */
	public CableForecastInputDao() {
		this.type = CableForecastInput.class;
	}

	public CableForecastInput findCableForecastInputForBlock(Cable cable, Date blockDay, Integer blockHour,
			Integer blockNumber) {
		try {
			return (CableForecastInput) em.createNamedQuery("CableForecastInput.findCableForecastInputForBlock")
					.setParameter("cable", cable).setParameter("day", blockDay).setParameter("hour", blockHour)
					.setParameter("block", blockNumber).getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<CableForecastInput> getForForecastInput(Cable cable, int totalNumber) {
		List<CableForecastInput> cfs = this.em.createNamedQuery(type.getSimpleName() + ".getForForecastInput", type)
				.setParameter("cable", cable).setMaxResults(totalNumber).getResultList();
		return cfs;
	}

	public CableForecastInput findLastCableForecastInputAfterDate(Cable cable, Date fromDate) {

		@SuppressWarnings("unchecked")
		List<CableForecastInput> results = em
				.createNamedQuery("CableForecastInput.findLastCableForecastInputAfterDate")
				.setParameter("cable", cable).setParameter("fromDate", fromDate).setMaxResults(1).getResultList();
		if (results.isEmpty()) {
			return null;
		} else {
			return results.get(0);
		}
	}

	@SuppressWarnings("unchecked")
	public List<CableForecastInput> getInputsForCableForecast(CableForecast cf) {
		Date fromDate = null;
		Date toDate = null;

		List<CableForecastOutput> cfos = cf.getCableForecastOutputs();
		for (CableForecastOutput cfo : cfos) {
			if (fromDate == null) {
				fromDate = ForecastBlockUtil.getStartDateOfBlock(cfo.getDay(), cfo.getHour(), cfo.getBlock(),
						cf.getBlockMinutes());
				toDate = new Date(fromDate.getTime());
			} else {
				Date blkStart = ForecastBlockUtil.getStartDateOfBlock(cfo.getDay(), cfo.getHour(), cfo.getBlock(),
						cf.getBlockMinutes());
				if (blkStart.before(fromDate)) {
					fromDate = blkStart;
				} else if (blkStart.after(toDate)) {
					toDate = blkStart;
				}
			}
		}

		List<CableForecastInput> results = em.createNamedQuery("CableForecastInput.findCableForecastInputBetweenDates")
				.setParameter("cable", cf.getCable()).setParameter("fromDate", fromDate).setParameter("toDate", toDate)
				.getResultList();
		return results;
	}
	
	public Map<Cable, Date> getLastInputForCables() {
		@SuppressWarnings("rawtypes")
		List result = em.createNamedQuery("CableForecastInput.getLastInputForCables").getResultList();

		Map<Cable, Date> values = new HashMap<Cable, Date>();

		for (Object object : result) {
			if (object instanceof Object[]) {
				Object[] arr = (Object[]) object;
				values.put((Cable) arr[0], (Date) arr[1]);
			}
		}

		return values;
	}

}
