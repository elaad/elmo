package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.NoResultException;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.Measurement;

@Named
public class MeasurementDao extends GenericDao<Measurement> {

	public MeasurementDao() {
		this.type = Measurement.class;
	}

	public Measurement getMeasurementForCableAndDate(Date date, Cable cable) {

		try {
			Measurement measurement = this.em
					.createNamedQuery(type.getSimpleName() + ".getMeasurementForCableAndDate", type)
					.setParameter("date", date).setParameter("cable", cable).getSingleResult();

			return measurement;
		} catch (NoResultException nre) {
			return null;
		}
	}

	public List<Measurement> getMeasurementForCableBetweenDates(Date startDate, Date endDate, Cable cable) {
		return this.em.createNamedQuery(type.getSimpleName() + ".getMeasurementForCableBetweenDates", type)
				.setParameter("startDate", startDate).setParameter("endDate", endDate).setParameter("cable", cable)
				.getResultList();
	}

	public List<Measurement> getMeasurementForProviderIdBetweenDates(Date startDate, Date endDate, String providerId) {
		return this.em.createNamedQuery(type.getSimpleName() + ".getMeasurementForProviderIdBetweenDates", type)
				.setParameter("startDate", startDate).setParameter("endDate", endDate).setParameter("mpid", providerId)
				.getResultList();
	}

	public List<Measurement> getMeasurementBetweenDates(Date startDate, Date endDate) {
		return this.em.createNamedQuery(type.getSimpleName() + ".getMeasurementBetweenDates", type)
				.setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();
	}

	public Measurement findClosestCompleteMeasurement(Measurement measurement, boolean doForward) {
		@SuppressWarnings("unchecked")
		List<Measurement> results = em
				.createNamedQuery(
						doForward ? "Measurement.getClosestNextCompleteMeasurement"
								: "Measurement.getClosestPrevCompleteMeasurement")
				.setParameter("cable", measurement.getCable()).setParameter("complete", true)
				.setParameter("date", measurement.getDate()).setMaxResults(1).getResultList();
		if (results.isEmpty()) {
			return null;
		} else {
			return results.get(0);
		}
	}

	public Map<Cable, Long> getPeriodIncompleteMeasurmentPerCable(Date fromDate, Date toDate) {
		@SuppressWarnings("rawtypes")
		List result = em.createNamedQuery("Measurement.getPeriodIncompleteMeasurmentPerCable")
				.setParameter("from", fromDate).setParameter("to", toDate).getResultList();

		Map<Cable, Long> counts = new HashMap<Cable, Long>();

		for (Object object : result) {
			if (object instanceof Object[]) {
				Object[] arr = (Object[]) object;
				counts.put((Cable) arr[0], (Long) arr[1]);
			}
		}

		return counts;
	}

}
