package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import nl.enexis.scip.model.LocationMeasurement;
import nl.enexis.scip.model.LocationMeasurementValue;

@Named
public class LocationMeasurementValueDao extends GenericDao<LocationMeasurementValue> {

	/**
	 * Default constructor.
	 */
	public LocationMeasurementValueDao() {
		this.type = LocationMeasurementValue.class;
	}

	@SuppressWarnings("unchecked")
	public List<LocationMeasurementValue> getIrradiations(LocationMeasurement locationMeasurement, int numberOfInputs,
			Date startDateOfBlock, boolean beforeDate) {
		if (beforeDate) {
			return em.createNamedQuery("LocationMeasurementValue.getLocationMeasurmentValuesBeforeDate")
					.setParameter("locationMeasurement", locationMeasurement).setParameter("toDate", startDateOfBlock)
					.setMaxResults(numberOfInputs).getResultList();
		} else {
			return em.createNamedQuery("LocationMeasurementValue.getLocationMeasurmentValuesFromDate")
					.setParameter("locationMeasurement", locationMeasurement)
					.setParameter("fromDate", startDateOfBlock).setMaxResults(numberOfInputs).getResultList();

		}
	}

	@SuppressWarnings("unchecked")
	public List<LocationMeasurementValue> getLocationMeasurementValuesAfterDate(
			LocationMeasurement locationMeasurement, Date startDateOfBlock) {
		return em.createNamedQuery("LocationMeasurementValue.getLocationMeasurmentValuesFromDate")
				.setParameter("locationMeasurement", locationMeasurement).setParameter("fromDate", startDateOfBlock)
				.getResultList();
	}

	public Map<String, HashMap<Boolean, Date>> getLastIrrValueDatesForSu() {
		@SuppressWarnings("rawtypes")
		List result = em.createNamedQuery("LocationMeasurementValue.getLastIrrValuesForSu").getResultList();

		Map<String, HashMap<Boolean, Date>> values = new HashMap<String, HashMap<Boolean, Date>>();

		for (Object object : result) {
			if (object instanceof Object[]) {
				Object[] arr = (Object[]) object;
				HashMap<Boolean, Date> dates;
				if (values.containsKey(arr[0])) {
					dates = values.get(arr[0]);
					if (dates == null) {
						dates = new HashMap<Boolean, Date>();
						values.put((String) arr[0], dates);
					}
				} else {
					dates = new HashMap<Boolean, Date>();
					values.put((String) arr[0], dates);
				}
				dates.put((Boolean) arr[1], (Date) arr[2]);
			}
		}

		return values;
	}
}
