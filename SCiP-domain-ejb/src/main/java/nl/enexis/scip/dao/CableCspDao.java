package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;

import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.enumeration.BehaviourType;

@Named
public class CableCspDao extends ActiveTopologyDao<CableCsp> {

	/**
	 * Default constructor.
	 */
	public CableCspDao() {
		this.type = CableCsp.class;
	}

	public CableCsp getCableCspByCableAndCsp(Cable cable, Csp csp) {
		try {
			return (CableCsp) em.createNamedQuery("CableCsp.getCableCspByCableAndCsp").setParameter("cable", cable)
					.setParameter("csp", csp).getSingleResult();
		} catch (NoResultException nre) {
			return null;
		} catch (NonUniqueResultException nure) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CableCsp> getAllCompleteActiveCableCsps(BehaviourType behaviour) {
		if (BehaviourType.SC.equals(behaviour)) {
			return em.createNamedQuery("CableCsp.getAllScCompleteActiveCableCsps").getResultList();
		} else {
			return em.createNamedQuery("CableCsp.getAllSuCompleteActiveCableCsps").getResultList();
		}
	}

}
