package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.model.AuditLogItem;
import nl.enexis.scip.model.AuditTrailEntity;
import nl.enexis.scip.model.enumeration.DatabaseAction;
import nl.enexis.scip.util.xml.JaxbUtil;

public abstract class GenericAuditDao<T extends AuditTrailEntity> extends GenericDao<T> {

    @Inject
    @Named("auditLogItemDao")
    private AuditLogItemDao auditLogItemDao;
    
    public GenericAuditDao() {
    }

    @Override
    public T create(final T t) {
        T newT = super.create(t);
        this.auditAction(t, DatabaseAction.INSERT);
   
        return newT;
    }

    @Override
    public T merge(final T t) {
        this.auditAction(t, DatabaseAction.UPDATE);
        
        return super.merge(t);
    }

    @Override
    public void delete(final T t) {
        this.auditAction(t, DatabaseAction.DELETE);
        super.delete(t);
    }

    @Override
    public T update(final T t) {
        this.auditAction(t, DatabaseAction.UPDATE);
        
        return super.update(t);
    }

    public T topologyDelete(final T t) {
        this.auditAction(t, DatabaseAction.DELETE);
        
        return super.update(t);
    }

    protected void auditAction(final T t, DatabaseAction action) {
        if (AuditTrailEntity.class.isAssignableFrom(this.type)) {
            AuditTrailEntity entity = t;
            if (entity.getUserId() != null && entity.getUserId().length() > 0) {
                String storedState = "";
                String newState = "";
                
                if (action != DatabaseAction.INSERT) {
                    storedState = entity.getStoredState(); 
                }
                if (action != DatabaseAction.DELETE) {
                    newState = JaxbUtil.toXmlString(entity); 
                }
                AuditLogItem auditLogItem = new AuditLogItem(t.getClass().getSimpleName(), 
                        entity.getRecordKey(),
                        action,
                        entity.getUserId(),
                        newState,
                        storedState,
                        entity.getComments());
                auditLogItemDao.create(auditLogItem);
            }
        }
    }
}

