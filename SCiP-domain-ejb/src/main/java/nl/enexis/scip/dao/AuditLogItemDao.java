package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import javax.inject.Named;
import javax.persistence.Query;

import nl.enexis.scip.model.AuditLogItem;

@Named
public class AuditLogItemDao extends GenericDao<AuditLogItem> {

	/**
	 * Default constructor.
	 */
	public AuditLogItemDao() {
		this.type = AuditLogItem.class;
	}
	
	
	public List<AuditLogItem> getBetweenDates(Date startDate, Date endDate) {
		return this.em.createNamedQuery(type.getSimpleName() + ".getBetweenDates", type)
			.setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String> getDistinctEntities() {
		final Query query = this.em.createQuery("SELECT distinct o.tableName FROM " 
				+ type.getSimpleName() + " o ORDER BY o.tableName ASC");
		return query.getResultList();
	}
}
