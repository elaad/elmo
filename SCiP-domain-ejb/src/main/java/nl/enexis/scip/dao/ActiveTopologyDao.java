package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;

import javax.persistence.Query;

import nl.enexis.scip.model.ActiveTopologyEntity;

public abstract class ActiveTopologyDao<T extends ActiveTopologyEntity> extends GenericAuditDao<T> {

    public ActiveTopologyDao() {
        
    }
    
    @SuppressWarnings("unchecked")
    public List<T> getAll(boolean showDeleted) {
        if (showDeleted) {
            return super.getAll();
        }
        final StringBuffer queryString = new StringBuffer("SELECT o from ");

        queryString.append(type.getSimpleName()).append(" o ");
        queryString.append(" where deleted=0");

        final Query query = this.em.createQuery(queryString.toString());

        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<T> getAllActive(boolean isActive) {
        final StringBuffer queryString = new StringBuffer("SELECT o from ");

        queryString.append(type.getSimpleName()).append(" o ");
        queryString.append(" where deleted=0 and active=" + (isActive ? "1" : "0"));

        final Query query = this.em.createQuery(queryString.toString());

        return query.getResultList();
    }

    @Override
    public void delete(final T t) {
        t.setDeleted(true);
        t.setActive(false);
        super.topologyDelete(t); // never really delete a topology entity.
    }
}