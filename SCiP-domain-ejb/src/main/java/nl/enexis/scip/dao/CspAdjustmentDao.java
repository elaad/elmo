package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.List;

import javax.inject.Named;

import nl.enexis.scip.model.CspAdjustment;

@Named
public class CspAdjustmentDao extends GenericDao<CspAdjustment> {

	/**
	 * Default constructor.
	 */
	public CspAdjustmentDao() {
		this.type = CspAdjustment.class;
	}
	
	public List<CspAdjustment> getByCspAdjustmentMessageId(long msgId) {
		List<CspAdjustment> adjustments = 
			em.createNamedQuery("CspAdjustment.getByCspAdjustmentMessageId").setParameter("msgId", msgId).getResultList();
		for (CspAdjustment adj : adjustments) {
			adj.getCableCsp().getCable().getCableId();  // needed because CableCsp is LAZY
		}
		return adjustments;
	}

}
