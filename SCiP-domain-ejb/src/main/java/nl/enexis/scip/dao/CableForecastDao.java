package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Date;
import java.util.List;

import javax.inject.Named;

import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.util.ForecastBlockUtil;

@Named
public class CableForecastDao extends GenericDao<CableForecast> {

	/**
	 * Default constructor.
	 */
	public CableForecastDao() {
		this.type = CableForecast.class;
	}

	public CableForecast getLastCableForecast(Cable cable) {
		return (CableForecast) em.createNamedQuery("CableForecast.getAllForCableOrderByDatetimeDesc")
				.setParameter("cable", cable).setMaxResults(1).getSingleResult();
	}

	public List<CableForecast> search(Cable cable, Date dateFrom, Date dateTo) {
		List<CableForecast> cfs = this.em.createNamedQuery(type.getSimpleName() + ".search", type)
				.setParameter("cable", cable).setParameter("dateFrom", dateFrom).setParameter("dateTo", dateTo)
				.getResultList();
		return cfs;
	}

	public CableForecast getFirstCableForecastOfDay(Cable cable, Date date) {
		Date todate = ForecastBlockUtil.addDaysToDate(date, 1);
		@SuppressWarnings("rawtypes")
		List results = em.createNamedQuery("CableForecast.getCableForecastsOfDay").setParameter("cable", cable)
				.setParameter("fromDate", date).setParameter("toDate", todate).setParameter("success", true)
				.setMaxResults(1).getResultList();

		if (results.isEmpty()) {
			return null;
		} else {
			return (CableForecast) results.get(0);
		}

	}

}
