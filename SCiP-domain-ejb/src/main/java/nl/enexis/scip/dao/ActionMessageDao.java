package nl.enexis.scip.dao;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.persistence.Query;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.model.ActionMessage;
import nl.enexis.scip.action.model.ActionMessage.Level;

@Named
public class ActionMessageDao extends GenericDao<ActionMessage> {

	public ActionMessageDao() {
		this.type = ActionMessage.class;
	}

	@SuppressWarnings("unchecked")
	public List<ActionMessage> searchActionMessages(String actionId, Date fromDate, Date toDate, String targetType,
			String targetId, List<ActionEvent> selectedEvents, List<Level> selectedLevels,
			List<ActionExceptionType> selectedExceptionTypes, String actor) {

		Query query;

		if (targetType != null && targetType.length() > 0 || targetId != null && targetId.length() > 0) {
			query = this.em
					.createNamedQuery("ActionMessage.searchMessagesWithTarget", type)
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("selectedLevels",
							selectedLevels.isEmpty() ? Arrays.asList(Level.values()) : selectedLevels)
					.setParameter("selectedEvents",
							selectedEvents.isEmpty() ? Arrays.asList(ActionEvent.values()) : selectedEvents)
					.setParameter(
							"selectedExceptionTypes",
							selectedExceptionTypes.isEmpty() ? Arrays.asList(ActionExceptionType.values())
									: selectedExceptionTypes)
					.setParameter("actionId", actionId == null || actionId.length() == 0 ? "%" : "%" + actionId + "%")
					.setParameter("actor", actor == null || actor.length() == 0 ? "%" : actor)
					.setParameter("targetType", targetType == null || targetType.length() == 0 ? "%" : targetType)
					.setParameter("targetId", targetId == null || targetId.length() == 0 ? "%" : "%" + targetId + "%");
		} else {
			query = this.em
					.createNamedQuery("ActionMessage.searchMessages", type)
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("selectedLevels",
							selectedLevels.isEmpty() ? Arrays.asList(Level.values()) : selectedLevels)
					.setParameter("selectedEvents",
							selectedEvents.isEmpty() ? Arrays.asList(ActionEvent.values()) : selectedEvents)
					.setParameter(
							"selectedExceptionTypes",
							selectedExceptionTypes.isEmpty() ? Arrays.asList(ActionExceptionType.values())
									: selectedExceptionTypes)
					.setParameter("actor", actor == null || actor.length() == 0 ? "%" : actor)
					.setParameter("actionId", actionId == null || actionId.length() == 0 ? "%" : "%" + actionId + "%");
		}

		List<ActionMessage> messages = query.getResultList();

		return messages;
	}

	public Map<String, Long> getConnectivityErrorCountsPerParty(Date fromDate, Date toDate) {
		@SuppressWarnings("rawtypes")
		List result = em.createNamedQuery("ActionMessage.getConnectivityErrorCountsPerParty")
				.setParameter("fromDate", fromDate).setParameter("toDate", toDate).getResultList();

		Map<String, Long> counts = new HashMap<String, Long>();

		for (Object object : result) {
			if (object instanceof Object[]) {
				Object[] arr = (Object[]) object;
				String key = (String) arr[1] + " ("
						+ ("nl.enexis.scip.model.MeasurementProvider".equalsIgnoreCase((String) arr[0]) ? "MP" : "CSP")
						+ ")";
				counts.put(key, (Long) arr[2]);
			}
		}

		return counts;
	}

}
