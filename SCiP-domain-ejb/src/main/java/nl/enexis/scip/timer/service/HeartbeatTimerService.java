package nl.enexis.scip.timer.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.util.concurrent.atomic.AtomicBoolean;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.timer.TimerInterface;

import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.service.Service;
import org.jboss.msc.service.ServiceName;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.jboss.msc.value.Value;

public class HeartbeatTimerService implements Service<Environment> {

	private final Value<ServerEnvironment> env;
	private final AtomicBoolean started = new AtomicBoolean(false);

	public static final ServiceName DEFAULT_SERVICE_NAME = ServiceName.JBOSS.append("scip", "singleton",
			"heartbeatTimer", "default");
	public static final ServiceName QUORUM_SERVICE_NAME = ServiceName.JBOSS.append("scip", "singleton",
			"heartbeatTimer", "quorum");

	public HeartbeatTimerService(Value<ServerEnvironment> env) {
		this.env = env;
	}

	@Override
	public Environment getValue() {
		if (!this.started.get()) {
			throw new IllegalStateException(ActionEvent.HEARTBEAT + ": The " + this.getClass().getName()
					+ " is not ready!");
		}
		return new Environment(this.env.getValue().getNodeName());
	}

	@Override
	public void start(StartContext context) throws StartException {
		if (!started.compareAndSet(false, true)) {
			throw new StartException(ActionEvent.HEARTBEAT + ": The " + this.getClass().getName()
					+ " is already started!");
		}

		ActionLogger.info(ActionEvent.HEARTBEAT + ": Starting " + this.getClass().getName() + "!", new Object[] {},
				null, false);

		try {
			InitialContext ic = new InitialContext();
			((TimerInterface) ic.lookup("java:global/SCiP/SCiP-domain-ejb/HeartbeatTimer!nl.enexis.scip.timer.TimerInterface"))
					.start();
		} catch (Exception e) {
			throw new StartException(ActionEvent.HEARTBEAT + ": Could not initialize HeartbeatTimer", e);
		}
	}

	@Override
	public void stop(StopContext context) {
		if (!started.compareAndSet(true, false)) {
			ActionLogger.warn(ActionEvent.HEARTBEAT + ": The " + this.getClass().getName() + " is not active!",
					new Object[] {}, null, false);
		} else {
			ActionLogger.info(ActionEvent.HEARTBEAT + ": Stopping " + this.getClass().getName() + "!", new Object[] {},
					null, false);

			try {
				InitialContext ic = new InitialContext();
				((TimerInterface) ic
						.lookup("java:global/SCiP/SCiP-domain-ejb/HeartbeatTimer!nl.enexis.scip.timer.TimerInterface"))
						.stop();
			} catch (NamingException e) {
				ActionLogger.error(ActionEvent.HEARTBEAT + ": Could not stop timer", new Object[] {}, e, false);
			}
		}
	}
}