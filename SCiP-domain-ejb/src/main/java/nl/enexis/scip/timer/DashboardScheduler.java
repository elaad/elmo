package nl.enexis.scip.timer;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.inject.Named;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.action.interceptor.ActionContext;
import nl.enexis.scip.service.GeneralService;

@Startup
@Singleton
public class DashboardScheduler implements TimerInterface {

	private boolean isInitialized = false;
	private ActionEvent FOR_EVENT = ActionEvent.DASHBOARD;

	@Resource
	TimerService timerService;

	@Inject
	@Named("generalService")
	GeneralService generalService;

	@Resource
	private BeanManager beanManager;

	private Timer timer = null;

	public DashboardScheduler() {
	}

	@Override
	@PostConstruct
	@ActionContext(event = ActionEvent.TIMER)
	public void start() throws ActionException {
		TimerConfig timerConfig = new TimerConfig(this.FOR_EVENT.toString(), false);
		timer = timerService.createSingleActionTimer(5000, timerConfig);
	}

	@Override
	public void reset() throws ActionException {
		try {
			boolean newTimer = false;

			String minutes = generalService.getConfigurationParameter("DashboardTimerMinutes").getValue();
			String hours = generalService.getConfigurationParameter("DashboardTimerHours").getValue();

			if (timer != null) {
				newTimer = !(timer.isCalendarTimer() && timer.getSchedule().getHour().equals(hours) && timer
						.getSchedule().getMinute().equals(minutes));
			} else {
				newTimer = true;
			}

			if (newTimer) {
				if (timer != null) {
					this.stop();
				}

				ScheduleExpression schedule = new ScheduleExpression();
				schedule.minute(minutes);
				schedule.hour(hours);

				TimerConfig timerConfig = new TimerConfig(this.FOR_EVENT.toString(), false);

				try {
					timer = timerService.createCalendarTimer(schedule, timerConfig);
				} catch (Exception e) {
					throw new ActionException(ActionExceptionType.TIMER, "Error setting timer", e);
				}

				ActionLogger.info("Timer {0} (re)set to schedule {1}",
						new Object[] { timer.getInfo(), schedule.toString() }, null, true);
			}

			this.isInitialized = true;
		} catch (Exception e) {
			if (e instanceof ActionException) {
				throw (ActionException) e;
			}

			throw new ActionException(ActionExceptionType.TIMER, "Error setting timer", e);
		}
	}

	@Timeout
	@AccessTimeout(value = 0, unit = TimeUnit.SECONDS)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@ActionContext(event = ActionEvent.TIMER, doRethrow = false)
	private void scheduledTimeout(Timer timer) throws Exception {
		if (!this.isInitialized) {
			ActionLogger.info("Initializing timer {0}", new Object[] { timer.getInfo() }, null, false);
			this.reset();
		} else {
			this.reset();

			this.beanManager.fireEvent(this.FOR_EVENT, new Annotation[] {});
		}
	}

	@Override
	public void stop() {
		ActionLogger.debug("Stopping timer " + timer.getInfo(), new Object[] {}, null, false);
		this.timer.cancel();
		this.timer = null;
	}

	@Override
	public ActionEvent getTimerEvent() {
		return this.FOR_EVENT;
	}
}