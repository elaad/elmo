package nl.enexis.scip.timer.service.activator;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import nl.enexis.scip.action.ActionEvent;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.timer.service.DeviationDayDeterminationTimerService;

import org.jboss.as.server.ServerEnvironment;
import org.jboss.as.server.ServerEnvironmentService;
import org.jboss.msc.service.ServiceActivator;
import org.jboss.msc.service.ServiceActivatorContext;
import org.jboss.msc.service.ServiceController;
import org.jboss.msc.service.ServiceName;
import org.jboss.msc.value.InjectedValue;
import org.wildfly.clustering.singleton.SingletonServiceBuilderFactory;
import org.wildfly.clustering.singleton.election.NamePreference;
import org.wildfly.clustering.singleton.election.PreferredSingletonElectionPolicy;
import org.wildfly.clustering.singleton.election.SimpleSingletonElectionPolicy;

public class DeviationDayDeterminationTimerServiceActivator implements ServiceActivator {

	@Override
	public void activate(ServiceActivatorContext context) {
		ActionLogger.info(ActionEvent.DEVIATION_DAY_DETERMINATION
				+ " DeviationDayDeterminationTimerService will be installed!", new Object[] {}, null, false);

		boolean isClustered = new Boolean(System.getProperty("scip.cluster.enabled", "false"));

		if (isClustered) {
			Map<String, String> preferences = new HashMap<String, String>();
			Properties props = new Properties();
			try {
				props.load(this.getClass().getClassLoader()
						.getResourceAsStream("/nl/enexis/scip/timer/node.properties"));
			} catch (IOException e) {
				throw new RuntimeException(e);
			} 
			preferences.put("preferedNode", props.getProperty("scip.timer.node.deviationDayDetermination"));
			preferences.put("container", props.getProperty("scip.timer.container"));
			preferences.put("cache", props.getProperty("scip.timer.cache"));

			install(DeviationDayDeterminationTimerService.DEFAULT_SERVICE_NAME, 1, preferences, context);
			// install(DeviationDayDeterminationTimerService.QUORUM_SERVICE_NAME,
			// 2,
			// preferences, context);
		} else {
			ActionLogger.info(ActionEvent.DEVIATION_DAY_DETERMINATION
					+ " DeviationDayDeterminationTimerService will NOT be installed! NO CLUSTER ENVIRONMENT!",
					new Object[] {}, null, false);
		}
	}

	private static void install(ServiceName name, int quorum, Map<String, String> preferences,
			ServiceActivatorContext context) {
		InjectedValue<ServerEnvironment> env = new InjectedValue<>();
		DeviationDayDeterminationTimerService service = new DeviationDayDeterminationTimerService(env);
		ServiceController<?> factoryService = context.getServiceRegistry().getRequiredService(
				SingletonServiceBuilderFactory.SERVICE_NAME.append(preferences.get("container"),
						preferences.get("cache")));
		SingletonServiceBuilderFactory factory = (SingletonServiceBuilderFactory) factoryService.getValue();
		factory.createSingletonServiceBuilder(name, service) 
				.electionPolicy(
						new PreferredSingletonElectionPolicy(new SimpleSingletonElectionPolicy(), new NamePreference(
								preferences.get("preferedNode")))).requireQuorum(quorum)
				.build(context.getServiceTarget())
				.addDependency(ServerEnvironmentService.SERVICE_NAME, ServerEnvironment.class, env)// .install();
				.setInitialMode(ServiceController.Mode.ACTIVE).install();
	}

}
