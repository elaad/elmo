package nl.enexis.scip.cluster.command;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */


import javax.naming.InitialContext;
import javax.naming.NamingException;

import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.service.TimersService;

import org.wildfly.clustering.dispatcher.Command;

public class ResetTimersCommand implements Command<Boolean, String> {

	private static final long serialVersionUID = -3405593925871250676L;

	@Override
	public Boolean execute(String context) {
		try {
			TimersService timerService;
			try {
				timerService = (TimersService) new InitialContext()
						.lookup("java:global/SCiP/SCiP-domain-ejb/TimersServiceImpl!nl.enexis.scip.service.TimersServiceLocal");
			} catch (NamingException e) {
				ActionLogger.error(e.getMessage(), new Object[] { }, e, true);
//				e.printStackTrace();
				return false;
			}
			timerService.resetLocalTimers();
			return true;
		} catch (ActionException e) {
			ActionLogger.error(e.getMessage(), new Object[] { }, e, true);
//			e.printStackTrace();
			return false;
		}
	}
}