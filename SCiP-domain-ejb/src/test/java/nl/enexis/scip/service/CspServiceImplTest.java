package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import nl.enexis.scip.UnitTestHelper;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.ConfigurationParameter;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspAdjustment;
import nl.enexis.scip.model.CspAdjustmentMessage;
import nl.enexis.scip.model.CspAdjustmentValue;
import nl.enexis.scip.model.CspConnection;
import nl.enexis.scip.model.CspForecast;
import nl.enexis.scip.model.CspForecast.AssignmentChange;
import nl.enexis.scip.model.CspForecastMessage;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspGetForecastMessage;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.Dso;
import nl.enexis.scip.util.ForecastBlockUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ActionLogger.class })
public class CspServiceImplTest {

	@InjectMocks
	private CspServiceImpl cspServiceImpl;

	@Mock
	private CspServiceLocal self;

	@Mock
	private ForecastStorageService forecastStorageService;

	@Mock
	private TopologyStorageService topologyStorageService;

	@Mock
	private TopologyService topologyService;

	@Mock
	private GeneralService generalService;

	@Mock
	private CspStorageService cspStorageService;

	@Mock
	private CspMessagingService cspMessagingService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ActionLogger.class);
	}

	@Test
	public void testDoCspForecastAsync() {
		try {
			doThrow(ActionException.class).when(self).doCspForecast(anyString());
			cspServiceImpl.doCspForecastAsync("CABLEFORCASTID");
		} catch (ActionException ae) {
			fail("Should not throw exception.");
		}
	}

	private CspUsageValue makeCspUsageValue(Cable cable, Date date) {
		CspUsageValue cspUsageValue = new CspUsageValue();
		CableCsp cableCsp = this.makeCableCsp(this.makeCsp("CSPEAN13-0"), cable);

		cspUsageValue.setCableCsp(cableCsp);
		cspUsageValue.setDayOfWeek(1);
		cspUsageValue.setHour(2);
		cspUsageValue.setBlock(3);
		cspUsageValue.setDay(date);

		return cspUsageValue;
	}

	private List<CspConnection> makeCspConnectionList(int size, int factor) {
		List<CspConnection> cspConnectionList = new ArrayList<CspConnection>();
		for (int i = 1; i - 1 < size; i++) {
			CspConnection cspConnection = new CspConnection();
			cspConnection.setActive(i % 2 == 0 ? false : true); // half are active
			cspConnection.setCapacity(new BigDecimal(i * factor));
			cspConnectionList.add(cspConnection);
		}
		return cspConnectionList;
	}

	private List<CspForecastValue> makeCspForecastValueList(int size) {
		List<CspForecastValue> cspForecastValueList = new ArrayList<CspForecastValue>();
		for (int i = 0; i < size; i++) {
			CspAdjustmentValue cspAdjustmentValue = new CspAdjustmentValue();
			cspAdjustmentValue.setAssigned(new BigDecimal(i * 17.6));
			CspForecastValue cspForecastValue = new CspForecastValue();
			// cspAdjustmentValue.setCspForecastValue(cspForecastValue);
			cspForecastValue.setCspAdjustmentValue(cspAdjustmentValue);
			cspForecastValue.setAssigned(new BigDecimal(i * 133.011));
			cspForecastValueList.add(cspForecastValue);
		}
		return cspForecastValueList;
	}

	private Csp makeCsp(String ean13) {
		Csp csp = new Csp();
		csp.setEan13(ean13);
		return csp;
	}

	private Dso makeDso(String ean13) {
		Dso dso = new Dso();
		dso.setEan13(ean13);
		return dso;
	}

	private CspForecast makeCspForecast(AssignmentChange assignmentChange, int cspNumber, long id) {
		CspForecast cspForecast = new CspForecast();
		cspForecast.setChange(assignmentChange);
		cspForecast.setChangeValue(new BigDecimal("111"));
		Cable cable = this.makeCable("CABLEID");
		cspForecast.setCableCsp(this.makeCableCsp(this.makeCsp("CSPEAN13-" + cspNumber), cable));
		cspForecast.setId(id);
		return cspForecast;
	}

	private CableCsp makeCableCsp(Csp csp, Cable cable) {
		CableCsp cableCsp = new CableCsp();
		cableCsp.setActive(true);
		cableCsp.setId(555L);
		cableCsp.setCable(cable);
		cableCsp.setCsp(csp);
		return cableCsp;
	}

	private Cable setCableCspList(Cable cable, int amountOfCableCsps) {
		List<CableCsp> cableCspList = new ArrayList<CableCsp>();
		for (long i = 0; i < amountOfCableCsps; i++) {
			Csp csp = this.makeCsp("CSPEAN13-" + i);
			CableCsp cableCsp = this.makeCableCsp(csp, cable);
			cableCspList.add(cableCsp);
			csp.setCableCsps(cableCspList);
		}
		cable.setCableCsps(cableCspList);
		return cable;
	}

	private CableForecast makeCableForecastWithExplicitOutput(int amountOfCableCsps, CableForecastOutput cableForecastOutput) {
		return this.makeCableForecastWithCableForecastOutput(amountOfCableCsps, cableForecastOutput);
	}

	private CableForecast makeCableForecast(int amountOfCableCsps) {
		return this.makeCableForecastWithCableForecastOutput(amountOfCableCsps, this.makeCableForecastOutput2015Dec31());
	}

	private CableForecast makeCableForecastWithCableForecastOutput(int amountOfCableCsps, CableForecastOutput cableForecastOutput) {
		CableForecast cableForecast = new CableForecast();

		Cable cable = this.makeCable("CABLEID");
		cable = this.setCableCspList(cable, amountOfCableCsps);

		cableForecast.setCable(cable);
		cableForecast.setBlockMinutes(15);
		cableForecast.setRemainingCapacityFactor(new BigDecimal("0.40"));

		cableForecastOutput.setCableForecast(cableForecast);

		List<CableForecastOutput> cableForecastOutputList = new ArrayList<CableForecastOutput>();
		cableForecastOutputList.add(cableForecastOutput);

		cableForecast.setCableForecastOutputs(cableForecastOutputList);

		return cableForecast;
	}

	private Cable makeCable(String cableId, BigDecimal maxCarCapacity) {
		Cable cable = new Cable();
		Dso dso = this.makeDso("DSOEAN13");
		cable.setDso(dso);
		cable.setCableId(cableId);
		cable.setActive(true);
		cable.setMaxCarCapacity(maxCarCapacity);
		return cable;
	}

	private Cable makeCable(String cableId) {
		return this.makeCable(cableId, new BigDecimal("25"));
	}

	private CableForecastOutput makeCableForecastOutputWithDate(Date date) {
		CableForecastOutput cableForecastOutput = new CableForecastOutput();
		cableForecastOutput.setDay(UnitTestHelper.getDayOfDateWithoutTime(date));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		cableForecastOutput.setDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK));
		cableForecastOutput.setHour(11);
		cableForecastOutput.setBlock(1);
		cableForecastOutput.setAvailableCapacity(new BigDecimal("200"));
		return cableForecastOutput;
	}

	private CableForecastOutput makeCableForecastOutput2015Dec31() {
		return this.makeCableForecastOutputWithDate(UnitTestHelper.getDate(2015, Calendar.DECEMBER, 31, 11, 00, 00));
	}

	private CableForecastOutput makeCableForecastOutput2016Jun11() {
		return this.makeCableForecastOutputWithDate(UnitTestHelper.getDate(2016, Calendar.JUNE, 11, 11, 00, 00));
	}

	private CableForecastOutput makeCableForecastOneYearAddedToNow() {
		CableForecastOutput cableForecastOutput = new CableForecastOutput();
		cableForecastOutput.setDay(UnitTestHelper.getCurrentDateIncreasedWithOneYear());
		cableForecastOutput.setHour(Calendar.getInstance().get(Calendar.HOUR));
		cableForecastOutput.setBlock(1);
		cableForecastOutput.setAvailableCapacity(new BigDecimal("700"));
		return cableForecastOutput;
	}

	// Verify that the available usage weeks appear
	@Test
	public void testAvailableUsageWeeks() throws Exception {
		CableForecast cableForecast = this.makeCableForecastWithExplicitOutput(1, this.makeCableForecastOutput2016Jun11());
		ConfigurationParameter configurationParameter = new ConfigurationParameter();
		configurationParameter.setValue("500");

		int result = cspServiceImpl.getAvailableUsageWeeks(cableForecast, UnitTestHelper.getDate23Dec2015at11h00m00s(), new Integer(
				configurationParameter.getValue()).intValue());
		assertEquals(24, result);

		cableForecast = this.makeCableForecastWithExplicitOutput(1, this.makeCableForecastOutputWithDate(UnitTestHelper.getCurrentDateMinusOneYear()));
		result = cspServiceImpl.getAvailableUsageWeeks(cableForecast, UnitTestHelper.getCurrentDateIncreasedWithOneYear(), new Integer(
				configurationParameter.getValue()).intValue());

		assertEquals(0, result);
	}

	// Verify that exception is thrown if there is no csp
	@Test
	public void testDoCspForecast_ExceptionNoCspForCable() throws Exception {
		CableForecast cableForecast = this.makeCableForecast(0);
		Cable cable = cableForecast.getCable();
		when(forecastStorageService.findCableForecastById(anyString())).thenReturn(cableForecast);
		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);
		try {
			cspServiceImpl.doCspForecast("CABLEFORCASTID");
			fail("Expected ActionException due to no Csps for Cable");
		} catch (ActionException ae) {
			assertEquals("No Csps for Cable! Cable: CABLEID", ae.getMessage());
			assertEquals(ActionExceptionType.DATA, ae.getType());
		}
	}

	// Verify that exception is thrown if there is no active csp
	@Test
	public void testDoCspForecast_ExceptionNoActiveCspForCable() throws Exception {
		CableForecast cableForecast = this.makeCableForecast(1);
		Cable cable = cableForecast.getCable();
		when(forecastStorageService.findCableForecastById(anyString())).thenReturn(cableForecast);
		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);
		when(topologyService.hasCableActiveCsp(anyString(), anyBoolean())).thenReturn(false);
		try {
			cspServiceImpl.doCspForecast("CABLEFORCASTID");
			fail("Expected ActionException due to no active Csps for Cable");
		} catch (ActionException ae) {
			assertEquals("No active Csps for Cable! Cable: CABLEID", ae.getMessage());
			assertEquals(ActionExceptionType.DATA, ae.getType());
		}
	}

	// Assure that the correct csp connection factors are returned and that their value is correctly calculated
	@Test
	public void getCspConnectionFactors() throws Exception {
		CableForecast cableForecast = this.makeCableForecast(2);
		Cable cable = cableForecast.getCable();
		List<CableCsp> cableCspList = cable.getCableCsps();
		cableCspList.get(0).getCsp();
		cableCspList.get(1).getCsp();

		List<CspConnection> cspConnectionList1 = this.makeCspConnectionList(4, 100);
		List<CspConnection> cspConnectionList2 = this.makeCspConnectionList(4, 3);

		cableCspList.get(0).setCspConnections(cspConnectionList1);
		cableCspList.get(1).setCspConnections(cspConnectionList2);

		when(topologyService.isCspActiveForCable(anyString(), anyString())).thenReturn(true);
		when(topologyService.isCspActiveForCable(anyString(), anyString())).thenReturn(true);

		Map<CableCsp, BigDecimal> result = cspServiceImpl.getCspConnectionFactors(cable);

		assertEquals(new BigDecimal(0.971).doubleValue(), result.get(cable.getCableCsps().get(0)).doubleValue(), 0.0001);
		assertEquals(new BigDecimal(0.029).doubleValue(), result.get(cable.getCableCsps().get(1)).doubleValue(), 0.0001);
	}

	// Setup csp forcast list if contained in the cspConnectionFactors map. Then verify if they are correctly
	// transformed
	@Test
	public void setupCspForecastBasedOnCspConnectionFactors() throws Exception {
		CableForecast cableForecast = this.makeCableForecast(2);
		List<CableCsp> cableCspList = cableForecast.getCable().getCableCsps();
		Map<CableCsp, BigDecimal> cspConnectionFactors = new HashMap<CableCsp, BigDecimal>();
		cspConnectionFactors.put(cableCspList.get(0), new BigDecimal(0.32));
		cspConnectionFactors.put(cableCspList.get(1), new BigDecimal(0.111111));

		List<CspForecast> cspForecastList = cspServiceImpl.setupCspForecastBasedOnCspConnectionFactors(cableForecast, cableCspList,
				cspConnectionFactors);

		assertEquals(cableForecast, cspForecastList.get(0).getCableForecast());
		assertEquals(cableForecast, cspForecastList.get(1).getCableForecast());

		assertEquals(cableCspList.get(0), cspForecastList.get(0).getCableCsp());
		assertEquals(cableCspList.get(1), cspForecastList.get(1).getCableCsp());

		assertEquals(cableCspList.get(0).getNumberOfBlocks(), cspForecastList.get(0).getCableCsp().getNumberOfBlocks());
		assertEquals(cableCspList.get(1).getNumberOfBlocks(), cspForecastList.get(1).getCableCsp().getNumberOfBlocks());
	}

	// Calculate extra usage blocks for the cspUsageValue's. Verify if they are correctly calculated
	@Test
	public void getCspUsageValues() throws Exception {
		Cable cable = this.makeCable("CABLEID");
		// time should not matter, but should be consistent with the results in the end
		Date date = UnitTestHelper.getDate23Dec2015at11h00m00s();
		ConfigurationParameter configurationParameter1 = new ConfigurationParameter();
		configurationParameter1.setValue("4");
		when(generalService.getConfigurationParameter(eq("CspForecastDivisionExtraUsageBlocks"))).thenReturn(configurationParameter1);
		ConfigurationParameter configurationParameter2 = new ConfigurationParameter();
		configurationParameter2.setValue("15");
		when(generalService.getConfigurationParameter(eq("ForecastBlockMinutes"))).thenReturn(configurationParameter2);

		int dayOfTheWeek = Calendar.MONDAY;
		int hour = 23;
		int block = 2;
		cspServiceImpl.getCspUsageValues(cable, dayOfTheWeek, hour, block, date);

		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.MONDAY), eq(23), eq(2),
				eq(UnitTestHelper.getDate23Dec2015at11h00m00s())); // initialization

		// first loop, future
		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.MONDAY), eq(23), eq(3),
				eq(UnitTestHelper.getDate23Dec2015at11h00m00s()));
		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.MONDAY), eq(23), eq(4),
				eq(UnitTestHelper.getDate23Dec2015at11h00m00s()));
		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.TUESDAY), eq(0), eq(1),
				eq(UnitTestHelper.getDate(2015, Calendar.DECEMBER, 24, 11, 0, 0)));
		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.TUESDAY), eq(0), eq(2),
				eq(UnitTestHelper.getDate(2015, Calendar.DECEMBER, 24, 11, 0, 0)));

		// second loop, past
		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.MONDAY), eq(23), eq(2),
				eq(UnitTestHelper.getDate23Dec2015at11h00m00s()));
		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.MONDAY), eq(23), eq(1),
				eq(UnitTestHelper.getDate23Dec2015at11h00m00s()));
		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.MONDAY), eq(22), eq(4),
				eq(UnitTestHelper.getDate23Dec2015at11h00m00s()));
		verify(cspStorageService, times(1)).getCspUsageValuesForCableForecastBlock(eq(cable), eq(Calendar.MONDAY), eq(22), eq(3),
				eq(UnitTestHelper.getDate23Dec2015at11h00m00s()));
	}

	// Verify that the cspUsageFactors are correctly setup
	@Test
	public void getCspUsageFactors() throws Exception {
		CableForecast cableForecast = this.makeCableForecastWithCableForecastOutput(2, this.makeCableForecastOutput2016Jun11());
		Cable cable1 = cableForecast.getCable();
		Cable cable2 = this.makeCable("CABLEID2");
		cable1.getCableCsps().get(0).getCsp();
		CableForecastOutput cableForecastOutput = cableForecast.getCableForecastOutputs().get(0);
		int availableUsageWeeks = 5;

		// prepare getCspUsageValues
		ConfigurationParameter configurationParameter1 = new ConfigurationParameter();
		configurationParameter1.setValue("4");
		when(generalService.getConfigurationParameter(eq("CspForecastDivisionExtraUsageBlocks"))).thenReturn(configurationParameter1);
		ConfigurationParameter configurationParameter2 = new ConfigurationParameter();
		configurationParameter2.setValue("15");
		when(generalService.getConfigurationParameter(eq("ForecastBlockMinutes"))).thenReturn(configurationParameter2);
		when(topologyService.isCspActiveForCable(anyString(), anyString())).thenReturn(true);

		// prepare getCspUsageFactors
		List<CspUsageValue> cspUsageValueList = new ArrayList<CspUsageValue>();
		CspUsageValue cspUsageValue1 = this.makeCspUsageValue(cable1, cableForecastOutput.getDay());
		cspUsageValue1.setEnergyConsumption(new BigDecimal(14.75));
		cspUsageValueList.add(cspUsageValue1);

		cspUsageValue1 = this.makeCspUsageValue(cable1, cableForecastOutput.getDay());
		cspUsageValue1.setEnergyConsumption(new BigDecimal(111.11));
		cspUsageValueList.add(cspUsageValue1);

		CspUsageValue cspUsageValue2 = this.makeCspUsageValue(cable2, cableForecastOutput.getDay());
		cspUsageValue2.setEnergyConsumption(new BigDecimal(4.95));
		cspUsageValueList.add(cspUsageValue2);

		when(cspStorageService.getCspUsageValuesForCableForecastBlock(eq(cable1), eq(7), eq(11), eq(2), any(Date.class))).thenReturn(
				cspUsageValueList);

		Map<CableCsp, BigDecimal> cspUsageFactors = cspServiceImpl.getCspUsageFactors(cableForecastOutput, availableUsageWeeks);

		assertEquals(new BigDecimal("0.962").doubleValue(), cspUsageFactors.get(cspUsageValueList.get(0).getCableCsp()).doubleValue(),
				0.001);
		assertEquals(new BigDecimal("0.038").doubleValue(), cspUsageFactors.get(cspUsageValueList.get(2).getCableCsp()).doubleValue(),
				0.001);
	}

	// Check if the assigned value in the cspForecastValue is correctand that the cspForecastValue it is correctly
	// mapped in the cspForecastValuesMap
	@Test
	public void getCspForecastsAndSetAssignedForecastValue() throws Exception {
		CspForecast cspForecastDown = this.makeCspForecast(AssignmentChange.DOWN, 0, 1447L);
		CspForecast cspForecastUp = this.makeCspForecast(AssignmentChange.UP, 1, 1447L);
		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		cspForecastList.add(cspForecastDown);
		cspForecastList.add(cspForecastUp);
		CableForecast cableForecast = this.makeCableForecastWithCableForecastOutput(2, this.makeCableForecastOutput2015Dec31());
		Map<CableCsp, BigDecimal> cspConnectionFactors = new HashMap<CableCsp, BigDecimal>();
		cspConnectionFactors.put(cspForecastList.get(0).getCableCsp(), new BigDecimal(133.7));
		cspConnectionFactors.put(cspForecastList.get(1).getCableCsp(), new BigDecimal(0.666));
		CableForecastOutput cableForecastOutput = cableForecast.getCableForecastOutputs().get(0);
		SortedMap<Long, CspForecastValue> cspForecastValuesMap = new TreeMap<Long, CspForecastValue>();
		Map<CableCsp, BigDecimal> cspUsageFactorMap = new HashMap<CableCsp, BigDecimal>();
		cspUsageFactorMap.put(cspForecastList.get(0).getCableCsp(), new BigDecimal(14.47));
		cspUsageFactorMap.put(cspForecastList.get(1).getCableCsp(), new BigDecimal(41.41));

		CspServiceImpl cspServiceImplSpy = spy(cspServiceImpl);
		doReturn(cspUsageFactorMap).when(cspServiceImplSpy).getCspUsageFactors(any(CableForecastOutput.class), anyInt());
		CspAdjustmentValue cspAdjustmentValue = new CspAdjustmentValue();
		CspAdjustment cspAdjustment = new CspAdjustment();
		CspAdjustmentMessage cspAdjustmentMessage = new CspAdjustmentMessage();
		cspAdjustmentMessage.setDateTime(UnitTestHelper.getDate23Dec2015at11h00m27s());
		cspAdjustment.setCspAdjustmentMessage(cspAdjustmentMessage);
		cspAdjustmentValue.setCspAdjustment(cspAdjustment);
		when(cspStorageService.getLastCspAdjustmentsValueForBlock(any(CableCsp.class), any(Date.class), anyInt(), anyInt())).thenReturn(
				null).thenReturn(cspAdjustmentValue);

		List<CspForecastValue> result = cspServiceImplSpy.getCspForecastsAndSetAssignedForecastValue(3, new BigDecimal(0.55),
				cspConnectionFactors, cspForecastList, cableForecastOutput, cspForecastValuesMap);

		assertEquals(new BigDecimal(8174.820).doubleValue(), result.get(0).getAssigned().doubleValue(), 0.001);
		assertEquals(new BigDecimal(2769.024).doubleValue(), result.get(1).getAssigned().doubleValue(), 0.001);

		assertEquals(cspForecastValuesMap.get(1L), result.get(0));
		assertEquals(cspForecastValuesMap.get(new Long(UnitTestHelper.getDate23Dec2015at11h00m27s().getTime())), result.get(1));
	}

	// Compute and validate netto remaining value
	@Test
	public void processNettoReturns() {
		SortedMap<Long, CspForecastValue> cspForecastValuesMap = new TreeMap<Long, CspForecastValue>();
		CspForecastValue customCspForecastValue1 = new CspForecastValue();
		customCspForecastValue1.setAssigned(new BigDecimal(1.337));
		CspForecastValue customCspForecastValue2 = new CspForecastValue();
		customCspForecastValue2.setAssigned(new BigDecimal(66.006));
		List<CspForecastValue> cspForecastValueList = this.makeCspForecastValueList(3);
		cspForecastValueList.get(0).getCspAdjustmentValue().setCspForecastValue(customCspForecastValue1);
		cspForecastValueList.get(1).getCspAdjustmentValue().setCspForecastValue(customCspForecastValue2);
		cspForecastValueList.get(2).setCspAdjustmentValue(null); // test null
		cspForecastValuesMap.put(2L, cspForecastValueList.get(0));
		cspForecastValuesMap.put(1L, cspForecastValueList.get(1));
		cspForecastValuesMap.put(1337L, cspForecastValueList.get(2));
		BigDecimal actualRemaining = new BigDecimal(30);

		BigDecimal result = cspServiceImpl.processNettoReturns(cspForecastValuesMap, actualRemaining);

		assertEquals(new BigDecimal("79.405").doubleValue(), result.doubleValue(), 0.001);
	}

	// Compute and validate netto extra value
	@Test
	public void processNettoExtra() {
		SortedMap<Long, CspForecastValue> cspForecastValuesMap = new TreeMap<Long, CspForecastValue>();
		CspForecastValue customCspForecastValue1 = new CspForecastValue();
		customCspForecastValue1.setAssigned(new BigDecimal(1.337));
		CspForecastValue customCspForecastValue2 = new CspForecastValue();
		customCspForecastValue2.setAssigned(new BigDecimal(66.006));
		CspForecastValue customCspForecastValue3 = new CspForecastValue();
		customCspForecastValue3.setAssigned(new BigDecimal(412.11));
		List<CspForecastValue> cspForecastValueList = this.makeCspForecastValueList(4);
		cspForecastValueList.get(0).getCspAdjustmentValue().setCspForecastValue(customCspForecastValue1);
		cspForecastValueList.get(1).getCspAdjustmentValue().setCspForecastValue(customCspForecastValue2);
		cspForecastValueList.get(2).setCspAdjustmentValue(null); // test null
		cspForecastValuesMap.put(2L, cspForecastValueList.get(0));
		cspForecastValuesMap.put(1L, cspForecastValueList.get(1));
		cspForecastValuesMap.put(1337L, cspForecastValueList.get(2));
		BigDecimal actualRemaining = new BigDecimal(30);

		BigDecimal result = cspServiceImpl.processNettoExtra(cspForecastValuesMap, actualRemaining);

		assertEquals(new BigDecimal("28.663").doubleValue(), result.doubleValue(), 0.001);

		cspForecastValueList.get(3).getCspAdjustmentValue().setCspForecastValue(customCspForecastValue3);
		cspForecastValuesMap.put(UnitTestHelper.getDate23Dec2015at11h33m27s().getTime(), cspForecastValueList.get(3));

		result = cspServiceImpl.processNettoExtra(cspForecastValuesMap, actualRemaining);

		assertEquals(new BigDecimal("0").doubleValue(), result.doubleValue(), 0.001);
	}

	// Verify that not equal cspForecastBlocks return null
	@Test
	public void getCspForecastBlock_NotEqualDate() throws ActionException {
		CspForecast cspForecast = this.makeCspForecast(AssignmentChange.DOWN, 0, 1447L);
		Date date = UnitTestHelper.getDate(2015, Calendar.APRIL, 11, 5, 1, 1);
		List<CspForecastValue> cspForecastValueList = this.makeCspForecastValueList(1);
		cspForecastValueList.get(0).setCableForecastOutput(this.makeCableForecastOutputWithDate(date));
		cspForecast.setCspForecastValues(cspForecastValueList);
		int blockMinutes = 15;
		Map<String, Object> blockValues = ForecastBlockUtil.getBlockValues(UnitTestHelper.getDate23Dec2015at11h00m27s(), blockMinutes);
		CspForecastValue result = cspServiceImpl.getCspForecastBlock(cspForecast, blockValues, blockMinutes);

		assertEquals(null, result);
	}

	// Verify that equal cspForecastBlocks return the forecast
	@Test
	public void getCspForecastBlock_EqualDate() throws ActionException {
		CspForecast cspForecast = this.makeCspForecast(AssignmentChange.UP, 0, 1447L);
		Date date = UnitTestHelper.getDate23Dec2015at11h00m27s();
		List<CspForecastValue> cspForecastValueList = this.makeCspForecastValueList(1);
		cspForecastValueList.get(0).setCableForecastOutput(this.makeCableForecastOutputWithDate(date));
		cspForecast.setCspForecastValues(cspForecastValueList);
		int blockMinutes = 15;
		Map<String, Object> blockValues = ForecastBlockUtil.getBlockValues(UnitTestHelper.getDate23Dec2015at11h00m27s(), blockMinutes);
		CspForecastValue result = cspServiceImpl.getCspForecastBlock(cspForecast, blockValues, blockMinutes);

		assertEquals(cspForecastValueList.get(0), result);
	}

	// Assert that, in case of no csp forecast, the forecast goes up and the correct value is computed
	@Test
	public void setCspForecastChange_CspForecastsIsNull() {
		CspForecast newCspForecast = this.makeCspForecast(null, 1, 1447L);
		newCspForecast.setCableForecast(this.makeCableForecast(0));
		List<CspForecast> lastCspForecastList = new ArrayList<CspForecast>();
		BigDecimal noCspChangeFactor = new BigDecimal("0.749");

		cspServiceImpl = new CspServiceImpl() {
			@Override
			protected CspForecastValue getCspForecastBlock(CspForecast cspForecast, Map<String, Object> blockValues, int blockMinutes) {
				CspForecastValue cspForecastValue = CspServiceImplTest.this.makeCspForecastValueList(1).get(0);
				cspForecastValue.setAssigned(new BigDecimal(504L));
				return cspForecastValue;
			}
		};

		cspServiceImpl.setCspForecastChange(newCspForecast, lastCspForecastList, noCspChangeFactor);

		assertEquals(new BigDecimal(504L), newCspForecast.getChangeValue());
		assertEquals(AssignmentChange.UP, newCspForecast.getChange());
	}

	// Test if the cspForecast is correctly adapted
	@Test
	public void setCspForecastChange_AssignmentChangeUpAndEqual() {
		CspForecast newCspForecast = this.makeCspForecast(null, 1, 1447L);
		newCspForecast.setCableForecast(this.makeCableForecast(0));
		List<CspForecast> lastCspForecastList = new ArrayList<CspForecast>();
		CspForecast cspForecast1 = this.makeCspForecast(null, 1, 827L);
		CspForecast cspForecast2 = this.makeCspForecast(null, 1, 37L);
		CspForecast cspForecast3 = this.makeCspForecast(null, 1, 2371L);
		lastCspForecastList.add(cspForecast1);
		lastCspForecastList.add(cspForecast2);
		lastCspForecastList.add(cspForecast3);
		BigDecimal noCspChangeFactor = new BigDecimal("0.749");

		cspServiceImpl = new CspServiceImpl() {
			@Override
			protected CspForecastValue getCspForecastBlock(CspForecast cspForecast, Map<String, Object> blockValues, int blockMinutes) {
				CspForecastValue cspForecastValue = CspServiceImplTest.this.makeCspForecastValueList(1).get(0);
				// manipulation to distinguish which Assigned value to return (only for test purposes)
				if (cspForecast.getId() == 1447L) {
					cspForecastValue.setAssigned(new BigDecimal(33.81));
				} else if (cspForecast.getId() == 827L) {
					cspForecastValue.setAssigned(new BigDecimal(6.801));
				} else if (cspForecast.getId() == 37L) {
					cspForecastValue.setAssigned(new BigDecimal(36.321));
				} else if (cspForecast.getId() == 2371L) {
					cspForecastValue.setAssigned(new BigDecimal(15));
				}
				return cspForecastValue;
			}
		};

		cspServiceImpl.setCspForecastChange(newCspForecast, lastCspForecastList, noCspChangeFactor);

		assertEquals(new BigDecimal(18.810).doubleValue(), newCspForecast.getChangeValue().doubleValue(), 0.001);
		assertEquals(AssignmentChange.UP, newCspForecast.getChange());
	}

	// Test if the cspForecast is correctly adapted
	@Test
	public void setCspForecastChange_AssignmentChangeDown() {
		CspForecast newCspForecast = this.makeCspForecast(null, 1, 1447L);
		CableForecast cableForecast = this.makeCableForecast(0);
		cableForecast.getCable().setMaxCarCapacity(new BigDecimal(-12));
		newCspForecast.setCableForecast(cableForecast);
		List<CspForecast> lastCspForecastList = new ArrayList<CspForecast>();
		CspForecast cspForecast1 = this.makeCspForecast(null, 1, 827L);
		lastCspForecastList.add(cspForecast1);
		BigDecimal noCspChangeFactor = new BigDecimal("0.701");

		cspServiceImpl = new CspServiceImpl() {
			@Override
			protected CspForecastValue getCspForecastBlock(CspForecast cspForecast, Map<String, Object> blockValues, int blockMinutes) {
				CspForecastValue cspForecastValue = CspServiceImplTest.this.makeCspForecastValueList(1).get(0);
				// manipulation to distinguish which Assigned value to return (only for test purposes)
				if (cspForecast.getId() == 1447L) {
					cspForecastValue.setAssigned(new BigDecimal(33.81));
				} else if (cspForecast.getId() == 827L) {
					cspForecastValue.setAssigned(new BigDecimal(36.801));
				}
				return cspForecastValue;
			}
		};

		cspServiceImpl.setCspForecastChange(newCspForecast, lastCspForecastList, noCspChangeFactor);
		assertEquals(new BigDecimal(-2.991).doubleValue(), newCspForecast.getChangeValue().doubleValue(), 0.001);
		assertEquals(AssignmentChange.DOWN, newCspForecast.getChange());
	}

	// Assure that new cspForecasts are stored
	@Test
	public void testDoCspForecast_StoreCspForecast() throws Exception {
		CspForecast cspForecast = this.makeCspForecast(AssignmentChange.DOWN, 1, 4839L);
		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		cspForecastList.add(cspForecast);
		CableForecast cableForecast = this.makeCableForecast(1);
		Cable cable = cableForecast.getCable();
		when(forecastStorageService.findCableForecastById(anyString())).thenReturn(cableForecast);
		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);
		when(topologyService.hasCableActiveCsp(anyString(), anyBoolean())).thenReturn(true);
		ConfigurationParameter configurationParameter = new ConfigurationParameter();
		configurationParameter.setValue("500");
		when(generalService.getConfigurationParameter(anyString())).thenReturn(configurationParameter);
		when(generalService.getConfigurationParameterAsString(anyString())).thenReturn("2");
		when(topologyService.lastCableCspTopologyChange(anyString())).thenReturn(UnitTestHelper.getDate23Dec2015at11h00m00s());

		CspServiceImpl cspServiceImplSpy = spy(cspServiceImpl);
		doReturn(cspForecastList).when(cspServiceImplSpy).setupCspForecastBasedOnCspConnectionFactors(any(CableForecast.class),
				any(List.class), any(Map.class));
		doReturn(new ArrayList<CspForecastValue>()).when(cspServiceImplSpy).getCspForecastsAndSetAssignedForecastValue(anyInt(),
				any(BigDecimal.class), any(Map.class), any(List.class), any(CableForecastOutput.class), any(SortedMap.class));
		doNothing().when(cspServiceImplSpy).setCspForecastChange(any(CspForecast.class), any(List.class), any(BigDecimal.class));

		cspServiceImplSpy.doCspForecast("CABLEFORCASTID");

		verify(forecastStorageService, times(1)).storeCspForecasts(eq(cspForecastList));
	}

	// Assert that warning is thrown due to no available cable which relates to a csp
	@Test
	public void testSendForecastRequestedByCsp_LogWarningNoCableCspRelation() throws Exception {
		CspGetForecastMessage cspGetForecastMessage = new CspGetForecastMessage();
		Set<String> forCables = new HashSet<String>();
		forCables.add("FORCABLE1_LogWarningNoCableCspRelation");
		forCables.add("FORCABLE2_LogWarningNoCableCspRelation");

		try {
			cspServiceImpl.sendForecastRequestedByCsp(cspGetForecastMessage, forCables);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}

		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(
				eq("Forecast requested for unknown cable csp relation; cable: FORCABLE1_LogWarningNoCableCspRelation; csp: null"),
				any(Object[].class), any(Throwable.class), eq(true));
		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(
				eq("Forecast requested for unknown cable csp relation; cable: FORCABLE2_LogWarningNoCableCspRelation; csp: null"),
				any(Object[].class), any(Throwable.class), eq(true));
		assertEquals(true, cspGetForecastMessage.isSuccessful());
	}

	// Assert that warning is thrown due to no available and active cable which relates to a csp
	@Test
	public void testSendForecastRequestedByCsp_LogWarningInactiveCableCspRelation() throws Exception {
		CspGetForecastMessage cspGetForecastMessage = new CspGetForecastMessage();
		Set<String> forCables = new HashSet<String>();
		forCables.add("FORCABLE1_LogWarningInactiveCableCspRelation");
		forCables.add("FORCABLE2_LogWarningInactiveCableCspRelation");
		CableForecast cableForecast = this.makeCableForecast(1);
		Cable cable = cableForecast.getCable();
		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);

		try {
			cspServiceImpl.sendForecastRequestedByCsp(cspGetForecastMessage, forCables);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}

		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(
				eq("Forecast requested for inactive cable csp relation; cable: FORCABLE1_LogWarningInactiveCableCspRelation; csp: null"),
				any(Object[].class), any(Throwable.class), eq(true));
		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(
				eq("Forecast requested for inactive cable csp relation; cable: FORCABLE2_LogWarningInactiveCableCspRelation; csp: null"),
				any(Object[].class), any(Throwable.class), eq(true));
		assertEquals(true, cspGetForecastMessage.isSuccessful());
	}

	// Assure exception is logged when no csp is present for the cable
	@Test
	public void testSendForecastRequestedByCsp_ExceptionNoCableCsp() throws Exception {
		CspGetForecastMessage cspGetForecastMessage = new CspGetForecastMessage();
		Set<String> forCables = new HashSet<String>();
		forCables.add("FORCABLE1_ExceptionNoCableCsp");
		forCables.add("FORCABLE2_ExceptionNoCableCsp");
		CableForecast cableForecast = this.makeCableForecast(1);
		Cable cable = cableForecast.getCable();

		CspForecast cspForecast = new CspForecast();
		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		cspForecastList.add(cspForecast);

		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);
		when(topologyService.isCspActiveForCable(anyString(), anyString())).thenReturn(true);
		when(forecastStorageService.getLastCspForecastsForCable(any(Cable.class))).thenReturn(cspForecastList);

		try {
			cspServiceImpl.sendForecastRequestedByCsp(cspGetForecastMessage, forCables);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}
		PowerMockito.verifyStatic(times(2));
		ActionLogger.warn(eq("Exception sending forecast for a cable"), any(Object[].class), any(Throwable.class), eq(true));
		assertEquals(true, cspGetForecastMessage.isSuccessful());
	}

	// Test uses new Date() within sendForecastRequestedByCsp verify that exception is loggend when enddate of
	// cspforecast is before current date
	@Test
	public void testSendForecastRequestedByCsp_NoForecastAvailable() throws Exception {
		CspGetForecastMessage cspGetForecastMessage = new CspGetForecastMessage();
		Set<String> forCables = new HashSet<String>();
		forCables.add("FORCABLE1_NoForecastAvailable");
		forCables.add("FORCABLE2_NoForecastAvailable");
		CableForecast cableForecast = this.makeCableForecast(1);
		Cable cable = cableForecast.getCable();

		CspForecast cspForecast = new CspForecast();
		cspForecast.setCableCsp(cable.getCableCsps().get(0));
		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		cspForecastList.add(cspForecast);

		List<CspForecastValue> cspForecastValueList = this.makeCspForecastValueList(1);
		cspForecastValueList.get(0).setCableForecastOutput(cableForecast.getCableForecastOutputs().get(0));
		cspForecast.setCspForecastValues(cspForecastValueList);

		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);
		when(topologyService.isCspActiveForCable(anyString(), anyString())).thenReturn(true);
		when(forecastStorageService.getLastCspForecastsForCable(any(Cable.class))).thenReturn(cspForecastList);
		when(topologyStorageService.findCspByEan13(anyString())).thenReturn(cable.getCableCsps().get(0).getCsp());

		try {
			cspServiceImpl.sendForecastRequestedByCsp(cspGetForecastMessage, forCables);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}

		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(eq("No forecast available for cable csp relation; cable: FORCABLE1_NoForecastAvailable; csp: null"),
				any(Object[].class), any(Throwable.class), eq(true));
		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(eq("No forecast available for cable csp relation; cable: FORCABLE2_NoForecastAvailable; csp: null"),
				any(Object[].class), any(Throwable.class), eq(true));

		assertEquals(true, cspGetForecastMessage.isSuccessful());
	}

	// Test uses new Date() within sendForecastRequestedByCsp to assure that sending the forecast is propagated to
	// sending
	// the forecast async
	@Test
	public void testSendForecastRequestedByCsp_SendForecastCables() throws Exception {
		CspGetForecastMessage cspGetForecastMessage = new CspGetForecastMessage();
		cspGetForecastMessage.setPriority(1337);
		cspGetForecastMessage.setId(666L);
		Set<String> forCables = new HashSet<String>();
		forCables.add("FORCABLE1_SendForecastCables");
		forCables.add("FORCABLE2_SendForecastCables");
		CableForecast cableForecast = this.makeCableForecastWithCableForecastOutput(1, this.makeCableForecastOneYearAddedToNow());
		Cable cable = cableForecast.getCable();

		CspForecast cspForecast = this.makeCspForecast(AssignmentChange.DOWN, 0, 1447L);
		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		cspForecastList.add(cspForecast);

		CspForecastValue cspForecastValue = new CspForecastValue();
		cspForecastValue.setCableForecastOutput(cableForecast.getCableForecastOutputs().get(0));
		List<CspForecastValue> cspForecastValueList = new ArrayList<CspForecastValue>();
		cspForecastValueList.add(cspForecastValue);
		cspForecast.setCspForecastValues(cspForecastValueList);

		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);
		when(topologyService.isCspActiveForCable(anyString(), anyString())).thenReturn(true);
		when(forecastStorageService.getLastCspForecastsForCable(any(Cable.class))).thenReturn(cspForecastList);
		when(topologyStorageService.findCspByEan13(anyString())).thenReturn(cable.getCableCsps().get(0).getCsp());

		try {
			cspServiceImpl.sendForecastRequestedByCsp(cspGetForecastMessage, forCables);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}

		verify(self, times(2)).sendForecastForCspAsync(eq(1447L), eq(1337));
		verify(forecastStorageService, times(1)).setCspGetForecastMessageSuccessful(eq(666L));
		assertEquals(true, cspGetForecastMessage.isSuccessful());
	}

	// When cable is not active, assure that forecast is not sent
	@Test
	public void testSendForecastRequestedByCsp_NoForecastCablesSent() throws Exception {
		CspGetForecastMessage cspGetForecastMessage = new CspGetForecastMessage();
		cspGetForecastMessage.setDso("DSOEAN13");
		cspGetForecastMessage.setId(12345L);
		Set<String> forCables = null;
		CableForecast cableForecast = this.makeCableForecast(1);
		when(topologyStorageService.findCspByEan13(anyString())).thenReturn(cableForecast.getCable().getCableCsps().get(0).getCsp());
		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cableForecast.getCable());

		try {
			cspServiceImpl.sendForecastRequestedByCsp(cspGetForecastMessage, forCables);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}
		verify(forecastStorageService, times(1)).getLastCspForecastsForCable(eq(cableForecast.getCable()));
	}

	// Assure error is logged due to no available forecast
	@Test
	public void testSendForecastForCable_LogActionExceptionNoForecast() throws ActionException {
		Cable cable = this.makeCable("CABLEID");
		int priority = 1;
		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		cspForecastList.add(this.makeCspForecast(AssignmentChange.DOWN, 0, 1447L));
		when(forecastStorageService.getLastCspForecastsForCable(any(Cable.class))).thenReturn(cspForecastList);
		when(forecastStorageService.getCspForcast(anyLong())).thenReturn(null);
		try {
			cspServiceImpl.sendForecastForCable(cable.getCableId(), priority);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}

		PowerMockito.verifyStatic(times(1));
		ActionLogger.error(eq("Error sending forecast to csp! Due to: No forecast available for Csp!"), any(Object[].class),
				any(Throwable.class), eq(true));
	}

	// The end date of the forecast lies in the past, so assure this logs an error
	@Test
	public void testSendForecastForCable_CspForecastEndDateAfterCurrentException() throws Exception {
		Cable cable = this.makeCable("CABLEID");
		int priority = 1;
		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		CspForecast cspForecast = this.makeCspForecast(AssignmentChange.DOWN, 0, 1447L);
		cspForecastList.add(cspForecast);

		CableForecast cableForecast = this.makeCableForecastWithCableForecastOutput(1, this.makeCableForecastOutput2015Dec31());
		List<CspForecastValue> cspForecastValueList = this.makeCspForecastValueList(1);
		cspForecastValueList.get(0).setCableForecastOutput(cableForecast.getCableForecastOutputs().get(0));
		cspForecast.setCspForecastValues(cspForecastValueList);

		when(forecastStorageService.getLastCspForecastsForCable(any(Cable.class))).thenReturn(cspForecastList);
		when(forecastStorageService.getCspForcast(anyLong())).thenReturn(cspForecastList.get(0));

		try {
			cspServiceImpl.sendForecastForCable(cable.getCableId(), priority);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}

		PowerMockito.verifyStatic(times(1));
		ActionLogger.error(eq("Error sending forecast to csp! Due to: No forecast available for Csp! Csp: CSPEAN13-0"),
				any(Object[].class), any(Throwable.class), eq(true));
	}

	// Assert that this forecast is sent and verify its parameters
	@Test
	public void testSendForecastForCable_UpdateAndPushCspForecast() throws Exception {
		Cable cable = this.makeCable("CABLEID");
		int priority = 6;
		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		CspForecast cspForecast = this.makeCspForecast(AssignmentChange.UP, 0, 1447L);
		cspForecastList.add(cspForecast);

		CableForecast cableForecast = this.makeCableForecastWithCableForecastOutput(1, this.makeCableForecastOneYearAddedToNow());
		List<CspForecastValue> cspForecastValueList = this.makeCspForecastValueList(1);
		cspForecastValueList.get(0).setCableForecastOutput(cableForecast.getCableForecastOutputs().get(0));
		cspForecast.setCspForecastValues(cspForecastValueList);

		when(forecastStorageService.getLastCspForecastsForCable(any(Cable.class))).thenReturn(cspForecastList);
		when(forecastStorageService.getCspForcast(anyLong())).thenReturn(cspForecastList.get(0));
		try {
			cspServiceImpl.sendForecastForCable(cable.getCableId(), priority);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}

		ArgumentCaptor<CspForecastMessage> argCspForecastMessage = ArgumentCaptor.forClass(CspForecastMessage.class);
		verify(forecastStorageService, times(1)).updateCspForecastMessageWithCspForecasts(argCspForecastMessage.capture());
		assertEquals("CSPEAN13-0", argCspForecastMessage.getValue().getCsp());
		assertEquals("DSOEAN13", argCspForecastMessage.getValue().getDso());
		assertEquals(6, argCspForecastMessage.getValue().getPriority());
		assertEquals("EventId should be the first msg number. Thus, should contain :1", true, argCspForecastMessage.getValue().getEventId()
				.contains(":1"));

		verify(forecastStorageService, times(1)).storeCspForecastMessage(any(CspForecastMessage.class));
		verify(cspMessagingService, times(1)).pushCspForecast(any(CspForecastMessage.class), eq(cspForecast.getCableCsp().getCsp()));
		verify(forecastStorageService, times(1)).setCspForecastMessageSuccessful(anyLong());
	}

	// Compare the last forecast for a cable with the cableid. Update the cspvalue and assure that the correct values
	// are updated through updateCspForecastValue
	@Test
	public void testSendForecastForCable_ChangeUpAssignementDueToDownActionException() throws Exception {
		Cable cable = this.makeCable("CABLEID");
		int priority = 2;

		CableForecast cableForecast1 = this.makeCableForecastWithCableForecastOutput(1, this.makeCableForecastOneYearAddedToNow());
		CableForecast cableForecast2 = this.makeCableForecastWithCableForecastOutput(1, this.makeCableForecastOneYearAddedToNow());
		List<CspForecastValue> cspForecastValueList = this.makeCspForecastValueList(2);
		cspForecastValueList.get(0).setCableForecastOutput(cableForecast1.getCableForecastOutputs().get(0));
		cspForecastValueList.get(1).setCableForecastOutput(cableForecast2.getCableForecastOutputs().get(0));
		cspForecastValueList.get(1).setAssigned(new BigDecimal("123.00"));

		List<CspForecast> cspForecastList = new ArrayList<CspForecast>();
		CspForecast cspForecastDown = this.makeCspForecast(AssignmentChange.DOWN, 0, 1447L);
		cspForecastList.add(cspForecastDown);
		CspForecast cspForecastUp = this.makeCspForecast(AssignmentChange.UP, 0, 1447L);
		cspForecastUp.setChangeBlock(cspForecastValueList.get(1));
		cspForecastList.add(cspForecastUp);

		cspForecastDown.setCspForecastValues(cspForecastValueList);

		when(forecastStorageService.getLastCspForecastsForCable(any(Cable.class))).thenReturn(cspForecastList);
		when(forecastStorageService.getCspForcast(anyLong())).thenReturn(null).thenReturn(cspForecastList.get(0));
		try {
			cspServiceImpl.sendForecastForCable(cable.getCableId(), priority);
		} catch (Exception e) {
			fail("No exception should be thrown.");
		}

		PowerMockito.verifyStatic(times(1));
		ActionLogger.error(eq("Error sending forecast to csp! Due to: No forecast available for Csp!"), any(Object[].class),
				any(Throwable.class), eq(true));

		ArgumentCaptor<CspForecastValue> argCspForecastValue = ArgumentCaptor.forClass(CspForecastValue.class);
		verify(forecastStorageService, times(1)).updateCspForecastValue(argCspForecastValue.capture());
		assertEquals(12, argCspForecastValue.getValue().getAssigned().intValue());
		assertEquals(123, argCspForecastValue.getValue().getOriginalAssigned().intValue());

		// update after reassignment of forecast value (therefore msg number is 2)
		ArgumentCaptor<CspForecastMessage> argCspForecastMessage = ArgumentCaptor.forClass(CspForecastMessage.class);
		verify(forecastStorageService, times(1)).updateCspForecastMessageWithCspForecasts(argCspForecastMessage.capture());
		assertEquals("CSPEAN13-0", argCspForecastMessage.getValue().getCsp());
		assertEquals("DSOEAN13", argCspForecastMessage.getValue().getDso());
		assertEquals(2, argCspForecastMessage.getValue().getPriority());
		assertEquals("EventId should be the second msg number. Thus, should contain :2", true, argCspForecastMessage.getValue()
				.getEventId().contains(":2"));
	}
}
