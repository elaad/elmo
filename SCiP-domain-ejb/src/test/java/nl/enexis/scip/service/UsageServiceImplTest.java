package nl.enexis.scip.service;

/*
 * #%L
 * SCiP-domain-ejb
 * %%
 * Copyright (C) 2015 - 2016 ENEXIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import nl.enexis.scip.UnitTestHelper;
import nl.enexis.scip.action.ActionContext;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionTargetInOut;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableCsp;
import nl.enexis.scip.model.ConfigurationParameter;
import nl.enexis.scip.model.Csp;
import nl.enexis.scip.model.CspForecastValue;
import nl.enexis.scip.model.CspUsage;
import nl.enexis.scip.model.CspUsageMessage;
import nl.enexis.scip.model.CspUsageValue;
import nl.enexis.scip.model.enumeration.BehaviourType;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

//@RunWith(PowerMockRunner.class)
//@PrepareForTest({ Date.class })
public class UsageServiceImplTest {

	@InjectMocks
	private UsageServiceImpl usageServiceImpl;

	// @org.mockito.Spy causes bug when combined with PowerMockRunner, therefore might be
	// initialized in @Before
	@org.mockito.Spy
	private CspUsageMessage cspUsageMessage;

	@Mock
	private CspStorageService cspStorageService;

	@Mock
	private TopologyService topologyService;

	@Mock
	private TopologyStorageService topologyStorageService;

	@Mock
	private GeneralService generalService;

	@Mock
	private UsageServiceLocal self;

	@Mock
	private ForecastStorageService forecastStorageService;

	@Before
	public void setup() throws NoSuchMethodException {
		MockitoAnnotations.initMocks(this);
		// this.cspUsageMessage = spy(CspUsageMessage.class);
	}

	@Test
	public void testReceiveUsage() throws Exception {
		// prepare
		when(cspUsageMessage.getEventId()).thenReturn("EVENTID");
		when(cspStorageService.findCspUsageMessageByEventId(anyString())).thenReturn(cspUsageMessage);
		try {
			usageServiceImpl.receiveUsage(cspUsageMessage);
			fail("Should throw ActionException");
		} catch (ActionException ae) {
			assertEquals("Event not unique identified with id EVENTID", ae.getMessage());
			assertEquals(ActionExceptionType.MESSAGE, ae.getType());
		}
	}

	private CspUsage makeCspUsage(String cableId) {
		Cable cable = new Cable();
		cable.setCableId(cableId);
		cable.setBehaviour(BehaviourType.SC);

		CableCsp cableCsp = new CableCsp();
		cableCsp.setCable(cable);
		cable.setBehaviour(BehaviourType.SC);

		CspUsage cspUsage = new CspUsage();
		cspUsage.setCableCsp(cableCsp);
		return cspUsage;
	}

	private List<CspUsage> prepareForLoop() throws ActionException {
		List<CspUsage> cspUsageList = new ArrayList<CspUsage>();
		cspUsageList.add(this.makeCspUsage("CABLEID0"));
		cspUsageList.add(this.makeCspUsage("CABLEID1"));
		cspUsageList.add(this.makeCspUsage("CABLEID2"));
		cspUsageMessage.setCspUsages(cspUsageList);

		CspUsageValue cspUsageValue1 = spy(CspUsageValue.class);
		cspUsageValue1.setAverageCurrent(new BigDecimal("888"));
		List<CspUsageValue> cspUsageValueList1 = new ArrayList<CspUsageValue>();
		cspUsageValueList1.add(cspUsageValue1);

		CspUsageValue cspUsageValue2 = spy(CspUsageValue.class);
		cspUsageValue2.setAverageCurrent(new BigDecimal("1337"));
		List<CspUsageValue> cspUsageValueList2 = new ArrayList<CspUsageValue>();
		cspUsageValueList2.add(cspUsageValue2);

		CspUsageValue cspUsageValue3 = spy(CspUsageValue.class);
		List<CspUsageValue> cspUsageValueList3 = new ArrayList<CspUsageValue>();
		cspUsageValueList3.add(cspUsageValue3);

		cspUsageList.get(0).setCspUsageValues(cspUsageValueList1);
		cspUsageList.get(1).setCspUsageValues(cspUsageValueList2);
		cspUsageList.get(2).setCspUsageValues(cspUsageValueList3);

		return cspUsageList;
	}

	private void setLocalVariables() throws ActionException {
		ConfigurationParameter configurationParameter = mock(ConfigurationParameter.class);
		when(configurationParameter.getValue()).thenReturn("15");
		when(cspUsageMessage.getEventId()).thenReturn("EVENTID");
		when(cspStorageService.findCspUsageMessageByEventId(anyString())).thenReturn(null);
		when(generalService.getConfigurationParameter(anyString())).thenReturn(configurationParameter);
	}

	// Future cps usage input, therefore verify exception is logged
	@Test
	public void testReceiveUsage_ForLoopExceptionFuture() throws ActionException {
		// prepare
		Date dateMock = mock(Date.class);
		when(dateMock.after(any(Date.class))).thenReturn(true).thenReturn(true);
		this.setLocalVariables();
		List<CspUsage> cspUsageList = this.prepareForLoop();
		List<CspUsageValue> cspUsageValueList = cspUsageList.get(0).getCspUsageValues();
		when(cspUsageValueList.get(0).getStartedAt()).thenReturn(dateMock);
		try {
			usageServiceImpl.receiveUsage(cspUsageMessage);
			fail("Expected exception because current date is before start or end block date.");
		} catch (ActionException ae) {
			assertEquals("Exception does not contain correct massage ", true,
					ae.getMessage().contains("Usage reported for future. Reported start:"));
			assertEquals(ActionExceptionType.DATA, ae.getType());
		}
	}

	// The start date does not equal end date, therefore, verify that an exception is logged
	@Test
	public void testReceiveUsage_ForLoopExceptionBoundaries() throws ActionException {
		// prepare
		Date dateMock1 = mock(Date.class);
		when(dateMock1.after(any(Date.class))).thenReturn(false).thenReturn(false);
		Date dateMock2 = mock(Date.class);
		when(dateMock2.after(any(Date.class))).thenReturn(false).thenReturn(false);

		this.setLocalVariables();
		List<CspUsage> cspUsageList = this.prepareForLoop();
		List<CspUsageValue> cspUsageValueList = cspUsageList.get(0).getCspUsageValues();
		when(cspUsageValueList.get(0).getStartedAt()).thenReturn(dateMock1).thenReturn(UnitTestHelper.getDate(10000000));
		when(cspUsageValueList.get(0).getEndedAt()).thenReturn(dateMock2).thenReturn(UnitTestHelper.getDate(20000000));

		try {
			usageServiceImpl.receiveUsage(cspUsageMessage);
			fail("Expected exception because startdate does not equal enddate");
		} catch (ActionException ae) {
			assertEquals("Exception does not contain correct massage", true,
					ae.getMessage().contains("Usage reported over block boundaries. Reported start: "));
			assertEquals(ActionExceptionType.DATA, ae.getType());
		}
	}

	// Input csp usage is correctly setup. Verify that the correct values are propagated
	@Test
	public void testReceiveUsage_ForLoopMain() throws ActionException {
		// prepare
		Date dateMock1 = mock(Date.class);
		dateMock1.setTime(1450868400000L);
		when(dateMock1.after(any(Date.class))).thenReturn(false);
		Date dateMock2 = mock(Date.class);
		dateMock2.setTime(1450870407000L);
		when(dateMock2.after(any(Date.class))).thenReturn(false);

		this.setLocalVariables();
		List<CspUsage> cspUsageList = this.prepareForLoop();
		List<CspUsageValue> cspUsageValueList1 = cspUsageList.get(0).getCspUsageValues();
		List<CspUsageValue> cspUsageValueList2 = cspUsageList.get(1).getCspUsageValues();
		List<CspUsageValue> cspUsageValueList3 = cspUsageList.get(2).getCspUsageValues();

		when(cspUsageValueList1.get(0).getStartedAt()).thenReturn(dateMock1).thenReturn(UnitTestHelper.getDate23Dec2015at11h00m00s());
		when(cspUsageValueList1.get(0).getEndedAt()).thenReturn(dateMock2).thenReturn(UnitTestHelper.getDate23Dec2015at11h00m00s());

		when(cspUsageValueList2.get(0).getStartedAt()).thenReturn(dateMock1).thenReturn(
				UnitTestHelper.getDate(1997, Calendar.SEPTEMBER, 2, 2, 22, 2));
		when(cspUsageValueList2.get(0).getEndedAt()).thenReturn(dateMock2).thenReturn(
				UnitTestHelper.getDate(1997, Calendar.SEPTEMBER, 2, 2, 22, 2));

		when(cspUsageValueList3.get(0).getStartedAt()).thenReturn(dateMock1);
		when(cspUsageValueList3.get(0).getEndedAt()).thenReturn(dateMock2);

		usageServiceImpl.receiveUsage(cspUsageMessage);

		assertEquals(UnitTestHelper.getDate(2015, Calendar.DECEMBER, 23, 0, 0, 0), cspUsageValueList1.get(0).getDay());
		assertEquals(4, cspUsageValueList1.get(0).getDayOfWeek());
		assertEquals(11, cspUsageValueList1.get(0).getHour());
		assertEquals(1, cspUsageValueList1.get(0).getBlock());
		assertEquals("CABLEID0", cspUsageList.get(0).getCableCsp().getCable().getCableId());

		assertEquals(UnitTestHelper.getDate(1997, Calendar.SEPTEMBER, 2, 0, 0, 0), cspUsageValueList2.get(0).getDay());
		assertEquals(3, cspUsageValueList2.get(0).getDayOfWeek());
		assertEquals(2, cspUsageValueList2.get(0).getHour());
		assertEquals(2, cspUsageValueList2.get(0).getBlock());
		assertEquals("CABLEID1", cspUsageList.get(1).getCableCsp().getCable().getCableId());

		assertEquals(true, cspUsageMessage.isSuccessful());
	}

	// Verify that the deviations are correctly calculated
	@Test
	public void testDetermineCspDeviations() throws ActionException {
		// prepare
		List<CspUsage> cspUsageList = this.prepareForLoop();
		List<CspUsageValue> cspUsageValueList1 = cspUsageList.get(0).getCspUsageValues();
		List<CspUsageValue> cspUsageValueList2 = cspUsageList.get(1).getCspUsageValues();
		List<CspUsageValue> cspUsageValueList3 = cspUsageList.get(2).getCspUsageValues();
		CspForecastValue cspForecastValue = new CspForecastValue();
		cspForecastValue.setAssigned(new BigDecimal("500"));

		Csp csp = mock(Csp.class);
		when(topologyStorageService.findCspByEan13(anyString())).thenReturn(csp);
		when(cspStorageService.findCspUsageMessageByEventId(anyString())).thenReturn(cspUsageMessage);
		when(forecastStorageService.getLastCspForecastValueForCspUsageValue(any(CspUsageValue.class))).thenReturn(cspForecastValue)
				.thenReturn(cspForecastValue).thenReturn(null);

		usageServiceImpl.determineCspDeviations("CSPUSAGEMESSAGEEVENTID");

		assertEquals(new BigDecimal("388"), cspUsageValueList1.get(0).getDeviation());
		assertEquals(new BigDecimal("837"), cspUsageValueList2.get(0).getDeviation());
		assertEquals(null, cspUsageValueList3.get(0).getDeviation());
		assertEquals(1, ActionContext.getAction().getTargets().get(0).getNumber().intValue());
		assertEquals(ActionTargetInOut.IN, ActionContext.getAction().getTargets().get(0).getDirection());
	}

}