package nl.enexis.scip.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import nl.enexis.scip.UnitTestHelper;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.ConfigurationParameter;
import nl.enexis.scip.model.Measurement;
import nl.enexis.scip.model.MeasurementValue;
import nl.enexis.scip.model.Meter;
import nl.enexis.scip.model.enumeration.MeasuresFor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ActionLogger.class })
public class PrepareForecastInputServiceImplTest {

	@InjectMocks
	private PrepareForecastInputServiceImpl pfisi;

	@Mock
	private ForecastStorageService forecastStorageService;

	@Mock
	private MeasurementStorageService measurementStorageService;

	@Mock
	private TopologyStorageService topologyStorageService;

	@Mock
	private GeneralService generalService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ActionLogger.class);
	}

	private Cable makeCable(String cableId) {
		Cable cable = new Cable();
		cable.setCableId(cableId);
		cable.setActive(true);
		return cable;
	}

	private List<MeasurementValue> makeMeasurementValueList(int size, String id) {
		List<MeasurementValue> measurementValueList = new ArrayList<MeasurementValue>();
		for (int i = 0; i < size; i++) {
			Meter meter = new Meter();
			meter.setMeterId("METERID-" + id + "-" + i);
			meter.setMeasures(i % 2 == 0 ? MeasuresFor.TOTALS : MeasuresFor.CARS);
			MeasurementValue measurementValue = new MeasurementValue();
			measurementValue.setMeter(meter);
			measurementValue.setIGem1(new BigDecimal((i + 1) * 4.119));
			measurementValue.setIGem2(new BigDecimal((i + 1) * 1.139));
			measurementValue.setIGem3(new BigDecimal((i + 1) * 2.6));
			measurementValueList.add(measurementValue);
		}
		return measurementValueList;
	}

	private List<Measurement> makeMeasurementList(int size) {
		List<Measurement> measurementList = new ArrayList<Measurement>();
		for (long i = 0; i < size; i++) {
			Measurement measurement = new Measurement();
			measurement.setId(i);
			measurement.setComplete(i % 2 == 0 ? true : false);
			measurementList.add(measurement);
		}
		return measurementList;
	}

	private CableForecastInput makeCableForecastInputWithDate(Date date) {
		CableForecastInput cableForecastInput = new CableForecastInput();
		cableForecastInput.setBlockStart(date);
		return cableForecastInput;
	}

	// Assert that the calculated amp values and corresponding 'metadata' are correctly set
	@Test
	public void testPrepareForecastInputs_CalculationsAndMetadata() throws Exception {
		Cable cable = this.makeCable("CABLEID");
		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);
		ConfigurationParameter configurationParameter1 = new ConfigurationParameter();
		configurationParameter1.setValue("3");
		ConfigurationParameter configurationParameter2 = new ConfigurationParameter();
		configurationParameter2.setValue("2");
		ConfigurationParameter configurationParameter3 = new ConfigurationParameter();
		configurationParameter3.setValue("15");
		when(generalService.getConfigurationParameter("ForecastInputBlocks")).thenReturn(configurationParameter1);
		when(generalService.getConfigurationParameter("MeasurementsPerBlock")).thenReturn(configurationParameter2);
		when(generalService.getConfigurationParameter("ForecastBlockMinutes")).thenReturn(configurationParameter3);
		Date date = UnitTestHelper.getCurrentDateMinusOneHour();
		CableForecastInput input = this.makeCableForecastInputWithDate(date);
		when(forecastStorageService.getLastCableForecastInput(any(Cable.class), any(Date.class))).thenReturn(input);
		List<Measurement> measurementList = this.makeMeasurementList(2);
		measurementList.get(0).setMeasurementValues(this.makeMeasurementValueList(3, "1447"));
		measurementList.get(1).setMeasurementValues(this.makeMeasurementValueList(7, "12447"));
		PrepareForecastInputServiceImpl pfisiSpy = spy(pfisi);
		doReturn(measurementList).when(pfisiSpy).getMeasurementForCableBetweenDates(any(CableForecastInput.class), any(Date.class),
				any(Date.class), eq(cable), anyInt(), anyBoolean());

		pfisiSpy.prepareForecastInputs(cable);

		ArgumentCaptor<CableForecastInput> argResult = ArgumentCaptor.forClass(CableForecastInput.class);
		verify(forecastStorageService, times(3)).createCableForecastInput(argResult.capture());
		CableForecastInput resultLoop1 = argResult.getAllValues().get(0);
		assertEquals(new BigDecimal(41.19).doubleValue(), resultLoop1.getAmpNow1().doubleValue(), 0.001);
		assertEquals(new BigDecimal(11.39).doubleValue(), resultLoop1.getAmpNow2().doubleValue(), 0.001);
		assertEquals(new BigDecimal(26).doubleValue(), resultLoop1.getAmpNow3().doubleValue(), 0.001);

		assertEquals(new BigDecimal(41.19).doubleValue(), resultLoop1.getAmpMinusOneDay1().doubleValue(), 0.001);
		assertEquals(new BigDecimal(11.39).doubleValue(), resultLoop1.getAmpMinusOneDay2().doubleValue(), 0.001);
		assertEquals(new BigDecimal(26).doubleValue(), resultLoop1.getAmpMinusOneDay3().doubleValue(), 0.001);

		assertEquals(new BigDecimal(41.19).doubleValue(), resultLoop1.getAmpMinusSixDay1().doubleValue(), 0.001);
		assertEquals(new BigDecimal(11.39).doubleValue(), resultLoop1.getAmpMinusSixDay2().doubleValue(), 0.001);
		assertEquals(new BigDecimal(26).doubleValue(), resultLoop1.getAmpMinusSixDay3().doubleValue(), 0.001);

		assertEquals(new BigDecimal(41.19).doubleValue(), resultLoop1.getAmpMinusOneWeek1().doubleValue(), 0.001);
		assertEquals(new BigDecimal(11.39).doubleValue(), resultLoop1.getAmpMinusOneWeek2().doubleValue(), 0.001);
		assertEquals(new BigDecimal(26).doubleValue(), resultLoop1.getAmpMinusOneWeek3().doubleValue(), 0.001);

		assertEquals(cable, resultLoop1.getCable());
		assertEquals(UnitTestHelper.getDayOfDateWithoutTime(date), resultLoop1.getDay());
		// Calendar calendar = Calendar.getInstance();
		// calendar.setTime(date);
		// assertEquals(calendar.get(Calendar.HOUR_OF_DAY), resultLoop1.getHour());
	}

	// Assert that warning is logged in case there is no active cable
	@Test
	public void testPrepareForecastInputs_NoActiveCable() throws Exception {
		Cable cable = this.makeCable("CABLEID");
		cable.setActive(false);
		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(cable);

		pfisi.prepareForecastInputs(cable);

		PowerMockito.verifyStatic(times(1));
		ActionLogger.warn(eq("Trying to prepare forecast input for inactive cable {0}"), any(Object[].class), any(Throwable.class),
				eq(true));
	}

	// Assert that exception is raised in case there is no cable
	@Test
	public void testPrepareForecastInputs_NoCable() throws Exception {
		Cable cable = this.makeCable("CABLEID");
		when(topologyStorageService.findCableByCableId(anyString())).thenReturn(null);
		try {
			pfisi.prepareForecastInputs(cable);
		} catch (ActionException ae) {
			assertEquals("Cable not found! Cable: CABLEID", ae.getMessage());
			assertEquals(ActionExceptionType.DATA, ae.getType());
		}
	}

	// Verify measurement is calculated correctly
	@Test
	public void testCalculateMeasurementAmp() {
		CableForecastInput input = new CableForecastInput();
		boolean includeCarUsage = true;
		List<Measurement> measurementsAmp = this.makeMeasurementList(2);
		measurementsAmp.get(0).setMeasurementValues(this.makeMeasurementValueList(3, "1447"));
		measurementsAmp.get(1).setMeasurementValues(this.makeMeasurementValueList(7, "12447"));

		BigDecimal[] result = pfisi.calculateMeasurementAmps(input, measurementsAmp, includeCarUsage);

		assertEquals(new BigDecimal(24.714).doubleValue(), result[0].doubleValue(), 0.001);
		assertEquals(new BigDecimal(6.834).doubleValue(), result[1].doubleValue(), 0.001);
		assertEquals(new BigDecimal(15.6).doubleValue(), result[2].doubleValue(), 0.001);
	}

	// Verify that the dates in between forced dates are correctly edited
	@Test
	public void testGetMeasurementForCableBetweenDates_AssureDateCorrected() throws Exception {
		CableForecastInput input = new CableForecastInput();
		Date startDate = UnitTestHelper.getDateWithoutTime(2012, Calendar.SEPTEMBER, 12);
		Date endDate = UnitTestHelper.getDateWithoutTime(2012, Calendar.SEPTEMBER, 14);
		Cable cable = this.makeCable("CABLEID");
		int measurementsPerBlock = 4;
		boolean doCheckDeviatedDay = true;
		int numberOfMeasurements = 6;
		List<Measurement> measurementList = this.makeMeasurementList(numberOfMeasurements);
		when(measurementStorageService.getMeasurementForCableBetweenDates(any(Date.class), any(Date.class), any(Cable.class))).thenReturn(
				measurementList);
		PrepareForecastInputServiceImpl pfisiSpy = spy(pfisi);
		for (int i = 0; i < numberOfMeasurements; i++) {
			doReturn(measurementList.get(i)).when(pfisiSpy).replaceIncompleteMeasurement(eq(measurementList.get(i)));
		}

		List<Measurement> result = pfisiSpy.getMeasurementForCableBetweenDates(input, startDate, endDate, cable, measurementsPerBlock,
				doCheckDeviatedDay);

		verify(pfisiSpy, times(1)).replaceIncompleteMeasurement(eq(measurementList.get(1)));
		verify(pfisiSpy, times(1)).replaceIncompleteMeasurement(eq(measurementList.get(3)));
		verify(pfisiSpy, times(1)).replaceIncompleteMeasurement(eq(measurementList.get(5)));
		assertEquals(input.isCorrected(), !result.get(5).isComplete()); // 5 = numberOfMeasurements - 1 (and thus is the
																		// last of the list)
	}

	// Assert that, in case no measurements are found, it is logged
	@Test
	public void testGetMeasurementForCableBetweenDates_NoMeasurementsDebugLog() throws Exception {
		CableForecastInput input = new CableForecastInput();
		Date startDate = UnitTestHelper.getDateWithoutTime(2012, Calendar.SEPTEMBER, 12);
		Date endDate = UnitTestHelper.getDateWithoutTime(2012, Calendar.SEPTEMBER, 14);
		Cable cable = this.makeCable("CABLEID");
		int measurementsPerBlock = 4;
		boolean doCheckDeviatedDay = false;
		when(measurementStorageService.getMeasurementForCableBetweenDates(any(Date.class), any(Date.class), any(Cable.class))).thenReturn(
				new ArrayList<Measurement>());
		pfisi.getMeasurementForCableBetweenDates(input, startDate, endDate, cable, measurementsPerBlock, doCheckDeviatedDay);

		PowerMockito.verifyStatic(times(1));
		ActionLogger.debug(eq("No measurements found for {0} forecast input. Search starting at {1} and used {2} weeks in past"),
				any(Object[].class), any(Throwable.class), eq(false));
	}

	// Assert that, in case there is no nearby complete next or previous measurement, an exception should be thrown
	@Test
	public void testReplaceIncompleteMeasurement_NoCompleteNextOrPreviousMeasurement() throws Exception {
		Measurement measurement = new Measurement();
		measurement.setCable(this.makeCable("CABLEID"));
		measurement.setDate(UnitTestHelper.getDate23Dec2015at11h00m27s());
		when(measurementStorageService.getClosestCompleteMeasurement(any(Measurement.class), anyBoolean())).thenReturn(null);

		try {
			pfisi.replaceIncompleteMeasurement(measurement);
			fail("Should throw exception due to no complete next and previous measurement.");
		} catch (ActionException ae) {
			assertEquals(
					"No next and previous complete measurement to replace incomplete measurement for Measurement [date=Wed Dec 23 11:00:27 GMT 2015, cable=Cable [cableId=CABLEID]]",
					ae.getMessage());
			assertEquals(ActionExceptionType.DATA, ae.getType());
		}
	}

	// Verifies that replacement measurement is correctly calculated
	@Test
	public void testReplaceIncompleteMeasurement_CalculateClosestToCompleteMeasurement() throws Exception {
		Measurement measurement1 = new Measurement();
		measurement1.setMeasurementValues(this.makeMeasurementValueList(4, "m1"));

		Measurement measurement2 = new Measurement();
		measurement2.setMeasurementValues(this.makeMeasurementValueList(2, "m2"));

		when(measurementStorageService.getClosestCompleteMeasurement(any(Measurement.class), eq(false))).thenReturn(measurement1);
		when(measurementStorageService.getClosestCompleteMeasurement(any(Measurement.class), eq(true))).thenReturn(measurement2);

		Measurement result = pfisi.replaceIncompleteMeasurement(measurement1);

		assertEquals(new BigDecimal(10.298).doubleValue(), result.getMeasurementValues().get(0).getIGem1().doubleValue(), 0.001);
		assertEquals(new BigDecimal(2.848).doubleValue(), result.getMeasurementValues().get(0).getIGem2().doubleValue(), 0.001);
		assertEquals(new BigDecimal(6.5).doubleValue(), result.getMeasurementValues().get(0).getIGem3().doubleValue(), 0.001);

		assertEquals(new BigDecimal(16.476).doubleValue(), result.getMeasurementValues().get(1).getIGem1().doubleValue(), 0.001);
		assertEquals(new BigDecimal(4.556).doubleValue(), result.getMeasurementValues().get(1).getIGem2().doubleValue(), 0.001);
		assertEquals(new BigDecimal(10.4).doubleValue(), result.getMeasurementValues().get(1).getIGem3().doubleValue(), 0.001);

		// impl currently gets the last 2 meters, so in this case 0 and 1 of measurement2
		assertEquals(result.getMeasurementValues().get(0).getMeter(), measurement2.getMeasurementValues().get(0).getMeter());
		assertEquals(result.getMeasurementValues().get(1).getMeter(), measurement2.getMeasurementValues().get(1).getMeter());
	}

}
