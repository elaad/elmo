package nl.enexis.scip.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import nl.enexis.scip.UnitTestHelper;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.model.Cable;
import nl.enexis.scip.model.CableForecast;
import nl.enexis.scip.model.CableForecastDeviationDay;
import nl.enexis.scip.model.CableForecastInput;
import nl.enexis.scip.model.CableForecastOutput;
import nl.enexis.scip.model.enumeration.DeviationDayType;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class DeviationDayDeterminationServiceImplTest {

	@InjectMocks
	DeviationDayDeterminationServiceImpl dddsi;

	@Mock
	TopologyStorageService topologyStorageService;

	@Mock
	ForecastStorageService forecastStorageService;

	@Mock
	GeneralService generalService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	private CableForecastOutput makeCableForecastOutputWithDate(Date date) {
		CableForecastOutput cableForecastOutput = new CableForecastOutput();
		cableForecastOutput.setDay(UnitTestHelper.getDayOfDateWithoutTime(date));
		cableForecastOutput.setHour(4);
		cableForecastOutput.setBlock(2);
		return cableForecastOutput;
	}

	private CableForecastInput makeCableForecastInputWithDate(Date date) {
		CableForecastInput cableForecastInput = new CableForecastInput();
		cableForecastInput.setBlockStart(date);
		return cableForecastInput;
	}

	private Cable makeCable(String cableId) {
		Cable cable = new Cable();
		cable.setCableId(cableId);
		cable.setActive(true);
		return cable;
	}

	private List<CableForecastDeviationDay> makeCableForecastDeviationDayList(int size) {
		List<CableForecastDeviationDay> cableForecastDeviationDayList = new ArrayList<CableForecastDeviationDay>();
		for (int i = 0; i < size; i++) {
			CableForecastDeviationDay cableForecastDeviationDay1 = new CableForecastDeviationDay();
			cableForecastDeviationDay1.setDeviation(new BigDecimal(i * 4.119));
			cableForecastDeviationDayList.add(cableForecastDeviationDay1);
		}
		return cableForecastDeviationDayList;
	}

	private CableForecast makeCableForecastWithCableForecastOutput(CableForecastOutput cableForecastOutput) {
		CableForecast cableForecast = new CableForecast();

		Cable cable = this.makeCable("CABLEID");
		cableForecast.setCable(cable);
		cableForecast.setBlockMinutes(15);
		cableForecast.setRemainingCapacityFactor(new BigDecimal("0.40"));

		cableForecastOutput.setCableForecast(cableForecast);

		List<CableForecastOutput> cableForecastOutputList = new ArrayList<CableForecastOutput>();
		cableForecastOutputList.add(cableForecastOutput);

		cableForecast.setCableForecastOutputs(cableForecastOutputList);

		return cableForecast;
	}

	private CableForecast makeCableForecastWithoutCableForecastOutput() {
		CableForecast cableForecast = new CableForecast();

		Cable cable = this.makeCable("CABLEID");
		cableForecast.setCable(cable);
		cableForecast.setBlockMinutes(15);
		cableForecast.setRemainingCapacityFactor(new BigDecimal("0.40"));

		return cableForecast;
	}

	// Assert that cable forecast input is correctly mapped with the corresponding date
	@Test
	public void testMapCableForecastOutputs() throws ActionException {
		Date date1 = UnitTestHelper.getDateWithoutTime(2000, Calendar.JANUARY, 25);
		Date date2 = UnitTestHelper.getDateWithoutTime(2000, Calendar.JANUARY, 25);
		Date date3 = UnitTestHelper.getDateWithoutTime(2012, Calendar.MARCH, 3);
		Date date4 = UnitTestHelper.getDateWithoutTime(2000, Calendar.JANUARY, 25);

		List<CableForecastOutput> cableForecastOutputList = new ArrayList<CableForecastOutput>();
		cableForecastOutputList.add(this.makeCableForecastOutputWithDate(date1));
		cableForecastOutputList.add(this.makeCableForecastOutputWithDate(date2));
		cableForecastOutputList.add(this.makeCableForecastOutputWithDate(date3));
		cableForecastOutputList.add(this.makeCableForecastOutputWithDate(date4));

		CableForecast cableForecast = this.makeCableForecastWithoutCableForecastOutput();
		cableForecast.setCableForecastOutputs(cableForecastOutputList);

		Map<Long, CableForecastOutput> cfosMap = dddsi.mapCableForecastOutputs(cableForecast);

		assertEquals(2, cfosMap.size());
		for (CableForecastOutput cfo : cfosMap.values()) {
			assertEquals(4, cfo.getHour());
			assertEquals(2, cfo.getBlock());
		}
	}

	// Assert that cable forecast input is correctly mapped with the corresponding date
	@Test
	public void testMapCableForecastInputs() throws ActionException {
		Date date1 = UnitTestHelper.getDateWithoutTime(2040, Calendar.APRIL, 8);
		Date date2 = UnitTestHelper.getDateWithoutTime(1946, Calendar.MAY, 1);
		Date date3 = UnitTestHelper.getDateWithoutTime(1966, Calendar.JANUARY, 25);

		List<CableForecastInput> cfis = new ArrayList<CableForecastInput>();
		CableForecastInput cableForecastInput1 = this.makeCableForecastInputWithDate(date1);
		cfis.add(cableForecastInput1);
		CableForecastInput cableForecastInput2 = this.makeCableForecastInputWithDate(date2);
		cfis.add(cableForecastInput2);
		CableForecastInput cableForecastInput3 = this.makeCableForecastInputWithDate(date3);
		cfis.add(cableForecastInput3);

		Map<Long, CableForecastInput> cfisMap = dddsi.mapCableForecastInputs(cfis);

		assertEquals(cableForecastInput3, cfisMap.get(date3.getTime()));
		assertEquals(cableForecastInput2, cfisMap.get(date2.getTime()));
		assertEquals(cableForecastInput1, cfisMap.get(date1.getTime()));
	}

	// Assure otherDeviations is filled in case the forecast forces to
	@Test
	public void testUpdateDeviationsUntilNow() throws ActionException {
		Cable cable = this.makeCable("CABLEID");
		Date now = UnitTestHelper.getDateWithoutTime(2014, Calendar.JULY, 5);
		Date last = UnitTestHelper.getDateWithoutTime(2014, Calendar.JULY, 2);

		Map<Date, BigDecimal> calculatedDeviations = new TreeMap<Date, BigDecimal>();
		Map<Date, DeviationDayType> otherDeviations = new TreeMap<Date, DeviationDayType>();

		CableForecast cableForecast1 = this.makeCableForecastWithCableForecastOutput(this.makeCableForecastOutputWithDate(now));
		cableForecast1.setFixed(true);
		CableForecast cableForecast2 = this.makeCableForecastWithoutCableForecastOutput();

		CableForecastInput cableForecastInput = this.makeCableForecastInputWithDate(now);
		List<CableForecastInput> cableForecastInputList = new ArrayList<CableForecastInput>();
		cableForecastInputList.add(cableForecastInput);

		when(forecastStorageService.getFirstCableForecastOfDay(any(Cable.class), any(Date.class))).thenReturn(null).thenReturn(cableForecast1)
				.thenReturn(cableForecast2);
		when(forecastStorageService.getCableForecastInputs(any(CableForecast.class))).thenReturn(cableForecastInputList);

		DeviationDayDeterminationServiceImpl dddsiSpy = spy(dddsi);
		doNothing().when(dddsiSpy).setDeviations(any(Date.class), any(Map.class), any(Map.class), any(Map.class), any(Map.class));

		dddsiSpy.updateDeviationsUntilNow(cable, now, last, calculatedDeviations, otherDeviations);
		assertEquals(DeviationDayType.NO_FORECAST, otherDeviations.get(UnitTestHelper.getDateWithoutTime(2014, Calendar.JULY, 3)));
		assertEquals(DeviationDayType.FIXED_FORECAST, otherDeviations.get(UnitTestHelper.getDateWithoutTime(2014, Calendar.JULY, 4)));
		assertEquals(DeviationDayType.NO_FORECAST, otherDeviations.get(UnitTestHelper.getDateWithoutTime(2014, Calendar.JULY, 5)));
	}

	// Assure that, due to no output, no calculated deviations should be set
	@Test
	public void testSetDeviations_NoCableForecastOutputSet() {
		Date last = UnitTestHelper.getCurrentDateMinusOneYear();
		Map<Date, BigDecimal> calculatedDeviations = new TreeMap<Date, BigDecimal>();
		Map<Date, DeviationDayType> otherDeviations = new TreeMap<Date, DeviationDayType>();
		Map<Long, CableForecastOutput> cfosMap = new HashMap<Long, CableForecastOutput>();
		Map<Long, CableForecastInput> cfisMap = new HashMap<Long, CableForecastInput>();

		dddsi.setDeviations(last, calculatedDeviations, otherDeviations, cfosMap, cfisMap);
		assertEquals(DeviationDayType.NO_MEASUREMENTS, otherDeviations.get(last));
		assertEquals(0, calculatedDeviations.size());
		assertEquals(1, otherDeviations.size());
	}

	// Assure that deviations are correctly added to the intended deviations map
	@Test
	public void testSetDeviations_CableForecastOutputSet() {
		Date last = UnitTestHelper.getCurrentDateMinusOneYear();
		Map<Date, BigDecimal> calculatedDeviations = new TreeMap<Date, BigDecimal>();
		Map<Date, DeviationDayType> otherDeviations = new TreeMap<Date, DeviationDayType>();
		Map<Long, CableForecastOutput> cfosMap = new HashMap<Long, CableForecastOutput>();
		Map<Long, CableForecastInput> cfisMap = new HashMap<Long, CableForecastInput>();
		CableForecastOutput cableForecastOutput = this.makeCableForecastOutputWithDate(last);
		CableForecastInput cableForecastInput = this.makeCableForecastInputWithDate(last);

		calculatedDeviations = new TreeMap<Date, BigDecimal>();
		otherDeviations = new TreeMap<Date, DeviationDayType>();
		cableForecastOutput.setUsageExclCars1(new BigDecimal(389.9));
		cableForecastOutput.setUsageExclCars2(new BigDecimal(27.97));
		cableForecastOutput.setUsageExclCars3(new BigDecimal(34.1));
		cfosMap.put(1L, cableForecastOutput);
		cableForecastInput.setAmpNow1(new BigDecimal(94.09));
		cableForecastInput.setAmpNow2(new BigDecimal(51.191));
		cableForecastInput.setAmpNow3(new BigDecimal(1));
		cfisMap.put(1L, cableForecastInput);

		dddsi.setDeviations(last, calculatedDeviations, otherDeviations, cfosMap, cfisMap);
		assertEquals(new BigDecimal(117.377).doubleValue(), calculatedDeviations.get(last).doubleValue(), 0.001);
		assertEquals(null, otherDeviations.get(last));
		assertEquals(1, calculatedDeviations.size());
		assertEquals(0, otherDeviations.size());
	}

	// Verify that all the deviations are correctly set and stored
	@Test
	public void testSetCableForecastDeviationDay_NoCalculatedAverageDeviation() throws ActionException {
		Cable cable = this.makeCable("CABLEID");
		int maxDaysToCalculate = 3;
		double maxDeviationFactor = 0.55;
		Map<Date, BigDecimal> calculatedDeviations = new TreeMap<Date, BigDecimal>();
		Map<Date, DeviationDayType> otherDeviations = new TreeMap<Date, DeviationDayType>();
		Set<Date> dates = new TreeSet<Date>();
		Date date1 = UnitTestHelper.getDate23Dec2015at11h00m00s();
		Date date2 = UnitTestHelper.getDateWithoutTime(2001, Calendar.OCTOBER, 30);
		Date date3 = UnitTestHelper.getDateWithoutTime(2011, Calendar.APRIL, 2);
		dates.add(date1);
		dates.add(date2);
		dates.add(date3);
		calculatedDeviations.put(date1, new BigDecimal(333.33));
		otherDeviations.put(date2, DeviationDayType.NO_MEASUREMENTS);
		calculatedDeviations.put(date3, new BigDecimal(1.002));
		when(forecastStorageService.getNotDeviatedCableForecastDeviationDays(any(Cable.class), anyInt())).thenReturn(
				new ArrayList<CableForecastDeviationDay>()).thenReturn(this.makeCableForecastDeviationDayList(2));

		dddsi.storeCableForecastDeviationDays(cable, maxDaysToCalculate, maxDeviationFactor, calculatedDeviations, otherDeviations, dates);

		ArgumentCaptor<CableForecastDeviationDay> argStoreCableForecastDeviationDay = ArgumentCaptor
				.forClass(CableForecastDeviationDay.class);
		verify(forecastStorageService, times(3)).storeCableForecastDeviationDay(argStoreCableForecastDeviationDay.capture());
		assertEquals(3, argStoreCableForecastDeviationDay.getAllValues().size());

		CableForecastDeviationDay firstResult = argStoreCableForecastDeviationDay.getAllValues().get(0);
		assertEquals(cable, firstResult.getCable());
		assertEquals(date2, firstResult.getDay());
		assertEquals(new BigDecimal("0.00"), firstResult.getDeviation());
		assertEquals(new BigDecimal("0.00"), firstResult.getAverageDeviation());
		assertEquals(DeviationDayType.NO_MEASUREMENTS, firstResult.getType());

		CableForecastDeviationDay secondResult = argStoreCableForecastDeviationDay.getAllValues().get(1);
		assertEquals(cable, secondResult.getCable());
		assertEquals(date3, secondResult.getDay());
		assertEquals(new BigDecimal("1.002").doubleValue(), secondResult.getDeviation().doubleValue(), 0.001);
		assertEquals(new BigDecimal("2.059").doubleValue(), secondResult.getAverageDeviation().doubleValue(), 0.001);
		assertEquals(DeviationDayType.NO_DEVIATION, secondResult.getType());

		CableForecastDeviationDay ThirdResult = argStoreCableForecastDeviationDay.getAllValues().get(2);
		assertEquals(cable, ThirdResult.getCable());
		assertEquals(date1, ThirdResult.getDay());
		assertEquals(new BigDecimal("333.330").doubleValue(), ThirdResult.getDeviation().doubleValue(), 0.001);
		assertEquals(new BigDecimal("2.059").doubleValue(), ThirdResult.getAverageDeviation().doubleValue(), 0.001);
		assertEquals(DeviationDayType.DEVIATION, ThirdResult.getType());
	}
}
