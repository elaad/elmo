package nl.enexis.scip.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import nl.enexis.scip.UnitTestHelper;
import nl.enexis.scip.action.ActionException;
import nl.enexis.scip.action.ActionExceptionType;
import nl.enexis.scip.action.ActionLogger;
import nl.enexis.scip.dao.CspLocationMeasurementDao;
import nl.enexis.scip.dao.LocationMeasurementDao;
import nl.enexis.scip.dao.LocationMeasurementValueDao;
import nl.enexis.scip.model.ConfigurationParameter;
import nl.enexis.scip.model.CspLocationMeasurement;
import nl.enexis.scip.model.LocationMeasurement;
import nl.enexis.scip.model.LocationMeasurementValue;
import nl.enexis.scip.model.LocationMeasurementValuePK;
import nl.enexis.scip.model.MeasurementProvider;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ActionLogger.class, LocationMeasurementsServiceImpl.class, IOUtils.class })
public class LocationMeasurementsServiceImplTest {

	@InjectMocks
	private LocationMeasurementsServiceImpl lmsi;

	@Mock
	private LocationMeasurementsServiceLocal self;

	@Mock
	private CspMessagingService cspMessagingService;

	@Mock
	private LocationMeasurementDao locationMeasurementDao;

	@Mock
	private CspLocationMeasurementDao cspLocationMeasurementDao;

	@Mock
	private LocationMeasurementValueDao locationMeasurementValueDao;

	@Mock
	private GeneralService generalService;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ActionLogger.class);
	}

	// Verify that filename is null when no exception is thrown
	@Test
	public void testFetchMeasurementValues_FtpFilesProcessedCorrectly() throws Exception {
		// prepare
		LocationMeasurement locationMeasurement = new LocationMeasurement();
		FTPClient ftpClient = mock(FTPClient.class);
		ConfigurationParameter baseDirCP = new ConfigurationParameter();
		baseDirCP.setValue("BASEDIR/");
		when(generalService.getConfigurationParameter(eq("BaseDir"))).thenReturn(baseDirCP);
		LocationMeasurementsServiceImpl lmsiSpy = spy(lmsi);
		doNothing().when(lmsiSpy).disconnectFtp(eq(ftpClient));
		doNothing().when(lmsiSpy).isSuccessReply(eq(ftpClient));

		SortedSet<FTPFile> ftpFileSet = new TreeSet<FTPFile>(new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				return -1;
			}

		});

		FTPFile ftpFileMock = mock(FTPFile.class);
		when(ftpFileMock.isFile()).thenReturn(true);
		when(ftpFileMock.getName()).thenReturn("FILENAME");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(UnitTestHelper.getDate23Dec2015at11h00m27s());
		when(ftpFileMock.getTimestamp()).thenReturn(calendar);
		ftpFileSet.add(ftpFileMock);
		doReturn(ftpFileSet).when(lmsiSpy).getSortedList(any());

		InputStream inputStreamMock = mock(InputStream.class);
		when(ftpClient.retrieveFileStream(any())).thenReturn(inputStreamMock);
		FileOutputStream fileOutputStreamMock = mock(FileOutputStream.class);
		PowerMockito.whenNew(FileOutputStream.class).withAnyArguments().thenReturn(fileOutputStreamMock);
		PowerMockito.whenNew(FTPClient.class).withAnyArguments().thenReturn(ftpClient);
		PowerMockito.mockStatic(IOUtils.class);
		PowerMockito.when(IOUtils.copy(eq(inputStreamMock), eq(fileOutputStreamMock))).thenReturn(1337);
		doNothing().when(lmsiSpy).processLocationMeasurement(eq(locationMeasurement), eq(ftpClient), eq("BASEDIR/"), anyString());
		doNothing().when(lmsiSpy).connectToFtp(eq(ftpClient));

		// execute
		try {
			lmsiSpy.fetchMeasurementValues(locationMeasurement);
		} catch (Exception e) {
			fail("Should not throw exception.");
		}

		// verify
		PowerMockito.verifyStatic(times(1));
		ActionLogger.debug(eq("FTP: FILE: FILENAME; Datetime: Wed Dec 23 11:00:27 GMT 2015"), any(Object[].class), any(Throwable.class),
				eq(false));
		verify(lmsiSpy, times(2)).isSuccessReply(eq(ftpClient));
		verify(lmsiSpy, times(1)).disconnectFtp(eq(ftpClient));
	}

	// Assert that exception is handled accordingly when thrown
	@Test
	public void testFetchMeasurementValues_FtpFilesProcessedWithException() throws Exception {
		// prepare
		LocationMeasurement locationMeasurement = new LocationMeasurement();
		FTPClient ftpClient = mock(FTPClient.class);
		ConfigurationParameter baseDirCP = new ConfigurationParameter();
		baseDirCP.setValue("BASEDIR/");
		when(generalService.getConfigurationParameter(eq("BaseDir"))).thenReturn(baseDirCP);
		LocationMeasurementsServiceImpl lmsiSpy = spy(lmsi);
		doNothing().when(lmsiSpy).disconnectFtp(eq(ftpClient));
		doNothing().when(lmsiSpy).isSuccessReply(eq(ftpClient));

		// sorting not relevant because it is not tested here
		SortedSet<FTPFile> ftpFileSet = new TreeSet<FTPFile>(new Comparator<Object>() {
			@Override
			public int compare(Object o1, Object o2) {
				return -1;
			}
		});

		FTPFile ftpFileMock = mock(FTPFile.class);
		when(ftpFileMock.isFile()).thenReturn(true);
		when(ftpFileMock.getName()).thenReturn("FILENAME");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(UnitTestHelper.getDate23Dec2015at11h00m27s());
		when(ftpFileMock.getTimestamp()).thenReturn(calendar);
		ftpFileSet.add(ftpFileMock);
		doReturn(ftpFileSet).when(lmsiSpy).getSortedList(any());
		InputStream inputStreamMock = mock(InputStream.class);
		when(ftpClient.retrieveFileStream(any())).thenReturn(inputStreamMock);
		FileOutputStream fileOutputStreamMock = mock(FileOutputStream.class);
		PowerMockito.whenNew(FileOutputStream.class).withAnyArguments().thenReturn(fileOutputStreamMock);
		PowerMockito.whenNew(FTPClient.class).withAnyArguments().thenReturn(ftpClient);
		PowerMockito.mockStatic(IOUtils.class);
		PowerMockito.when(IOUtils.copy(eq(inputStreamMock), eq(fileOutputStreamMock))).thenReturn(1337);
		doThrow(new IOException("ACTIONEXCEPTION")).when(lmsiSpy).processLocationMeasurement(eq(locationMeasurement), eq(ftpClient),
				eq("BASEDIR/"), anyString());
		doNothing().when(lmsiSpy).connectToFtp(eq(ftpClient));
		doNothing().when(lmsiSpy).newFilePath(eq("FILENAME"), eq("BASEDIR/"));

		try {
			lmsiSpy.fetchMeasurementValues(locationMeasurement);
			fail("Should throw exception.");
		} catch (ActionException ae) {
			assertEquals("FILE 'FILENAME' not processed.", ae.getMessage());
			assertEquals(ActionExceptionType.FTP, ae.getType());
		}

		PowerMockito.verifyStatic(times(1));
		ActionLogger.debug(eq("FTP: FILE: FILENAME; Datetime: Wed Dec 23 11:00:27 GMT 2015"), any(Object[].class), any(Throwable.class),
				eq(false));
		verify(lmsiSpy, times(1)).disconnectFtp(eq(ftpClient));
		verify(lmsiSpy, times(1)).newFilePath(eq("FILENAME"), eq("BASEDIR/"));
	}

	// Assure that no file is created when there is no directory assigned
	@Test
	public void testNewFilePath() {
		try {
			lmsi.newFilePath("LASTFILENAME", null);
			lmsi.newFilePath(null, "DIR");
			lmsi.newFilePath(null, null);
			PowerMockito.verifyStatic(times(0));
			ActionLogger.warn(anyString(), any(Object[].class), any(Throwable.class), eq(false));
		} catch (Exception e) {
			fail("Should not throw exception when no directory is provided");
		}
	}

	// Assure that process location measurement is correctly completed when no exceptions are raised
	@Test
	public void testProcessLocationMeasurement_CorrecltyProcessed() throws Exception {
		LocationMeasurement locationMeasurement = new LocationMeasurement();
		FTPClient ftpClient = mock(FTPClient.class);
		String baseDir = "BASEDIR";
		String fileName = "FILENAME";
		ConfigurationParameter baseDirCP = new ConfigurationParameter();
		baseDirCP.setValue("");
		when(generalService.getConfigurationParameter(any())).thenReturn(baseDirCP);

		PowerMockito.whenNew(FileInputStream.class).withAnyArguments().thenReturn(null);
		File fileMock = mock(File.class);
		PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(fileMock);
		LocationMeasurementsServiceImpl lmsiSpy = spy(lmsi);
		doNothing().when(lmsiSpy).isSuccessReply(any());

		lmsiSpy.processLocationMeasurement(locationMeasurement, ftpClient, baseDir, fileName);

		verify(ftpClient, times(1)).completePendingCommand();
		verify(ftpClient, times(1)).deleteFile(eq(fileName));
		verify(generalService, times(1)).getConfigurationParameter(eq("ProcessedDir")); // indicates that success = true
	}

	// Assert that if no success, action exception is thrown and the file is deleted
	@Test
	public void testProcessLocationMeasurement_ExceptionHandling() throws Exception {
		LocationMeasurement locationMeasurement = new LocationMeasurement();
		FTPClient ftpClient = mock(FTPClient.class);
		String baseDir = "BASEDIR";
		String fileName = "FILENAME";
		ConfigurationParameter baseDirCP = new ConfigurationParameter();
		baseDirCP.setValue("");
		when(generalService.getConfigurationParameter(any())).thenReturn(baseDirCP);

		PowerMockito.whenNew(FileInputStream.class).withAnyArguments().thenReturn(null);
		File fileMock = mock(File.class);
		PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(fileMock);
		doThrow(new RuntimeException("RUNTIMEEXCEPTION")).when(self).processLocationMeasurementFile(eq(locationMeasurement),
				any(FileInputStream.class));
		LocationMeasurementsServiceImpl lmsiSpy = spy(lmsi);
		doNothing().when(lmsiSpy).isSuccessReply(any());

		try {
			lmsiSpy.processLocationMeasurement(locationMeasurement, ftpClient, baseDir, fileName);
		} catch (ActionException ae) {
			assertEquals(ActionExceptionType.FTP, ae.getType());
			assertEquals("FILE 'FILENAME' not processed.", ae.getMessage());
		}
		verify(ftpClient, times(1)).completePendingCommand();
		verify(ftpClient, times(1)).deleteFile(eq(fileName));
		verify(generalService, times(1)).getConfigurationParameter(eq("ErrorDir")); // indicates that success = false
	}

	// Assure that no exception is thrown when connected without exception
	@Test
	public void testPrepareConnectToFtp_Connects() throws Exception {
		ConfigurationParameter configurationParameter = new ConfigurationParameter();
		configurationParameter.setValue("CONNECTS");
		when(generalService.getConfigurationParameter(eq("FtpServer"))).thenReturn(configurationParameter);
		FTPClient ftpClient = mock(FTPClient.class);
		doNothing().when(ftpClient).connect(eq(configurationParameter.getValue()));

		LocationMeasurementsServiceImpl lmsiSpy = spy(lmsi);
		doNothing().when(lmsiSpy).isSuccessReply(eq(ftpClient));

		try {
			lmsiSpy.prepareConnectToFtp(ftpClient);
		} catch (ActionException ae) {
			fail("Should not throw ActionException when no failure");
		}
	}

	// Assure that an exception is thrown when connection raises an exception
	@Test
	public void testPrepareConnectToFtp_FailsToConnect() throws Exception {
		ConfigurationParameter configurationParameter = new ConfigurationParameter();
		configurationParameter.setValue("CONNECTS");
		when(generalService.getConfigurationParameter(eq("FtpServer"))).thenReturn(configurationParameter);
		FTPClient ftpClient = mock(FTPClient.class);
		doNothing().when(ftpClient).connect(eq(configurationParameter.getValue()));

		LocationMeasurementsServiceImpl lmsiSpy = spy(lmsi);
		doThrow(new ActionException("ACTIONEXCEPTION")).when(lmsiSpy).isSuccessReply(eq(ftpClient));

		try {
			lmsiSpy.prepareConnectToFtp(ftpClient);
			fail("Should throw ActionException when fails to connect");
		} catch (ActionException ae) {
			assertEquals("Failed to connect FTP server Meteo Consult", ae.getMessage());
			assertEquals(ae.getType(), ActionExceptionType.CONNECTION);
			assertEquals("Meteo", ((MeasurementProvider) ae.getTargets().get(0)).getId());
		}
	}

	// Assert that the csp location measurement and the location measurement value is correctly updated and merged
	@Test
	public void testProcessLocationMeasurementFile_ProcessedCorrectly() throws Exception {
		ConfigurationParameter dateFormat = new ConfigurationParameter();
		dateFormat.setValue("yyyy/MM/dd");
		when(generalService.getConfigurationParameter("FetchLocationMeasurementDateFormat")).thenReturn(dateFormat);
		ConfigurationParameter separator = new ConfigurationParameter();
		separator.setValue("@");
		when(generalService.getConfigurationParameter("FetchLocationMeasurementSeparator")).thenReturn(separator);
		ConfigurationParameter dateField = new ConfigurationParameter();
		dateField.setValue("0");
		when(generalService.getConfigurationParameter("FetchLocationMeasurementDateField")).thenReturn(dateField);
		ConfigurationParameter valueField = new ConfigurationParameter();
		valueField.setValue("1");
		when(generalService.getConfigurationParameter("FetchLocationMeasurementValueField")).thenReturn(valueField);

		InputStreamReader isr = mock(InputStreamReader.class);
		PowerMockito.whenNew(InputStreamReader.class).withAnyArguments().thenReturn(isr);
		BufferedReader bf = mock(BufferedReader.class);
		PowerMockito.whenNew(BufferedReader.class).withAnyArguments().thenReturn(bf);

		LocationMeasurement locationMeasurement = new LocationMeasurement();
		locationMeasurement.setId(1337L);
		List<CspLocationMeasurement> cspLocationMeasurementList = new ArrayList<CspLocationMeasurement>();
		CspLocationMeasurement cspLocationMeasurement = new CspLocationMeasurement();
		cspLocationMeasurementList.add(cspLocationMeasurement);
		locationMeasurement.setCspLocationMeasurements(cspLocationMeasurementList);

		InputStream is = mock(FileInputStream.class);
		BufferedReader input = new BufferedReader(new InputStreamReader(is));
		when(input.readLine()).thenReturn("HEADERLINE").thenReturn("1995/12/08@666").thenReturn(null);

		lmsi.processLocationMeasurementFile(locationMeasurement, is);

		PowerMockito.verifyStatic(times(1));
		ActionLogger.trace(eq("INPUT LINE: 1995/12/08@666"), any(Object[].class), any(Throwable.class), eq(false));

		ArgumentCaptor<CspLocationMeasurement> argClm = ArgumentCaptor.forClass(CspLocationMeasurement.class);
		verify(cspLocationMeasurementDao, times(1)).update(argClm.capture());
		// explicit date creation due to the use of SDF (i.e. the UnitTestHelper methods are not consistent with the
		// implementation)
		Date assertDate = new SimpleDateFormat("yyyy/MM/dd").parse("1995/12/08");
		assertEquals(assertDate, argClm.getValue().getSendFrom());
		ArgumentCaptor<LocationMeasurementValue> argLocationMeasurementValue = ArgumentCaptor.forClass(LocationMeasurementValue.class);
		verify(locationMeasurementValueDao, times(1)).merge(argLocationMeasurementValue.capture());
		LocationMeasurementValuePK lmvPK = argLocationMeasurementValue.getValue().getKey();
		assertEquals(assertDate, lmvPK.getDatetime());
		assertEquals(1337L, lmvPK.getLocationMeasurementId());
		assertEquals(new BigDecimal(666).doubleValue(), argLocationMeasurementValue.getValue().getValue().doubleValue(), 0.001);
		assertEquals(locationMeasurement, argLocationMeasurementValue.getValue().getLocationMeasurement());
	}

	// Verify that action exception is thrown in case of an exception
	@Test
	public void testProcessLocationMeasurementFile_ExceptionHandling() throws Exception {
		when(generalService.getConfigurationParameter("FetchLocationMeasurementDateFormat")).thenThrow(new RuntimeException("EXCEPTION"));
		LocationMeasurement locationMeasurement = new LocationMeasurement();
		InputStream is = mock(FileInputStream.class);
		try {
			lmsi.processLocationMeasurementFile(locationMeasurement, is);
			fail("Expected ActionException");
		} catch (ActionException ae) {
			assertEquals("EXCEPTION", ae.getMessage());
			assertEquals(ActionExceptionType.FTP, ae.getType());
		}
	}

	// Verify when successful ftp reply it is logged
	@Test
	public void testIsSuccessReply_Succes() throws Exception {
		FTPClient ftpClient = mock(FTPClient.class);
		when(ftpClient.getReplyCode()).thenReturn(250);
		when(ftpClient.getReplyString()).thenReturn("REPLYSTRING");

		lmsi.isSuccessReply(ftpClient);

		PowerMockito.verifyStatic(times(1));
		ActionLogger.trace(eq("FTP reply: REPLYSTRING"), any(Object[].class), any(Throwable.class), eq(false));
	}

	// Verify that an action exception is thrown in case no successful ftp reply
	@Test
	public void testIsSuccessReply_NotSucces() throws Exception {
		FTPClient ftpClient = mock(FTPClient.class);
		when(ftpClient.getReplyCode()).thenReturn(300);
		when(ftpClient.getReplyString()).thenReturn("REPLYSTRING");

		try {
			lmsi.isSuccessReply(ftpClient);
			fail("No exception thrown, while expected due to failure ftp reply.");
		} catch (ActionException ae) {
			assertEquals("FTP reply: REPLYSTRING", ae.getMessage());
			assertEquals(ActionExceptionType.FTP, ae.getType());
		}
		verify(ftpClient, times(1)).disconnect();
	}

	// Assert that the list is correctly sorted according to the timestamps
	@Test
	public void testGetSortedList() {
		FTPFile[] ftpFileArray = { new FTPFile(), new FTPFile(), new FTPFile() };

		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(UnitTestHelper.getDate23Dec2015at11h00m27s());
		ftpFileArray[0].setTimestamp(calendar1);

		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(UnitTestHelper.getDate23Dec2015at11h00m00s());
		ftpFileArray[1].setTimestamp(calendar2);

		Calendar calendar3 = Calendar.getInstance();
		calendar3.setTime(UnitTestHelper.getDate23Dec2015at11h33m27s());
		ftpFileArray[2].setTimestamp(calendar3);

		SortedSet<FTPFile> result = lmsi.getSortedList(ftpFileArray);

		assertEquals(UnitTestHelper.getDate23Dec2015at11h00m00s(), result.first().getTimestamp().getTime());
		assertEquals(UnitTestHelper.getDate23Dec2015at11h33m27s(), result.last().getTimestamp().getTime());
		assertEquals(3, result.size());
	}

}
